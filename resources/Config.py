#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent
import sys
sys.dont_write_bytecode = True


class _Config(object):
   def __init__(self):
      self.sup_user = "root"
      self.sup_pass = "QQoFS9pPR5SPtV1XB16Oze9Voxn1ag"
      self.sup_url  = "http://127.0.0.1:5580"
      self.sup_url2  = "https://10.200.2.29:2443"
      self.ring = None
      self.cos = None
      self.coses = []
      self.model_json = "/tmp/model.json"
      self.model_json_arg = False
      self.fail_serv = None
      self.fail_dc = None
      self.fail_dg = None
      self.fail_events = []
      self.extended = True
      self.verbose = True
      self.test_params = True
      self.dcprefix = None

Config = _Config()
