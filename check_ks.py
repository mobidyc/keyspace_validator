#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent
import sys
sys.dont_write_bytecode = True

# fix #RING-29951
import logging
logging.basicConfig(
   format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(sys.argv[0])

import getopt
import os
import json
import subprocess
import traceback
from pprint import pprint as pp
from resources.Config import Config as c
import reverse_ring
import scality.common
from scality.sprov.ring import Ring as SprovRing
from scality.sprov.cos import ClassOfService, Schema
from scality.sprov.event import FailureSet, FailureEventSet
from scality.sprov.component import Server, Node, DiskGroup, Site, Rack

# Values tested during the Extended test mode
all_coses = ("ARC14+4", "ARC9+3", "ARC7+5", "ARC5+7", "ARC8+4", "ARC4+2", 5, 4,
             "4+", 3, 2)
# Failures tested during the Extended test mode
failure_site_with_diskgroup = [('Site', 1, 'Server', 2, 'DiskGroup', 2),
                               ('Site', 1, 'Server', 2, 'DiskGroup', 1),
                               ('Site', 1, 'Server', 2),
                               ('Site', 1, 'Server', 1, 'DiskGroup', 2),
                               ('Site', 1, 'Server', 1, 'DiskGroup', 1),
                               ('Site', 1, 'Server', 1),
                               ('Site', 1, 'DiskGroup', 2),
                               ('Site', 1, 'DiskGroup', 1), ('Site', 1)]
failure_site_no_diskgroup = [('Site', 1, 'Server', 2), ('Site', 1, 'Server',
                                                        1), ('Site', 1)]
failure_with_diskgroup = [('Server', 6, 'DiskGroup', 2),
                          ('Server', 6, 'DiskGroup', 1), ('Server', 6),
                          ('Server', 5, 'DiskGroup', 2),
                          ('Server', 5, 'DiskGroup', 1), ('Server', 5),
                          ('Server', 4, 'DiskGroup', 2),
                          ('Server', 4, 'DiskGroup', 1), ('Server', 4),
                          ('Server', 3, 'DiskGroup', 2),
                          ('Server', 3, 'DiskGroup', 1), ('Server', 3),
                          ('Server', 2, 'DiskGroup', 2),
                          ('Server', 2, 'DiskGroup', 1), ('Server', 2),
                          ('Server', 1, 'DiskGroup', 2),
                          ('Server', 1, 'DiskGroup', 1), ('Server', 1),
                          ('DiskGroup', 2), ('DiskGroup', 1)]
failure_no_diskgroup = [('Server', 6), ('Server', 5), ('Server', 4),
                        ('Server', 3), ('Server', 2), ('Server', 1)]


def parse_args(*argv):
   shrt_args = "hr:s:l:p:c:S:D:G:q0a:RP:j:"
   long_args = [
      "help", "ring=", "supurl=", "supuser=", "suppass=", "cos=",
      "failure_server=", "failure_site=", "failure_diskgroup=", "simple",
      "silent", "addfail=", "ring_params", "dcprefix", "json"
   ]

   try:
      opts, args = getopt.getopt(sys.argv[1:], shrt_args, long_args)
      del args
   except getopt.GetoptError as e:
      logger.error(e)
      sys.exit(1)

   for opt, arg in opts:
      if opt in ("-h", "--help"):
         usage(sys.stdout)
         sys.exit(0)
      elif opt in ("-r", "--ring"):
         c.ring = arg
      elif opt in ("-s", "--supurl"):
         c.sup_url = arg
      elif opt in ("-l", "--supuser"):
         c.sup_user = arg
      elif opt in ("-p", "--suppass"):
         c.sup_pass = arg
      elif opt in ("-c", "--cos"):
         c.cos = arg
      elif opt in ("-S", "--failure_server"):
         c.fail_serv = arg
      elif opt in ("-D", "--failure_site"):
         c.fail_dc = arg
      elif opt in ("-G", "--failure_diskgroup"):
         c.fail_dg = arg
      elif opt in ("-0", "--silent"):
         c.verbose = False
      elif opt in ("-q", "--simple"):
         c.extended = False
      elif opt in ("-R", "--ring_params"):
         c.test_params = False
      elif opt in ("-P", "--dcprefix"):
         c.dcprefix = int(arg)
      elif opt in ("-j", "--json"):
         c.model_json = arg
         c.model_json_arg = True
      elif opt in ("-a", "--addfail"):
         if len(arg) % 2 > 0:
            print "invalid number of addfail args"
            sys.exit(1)
         fail_tuple = ()
         f = 0
         while f < len(arg):
            if arg[f] == "S":
               fail_tuple = fail_tuple + ("Site", arg[f + 1])
            elif arg[f] == "s":
               fail_tuple = fail_tuple + ("Server", arg[f + 1])
            elif arg[f] == "d":
               fail_tuple = fail_tuple + ("DiskGroup", arg[f + 1])
            f += 2
         c.fail_events.append(fail_tuple)
      else:
         logger.error("Unknown argument: {0}".format(opt))
         usage(sys.stderr)
         sys.exit(2)

   if not c.model_json_arg and None == c.ring:
      logger.error("Missing ring argument")
      usage(sys.stderr)
      sys.exit(2)

   # Compute the event list
   fail_tuple = ()
   if c.fail_dc != None:
      fail_tuple = fail_tuple + (
         "Site",
         int(c.fail_dc),
      )
   if c.fail_serv != None:
      fail_tuple = fail_tuple + (
         "Server",
         int(c.fail_serv),
      )
   if c.fail_dg != None:
      fail_tuple = fail_tuple + (
         "DiskGroup",
         int(c.fail_dg),
      )
   if len(fail_tuple) > 0:
      c.fail_events.append(fail_tuple)

   if None == c.cos and not c.extended:
      logger.error(
         "Missing cos argument, please do not disable the extended mode")
      usage(sys.stderr)
      sys.exit(2)
   if c.cos:
      c.coses = c.cos.split(',')
      if not c.fail_events:
         logger.error(
            "Missing failure argument, please use the 'S', 'D', 'G' or 'a' argument with cos"
         )
         usage(sys.stderr)
         sys.exit(2)

   if not c.verbose:
      logging.disable(logging.WARNING)


def usage(output):
   output.write("""Usage: {progname} [options]
      Options:
      -h|--help ______________ Show this help message
      -r|--ring ______________ *ring name
      -c|--cos _______________ list of cos to analyze (ARC9+3,2,4,4+)
      -P|--dcprefix __________ Specify a dcprefix for costranslator
      -R|--ring_params________ Do not test the ring defined params (prov*, dclist, etc..)
      -S|--failure_server ____ Number of Server failure authorized
      -D|--failure_site ______ Number of Site failure authorized
      -G|--failure_diskgroup _ Number of Diskgroup failure authorized

      -s|--supurl ____________ Supervisor Url: {url} or {url2}
      -l|--supuser ___________ supervisor login: {user}
      -p|--suppass ___________ supervisor password: {pwd}

      -j|--json ______________ specify an existing model.json file with ring topology
      -0|--silent ____________ not a verbose mode
      -q|--simple ____________ Do not run the extended failure tests at the end

      * = mandatory option

      failure options are cumulative. It means if you use "-D 1 -S1" the keyspace
      will be tested aginst the failure of 1 site + 1 server at the same time.


      Advanced arguments:
      -a|--addfail ___________ Advanced mode to specify multiple failure tests, cumulative.
      this argument permits to add multiple mode failure.
         format is <component letter><nbr failure><component letter><nbr failure>...
         component letter can be: S=Site, s=Server, d=Diskgroup
         example:
         {progname} -a S1s1d3 -a s3d1
         will test both constraints:
            "1 Site and 1 Server and 3 DiskGroup"
            "3 Server and 1 DiskGroup"

""".format(
      progname=os.path.basename(sys.argv[0]),
      url=c.sup_url,
      url2=c.sup_url2,
      user=c.sup_user,
      pwd=c.sup_pass))


def get_model():
   if not c.model_json_arg:
      # Disable warning at this step
      prev_log_level = logging.root.manager.disable
      logging.disable(logging.WARNING)
      try:
         reverse_ring.sprov_compute(c.sup_url, c.sup_user, c.sup_pass, c.ring,
                                    c.test_params, c.model_json)
      except scality.common.ScalDaemonException as e:
         logger.error(e)
         sys.exit(1)
      logging.root.manager.disable = prev_log_level

   if not os.path.isfile(c.model_json):
      logger.error("Json model file not found")
      sys.exit(0)

   with open(c.model_json) as json_file:
      data = json.load(json_file)

   return data


def update_events(ring, cos, fail):
   # load the ARC/COS values
   try:
      ring.setClassesOfService([ClassOfService(x) for x in cos], dcprefix=c.dcprefix)
   except ValueError as e:
      logger.error(e)
      sys.exit(1)
   except Exception:
      logger.error("Generic Exception: {}".format(traceback.format_exc()))
      sys.exit(1)

   # Populate the failure events
   ring.events = FailureEventSet(None)
   for failures in fail:
      for group_failures in failures:
         afe = {}
         y = 0
         while y < len(failures):
            afe.update({failures[y]: int(failures[y + 1])})
            y += 2
      ring.events.addFailureEvent(
         FailureSet(**afe), [ClassOfService(x) for x in cos])

   # Update the topology
   ring.updateEvents()


def stats_result(ring):
   sprov_stats_binary = "sprov_stats"
   try:
      sp = subprocess.Popen(
         sprov_stats_binary, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
   except Exception, e:
      raise Exception(
         "Failed to call '%s': %s" % (" ".join(sprov_stats_binary), str(e)))

   (stdout, stderr) = sp.communicate(input=json.dumps(ring.toJson()))

   return {"stdout": stdout, "stderr": stderr, "rval": sp.returncode}


def get_validities(txt):
   res = ""
   register = False
   for line in txt.split('\n'):
      if line == "Classes of service:":
         register = True
         continue

      if register:
         if line[:3] == "===":
            break
         value = line.strip(" ")
         if value:
            res += str(value) + "\n"
   return res


def get_valid_only(txt):
   res = ""
   for line in txt.split("\n"):
      if line[:3] == "OK:":
         res += line[3:].strip() + "\n"

   if res:
      if res[:1] != "A":
         return "cos{0}".format(res)
   return res


def format_failure_event(f):
   afe = "\""
   y = 0
   while y < len(f):
      if y > 0:
         afe += " and "
      afe += "{0} {1}".format(f[y + 1], f[y])
      y += 2
   afe += "\""

   return afe


def main():
   topology = get_model()

   # Create ring object and load the topology
   ring = SprovRing()
   ring.loadJson(topology)

   if c.coses:
      update_events(ring, c.coses, c.fail_events)
      result = stats_result(ring)

      if result["rval"] != 0:
         logger.error("Sprov_stats templating error using cos:{0} events:{1}".format(c.coses, c.fail_events))
         sys.exit(1)

      output = get_validities(result["stdout"])

      if c.fail_events:
         logger.info(
            "The current keyspace has been tested against the following eventFailures:"
         )
         for elem in c.fail_events:
            print format_failure_event(elem)
      else:
         logger.info("result:")
      print "\t{0}".format(output)

   if not c.extended:
      return

   # We want to test more capabilities
   logger.info("Extended mode: Looking for all tolerated failures:\n")

   prev_log_level = logging.root.manager.disable
   logging.disable(logging.WARNING)

   fails = []
   if ring.hasChildrenOfType(Site):
      if ring.hasChildrenOfType(DiskGroup):
         fails.append(failure_site_with_diskgroup)
      else:
         fails.append(failure_site_no_diskgroup)

   if ring.hasChildrenOfType(DiskGroup):
      fails.append(failure_with_diskgroup)
   else:
      fails.append(failure_no_diskgroup)

   fail_tests = {}
   for cs in all_coses:
      for fs in fails:
         for f in fs:
            # Reinit schema to minimum
            ring.setSchema(6)

            update_events(ring, [cs], [f])
            res = stats_result(ring)

            if res["rval"] != 0:
               logger.error("Sprov_stats error using ring:{0} cos:{1} events:{2}".format(ring, cs, f))
               sys.exit(1)

            out = get_validities(res["stdout"])
            valid = get_valid_only(out)
            # print "{0}: {1}".format(cs, valid)

            if valid:
               if not cs in fail_tests:
                  fail_tests.update({cs: []})

               afe = format_failure_event(f)
               fail_tests[cs].append(afe)

               break

   sep = " or "
   if fail_tests:
      for costest in all_coses:
         if costest in fail_tests:
            print "Using {0} you can lose {1}".format(
               costest, sep.join(fail_tests[costest]))
         else:
            print "{0} has no secure valid failure for the current keyspace".format(
               costest)

   else:
      logger.critical("No secure valid coses found for the current keyspace")

   logging.root.manager.disable = prev_log_level


if __name__ == "__main__":
   parse_args(sys.argv)
   try:
      main()
   except KeyboardInterrupt:
      sys.exit(0)