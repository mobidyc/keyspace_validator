# keyspace_checker

Tool to validate the constraints failures on a current keyspace (from the Scality supervisor api)
Accepts a json topology file (generated with sprov).

# dependencies:
sprov_stats from the scality-sprov package

