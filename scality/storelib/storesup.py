# -*- coding: utf-8 -*-
"""Message Store supervisor interaction module

The main class is XCommandSup. It will send XCommands to a supervisor to
control the cluster, a DSO or query information about DSOs.
"""

__all__ = ["XCommandSup", "SupNode", "SupNodeTask", "ClusterNode",
           "ClusterResult", "MainResult", "DSOResult", "NodeResult"]

from xcommand import XCommand, get_child_elem, get_child_text

def get_nodes_by_tags_in_list(nodelist, tags):
    res_list = []
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            res_list.append(node)
    return res_list


def get_first_node_by_tags_in_list(nodelist, tags):
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            return node
    return None




class ClusterNode(object):
    """A ClusterNode is a representation of a cluster node sent back by the
    supervisor in XML format. It has convenience methods that parse the XML
    and give the relevant information.
    """
    def __init__(self, xmlnode):
        self.__node = xmlnode


    def __str__(self):
        res = self.get_name()
        res += "(%s:%d)" % (self.get_ip(), self.get_port())
        return res


    def get_type(self):
        """Get the type (product name) of the node (eg. "bizstorenode")
        """
        return get_child_text(self.__node, "type")


    def get_name(self):
        """Get the name of the cluster node
        """
        return get_child_text(self.__node, "name")


    def get_ip(self):
        """Get the ip of the cluster node
        """
        return get_child_text(self.__node, "ip")


    def get_port(self):
        """Get the port of the cluster node

        returns the port as an integer
        """
        return int(get_child_text(self.__node, "port"))


    def get_description(self):
        """Get the description of the cluster node
        """
        return get_child_text(self.__node, "description")


    def get_version(self):
        """Get the version of the product running on the node
        """
        return get_child_text(self.__node, "version")


    def get_revision(self):
        """Get the revision of the product running on the node
        """
        return get_child_text(self.__node, "revision")


    def get_build(self):
        """Get the build of the product running on the node
        """
        return get_child_text(self.__node, "build")



    def get_status(self):
        """Get the status of the cluster node

        returns one of: "Unknown", "Offline", "Online", "Incomplete"
        """
        status = get_child_text(self.__node, "status")
        if status == "-1":
            return "Unknown"
        elif status == "0":
            return "Offline"
        elif status == "1":
            return "Online"
        elif status == "2":
            return "Incomplete"
        else:
            return "???"


    def is_polling(self):
        """Returns True if the node is currently polled, False if not
        """
        return (get_child_text(self.__node, "polling") == "1")


    def get_last_poll(self):
        """Get the number of seconds elapsed since the last poll has been done
        """
        return int(get_child_text(self.__node, "difflastpoll"))



    def get_stats(self):
        """Get the statistics of a node

        returns a mapping containing statistics name and value.
        If ratios are available, their key is the name of the statistics
        entry with "_ratio" appended.

        Currently available statistics are:
        cpu
        cpu_ratio
        memory
        memory_ratio
        ibandwidth
        obandwidth
        """
        stats = {}
        for stat in self.__node.getElementsByTagName("stats")[0].childNodes:
            if stat.nodeType == stat.ELEMENT_NODE:
                stats[stat.nodeName] = get_child_text(stat, "value")
                if stat.getElementsByTagName("ratio"):
                    stats[stat.nodeName + "_ratio"] = get_child_text(stat,
                                                                     "ratio")
        return stats



class SupNode(object):
    """A SupNode is a representation of a store node sent back by the
    supervisor in XML format. It has convenience methods that parse the XML
    and give the relevant information.
    """
    def __init__(self, xmlnode):
        self.__node = xmlnode


    def __str__(self):
        res = self.get_addr() + "=" + self.get_vnodeid()
        res += "(" + self.get_key() + ")"
        return res


    def get_addr(self):
        """Get the node address, returns a string
        """
        return get_child_text(self.__node, "addr")

    def get_vnodeid(self):
        """Get the node vnode id, returns a string
        """
        return get_child_text(self.__node, "vnodeid")

    def get_name(self):
        """Get the node name, returns a string
        """
        return get_child_text(self.__node, "name")

    def get_dso_name(self):
        """Get the dso name in which the node is associated, returns a string
        """
        return get_child_text(get_child_elem(self.__node, "ring"), "dsoname")

    def get_state(self):
        """Get the main state of the node

        returns a string in NEW, LOADING, RUN, OUT_OF_SERVICE
        """
        return get_child_text(self.__node, "state")

    def get_states(self):
        """Get the states of the node

        returns a map of state-value pairs
        """
        states = {}
        statenode = get_child_elem(self.__node, "states")

        for state in statenode.getElementsByTagName("state"):
            name = get_child_text(state, "name")
            value = get_child_text(state, "value")
            states[name] = value

        return states

    def get_key(self):
        """Get the node running chord key, returns a string in BCH notation
        """
        return get_child_text(self.__node, "key")

    def get_assigned_key(self):
        """Get the node assigned chord key, returns a string in BCH notation
        """
        return get_child_text(get_child_elem(self.__node, "config"), "key")

    def get_predecessor_key(self):
        """Get the predecessor chord key, returns a string in BCH notation
        """
        return get_child_text(get_child_elem(get_child_elem(self.__node, "ring"), "predecessor"), "key")

    def get_successor_key(self):
        """Get the successor chord key, returns a string in BCH notation
        """
        return get_child_text(get_child_elem(get_child_elem(self.__node, "ring"), "successor"), "key")

    def get_range(self):
        """Get the node range of its chunks, returns a tuple of min and max
        key strings in BCH notation
        """
        rangenode = get_child_elem(self.__node, "range")
        return (get_child_text(rangenode, "start"),
                get_child_text(rangenode, "last"))

    def get_nbchunks(self):
        """Get the number of chunks stored on the node, returns an integer
        """
        stats = get_child_elem(self.__node, "stats")
        chunks = get_child_elem(stats, "chunks")
        return int(get_child_text(chunks, "nb"))

    def get_nbtasks(self):
        """Get the number of tasks running on the node, returns an integer
        """
        stats = get_child_elem(self.__node, "stats")
        tasks = get_child_elem(stats, "tasks")
        return int(get_child_text(tasks, "nb"))


    def get_chord_ip(self):
        """Get the node chord IP, returns a string
        """
        return get_child_text(get_child_elem(self.__node, "config"), "ip")

    def get_chord_port(self):
        """Get the node chord port, returns an int
        """
        return int(get_child_text(get_child_elem(self.__node, "config"), "port"))



class SupNodeTask(object):
    """A SupNodeTask is a representation of a store node task (move, rebuild,
    purge) sent back by the supervisor in XML format. It has convenience
    methods that parse the XML and give the relevant information.
    """
    def __init__(self, nodeaddr, vnodeid, xmltask):
        self.__nodeaddr = nodeaddr
        self.__vnodeid = vnodeid
        self.__task = xmltask


    def __str__(self):
        res = "Task:"
        res += self.get_node_addr() + ":" + self.get_node_vnodeid()
        res += "," + self.get_tid() + ":" + self.get_type()
        return res


    def get_node_addr(self):
        """Get the address of the node performing this task
        """
        return self.__nodeaddr


    def get_node_vnodeid(self):
        """Get the vnode id of the node performing this task
        """
        return self.__vnodeid


    def get_tid(self):
        """Get the task id, returns a string
        """
        return get_child_text(self.__task, "tid")


    def get_type(self):
        """Get the task type (one of move, rebuild, purge)
        """
        return get_child_text(self.__task, "type")


    def get_dest(self):
        """Get the task destination node, if any, returns a ip:port string
        """
        return get_child_text(self.__task, "dest")


    def is_running(self):
        """Get the running status of the task, returns a boolean
        """
        return (get_child_text(self.__task, "status") == "RUNNING"
                and True or False)


    def get_done(self):
        """Get the number of chunks already processed in the task,
        returns an integer
        """
        return int(get_child_text(self.__task, "done"))


    def get_total(self):
        """Get the total number of chunks affected by this task,
        returns an integer
        """
        return int(get_child_text(self.__task, "total"))


    def get_range(self):
        """Get the range of chunks affected by this task,
        returns a tuple of chord keys as strings in BCH notation
        """
        return (get_child_text(self.__task, "start"),
                get_child_text(self.__task, "end"))




class ClusterResult(object):
    """ClusterResult is a XML wrapper for the responses to all
    "cmp cluster list" and "cmp cluster view" commands (both give the list
    of cluster nodes). It has convenience methods that parse the XML and
    give the relevant information.
    """
    def __init__(self, resnode):
        self.__res = resnode


    def __get_group_node_from_path(self, path):
        cur_group = self.__res
        for groupname in path.strip("/").split("/"):
            for node in cur_group.getElementsByTagName("group"):
                if node.getAttribute("name") == groupname:
                    cur_group = node
                    break
            else:
                return None
        return cur_group


    def __xml_to_clusternode_list(self, xml_node_list):
        node_list = []
        for node in xml_node_list:
            node_list.append(ClusterNode(node))
        return node_list





    def group_exists(self, group_path):
        group_node = self.__get_group_node_from_path(group_path)
        return group_node is not None


    def get_nodes_in_group(self, group_path):
        """Get all cluster nodes belonging to the group located at group_path

        group_path is an absolute group path, elements are separated by "/"
        returns a list of ClusterNode
        """
        if group_path == "" or group_path[0] != "/":
            return None
        group_node = self.__get_group_node_from_path(group_path)
        if not group_node:
            return None
        xml_nodes = group_node.getElementsByTagName("imp")
        return self.__xml_to_clusternode_list(xml_nodes)


    def get_node_by_ipport(self, group_path, ipport):
        """Get the cluster nodes in the group located at group_path, having
        the identifier ipport

        group_path is an absolute group path, elements are separated by "/"
        ipport is a string of format ip:port
        returns a ClusterNode
        """
        splitipport = ipport.split(":")
        return self.get_node_by_ip(group_path,
                                   splitipport[0], int(splitipport[1]))


    def get_node_by_ip(self, group_path, ip, port=8084):
        """Get the cluster nodes in the group located at group_path, having
        the identifier ip and port

        group_path is an absolute group path, elements are separated by "/"
        returns a ClusterNode
        """
        if group_path == "" or group_path[0] != "/":
            return None
        group_node = self.__get_group_node_from_path(group_path)
        if not group_node:
            return None
        for xml_node in group_node.getElementsByTagName("imp"):
            node = ClusterNode(xml_node)
            if node.get_ip() == ip and node.get_port() == port:
                return node
        return None



class MainResult(object):
    """MainResult is a XML wrapper for the responses to all
    "supervisor config main" commands (gives the list of DSO).
    It has convenience methods that parse the XML and give the
    relevant information.
    """
    def __init__(self, resnode):
        self.__res = resnode


    def __str__(self):
        res = "DSOlist:"
        first = True
        for dso in self.get_dso_list():
            if first:
                first = False
            else:
                res += ","
            res += dso
        return res


    def get_dso_list(self):
        """Get the list of DSO monitored by the supervisor.

        returns the dso names as a list of strings
        """
        dso_list = []
        xml_dso_list = self.__res.getElementsByTagName("dso")
        for dso in xml_dso_list:
            dso_list.append(get_child_text(dso, "name"))
        return dso_list



class DSOResult(object):
    """DSOResult is a XML wrapper for the responses to all
    "supervisor config dso" commands. It has convenience methods that parse
    the XML and give the relevant information.
    """
    def __init__(self, resnode):
        self.__res = resnode


    def __str__(self):
        res = "DSO:" + self.get_dso_name()
        res += "("
        res += "affected:%d" % len(self.get_dso_nodes()) + ","
        res += "run:%d" % len(self.get_run_nodes())
        res += ")"
        return res


    def __get_nodes_by_tags(s, unaffected, tags):
        return get_nodes_by_tags_in_list((unaffected
                                          and s.__get_unaffected_xml_nodes()
                                          or s.__get_dso_xml_nodes()),
                                         tags)

    def __get_first_node_by_tags(s, unaffected, tags):
        return get_first_node_by_tags_in_list((unaffected
                                           and s.__get_unaffected_xml_nodes()
                                           or s.__get_dso_xml_nodes()),
                                              tags)

    def __get_dso_xml_nodes(self):
        dso = get_child_elem(self.__res, "dso")
        nodes_node = get_child_elem(dso, "nodes")
        return nodes_node.getElementsByTagName("node")

    def __get_unaffected_xml_nodes(self):
        nodes_node = get_child_elem(self.__res, "notaffected")
        return nodes_node.getElementsByTagName("node")


    def __xml_to_node_list(self, xml_node_list):
        node_list = []
        for node in xml_node_list:
            node_list.append(SupNode(node))
        return node_list


    def get_dso_name(self):
        """Get the name of this DSO
        """
        dso = get_child_elem(self.__res, "dso")
        return get_child_text(dso, "name")


    def get_dso_params(self):
        """Get the parameters of this DSO.

        returns a mapping of parameters and their values. Current mapping
        contains:
        proxy_auto -> boolean
        purge_auto -> boolean
        move_auto -> boolean
        rebuild_auto -> boolean
        """
        dso = get_child_elem(self.__res, "dso")
        params_node = get_child_elem(dso, "params")
        params = {}
        params["proxy_auto"] = (get_child_text(params_node,
                                               "proxy_auto") == "1")
        params["purge_auto"] = (get_child_text(params_node,
                                               "purge_auto") == "1")
        params["move_auto"] = (get_child_text(params_node,
                                              "move_auto") == "1")
        params["rebuild_auto"] = (get_child_text(params_node,
                                                 "rebuild_auto") == "1")
        return params


    def get_dso_states(self):
        """Get the states of this DSO.

        returns a mapping of states names and values. Current mapping
        can contain:
        RUN -> yes
        BALANCING -> yes
        MISSING NODE -> yes
        DUPE KEY -> yes
        NOT STABILIZED -> yes
        """
        dso = get_child_elem(self.__res, "dso")
        states_node = get_child_elem(dso, "states")
        states = {}
        for s in states_node.getElementsByTagName("state"):
            key = get_child_text(s, "name")
            states[key] = get_child_text(s, "value")
        return states


    def get_dso_nodes(self):
        """Get the list of nodes belonging to this DSO.

        returns a list of SupNode objects
        """
        xml_node_list = self.__get_dso_xml_nodes()
        return self.__xml_to_node_list(xml_node_list)

    def get_unaffected_nodes(self):
        """Get the list of nodes not affected to a DSO.

        returns a list of SupNode objects
        """
        xml_node_list = self.__get_unaffected_xml_nodes()
        return self.__xml_to_node_list(xml_node_list)


    def get_node_by_addr(self, addr, vnodeid, unaffected=False):
        """Get the node with specified address and vnodeid.

        addr and vnodeid specify the node to get
        if unaffected is True, look in the list of unaffected nodes instead of
        DSO nodes
        returns a SupNode object
        """
        xml_node_list = self.__get_nodes_by_tags(unaffected,
                                                 {"addr": addr,
                                                  "vnodeid": vnodeid})
        if not xml_node_list:
            return None
        return SupNode(xml_node_list[0])


    def get_node_vnodes(self, addr, unaffected=False):
        """Get all vnodes of a particular physical node.

        addr specify the physical node address
        if unaffected is True, look in the list of unaffected nodes instead of
        DSO nodes
        returns a list of SupNode
        """
        xml_node_list = self.__get_nodes_by_tags(unaffected,
                                                 {"addr": addr})
        if not xml_node_list:
            return None
        res = [SupNode(xml_node) for xml_node in xml_node_list]
        return res


    def get_nodes_by_state(self, state, unaffected=False):
        """Get the nodes with specified state.

        state specifies the matching state
        if unaffected is True, look in the list of unaffected nodes instead of
        DSO nodes
        returns a list of SupNode objects
        """
        xml_node_list = self.__get_nodes_by_tags(unaffected,
                                                 {"state": state})
        return self.__xml_to_node_list(xml_node_list)


    def get_new_nodes(self):
        """Get the nodes having the state NEW in the DSO

        returns a list of SupNode objects
        """
        node_list = self.get_nodes_by_state(state="NEW")
        return node_list

    def get_oos_nodes(self):
        """Get the nodes having the state OUT_OF_SERVICE in the DSO

        returns a list of SupNode objects
        """
        node_list = self.get_nodes_by_state(state="OUT_OF_SERVICE")
        return node_list

    def get_run_nodes(self):
        """Get the nodes having the state RUN in the DSO

        returns a list of SupNode objects
        """
        node_list = self.get_nodes_by_state(state="RUN")
        return node_list

    def get_balancing_nodes(self):
        """Get the nodes in balancing state

        returns a list of SupNode objects
        """
        xml_node_list = self.__get_nodes_by_tags(False,
                                                 {"balancing": "yes"})
        return self.__xml_to_node_list(xml_node_list)


class NodeResult(object):
    """NodeResult is a XML wrapper for the responses to all
    "supervisor config node" commands. It has convenience methods that parse
    the XML and give the relevant information.
    """
    def __init__(self, resnode):
        self.__res = resnode


    def __str__(self):
        return str(self.get_node())


    def __xml_to_task_list(self, nodeaddr, vnodeid, xml_tasks):
        task_list = []
        for task in xml_tasks:
            task_list.append(SupNodeTask(nodeaddr, vnodeid, task))
        return task_list


    def get_node(self):
        """Get the node object linked to this result

        returns a SupNode object
        """
        xml_node = self.__res.getElementsByTagName("node")[0]
        return SupNode(xml_node)


    def get_tasks(self):
        """Get the tasks affected to the node

        returns a list of SupNodeTask objects
        """
        xml_task_node = get_child_elem(self.__res, "tasks")
        xml_tasks = xml_task_node.getElementsByTagName("task")
        node = self.get_node()
        return self.__xml_to_task_list(node.get_addr(), node.get_vnodeid(),
                                       xml_tasks)


    def get_running_tasks(self):
        """Get the tasks currently running on the node

        returns a list of SupNodeTask objects
        """
        xml_task_node = get_child_elem(self.__res, "tasks")
        xml_tasks = xml_task_node.getElementsByTagName("task")
        xml_running_tasks = get_nodes_by_tags_in_list(xml_tasks,
                                                      {"status": "RUNNING"})
        node = self.get_node()
        return self.__xml_to_task_list(node.get_addr(), node.get_vnodeid(),
                                       xml_running_tasks)




class XCommandSup(XCommand):
    """This class is a high-level handler for supervisor XCommands.

    It contains methods to send all XCommands understood by the supervisor,
    and these methods return wrapper objects that encapsulate the XML result
    sent back by the supervisor, that provide convenience methods to parse
    the results.
    """

    def __init__(self, url, login=None, passwd=None, verbose=False):
        """Initialize the supervisor handler.

        url is the URL of the XCommand interface on the supervisor
        """
        XCommand.__init__(self, url, login, passwd, verbose)
        self.dsoname = None


    def __check_dso(self):
        if not self.dsoname:
            raise Exception("DSO needs to be specified with use_dso()")




    def cluster_view(self):
        res = self.request({"cmd": "cmp cluster view"})
        resobj = res.getElementsByTagName("cmpclusterview")[0]
        return ClusterResult(resobj)


    def cluster_add_group(self, path, name):
        """Send a command that adds a new cluster group on the supervisor.

        path is the absolute path where the group will be added
        name is the name of the new group
        returns a ClusterResult object
        """
        if path == "" or path[-1] != "/":
            path += "/"
        res = self.request({"cmd": "cmp cluster list",
                            "action": "add",
                            "type": "group",
                            "path": path,
                            "name": name})
        resobj = res.getElementsByTagName("cmpclusterlist")[0]
        return ClusterResult(resobj)


    def cluster_delete_group(self, path, name):
        """Send a command that deletes a cluster group on the supervisor.

        path is the absolute path where the group resides
        name is the name of the group to delete
        returns a ClusterResult object
        """
        if path == "" or path[-1] != "/":
            path += "/"
        res = self.request({"cmd": "cmp cluster list",
                            "action": "delete",
                            "type": "group",
                            "path": path,
                            "name": name})
        resobj = res.getElementsByTagName("cmpclusterlist")[0]
        return ClusterResult(resobj)


    def cluster_add_node(self, path, ip, port=8084, name=None, descr=""):
        """Send a command that adds a cluster node in the supervisor

        path is the absolute path where to add the node (represents groups and
        subgroups)
        ip and port are identifiers of the node
        name is the name of the node, or None to set it as the ip
        descr is the description of the node
        returns a ClusterResult object
        """
        if path == "" or path[-1] != "/":
            path += "/"
        if name is None:
            name = ip
        res = self.request({"cmd": "cmp cluster list",
                            "action": "add",
                            "type": "imp",
                            "path": path,
                            "name": name,
                            "ip": ip,
                            "port": "%d" % port,
                            "description": descr})
        resobj = res.getElementsByTagName("cmpclusterlist")[0]
        return ClusterResult(resobj)


    def cluster_delete_node(self, path, ip, port=8084):
        """Send a command that deletes a cluster node in the supervisor

        path is the absolute path where the node resides
        ip and port are identifiers of the node
        returns a ClusterResult object
        """
        if path == "" or path[-1] != "/":
            path += "/"
        res = self.request({"cmd": "cmp cluster list",
                            "action": "delete",
                            "type": "imp",
                            "path": path,
                            "ip": ip,
                            "port": "%d" % port})
        resobj = res.getElementsByTagName("cmpclusterlist")[0]
        return ClusterResult(resobj)





    def use_dso(self, dsoname):
        """Define the current working DSO. This is mandatory for all
        "supervisor config dso" commands because they all return a DSOResult.

        dsoname is the name of the current dso
        """
        self.dsoname = dsoname




    def dso_list(self):
        """Send a command that gets the list of DSOs maintained by the
        supervisor.

        returns a MainResult object
        """
        res = self.request({"cmd": "supervisor config main"})
        resobj = res.getElementsByTagName("supervisorconfigmain")[0]
        return MainResult(resobj)


    def dso_add(self, dsoname):
        """Send a command that adds a new DSO on the supervisor.

        dsoname is the name of the new DSO
        returns a MainResult object
        """
        res = self.request({"cmd": "supervisor config main",
                            "action": "add",
                            "dsoname": dsoname})
        resobj = res.getElementsByTagName("supervisorconfigmain")[0]
        return MainResult(resobj)


    def dso_del(self, dsoname):
        """Send a command that deletes a DSO on the supervisor. The DSO
        must be emptied first (no assigned nodes)

        dsoname is the name of the DSO to delete
        returns a MainResult object
        """
        res = self.request({"cmd": "supervisor config main",
                      "action": "del",
                      "dsoname": dsoname})
        resobj = res.getElementsByTagName("supervisorconfigmain")[0]
        return MainResult(resobj)




    def dso_view(self):
        """Send a command that shows details of the current DSO.

        dsoname is the name of the DSO to show
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "view",
                            "dsoname": self.dsoname})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)


    def dso_params(self,
                   proxy_auto=None,
                   purge_auto=None,
                   move_auto=None,
                   rebuild_auto=None):
        """Send a command that sets some DSO automatic parameters.

        dsoname is the name of the DSO to show
        proxy_auto, purge_auto, move_auto, rebuild_auto are booleans,
        parameters left as None will not be changed.
        returns a DSOResult object
        """
        self.__check_dso()
        params = {"cmd": "supervisor config dso",
                  "action": "params",
                  "dsoname": self.dsoname}
        if proxy_auto is not None:
            params["proxy_auto"] = (proxy_auto and "1" or "0")
        if purge_auto is not None:
            params["purge_auto"] = (purge_auto and "1" or "0")
        if move_auto is not None:
            params["move_auto"] = (move_auto and "1" or "0")
        if rebuild_auto is not None:
            params["rebuild_auto"] = (rebuild_auto and "1" or "0")
        res = self.request(params)
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)



    def node_assignid(self, hostport, vnodeid, chordid):
        """Send a command that assign a chord id to a node.

        hostport and vnodeid specify the node
        chordid is a chord key in BCH notation
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "assignid",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "key": chordid,
                            "dsoname": self.dsoname})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)


    def node_join(self, hostport, vnodeid):
        """Send a command that make a node join the DSO where it is affected.

        hostport and vnodeid specify the node
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "join",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "dsoname": self.dsoname})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)


    def node_leave(self, hostport, vnodeid):
        """Send a command that make a node leave the DSO where it is affected.

        hostport and vnodeid specify the node
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "leave",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "dsoname": self.dsoname})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)


    def node_associate(self, hostport, vnodeid):
        """Send a command that associates a node to the current DSO.

        hostport and vnodeid specify the node to associate to the DSO dsoname
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "associate",
                            "dsoname": self.dsoname,
                            "addr": hostport,
                            "vnodeid": vnodeid})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)


    def node_deassociate(self, hostport, vnodeid):
        """Send a command that deassociates a node from its DSO.

        hostport and vnodeid specify the node to deassociate
        returns a DSOResult object
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config dso",
                            "action": "deassociate",
                            "dsoname": self.dsoname,
                            "addr": hostport,
                            "vnodeid": vnodeid})
        resobj = res.getElementsByTagName("supervisorconfigdso")[0]
        return DSOResult(resobj)






    def node_view(self, hostport, vnodeid):
        """Send a command that give details about a node.

        hostport and vnodeid specify the requested node
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "addr": hostport,
                            "vnodeid": vnodeid})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)


    def node_proxy(self, hostport, vnodeid,
                   dst_addr, range_start, range_end):
        """Send a command that sets a proxy on a node.

        hostport and vnodeid specify the target node
        dst_addr is the proxified node (where the real commands are executed)
        range_start and range_end give the id range where the proxy will be
        triggered
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "action": "proxy",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "dst_addr": dst_addr,
                            "range_start": range_start,
                            "range_end": range_end})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)


    def node_move(self, hostport, vnodeid):
        """Send a command that will start a move on a node

        hostport and vnodeid specify the target node
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "action": "move",
                            "addr": hostport,
                            "vnodeid": vnodeid})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)


    def node_rebuild(self, hostport, vnodeid):
        """Send a command that will start a rebuild on a node

        hostport and vnodeid specify the target node
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "action": "rebuild",
                            "addr": hostport,
                            "vnodeid": vnodeid})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)


    def node_purge(self, hostport, vnodeid, expiredelay):
        """Send a command that will start a purge on a node

        hostport and vnodeid specify the target node
        expiredelay is the expiry delay of deleted chunks, in seconds
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "action": "purge",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "purgedelay": expiredelay})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)


    def node_task_cancel(self, hostport, vnodeid, taskid):
        """Cancel a task belonging to a node

        hostport and vnodeid specify the target node
        taskid is the id of the task
        returns a NodeResult object
        """
        res = self.request({"cmd": "supervisor config node",
                            "action": "task_cancel",
                            "addr": hostport,
                            "vnodeid": vnodeid,
                            "tid": taskid})
        resobj = res.getElementsByTagName("supervisorconfignode")[0]
        return NodeResult(resobj)



    def bootstrap_set_auto(self, hostport):
        """Set automatically a list of bootstrap nodes on the specified
        store.

        hostport is the target store
        """
        self.__check_dso()
        res = self.request({"cmd": "supervisor config bizstore",
                            "action": "setauto",
                            "ident": hostport,
                            "dsoname": self.dsoname})
