# -*- coding: utf-8 -*-
""" Store utils

"""

import random
import time
import sys
import os
import math
try:
    import hashlib
    def md5():
        return hashlib.new("md5")
except ImportError, e:
    from md5 import md5
import bitset
import struct

CRLF = "\r\n"
BOUNDARYCOUNTER = 0

random.seed("%d:%lf" % (os.getpid(), time.time()))

globaltransacid = (long(random.randint(0, 65535)) << (32+16)) + (long(random.randint(0, 65535)) << 32)

""" Key = entropy, class, replica """
CHUNKID_BITS = 160
CHUNKID_ENTROPY_BITS = 152
CHUNKID_ENTROPY_MAX = (1L << CHUNKID_ENTROPY_BITS) - 1
CHUNKID_ENTROPY_MOD = (1L << CHUNKID_ENTROPY_BITS)
CHUNKID_EXTRA_BITS = CHUNKID_BITS - CHUNKID_ENTROPY_BITS
CHUNKID_REPLICA_BITS = 4
CHUNKID_CLASS_BITS = 4

CHUNKID_CLASS_RAIN = 0x7
CHUNKID_RAIN_K_BITS = 6
CHUNKID_RAIN_M_BITS = 6
CHUNKID_RAIN_SCHEMA_BITS = 8
CHUNKID_RAIN_REPLICA_MSB_BITS = 4
CHUNKID_RAIN_EXTRA_BITS = (CHUNKID_RAIN_K_BITS +
                           CHUNKID_RAIN_M_BITS +
                           CHUNKID_RAIN_SCHEMA_BITS +
                           CHUNKID_RAIN_REPLICA_MSB_BITS +
                           CHUNKID_EXTRA_BITS)
CHUNKID_RAIN_ENTROPY_BITS = (CHUNKID_BITS - CHUNKID_RAIN_EXTRA_BITS)
CHUNKID_RAIN_ENTROPY_MAX = (1L << CHUNKID_RAIN_ENTROPY_BITS) - 1
CHUNKID_RAIN_ENTROPY_MOD = (1L << CHUNKID_RAIN_ENTROPY_BITS)

# COS.TRANSLATION SPECIAL CLASSES (9 & A)
# First two bits are reserved for DC index from 0 to 3
# Class 9 is 1 replica per DC.
# Class A is 2 replicas per DC (following a similar schema as CoS 1 on a smaller scale)

CHUNKID_CLASS_COST_DC_BITS = 2
CHUNKID_CLASS_COST_DC_BASE = CHUNKID_BITS - CHUNKID_CLASS_COST_DC_BITS
CHUNKID_CLASS_COST_DC_BIT0 = 1 << (CHUNKID_CLASS_COST_DC_BASE + 1)
CHUNKID_CLASS_COST_DC_BIT1 = 1 << (CHUNKID_CLASS_COST_DC_BASE)
CHUNKID_COST_ENTROPY_BITS = CHUNKID_ENTROPY_BITS - CHUNKID_CLASS_COST_DC_BITS
CHUNKID_COST_DCNB = 4
CHUNKID_COST_DCSET = list(range(CHUNKID_COST_DCNB))
CHUNKID_COST_ENTROPY_MOD = 1 << CHUNKID_COST_ENTROPY_BITS

CHUNKID_CLASS_COST = { 0x9: {"class": 0x9, "replicaset": list(range(4)),   "entropy": (1 << CHUNKID_COST_ENTROPY_BITS) / 1, "nbreplicadc": 1, },
                       0xA: {"class": 0xA, "replicaset": list(range(4*2)), "entropy": (1 << CHUNKID_COST_ENTROPY_BITS) / 2, "nbreplicadc": 2, },
                     }

replica_schemes = {
    3: { "scheme": 5, "replicaposition": [ 0, 3, 2, 5 ] },
    4: { "scheme": 5, "replicaposition": [ 0, 3, 2, 5, 1 ] },
    6: { "scheme": 3, "replicaposition": [ 0, 2, 3 ] },
    8: { "scheme": 5, "replicaposition": [ 0, 3, 2 ] }
    }

def flatten_sequence(sequence):
    """ Loop trough a list that can contain any number of over list inside
    and return all scalar elements in a generator """
    """ Example sequence:
    ["S0005query\n", "B0003cmd\n", "V0002idI5\n", "V0004typeT000000000005query\n", "V0004nameT000000000009put_local\n", "b\n", "B0004data\n", ["V0002idT0000000000408475A91E91A8A19289DD63D675D3D2E451AA7A00\n", "V0007versionI1\n", "V0008contentsR0000000000481GFGH4hwmv,rml2212r!~dfg1GFGH4hw20399uig348hvhfg\n"], "b\n", "s\n"]
    """
    for item in sequence:
        if isinstance(item, list):
            for subitem in flatten_sequence(item):
                yield subitem
        elif isinstance(item, str):
            yield item
        else:
            # Item may be an object or generator, this works if it has a __iter__ method
            for subitem in item:
                yield subitem

"""
From ActiveState Code: http://code.activestate.com/recipes/146306/
MIT License: http://www.opensource.org/licenses/mit-license.php
"""
def encode_multipart_formdata(fields, files):
    """
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return (content_type, body) ready for httplib.HTTP instance
    """
    global BOUNDARYCOUNTER
    BOUNDARY = "----------ThIs_Is_%d_bouNdaRY_%d" % (BOUNDARYCOUNTER+11, BOUNDARYCOUNTER)
    BOUNDARYCOUNTER += 1
    L = []
    for (key, value) in fields:
        L.append("--" + BOUNDARY + CRLF)
        L.append("Content-Disposition: form-data; name=\"%s\"%s" % (key, CRLF))
        L.append(CRLF)
        L.append(value)
        L.append(CRLF)
    for (key, filename, value) in files:
        L.append("--" + BOUNDARY + CRLF)
        L.append("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"%s" % (key, filename, CRLF))
        L.append("Content-Type: %s%s" % (get_content_type(filename), CRLF))
        L.append(CRLF)
        L.append(value)
        L.append(CRLF)
    L.append("--" + BOUNDARY + "--" + CRLF)
    L.append(CRLF)
    content_type = "multipart/form-data; boundary=%s" % BOUNDARY
    return content_type, L


def SeedTransacIds():
    global globaltransacid

    globaltransacid = (long(random.randint(0, 65535)) << (32+16)) + (long(random.randint(0, 65535)) << 32)

def GetTransacId():
    global globaltransacid
    globaltransacid += 1L
    return globaltransacid


def wait_for(name, poll_interval, timeout,
             checkfunc, *funcargs):
    sys.stderr.write("Waiting for %s" % name)
    sys.stderr.flush()
    elapsed = 0
    sys.stderr.write(".")
    sys.stderr.flush()
    (status, var) = checkfunc(*funcargs)
    while not status:
        time.sleep(poll_interval)
        elapsed += poll_interval
        if elapsed >= timeout:
            raise Exception("%s timeout, there must be a problem" % name)
        sys.stderr.write(".")
        sys.stderr.flush()
        (status, var) = checkfunc(*funcargs)
    sys.stderr.write(" ok\n")
    return var

HEXACHARS = ["0","1","2","3","4","5","6","7","8","9",
             "A","B","C","D","E","F"]

def genid(MDclass="00"):
    id = ""
    for i in xrange(38):
        id += random.choice(HEXACHARS)
    id += MDclass
    return id

def genid_prefix(length=6):
    id = ""
    for i in xrange(length):
        id += random.choice(HEXACHARS)
    return id

def genid_from_int(base, MDclass="00"):
    """ Generation an ID and encode %08X integer inside so that it can be recovered from
    the key """

    """ 20 bytes = 120 bits MD5 of base as string, base as 32 bits, 8 bits class """
    basehexa = "%08X" % base
    m = md5()
    m.update(basehexa)
    return m.hexdigest()[0:30] + basehexa + MDclass

def genint_from_id(keySTR):
    basehexa = keySTR[30:38]
    m = md5()
    m.update(basehexa)
    if m.hexdigest()[0:30] != keySTR[0:30]:
        raise ValueError('This key %r was not generated from genid_from_int' % keySTR[0:30])
    return int(basehexa, 16)    


# CLASS/REPLICA MANIP

def getClassNReplicas(cl):
    if not isClassValid(cl):
        raise ValueError("Invalid class " + str(cl))
    if cl in replica_schemes:
        return len(replica_schemes[cl]["replicaposition"])
    if cl == CHUNKID_CLASS_RAIN:
        raise ValueError("Can't get nreplicas from rain class number")
    if cl in CHUNKID_CLASS_COST:
        raise ValueError("Can't get nreplicas from COST class")
    return cl + 1

def getClassSchema(cl):
    if not isClassValid(cl):
        raise Exception("Invalid class " + str(cl))
    if cl in replica_schemes:
        return replica_schemes[cl]["scheme"]
    if cl == CHUNKID_CLASS_RAIN:
        raise Exception("Can't get schema from rain class number")
    if cl in CHUNKID_CLASS_COST:
        raise ValueError("Can't get schema from COST class number")
    return cl

def getReplicaSchemaPosition(cl, replicaNum):
    if not isClassValid(cl):
        raise Exception("Invalid class " + str(cl))
    if cl in replica_schemes:
        if (replicaNum < 0 or
            replicaNum >= len(replica_schemes[cl]["replicaposition"])):
            raise Exception("Invalid replica number " + str(replicaNum) + " for class " + str(cl))
        return replica_schemes[cl]["replicaposition"][replicaNum]
    return replicaNum

def isClassValid(c):
    if c >= 0 and c <= 5: # standard classes
        return 1
    if c in replica_schemes: # special classes
        return 1
    if c == CHUNKID_CLASS_RAIN:
        return 1
    if c in CHUNKID_CLASS_COST:
        return 1
    return 0

# INTEGER KEYS

def getClass(key):
   """ returns the class of this key """
   return (key & 0xF0) >> CHUNKID_REPLICA_BITS

def getReplica(key):
    """ returns the replica number of this key """
    cl = getClass(key)
    rep = key & 0x0F
    if cl == CHUNKID_CLASS_RAIN:
        rep += (((key >> CHUNKID_EXTRA_BITS) &
                 ((1 << CHUNKID_RAIN_REPLICA_MSB_BITS) - 1))
                << CHUNKID_REPLICA_BITS)
    return rep

def getDC(key):
    """ return the DC index for a given key (in the range 0 <- 3) """
    return (key & ( CHUNKID_CLASS_COST_DC_BIT0 | CHUNKID_CLASS_COST_DC_BIT1)) >> CHUNKID_CLASS_COST_DC_BASE

def getRainK(key):
    return ((key >> (CHUNKID_EXTRA_BITS +
                    CHUNKID_RAIN_REPLICA_MSB_BITS +
                    CHUNKID_RAIN_SCHEMA_BITS +
                    CHUNKID_RAIN_M_BITS)) &
            ((1 << CHUNKID_RAIN_K_BITS) - 1))

def getRainM(key):
    return ((key >> (CHUNKID_EXTRA_BITS +
                     CHUNKID_RAIN_REPLICA_MSB_BITS +
                     CHUNKID_RAIN_SCHEMA_BITS)) &
            ((1 << CHUNKID_RAIN_M_BITS) - 1))

def getRainSchema(key):
    return ((key >> (CHUNKID_EXTRA_BITS +
                     CHUNKID_RAIN_REPLICA_MSB_BITS)) &
            ((1 << CHUNKID_RAIN_SCHEMA_BITS) - 1))



def get_next_replica_int(entropyKey, cl, replica, nextReplica):
    nextKey = (entropyKey
               - CHUNKID_ENTROPY_MOD * replica / (cl + 1)
               + CHUNKID_ENTROPY_MOD * nextReplica / (cl + 1))
    if (nextKey < 0):
        nextKey += CHUNKID_ENTROPY_MOD
    elif nextKey >= CHUNKID_ENTROPY_MOD:
        nextKey -= CHUNKID_ENTROPY_MOD

    return nextKey

def get_next_rain_replica_int(entropyKey, schema, replica, nextReplica):
    nextKey = (entropyKey
               - CHUNKID_RAIN_ENTROPY_MOD * replica / schema
               + CHUNKID_RAIN_ENTROPY_MOD * nextReplica / schema)
    if (nextKey < 0):
        nextKey += CHUNKID_RAIN_ENTROPY_MOD
    elif nextKey >= CHUNKID_RAIN_ENTROPY_MOD:
        nextKey -= CHUNKID_RAIN_ENTROPY_MOD

    return nextKey

def get_next_replica(keySTR, cls1=0):
    """ Returns the next replica for key, calling this function recursively is infinite
        as this will loop over all keys, even for a class 0 """
    key = long(keySTR, 16)

    cl = getClass(key)

    if not isClassValid(cl):
        raise Exception("Invalid class " + str(cl))

    replica = getReplica(key)

    if cls1 == 1 and cl == 1:
        clt = 6 # class translation 1 -> 6
    else:
        clt = cl
    if clt in replica_schemes:
        entropyKey = key >> CHUNKID_EXTRA_BITS
        scheme = replica_schemes[clt]
        nextReplica = (replica + 1) % len(scheme["replicaposition"])
        nextKey = get_next_replica_int(entropyKey,
                                       scheme["scheme"],
                                       scheme["replicaposition"][replica],
                                       scheme["replicaposition"][nextReplica])
        nextKey = (nextKey << CHUNKID_EXTRA_BITS) | (cl << CHUNKID_REPLICA_BITS) | nextReplica
    elif clt == CHUNKID_CLASS_RAIN:
        entropyKey = key >> CHUNKID_RAIN_EXTRA_BITS
        scheme = getRainSchema(key)
        nreplicas = getRainK(key) + getRainM(key)
        nextReplica = (replica + 1) % nreplicas
        nextKey = get_next_rain_replica_int(entropyKey,
                                            scheme,
                                            replica, nextReplica)
        nextKey = ((nextKey << CHUNKID_RAIN_EXTRA_BITS) |
                   (key & ((1 << CHUNKID_RAIN_EXTRA_BITS) - 1) &
                    ~((1 << (CHUNKID_EXTRA_BITS + CHUNKID_RAIN_REPLICA_MSB_BITS)) - 1)) |
                   ((nextReplica >> CHUNKID_REPLICA_BITS) << CHUNKID_EXTRA_BITS) |
                   (cl << CHUNKID_REPLICA_BITS) |
                   (nextReplica & ((1 << CHUNKID_REPLICA_BITS) - 1)))
    else:
        entropyKey = key >> CHUNKID_EXTRA_BITS
        nextReplica = (replica + 1) % (clt+1)
        nextKey = get_next_replica_int(entropyKey, clt, replica, nextReplica)
        nextKey = (nextKey << CHUNKID_EXTRA_BITS) | (cl << CHUNKID_REPLICA_BITS) | nextReplica

    return "%X" % nextKey

def _setReplica(key_bn, replicanb):
    """ internal: set replica number. key_bn replica nb should be set to zero """
    return key_bn | (replicanb)

def _setClass(key_bn, classnb):
    """ internal: set class number. key_bn class nb should be set to zero """
    return key_bn | (classnb << CHUNKID_REPLICA_BITS)

def _setDC(key_bn, dcnb):
    """ internal: set DC number. key_bn DC nb should be set to zero """
    return key_bn | (dcnb << CHUNKID_CLASS_COST_DC_BASE)

def get_replica_cost(keySTR, nextReplica):
    """ @return the replica of keySTR for the specified DC and replica
    Only support COST classes
    """
    key = long(keySTR, 16)
    cl = getClass(key)

    # Sanity checks
    if cl not in CHUNKID_CLASS_COST:
        raise ValueError("class is not COST type")

    cos_conf = CHUNKID_CLASS_COST[cl]

    if nextReplica not in cos_conf["replicaset"]:
        raise IndexError("nextReplica is out of bound")

    # Get current info
    replica = getReplica(key)
    curDC = getDC(key)

    if replica not in cos_conf["replicaset"]:
        raise ValueError("key replica not valid")

    if replica == nextReplica:
        return keySTR

    # get the replica index inside a single pie slice.
    replica_mod = replica % cos_conf["nbreplicadc"]
    nextReplica_mod = nextReplica % cos_conf["nbreplicadc"]

    # clear DC and shift 8
    resKey = (key - (curDC << CHUNKID_CLASS_COST_DC_BASE)) >> (CHUNKID_CLASS_BITS + CHUNKID_REPLICA_BITS)

    # get nextReplica
    resKey = resKey - replica_mod * cos_conf["entropy"] + nextReplica_mod * cos_conf["entropy"]
    while resKey < 0:
        resKey += CHUNKID_COST_ENTROPY_MOD

    while resKey >= CHUNKID_COST_ENTROPY_MOD:
        resKey -= CHUNKID_COST_ENTROPY_MOD

    # compute the next DC
    nextDC = curDC + int(nextReplica / cos_conf["nbreplicadc"]) - int(replica / cos_conf["nbreplicadc"]) 

    while nextDC >= CHUNKID_COST_DCNB:
        nextDC -= CHUNKID_COST_DCNB
    while nextDC < 0:
        nextDC += CHUNKID_COST_DCNB

    resKey = (resKey << (CHUNKID_CLASS_BITS + CHUNKID_REPLICA_BITS))

    resKey = _setClass(resKey, cl)
    resKey = _setReplica(resKey, nextReplica)
    resKey = _setDC(resKey, nextDC)

    return "%X" % resKey

def get_replica(keySTR, nextReplica, cls1=0):
    """ Returns the given replica for key"""

    key = long(keySTR, 16)

    cl = getClass(key)

    if not isClassValid(cl):
        raise ValueError("Invalid class " + str(cl))

    if cl in CHUNKID_CLASS_COST:
        return get_replica_cost(keySTR, nextReplica)

    replica = getReplica(key)

    if cls1 == 1 and cl == 1:
        clt = 6 # class translation 1 -> 6
    else:
        clt = cl
    if clt in replica_schemes:
        entropyKey = key >> CHUNKID_EXTRA_BITS
        scheme = replica_schemes[clt]
        nextKey = get_next_replica_int(entropyKey,
                                       scheme["scheme"],
                                       scheme["replicaposition"][replica],
                                       scheme["replicaposition"][nextReplica])
        nextKey = (nextKey << CHUNKID_EXTRA_BITS) | (cl << CHUNKID_REPLICA_BITS) | nextReplica
    elif clt == CHUNKID_CLASS_RAIN:
        entropyKey = key >> CHUNKID_RAIN_EXTRA_BITS
        scheme = getRainSchema(key)
        nextKey = get_next_rain_replica_int(entropyKey,
                                            scheme,
                                            replica, nextReplica)
        nextKey = ((nextKey << CHUNKID_RAIN_EXTRA_BITS) |
                   (key & ((1 << CHUNKID_RAIN_EXTRA_BITS) - 1) &
                    ~((1 << (CHUNKID_EXTRA_BITS + CHUNKID_RAIN_REPLICA_MSB_BITS)) - 1)) |
                   ((nextReplica >> CHUNKID_REPLICA_BITS) << CHUNKID_EXTRA_BITS) |
                   (cl << CHUNKID_REPLICA_BITS) |
                   (nextReplica & ((1 << CHUNKID_REPLICA_BITS) - 1)))
    else:
        entropyKey = key >> CHUNKID_EXTRA_BITS
        nextKey = get_next_replica_int(entropyKey, clt, replica, nextReplica)
        nextKey = (nextKey << CHUNKID_EXTRA_BITS) | (cl << CHUNKID_REPLICA_BITS) | nextReplica

    return "%X" % nextKey

def get_all_replicas(keySTR, cls1=0, includeSelf=False):
    """ returns all replicas for keySTR, excluding itself """
    gen = keySTR

    cl = getClass(long(keySTR, 16))

    if cl in CHUNKID_CLASS_COST:
        for replica in CHUNKID_CLASS_COST[cl]["replicaset"]:
            res = get_replica_cost(keySTR, replica)
            if includeSelf is True or int(res, 16) != int(keySTR, 16):
                yield res
        return

    if cls1 == 1 and cl == 1:
        clt = 6 # class translation 1 -> 6
    else:
        clt = cl
    if clt in replica_schemes:
        nreplicas = len(replica_schemes[clt]["replicaposition"])
    elif clt == CHUNKID_CLASS_RAIN:
        key = long(keySTR, 16)
        nreplicas = getRainK(key) + getRainM(key)
    else:
        nreplicas = clt + 1

    if includeSelf is True:
        yield keySTR

    for i in xrange(nreplicas - 1):
        gen = get_next_replica(gen, cls1)
        yield gen

def get_padded_key(key):
    """ returns a 40ish padded key """
    return key.rjust(40, "0")

def uks_gen(oid, volid, svcid, spid, cls="0"):
    bset = bitset.BitSet()
    bset.set_from_val(int(oid, 16), 64, 64)
    bset.set_from_val(int(volid, 16), 32, 32)
    bset.set_from_val(int(svcid, 16), 8, 24)
    bset.set_from_val(int(spid, 16), 24, 0)

    m = md5()
    m.update(bset.to_bin(128))
    hash = m.digest()
    bset.set_from_binstr(hash, 24, 128)
    return("%0.38X" % bset.to_long() + "%X0" % int(cls, 16))

def uks_gen_raw(hash, oid, volid, svcid, spid, cls="0"):
    bset = bitset.BitSet()
    bset.set_from_val(int(hash, 16), 24, 128)
    bset.set_from_val(int(oid, 16), 64, 64)
    bset.set_from_val(int(volid, 16), 32, 32)
    bset.set_from_val(int(svcid, 16), 8, 24)
    bset.set_from_val(int(spid, 16), 24, 0)

    return("%0.38X" % bset.to_long() + "%X0" % int(cls, 16))

def uks_parse(key):
    key = get_padded_key(key)
    hash = key[0:6]
    oid = key[6:22]
    volid = key[22:30]
    svcid = key[30:32]
    spid = key[32:38]
    cls = key[38:39]
    replica = key[39:40]

    return(hash, oid, volid, svcid, spid, cls, replica)

def uks_parse_raw(key):
    return uks_parse(key)


def serialize_md(mds):
    md_def = [
        ("mdver", "I", 4, 4),
        ("mflags", "I", 4, 0),
        ("atime", "q", 8, 0),
        ("mtime", "q", 8, 0),
        ("ctime", "q", 8, 0),
        ("crf", "f", 4, 0.0),
        ("version", "I", 4, 0),
        ("dataversion", "I", 4, 0),
        ("crc32", "I", 4, 0),
        ("size", "I", 4, 0),
        ("archidlen", "I", 4, 0),
        ("archid", "20s", 20, ""),
        ("archversion", "I", 4, 0)
        ]

    format = "!"
    values = []

    if mds is None:
        mds = {}
    else:
        if not isinstance(mds, dict):
            raise Exception("mds must be a dict")
    for md_elem in md_def:
        (name, type, typesize, val) = md_elem
        if mds.has_key(name):
            val = mds[name]

        if type == "f":
            val = pack754_32(val)
            type = "4b"
            bytes = []
            for i in xrange(0, 4):
                byte = val & 255
                if byte >= 128:
                    byte -= 256
                val >>= 8
                bytes.append(byte)
            format += "bI%dsI4b" % (len(name))
            values.extend([0, len(name), name, typesize,
                           bytes[0], bytes[1], bytes[2], bytes[3]])
        else:
            format += "bI%dsI%s" % (len(name), type)
            values.extend([0, len(name), name, typesize, val])

    raw_md = struct.pack(format, *tuple(values))
    return raw_md


def pack754(f, bits, expbits):
    significandbits = bits - expbits - 1 # -1 for sign bit

    if f == 0.0:
        return 0 # get this special case out of the way

    # check sign and begin normalization
    if f < 0:
        sign = 1
        fnorm = -f
    else:
        sign = 0
        fnorm = f

    # get the normalized form of f and track the exponent
    shift = 0
    while fnorm >= 2.0:
        fnorm /= 2.0
        shift += 1
    while fnorm < 1.0:
        fnorm *= 2.0
        shift -= 1
    fnorm = fnorm - 1.0

    # calculate the binary form (non-float) of the significand data
    significand = int(fnorm * ((1<<significandbits) + 0.5))

    # get the biased exponent
    exp = shift + ((1<<(expbits-1)) - 1) # shift + bias

    # return the final answer
    return (sign<<(bits-1)) | (exp<<(bits-expbits-1)) | significand

def pack754_32(f):
    return pack754(f, 32, 8)

def unpack754(i, bits, expbits):
    significandbits = bits - expbits - 1 # -1 for sign bit

    if i == 0:
        return 0.0

    # pull the significand
    result = float(i&((1<<significandbits)-1)) # mask
    result /= (1<<significandbits) # convert back to float
    result += 1.0 # add the one back on

    # deal with the exponent
    bias = (1<<(expbits-1)) - 1
    shift = ((i>>significandbits)&((1<<expbits)-1)) - bias
    while shift > 0:
        result *= 2.0
        shift -= 1
    while shift < 0:
        result /= 2.0
        shift += 1

    # sign it
    if (i>>(bits-1)) & 1:
        result *= -1.0

    return result

def unpack754_32(i):
    return unpack754(i, 32, 8)
