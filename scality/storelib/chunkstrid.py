# -*- coding: utf-8 -*-
""" chunk id as strings handling

"""

import random
import time
import sys
import math
from storeutils import getClassNReplicas, getClassSchema, getReplicaSchemaPosition, isClassValid
try:
    import hashlib
    def md5():
        return hashlib.new("md5")
except ImportError, e:
    from md5 import md5


HEXACHARS = ["0","1","2","3","4","5","6","7","8","9",
             "A","B","C","D","E","F"]

REPLICAJUMPS = [None,
                "80000000000000000000000000000000000000",
                "55555555555555555555555555555555555555",
                "40000000000000000000000000000000000000",
                "33333333333333333333333333333333333333",
                "2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"]
REPLICAJUMPS_REMAINDER = [None,
                          0,
                          1,
                          0,
                          1,
                          4]


enable_debug = True

def genStrId(cl, random_replica=False):
    id = ""
    for i in xrange(38):
        id += random.choice(HEXACHARS)
    id += HEXACHARS[cl]
    if random_replica:
        id += random.choice(HEXACHARS[0:getClassNReplicas(cl)])
    else:
        id += "0"
    return id

def getStrIdClass(id):
    return int(id[-2])

def getStrIdReplica(id):
    return int(id[-1])

def getStrIdReplicaKey(id, replicaNum):
    cl = getStrIdClass(id)
    rep = getStrIdReplica(id)

    schema = getClassSchema(cl)
    replicaPos = getReplicaSchemaPosition(cl, rep)
    newReplicaPos = getReplicaSchemaPosition(cl, replicaNum)

    if newReplicaPos == replicaPos and replicaNum == rep:
        return id

    workid = id[0:38]

    jump = REPLICAJUMPS[schema]
    rem = REPLICAJUMPS_REMAINDER[schema]

    diffrep = newReplicaPos - replicaPos
    delta_remainder = ((rem * newReplicaPos / (schema + 1)) -
                       (rem * replicaPos / (schema + 1)))

    if diffrep < 0:
        while replicaPos != newReplicaPos:
            workid = __strIdSub(workid, jump)
            replicaPos -= 1
    else:
        while replicaPos != newReplicaPos:
            workid = __strIdAdd(workid, jump)
            replicaPos += 1

    if delta_remainder >= 0:
        workid = __strIdAddInt(workid, delta_remainder)
    else:
        workid = __strIdSubInt(workid, -delta_remainder)

    id = workid
    id += HEXACHARS[cl]
    id += HEXACHARS[replicaNum]

    return id

def __strIdAdd(id, addid):
    if len(addid) > len(id):
        tmp = id
        id = addid
        addid = tmp
    if len(addid) < len(id):
        addid = ((len(id) - len(addid)) * "0") + addid
    result = ""
    carry = 0
    for i in xrange(len(addid) - 1, -1, -1):
        digit = HEXACHARS.index(id[i]) + HEXACHARS.index(addid[i]) + carry
        if digit >= 16:
            digit -= 16
            carry = 1
        else:
            carry = 0
        result = HEXACHARS[digit] + result
    # ignore last carry (wraps around 2^160)
    return result

def __strIdAddInt(id, addval):
    addid = "%X" % addval
    return __strIdAdd(id, addid)

def __strIdSub(id, subid):
    if len(subid) > len(id):
        tmp = id
        id = subid
        subid = tmp
    if len(subid) < len(id):
        subid = ((len(id) - len(subid)) * "0") + subid
    result = ""
    carry = 0
    for i in xrange(len(subid) - 1, -1, -1):
        digit = HEXACHARS.index(id[i]) - HEXACHARS.index(subid[i]) + carry
        if digit < 0:
            digit += 16
            carry = -1
        else:
            carry = 0
        result = HEXACHARS[digit] + result
    # ignore last carry (wraps around 2^160)
    return result

def __strIdSubInt(id, subval):
    subid = "%X" % subval
    return __strIdSub(id, subid)
