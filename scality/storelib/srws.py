# -*- coding: utf-8 -*-
""" REST module

"""

import re
import time
import random
import string
from httplib import *
from storeutils import flatten_sequence


class SRWS(object):

    def __init__(self, uri, path=None):
        self.uri = uri
        if path is not None:
            self.base_uri = path
        else:
            self.base_uri = "/srws/"
        self.resetconn();

    def resetconn(self):
        self.conn = HTTPConnection(self.uri)
        self.count = 0;

    def put(self, id, contents, extra_headers={}):
        body = ""
        if contents:
            for s in flatten_sequence(contents):
                body += s

        self.count += 1;
        if (self.count > 50):
            self.resetconn()

        hdrs = {"Connection": "keep-alive"}
        hdrs.update(extra_headers)
        self.conn.request("PUT", self.base_uri + id, body, hdrs)
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False
        resp.read()
        return True

    def get(self, id, extra_headers={}):

        extra_headers.update({"Connection": "keep-alive"})

        self.conn.request("GET", self.base_uri + id, None, extra_headers);
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False, "", None
        data = resp.read()
        return True, data, resp

    def delete(self, id):
        self.conn.request("DELETE", self.base_uri + id, None,
                          {"Connection": "keep-alive"})
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False
        data = resp.read()
        return True
