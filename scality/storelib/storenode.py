# -*- coding: utf-8 -*-
"""Message Store node interaction module

The main class is XCommandNode. It will send XCommands to a node
"""

__all__ = ["XCommandStore"]

from xcommand import XCommand, get_child_elem, get_child_text



CHUNKAPI_SFLAG_NOATIME = 1


def get_nodes_by_tags_in_list(nodelist, tags):
    res_list = []
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            res_list.append(node)
    return res_list


def get_first_node_by_tags_in_list(nodelist, tags):
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            return node
    return None




class ProxyEntry(object):
    def __init__(self, node):
        self.__node = node


    def __str__(self):
        range = self.get_range()
        return "(%s:%s)->%s" % (range[0], range[1], self.get_target())



    def get_target(self):
        return get_child_text(self.__node, "target")

    def get_range(self):
        return (get_child_text(self.__node, "start"),
                get_child_text(self.__node, "end"))

    def get_type(self):
        return get_child_text(self.__node, "type")

    def get_expiration_time(self):
        return get_child_text(self.__node, "expirationtime")

    def get_expiration_delta(self):
        return get_child_text(self.__node, "expirationdelta")



class ProxyTableResult(object):
    """ProxyTableResult is a XML wrapper for the responses to all
    "chord dump proxy table" xcommands. It has convenience methods that parse
    the XML and give the relevant information.
    """
    def __init__(self, node):
        self.__node = node


    def __xml_to_proxy_list(self, xml_proxy_list):
        proxy_list = []
        for node in xml_proxy_list:
            proxy_list.append(ProxyEntry(node))
        return proxy_list


    def get_proxy_list(self):
        xml_proxy_list = self.__node.getElementsByTagName("proxy")
        return self.__xml_to_proxy_list(xml_proxy_list)




class XCommandNode(XCommand):
    """This class is a high-level handler for node XCommands.

    It contains methods to control or query information about a node.
    These methods return wrapper objects that encapsulate the XML result
    sent back by the node, that provide convenience methods to parse
    the results.
    """

    def __init__(self, url, login=None, passwd=None, verbose=False):
        """Initialize the store handler.

        url is the URL of the XCommand interface on the store
        """
        XCommand.__init__(self, url, login, passwd, verbose)


    def dump_proxy_table(self, vnodeid):
        res = self.request({"cmd": "chord dump proxy table"})
        table = res.getElementsByTagName("chorddumpproxytable")[0]
        vnode = get_first_node_by_tags_in_list(table.childNodes,
                                               {"vindex": vnodeid})
        if not vnode:
            raise Exception("Error: no vnode %d found in proxy table" % vnodeid)
        return ProxyTableResult(vnode)


    def chunkapi_test_store_create(self,
                                   repository="/mnt/store/test",
                                   io_blk_size=16384):
        res = self.request({"cmd": "chunkapi test store create",
                            "repository": repository,
                            "sflags": "%d" % CHUNKAPI_SFLAG_NOATIME,
                            "io_blk_size": "%d" % io_blk_size})


    def chunkapi_test_perf_multi(self,
                                 n_ops=20,
                                 buffer_size=10000,
                                 percent_write=50,
                                 sample_size=5000,
                                 verbose=False,
                                 stop_at=50000):
        res = self.request({"cmd": "chunkapi test perf multi",
                            "n_ops": "%d" % n_ops,
                            "buffer_size": "%d" % buffer_size,
                            "percent_write": "%d" % percent_write,
                            "sample_size": "%d" % sample_size,
                            "verbose": (verbose and "1" or "0"),
                            "stop_at": "%d" % stop_at})
