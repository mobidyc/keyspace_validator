# -*- coding: utf-8 -*-
import urllib, urllib2, httplib, cookielib
from xml.dom.minidom import parse, parseString
from storeutils import encode_multipart_formdata, flatten_sequence

try:
    from xml.etree import cElementTree as ET
except ImportError:
    try:
        import xml.etree.ElementTree as ET
    except ImportError:
        try:
            import elementtree.ElementTree as ET
        except ImportError:
            raise ImportError("No suitable ElementTree implementation was found.")

import StringIO
import threading
import sys

# Note: this was backported to 2.7.5 on Centos7
try:
    import ssl

    __https_context_kwargs__ = {
        "context": ssl._create_unverified_context()
    }
except AttributeError:
    __https_context_kwargs__ = {}


class XCommandCredException(ValueError):
    pass

class XCommandAuthErrorHandler(urllib2.HTTPRedirectHandler):
    def __init__(self, login, passwd, opener=None):
        self.opener = opener
        self.login = login
        self.passwd = passwd


    def setOpener(self, opener):
        self.opener = opener

    def http_error_302(self, req, fp, code, msg, headers):

        if headers["location"] == "/login?loginerror=password":
            raise Exception("Invalid login/password");

        if headers["location"] == "/login":
            (content_type, auth_data_list) = encode_multipart_formdata(
                (("url", req.get_selector()),
                 ("login", self.login),
                 ("password", self.passwd)), ())
            auth_data = ""
            for s in flatten_sequence(auth_data_list):
                auth_data += s
            auth_req = urllib2.Request(url = req.get_type() + "://" +
                                       req.get_host() + "/login",
                                       data = auth_data,
                                       headers = {"Content-Type": content_type})
            auth_handle = self.opener.open(auth_req)
            return auth_handle
        else:
            result = urllib2.HTTPRedirectHandler.http_error_302(
                self, req, fp, code, msg, headers)
            result.status = code
            return result



def get_child_elem(parent, elemname):
    for e in parent.childNodes:
        if e.nodeName == elemname:
            return e
    return None

def get_child_text(parent, elemname):
    child = get_child_elem(parent, elemname)
    if not child:
        return None
    if child.firstChild:
        return child.firstChild.nodeValue
    return None

class XCommandResult(object):
    def __init__(self, xmlasfile):
        try:
            self.tree = ET.parse(xmlasfile)
        except Exception, e:
            raise Exception("Cannot parse response. XML not valid")

        root = self.tree.getroot()

        name = root.find("name")
        if name is None:
            raise Exception("Empty response")

        self.cmd = name.text
        self.res = root.find("res")

        # self.result points to the command xml, excluding all xcommand headers
        try:
            self.result = self.res.getchildren()[0]
        except IndexError:
            self.result = None

    def get_res_ET(self):
        return self.res

    def get_ET(self):
        return self.result

    def __str__(self):
        if self.result is None:
            result = ""
        else:
            result = ET.tostring(self.result, "UTF8")
        return "XCOMMAND '%s' result\n%s" % (self.cmd, result)

""" Keeps a dictionnary of auth cookies per host and login """
XCommandCookieJars = {}
def xcommand_cookie_jar_get(url, login):
    """ Create a new cookie jar or return an existing one if it already exists """
    return XCommandCookieJars.setdefault((url, login), cookielib.CookieJar())

""" Keeps a dictionnary of lock per host and login """
XCommandLocks= {}
def xcommand_lock_get(url, login):
    """ Create a new cookie jar or return an existing one if it already exists """
    return XCommandLocks.setdefault((url, login), threading.Lock())

class XCommand(object):

    def __init__(self, url, login=None, passwd=None, verbose=False,
                 config_rdonly=True, cmp_mode=False):
        """    Initialize an XCommand object.

    url: url of the xcommand interface
    login: login name
    passwd: login password
    verbose: if True, requests are logged
    config_rdonly: if True, previous product configuration will be
                   restored when object is destroyed

        """
        if login is None:
            raise XCommandCredException("missing login")
        if passwd is None:
            raise XCommandCredException("missing passwd")
        self.url = url
        self.cookie_jar = xcommand_cookie_jar_get(url, login)
        auth = XCommandAuthErrorHandler(login, passwd)
        self.opener = urllib2.build_opener(
            auth,
            urllib2.HTTPCookieProcessor(self.cookie_jar),
            urllib2.HTTPSHandler(**__https_context_kwargs__)
        )
        auth.setOpener(self.opener)
        self.verbose = verbose
        self.login = login
        self.passwd = passwd
        self.config_rdonly = config_rdonly
        self.config = None
        self.conf_changed = None
        self._cmp_mode = cmp_mode

    def __del__(self):
        if self.conf_changed is not None:
            for conf in self.conf_changed:
                module = conf[0]
                confvar = conf[1]
                value = self.config[confvar]
                res = self.config_set(module, confvar, value)
                if not res:
                    sys.stderr.write("Warning: can't reset config var %s=%s\n"
                                     % (confvar, value))

    def _login(self):

        (content_type, auth_data_list) = encode_multipart_formdata(
            (("login", self.login),
             ("password", self.passwd)), ())
        auth_data = ""
        for s in flatten_sequence(auth_data_list):
            auth_data += s

        auth_req = urllib2.Request(url = self.url + "/login",
                                   data = auth_data,
                                   headers = {"Content-Type": content_type})
        auth_handle = self.opener.open(auth_req)
        print self.cookie_jar

    def request(self, args):

        data = self._request(args)

        try:
            buffer = data.read()
            doc = parseString(buffer)
        except Exception,e:
            message = ("In received buffer:\n"
                       "================\n"
                       "%s"
                       "================\n"
                       "%s" % (buffer, e.message))
            raise Exception(message)
        res = get_child_elem(doc.documentElement, "res")
        if not res:
            raise Exception("Invalid XML: <res> child not found")
        reserror = get_child_elem(doc.documentElement, "reserror")
        if reserror:
            errtext = ""
            for e in reserror.childNodes:
                if e.nodeType == e.TEXT_NODE:
                    errtext = e.nodeValue
                    break
            raise Exception("XCommand returned an error: %s" % errtext)
        return res

    def request_post(self, args, post_data):
        data = self._request_post(args, post_data)

        try:
            buffer = data.read()
            doc = parseString(buffer)
        except Exception,e:
            message = ("In received buffer:\n"
                       "================\n"
                       "%s"
                       "================\n"
                       "%s" % (buffer, e.message))
            raise Exception(message)
        res = get_child_elem(doc.documentElement, "res")
        if not res:
            raise Exception("Invalid XML: <res> child not found")
        reserror = get_child_elem(doc.documentElement, "reserror")
        if reserror:
            errtext = ""
            for e in reserror.childNodes:
                if e.nodeType == e.TEXT_NODE:
                    errtext = e.nodeValue
                    break
            raise Exception("XCommand returned an error: %s" % errtext)
        return res

    def ET_request(self, cmd, args, path=None):
        args["cmd"] = cmd
        data = self._request(args, path=path)
        return XCommandResult(data)

    def _request(self, extra_args={}, output="xml", path=None):
        if not extra_args.has_key("cmd"):
            raise Exception("Request args needs a \"cmd\" key")

        if path is None:
            path = self._cmp_mode and "/xcommand" or "/do"

        args = {
            "suser": self.login,
            "spass": self.passwd,
            "output": output
        }
        args.update(extra_args)

        encoded_url = urllib.urlencode(args, True)
        full_url = self.url + path + "?" + encoded_url

        return self.opener.open(full_url)

    def _request_post(self, args, post_data, output="xml"):
        #print "requesting", args
        if not args.has_key("cmd"):
            raise Exception("Request args needs a \"cmd\" key")

        cmd = args.pop("cmd")
        encoded_url = urllib.urlencode({"cmd": cmd})
        if args:
            encoded_url += "&"
            encoded_url += urllib.urlencode(args)
        encoded_url += "&output=%s" % (output)
        full_url = self.url + "/do?" + encoded_url
        if self.verbose:
            print "request: %s" % full_url
        data =  self.opener.open(full_url, post_data)

        return data

    """ No only xcommands, any kind of request """
    def generic_request(self, page, args):
        encoded_url = ""
        if len(args):
            encoded_url = "?"
            encoded_url += urllib.urlencode(args)
        full_url = self.url + "/" + page + encoded_url
        if self.verbose:
            print "request: %s" % full_url
        datasocket = self.opener.open(full_url)

        return datasocket

    def config_set(self, module, confvar, value):
        """ XCommand helper to set config values on products
    module is the name of the module (eg. msgstore_storage_chunkapi)
    confvar is the name of the configuration variable
    value is the value to set to confvar

    returns True on success, False on error
        """

        if self.config_rdonly and self.config is None:
                self.config = self.config_get()
                self.conf_changed = set()

        res = self.request({"cmd": "config update module",
                            "module": module,
                            "e["+confvar+"]": value})

        resobj = get_child_elem(res, "configupdatemodule")
        mainobj = get_child_elem(resobj, "main")
        env = get_child_elem(mainobj, "env")
        status = get_child_text(env, "status")
        if status == "OK":
            self.conf_changed.add((module, confvar))
            return True
        return False

    def config_get(self):
        """ XCommand helper to return all configuration values on products

    returns a dictionary of config values
        """
        vars = {}
        res = self.request({"cmd": "config view module",
                            "module": "ov_core_config"})

        resobj = get_child_elem(res, "configviewmodule")
        mainobj = get_child_elem(resobj, "main")
        for env in mainobj.getElementsByTagName("env"):
            confvar = get_child_text(env, "name")
            value = get_child_text(env, "value")
            vars[confvar] = value

        return vars
