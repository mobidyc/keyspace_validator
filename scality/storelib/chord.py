# -*- coding: utf-8 -*-
""" Chord interaction module for Message Store

"""


__all__ = ["ChordCmd"]

import re
import string
from storeutils import flatten_sequence, encode_multipart_formdata, GetTransacId
import bcs
from httplib import *
import random
import os

MAXREQUESTSPERCONNECTION=200

class ChordCmdException(Exception):
    """ Unexpected response (unknown status for example) """
    pass

class ChordCmd(object):
    """
    This class is used to send chord opcodes to a specific node using HTTP
    and binary config-sections.
    """

    proto_flag_deleted = (1 << 0)
    proto_flag_placeholder = (1 << 1)
    proto_flag_sync = (1 << 4)
    proto_flag_split = (1 << 5)
    proto_xflag_rawop = (1 << 17)
    proto_xflag_nodata = (1 << 19)

    def __init__(self, hostport):
        """
        Initialize a ChordCmd object.

        hostport: host and port of the target node.
        """
        self.hostport = hostport
        if ':' not in hostport:
            raise ValueError("Need host:port combination, got %r" % hostport)
        self.conn = HTTPConnection(self.hostport)
        self.conn_done = 0

    def SendRequest(self, body):
        """
        Send a custom HTTP chord request to the node

        body is the body of the request

        If the request succeeds, True and the response body is returned
        as a tuple. If it fails, False and the reason of the failure are
        returned.

        """

        content_type, form_body = encode_multipart_formdata(
            (("data", body),), ())

        """ Only one concatenation """
        body = ""
        for s in flatten_sequence(form_body):
            body += s

        self.conn_done += 1
        self.conn.request("POST",
                          "/chord",
                          body,
                          {"Content-Type": content_type, "Connection": "keep-alive"})
        response = self.conn.getresponse()
        if response.status != 200:
            return False, response.reason
        respbody = response.read()

        if self.conn_done > MAXREQUESTSPERCONNECTION:
            self.conn.close()
            self.conn = HTTPConnection(self.hostport)
            self.conn_done = 0

        return True, respbody

    def SendOpcode(self, opcode, data):
        """
        Send a chord opcode formatted in binary format.

        opcode is the string opcode
        data contains the already-formatted arguments of the opcode
        in the "data" branch.

        the result of the SendRequest() call is returned.

        """
        id = 5
        s = []

        s.append(bcs.GenerateSectionStart("query"))
        s.append(bcs.GenerateAttrString("protocol", "chord"))
        s.append(bcs.GenerateAttrInt("version", 3))
        s.append(bcs.GenerateBranchStart("cmd"))
        s.append(bcs.GenerateInt("id", id))
        s.append(bcs.GenerateString("type", "query"))
        s.append(bcs.GenerateString("name", opcode))
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateBranchStart("data"))
        s.append(data)
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateSectionEnd())

        return self.SendRequest(s)

    def GenOwsOpcode(self, opcode, data):
        """
        Send a chord opcode formatted in binary format.

        opcode is the string opcode
        data contains the already-formatted arguments of the opcode
        in the "data" branch.

        the result of the SendRequest() call is returned.

        """
        id = 5
        s = ""
        s += bcs.GenerateSectionStart("query")
        s += bcs.GenerateAttrString("protocol", "ows")
        s += bcs.GenerateAttrInt("version", 2)
        s += bcs.GenerateBranchStart("cmd")
        s += bcs.GenerateInt("id", id)
        s += bcs.GenerateString("type", opcode)
        s += bcs.GenerateBranchEnd()
        s += bcs.GenerateBranchStart("data")
        s += data
        s += bcs.GenerateBranchEnd()
        s += bcs.GenerateSectionEnd()

        return s


    def GetStatus(self, response):
        """
        Get the response status of a chord command.

        The status depends on the opcode (see ov/modules/protocol/chord/README
        for details of status returned).

        """
        p = re.compile("^V0006statusT[0-9]{12}([a-z_]+)",
                       re.MULTILINE | re.IGNORECASE)
        res = p.search(response)
        if not res:
            return None
        chord_status = res.group(1)
        return chord_status.lower()





# V0002ipT000000000009127.0.0.1
# V0004portT0000000000044242

    def notify(self, ip, port, id):
        """
        send "notify" chord command.

        ip, port, id are the data arguments as strings.

        returns True if the command has been correcly sent, False otherwise.

        """
        opcode = "notify"
        data = ""

        data += bcs.GenerateString("ip", ip)
        data += bcs.GenerateString("port", port)
        data += bcs.GenerateString("id", id)

        (res, response) = self.SendOpcode(opcode, data)

        return res


    def notify_verify(self, ip, port, id):
        """
        send "notify_verify" chord command.

        ip, port, id are the data arguments as strings.

        returns True if the command has been correcly sent, False otherwise.

        """
        opcode = "notify_verify"
        data = ""

        data += bcs.GenerateString("ip", ip)
        data += bcs.GenerateString("port", port)
        data += bcs.GenerateString("id", id)

        (res, response) = self.SendOpcode(opcode, data)

        return res


    def get_predecessor(self):
        """
        send "get_predecessor" chord command.

        True and ip:port is returned if the command succeeds. Otherwise
        False is returned.

        """
        opcode = "get_predecessor"
        data = ""

        (res, response) = self.SendOpcode(opcode, data)

        if not res:
            return False, None

        ip = bcs.ParseString(response, "ip")
        port = bcs.ParseString(response, "port")

        if (ip and port):
            return True, ip + ":" + port
        else:
            return False, None


    def get_successor(self):
        """
        send "get_successor" chord command.

        True and ip:port is returned if the command succeeds. Otherwise
        False is returned.

        """
        opcode = "get_successor"
        data = ""

        (res, response) = self.SendOpcode(opcode, data)

        if not res:
            return False, None

        ip = bcs.ParseString(response, "ip")
        port = bcs.ParseString(response, "port")

        if (ip and port):
            return True, ip + ":" + port
        else:
            return False, None


    def find_successor(self, id):
        """
        send "find_successor" chord command.

        id is the chord id coded as a BCH string

        (True, ip:port, id) tuple is returned if the command
        succeeds. Otherwise (False, None, None) is returned.

        """
        opcode = "find_successor"
        data = ""

        data += bcs.GenerateString("id", id)
        (res, response) = self.SendOpcode(opcode, data)

        if not res:
            return False, None

        ip = bcs.ParseString(response, "ip")
        port = bcs.ParseString(response, "port")
        id = bcs.ParseString(response, "id")

        if (ip and port and id):
            return True, ip + ":" + port, id
        else:
            return False, None, None

    def discover_all_store_nodes(self):
        """ Try to discover all nodes currently online for this ring """
        nodes = {}
        key = "".join(["00" for i in xrange(20)])
        while True:
            res, node, id = self.find_successor(key)
            if res == True:
                if node in nodes:
                    break
                else:
                    nodes[node] = id
            key = "%X" % (long(id, 16) + 1)

        return True, nodes.keys()

    def put_local(self, id, version, contents, metadata=None,
                  atime=-1, mtime=-1, ctime=-1,
                  archid=None, archidlen=0, archversion=0,
                  dataversion=None,
                  deleted=False, placeholder=False,
                  split=False,
                  rawop=False, flags=0, size=-1):
        """
        send "put_local" chord command.

        id is the chunk id coded as a BCH string
        contents is the binary chunk contents

        True is returned if the command succeeds and the status is "ok".
        Otherwise False is returned.

        """
        opcode = "put_local"
        data = []
        sflags = flags

        data.append( bcs.GenerateString("id", id))
        data.append( bcs.GenerateInt("version", version))
        if dataversion is not None:
            data.append( bcs.GenerateInt("dataversion", dataversion))
        else:
            data.append( bcs.GenerateInt("dataversion", version))
        if contents is not None:
            data.append( bcs.GenerateRawList("contents", contents))
        else:
            sflags |= self.proto_xflag_nodata;
        data.append( bcs.GenerateInt("size", size))
        if metadata is not None:
            data.append( bcs.GenerateRaw("metadata", metadata))

        data.append( bcs.GenerateTimestamp("atime", atime))
        data.append( bcs.GenerateTimestamp("mtime", mtime))
        data.append( bcs.GenerateTimestamp("ctime", ctime))
        if archid is not None:
            data.append( bcs.GenerateString("archid", archid))
            data.append( bcs.GenerateInt("archidlen", archidlen))
            data.append( bcs.GenerateInt("archversion", archversion))

        if deleted:
            sflags |= self.proto_flag_deleted
        if placeholder:
            sflags |= self.proto_flag_placeholder
        if split:
            sflags |= self.proto_flag_split
        if rawop:
            sflags |= self.proto_xflag_rawop
        data.append( bcs.GenerateInt("flags", sflags))
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False
        return status == "ok"


    def get_local(self, id, range_start=None, range_stop=None, flags=0):
        """
        send "get_local" chord command.

        id is the chunk id coded as a BCH string

        True and the binary contents are returned if the command succeeds
        and the status is "ok". Otherwise False is returned.

        """
        opcode = "get_local"
        data = ""

        flags = int(flags)

        data += bcs.GenerateString("id", id)
        if range_start and range_stop:
            flags = flags | int(4194304)
            data += bcs.GenerateInt("range_start", int(range_start))
            data += bcs.GenerateInt("range_stop", int(range_stop))
        if flags:
            data += bcs.GenerateInt("flags", flags)
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False, 0, None, None

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False, 0, None, None
        if status != "ok":
            return False, 0, None, None
        version = bcs.ParseInt(response, "version")
        contents = bcs.ParseRaw(response, "contents")
        metadata = bcs.ParseRaw(response, "metadata")
        return True, version, contents, metadata


    def delete_local(self, id, version, mtime=-1):
        """
        send "delete_local" chord command.

        id is the chunk id coded as a BCH string

        True is returned if the command succeeds and the status is "ok".
        Otherwise False is returned.

        """
        opcode = "delete_local"
        data = ""

        data += bcs.GenerateString("id", id)
        data += bcs.GenerateInt("version", version)
        data += bcs.GenerateTimestamp("mtime", mtime)
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False
        return status == "ok"


    def check_local(self, id):
        """
        send "check_local" chord command.

        id is the chunk id coded as a BCH string

        The status of the command is returned as a string.
        It can be one of:

        pending_insert
        pending_delete
        pending_read
        free
        exist
        error

        """
        opcode = "check_local"
        data = ""

        data += bcs.GenerateString("id", id)
        (res, response) = self.SendOpcode(opcode, data)

        if not res:
            return False, 0, 0, False, False

        status = self.GetStatus(response)
        version = bcs.ParseInt(response, "version")
        size = bcs.ParseInt(response, "size")
        flags = bcs.ParseInt(response, "flags")
        if flags & self.proto_flag_deleted & 0xFFFFFFFF:
            deleted = True
        else:
            deleted = False

        if not status in ("pending_insert", "pending_delete", "pending_read",
                          "free", "exist", "error"):
            raise ChordCmdException("%s: Error in %s: unknown status: %s" % (self.hostport, opcode, status))

        return status, version, size, deleted


    def check(self, id):
        """
        send "check" chord command.

        id is the chunk id coded as a BCH string

        The status of the command is returned as a string.
        It can be one of:

        pending_insert
        pending_delete
        pending_read
        free
        exist
        error
        """
        opcode = "check"
        data = ""

        data += bcs.GenerateString("id", id)
        (res, response) = self.SendOpcode(opcode, data)

        if not res:
            return False, 0, 0, 0, None

        status = self.GetStatus(response)
        version = bcs.ParseInt(response, "version")
        size = bcs.ParseInt(response, "size")
        flags = bcs.ParseInt(response, "flags")
        metadata = bcs.ParseRaw(response, "metadata")

        if not status in ("pending_insert", "pending_delete", "pending_read",
                          "free", "exist", "error"):
            raise ChordCmdException("%s: Error in %s: unknown status: %s" % (self.hostport, opcode, status))
        try:
            if flags & self.proto_flag_deleted & 0xFFFFFFFF:
                deleted = True
            else:
                deleted = False
        except:
            deleted = False

        return status, version, size, deleted, metadata


    def reserve(self, id, intent, transacid=None):
        """
        send "reserve" chord command.

        id is the chunk id to reserve coded as a BCH string.
        intent is the intent of the reserve. It can be one of:

        insert_request
        read_request
        delete_request
        cancel_request

        The status of the command (string) and the transaction id (integer)
        are returned as a tuple.
        The returned status can be one of:

        pending_insert
        pending_delete
        pending_read
        free
        exist
        error

        """
        opcode = "reserve"
        if transacid is None or intent != "cancel_request":
            transacid = GetTransacId()
        data = ""

        if intent not in ("insert_request", "read_request", "delete_request", "cancel_request"):
            return False, 0, 0, False
        data += bcs.GenerateString("id", id)
        data += bcs.GenerateString("transaction_id", "%x" % int(transacid))
        data += bcs.GenerateString("intent", intent)

        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False, 0, 0, False

        status = self.GetStatus(response)
        if not status in ("pending_insert", "pending_delete", "pending_read",
                          "free", "exist", "error"):
            raise ChordCmdException("%s: Error in %s: unknown status: %s" % (self.hostport, opcode, status))
        version = bcs.ParseInt(response, "version")
        flags = bcs.ParseInt(response, "flags")
        deleted = False
        if flags and flags & self.proto_flag_deleted & 0xFFFFFFFF:
            deleted = True

        return status, transacid, version, deleted


    def put(self, transacid, id, version, contents=None, metadata=None):
        """
        send "put" chord command.

        transacid is the transaction id returned by the "reserve" opcode
        id is the chunk id coded as a BCH string
        contents is the binary chunk contents

        True is returned if the command succeeds and the status is "ok".
        Otherwise False is returned.

        """
        opcode = "put"
        data = []
        flags = 0

        data.append(bcs.GenerateString("id", id))
        data.append(bcs.GenerateString("transaction_id", "%x" % int(transacid)))
        data.append(bcs.GenerateInt("version", int(version)))
        if contents is not None:
            data.append(bcs.GenerateRawList("contents", contents))
        else:
            flags |= self.proto_xflag_nodata
        if metadata is not None:
            data.append(bcs.GenerateRaw("metadata", metadata))
        data.append(bcs.GenerateInt("flags", flags))
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False
        return status == "ok"


    def get(self, transacid, id, range_start=None, range_stop=None):
        """
        send "get" chord command.

        transacid is the transaction id returned by the "reserve" opcode
        id is the chunk id coded as a BCH string

        True and the binary contents are returned if the command succeeds
        and the status is "ok". Otherwise False is returned.

        """
        opcode = "get"
        data = ""

        data += bcs.GenerateString("id", id)
        data += bcs.GenerateString("transaction_id", "%x" % int(transacid))
        if range_start:
            data += bcs.GenerateInt("flags", int(4194304))
            data += bcs.GenerateInt("range_start", int(range_start))
            data += bcs.GenerateInt("range_stop", int(range_stop))
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False, 0, None, None

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False, 0, None, None
        if status != "ok":
            return False, 0, None, None

        version = bcs.ParseInt(response, "version")
        contents = bcs.ParseRaw(response, "contents")
        metadata = bcs.ParseRaw(response, "metadata")
        return True, version, contents, metadata

    def find_nearest_above(self, id, maxid, nnearest):
        opcode = "find_nearest_above"
        data = []
        data.append(bcs.GenerateString("id", id))
        data.append(bcs.GenerateString("maxid", maxid))
        data.append(bcs.GenerateInt("nnearest", int(nnearest)))

        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False, None, None, None
        status = self.GetStatus(response)
        if not status in ("free", "exist", "error"):
            return False, status, None, None

        if status == "exist":
            nresults = bcs.ParseInt(response, "nresults")
        else:
            nresults = 0

        idlist = []
        for i in range(nresults):
            e = {
                "id": bcs.ParseString(response, "id%d" % i)
            }
            try:
                e["version"] = bcs.ParseInt(response, "version%d" % i)
                e["flags"] = bcs.ParseInt(response, "flags%d" % i) & 0xFFFF
            except:
                pass

            try:
                ip = bcs.ParseString(response, "ip%d" % i)
                if ip is not None:
                    e["ip"] = ip
                    e["port"] = bcs.ParseString(response, "port%d" % i)
            except:
                pass
            idlist.append(e)

        return True, status, nresults, idlist


    def delete(self, transacid, id, version):
        """
        send "delete" chord command.

        transacid is the transaction id returned by the "reserve" opcode
        id is the chunk id coded as a BCH string
        version is the chunk version (returned by get)
        True is returned if the command succeeds and the status is "ok".
        Otherwise False is returned.

        """
        opcode = "delete"
        data = []

        data.append(bcs.GenerateString("id", id))
        data.append(bcs.GenerateString("transaction_id", "%x" % transacid))
        data.append(bcs.GenerateInt("version", version))
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False

        return status == "ok"

    def stack(self, proto, id, raw_data):
        """
        internal
        send "stack" chord command.

        proto is the stacked protocol identifier
        id is the target key
        raw_data is the protocol-specific data

        """
        opcode = "stack"
        data = ""

        data += bcs.GenerateString("id", id)
        data += bcs.GenerateString("proto", proto)
        data += bcs.GenerateRaw("raw", raw_data)
        (res, response) = self.SendOpcode(opcode, data)
        if not res:
            return False, None

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False, None
        if status != "ok":
            return False, None

        raw_answer = bcs.ParseRaw(response, "raw")
        return True, raw_answer


    def ows_del(self, id):
        """
        send "ows_del" chord command.

        id is the chunk id coded as a BCH string
        True is returned if the command succeeds and the status is "ok".
        Otherwise False is returned.

        """
        opcode = "delete"

        raw_data = self.GenOwsOpcode("delete", "")
        (res, response) = self.stack("ows", id, raw_data)
        if not res:
            return False

        status = self.GetStatus(response)
        if not status in ("ok", "error"):
            return False

        return status == "ok"


    """ Perform a get using a transaction but hides the transaction details internally """
    def getF(self, key):
        status, transacid, version, deleted = self.reserve(key, "read_request")
        if status != "exist":
            raise ChordCmdException("Transaction failed for key %s (%s)" % (key, status))
        status, v, contents, metadata = self.get(transacid, key)
        return status, v, contents, metadata

    """ Perform a put using a transaction but hides the transaction details internally """
    def putF(self, key, contents=None, metadata=None):
        status, transacid, version, deleted = self.reserve(key, "insert_request")
        if version is None:
            version = 64
        else:
            version = ((version >> 6) + 1) << 6

        if status == False:
            raise ChordCmdException("Transaction failed for key %s (%s)" % (key, status))
        if not(status == "exist" or status == "free"):
            raise ChordCmdException("Transaction refused for key %s (%s)" % (key, status))
        status = self.put(transacid, key, version, contents, metadata)
        return status

    def fuzz(self):
        """ Send garbage and wait for the worst """
        cmds = [os.urandom(random.randint(1, 14)) for x in xrange(10)]
        cmds.append(["delete", "get", "put", "check_local", "get_local", "put_local"])
        opcode = random.choice(cmds)

        data = []
        #if random.choice([True, False]):
        """ Send garbages that is a valid config section """
        for x in xrange(random.randint(1, 10)):
            data.append(bcs.GenerateString(os.urandom(random.randint(0, 14)),
                                       os.urandom(random.randint(0, 10000))))
        #else:
        #    """ not even a valid config section """
        #    data.append(os.urandom(random.randint(0, 10000)))

        (res, response) = self.SendOpcode(opcode, data)

        return res, response
