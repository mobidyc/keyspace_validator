# -*- coding: utf-8 -*-
""" BRS2 object """
# add boto path in python path
import sys
import os.path
import time
import urllib
import re

try:
    import hashlib
    def md5():
        return hashlib.new('md5')
except ImportError:
    from md5 import md5

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from boto.exception import S3ResponseError

from scality.storelib import storeutils


DEFAULT_SVCID = "40"


class BRS2Error(Exception):

    def __init__(self, exc):
        self.exc = exc

    def __str__(self):
        return str(self.exc)


class BRS2(object):

    def __init__(self, accessKey, secretKey, domain, ssl=False, port=None):
        self.accessKey = accessKey
        self.secretKey = secretKey
        self.domain = domain
        self.port = port
        self.ssl = ssl

        """ s3 connection object """
        self.s3c = S3Connection(aws_access_key_id = accessKey,
                                aws_secret_access_key = secretKey,
                                is_secure = ssl,
                                port = port,
                                host = domain)

        self.test_credentials()

    def test_credentials(self):
        try:
            self.s3c.get_all_buckets()
        except S3ResponseError, e:
            raise BRS2Error("Login error")
        except Exception, e:
            raise BRS2Error(e)

    def list_buckets(self):
        try:
            return [b.name for b in self.s3c.get_all_buckets()]
        except Exception, e:
            raise BRS2Error(e)

    def create_bucket(self, bucket):
        try:
            self.s3c.create_bucket(bucket)
        except Exception, e:
            raise BRS2Error(e)

    def delete_bucket(self, bucket):
        try:
            self.s3c.delete_bucket(bucket)
        except Exception, e:
            raise BRS2Error(e)

    def list_bucket(self, bucket):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            return [i.name for i in b.list()]
        except Exception, e:
            raise BRS2Error(e)

    def put_string(self, bucket, name, content):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            k = Key(b, name)
            k.set_contents_from_string(content)
        except Exception, e:
            raise BRS2Error(e)

    def get_string(self, bucket, name):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            k = Key(b, name)
            return k.get_contents_as_string()
        except Exception, e:
            raise BRS2Error(e)

    def download_to_file(self, bucket, name, localfile):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            k = Key(b, name)
            k.get_contents_to_filename(localfile)
        except Exception, e:
            raise BRS2Error(e)

    def upload_from_file(self, bucket, name, localfile):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            k = Key(b, name)
            k.set_contents_from_filename(localfile)
        except Exception, e:
            raise BRS2Error(e)

    def delete_key(self, bucket, name):
        try:
            b = self.s3c.get_bucket(bucket, validate=False)
            k = Key(b, name)
            k.delete()
        except Exception, e:
            raise BRS2Error(e)

    def test_compat(self):
        print "--- running S3Connection tests ---"
        c = self.s3c
        # create a new, empty bucket
        bucket_name = "test-%d" % int(time.time())
        bucket = c.create_bucket(bucket_name)
        # now try a get_bucket call and see if it's really there
        bucket = c.get_bucket(bucket_name)

        # Not supported yet
        # test logging
        #logging_bucket = c.create_bucket(bucket_name + "-log")
        #logging_bucket.set_as_logging_target()
        #bucket.enable_logging(target_bucket=logging_bucket, target_prefix=bucket.name)
        #bucket.disable_logging()
        #c.delete_bucket(logging_bucket)

        k = bucket.new_key()
        k.name = "foobar"
        s1 = "This is a test of file upload and download"
        s2 = "This is a second string to test file upload and download"
        k.set_contents_from_string(s1)
        fp = open("foobar", "wb")
        # now get the contents from s3 to a local file
        k.get_contents_to_file(fp)
        fp.close()
        fp = open("foobar")
        # check to make sure content read from s3 is identical to original
        assert s1 == fp.read(), "corrupted file"
        fp.close()

        # test generated URLs
        url = k.generate_url(3600)
        file = urllib.urlopen(url)
        assert s1 == file.read(), "invalid URL %s" % url
        url = k.generate_url(3600, force_http=True)
        file = urllib.urlopen(url)
        assert s1 == file.read(), "invalid URL %s" % url
        bucket.delete_key(k)
        # test a few variations on get_all_keys - first load some data
        # for the first one, let's override the content type
        phony_mimetype = "application/x-boto-test"
        headers = {"Content-Type": phony_mimetype}
        k.name = "foo/bar"
        k.set_contents_from_string(s1, headers)
        k.name = "foo/bas"
        k.set_contents_from_filename("foobar")
        k.name = "foo/bat"
        k.set_contents_from_string(s1)
        k.name = "fie/bar"
        k.set_contents_from_string(s1)
        k.name = "fie/bas"
        k.set_contents_from_string(s1)
        k.name = "fie/bat"
        k.set_contents_from_string(s1)
        # try resetting the contents to another value
        md5 = k.md5
        k.set_contents_from_string(s2)
        assert k.md5 != md5
        os.unlink("foobar")
        all = bucket.get_all_keys()
        assert len(all) == 6
        rs = bucket.get_all_keys(prefix="foo")
        assert len(rs) == 3
        rs = bucket.get_all_keys(prefix="", delimiter="/")
        assert len(rs) == 2
        rs = bucket.get_all_keys(maxkeys=5)
        assert len(rs) == 5
        # test the lookup method
        k = bucket.lookup("foo/bar")
        assert isinstance(k, bucket.key_class)
        assert k.content_type == phony_mimetype
        k = bucket.lookup("notthere")
        assert k == None
        # try some metadata stuff
        k = bucket.new_key()
        k.name = "has_metadata"
        mdkey1 = "meta1"
        mdval1 = "This is the first metadata value"
        k.set_metadata(mdkey1, mdval1)
        mdkey2 = "meta2"
        mdval2 = "This is the second metadata value"
        k.set_metadata(mdkey2, mdval2)
        k.set_contents_from_string(s1)
        k = bucket.lookup("has_metadata")
        assert k.get_metadata(mdkey1) == mdval1
        assert k.get_metadata(mdkey2) == mdval2
        k = bucket.new_key()
        k.name = "has_metadata"
        k.get_contents_as_string()
        assert k.get_metadata(mdkey1) == mdval1
        assert k.get_metadata(mdkey2) == mdval2
        bucket.delete_key(k)
        # test list and iterator
        rs1 = bucket.list()
        num_iter = 0
        for r in rs1:
            num_iter = num_iter + 1
        rs = bucket.get_all_keys()
        num_keys = len(rs)
        assert num_iter == num_keys
        # try a key with a funny character
        k = bucket.new_key()
        k.name = "testnewline\n"
        k.set_contents_from_string("This is a test")
        rs = bucket.get_all_keys()
        assert len(rs) == num_keys + 1
        bucket.delete_key(k)
        rs = bucket.get_all_keys()
        assert len(rs) == num_keys

        # Not supported yet
        # try some acl stuff
        #bucket.set_acl("public-read")
        #policy = bucket.get_acl()
        #assert len(policy.acl.grants) == 2

        #bucket.set_acl("private")
        #policy = bucket.get_acl()
        #assert len(policy.acl.grants) == 1

        #k = bucket.lookup("foo/bar")
        #k.set_acl("public-read")
        #policy = k.get_acl()
        #assert len(policy.acl.grants) == 2

        #k.set_acl("private")
        #policy = k.get_acl()
        #assert len(policy.acl.grants) == 1

        # try the convenience methods for grants
        #bucket.add_user_grant("FULL_CONTROL",
        #                      "c1e724fbfa0979a4448393c59a8c055011f739b6d102fb37a65f26414653cd67")
        #try:
        #    bucket.add_email_grant("foobar", "foo@bar.com")
        #except S3PermissionsError:
        #    pass
        # now delete all keys in bucket
        for k in all:
            bucket.delete_key(k)
        # now delete bucket
        c.delete_bucket(bucket)
        print "--- tests completed ---"


def _get_hash(value):
    dig = md5()
    dig.update(value)
    hashv = dig.hexdigest()
    hashv = "".join(re.split(r"(..)", hashv[0:16])[::-1]).upper()
    return hashv


def encodeKeyUser(username, cos, svcid=DEFAULT_SVCID):
    h = _get_hash(username)
    uks = storeutils.uks_gen_raw(h[10:16],
                                 h, "0", svcid, "1", cos)
    return uks


def encodeKeyBucket(bucket, cos, svcid=DEFAULT_SVCID):
    h = _get_hash(bucket)
    uks = storeutils.uks_gen_raw(h[8:14],
                                 "0", h[0:8], svcid, "2", cos)
    return uks


def encodeKeyFile(bucket, filename, cos, svcid=DEFAULT_SVCID):
    h1 = _get_hash(bucket)
    h2 = _get_hash(filename)
    h = "%.6X" % (int(h1[8:14], 16) ^ int(h2[10:16], 16))
    uks = storeutils.uks_gen_raw(h, h2, h1[0:8], svcid, "3", cos)
    return uks
