# -*- coding: utf-8 -*-
""" REST module

"""


__all__ = ["RestCmd"]

import re
import time
import random
import string
from httplib import *
from storeutils import flatten_sequence


class RestCmd(object):

    def __init__(self, uri, uri_prefix="perfest/"):
        self.uri = uri
        self.uri_prefix = uri_prefix # Append a prefix to all URI to prevent name clash
        self.base_uri = "/sws/%s" % uri_prefix
        self.resetconn();

    def resetconn(self):
        self.conn = HTTPConnection(self.uri)
        self.count = 0;

    def put(self, id, contents):
        body = ""
        for s in flatten_sequence(contents):
            body += s

        self.count += 1;
        if (self.count > 50):
            self.resetconn()

        self.conn.request("PUT", self.base_uri + id, body,
                          {"Connection": "keep-alive"})
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False
        resp.read()
        return True

    def get(self, id):
        self.conn.request("GET", self.base_uri + id, None,
                          {"Connection": "keep-alive"})
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False, ""
        data = resp.read()
        return True, data

    def delete(self, id):
        self.conn.request("DELETE", self.base_uri + id, None,
                          {"Connection": "keep-alive"})
        resp = self.conn.getresponse()
        if resp.status != 200:
            self.resetconn()
            return False
        data = resp.read()
        return True
