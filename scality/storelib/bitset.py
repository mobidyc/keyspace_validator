# -*- coding: utf-8 -*-


class BitSet(object):

    def __init__(self):
        self.value = 0L

    def bit_set(self, pos):
        self.value |= (1 << pos)

    def set_from_val(self, val, numbits, pos):
        for b in xrange(numbits):
            if val & (1 << b):
                self.bit_set(pos + b)
            else:
                self.bit_clear(pos + b)

    def set_from_binstr(self, binstr, numbits, pos):
        for i, c in zip(xrange(numbits/8), binstr):
            val = ord(c)
            for b in xrange(8):
                if val & (1 << b):
                    self.bit_set(pos + i * 8 + b)
                else:
                    self.bit_clear(pos + i * 8 + b)

    def bit_clear(self, pos):
        self.value &= ~(1 << pos)

    def to_bin(self, numbits):
        return "".join(chr(a) for a in [((self.value>>c) & 0x0ff) for c in xrange(0,numbits,8)])

    def to_long(self):
        return self.value
