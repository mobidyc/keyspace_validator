# -*- coding: utf-8 -*-
"""Testlink utility functions
"""

import traceback
import sys
import time


def _write_xml_data(data):
    data = data.replace("&", "&amp;").replace("<", "&lt;")
    data = data.replace("\"", "&quot;").replace(">", "&gt;")
    sys.stdout.write("%s" % data)


def test_start(name):
    print "<test>"
    print "<name>%s</name>" % name
    start = time.time()
    print "<starttime>%s</starttime>" % time.ctime(start)
    return start

def test_success(msg = None, start = None):
    print "<result>OK</result>"
    if start is not None:
        end = time.time()
        print "<duration>%g</duration>" % (end - start)
    if None != msg:
        sys.stdout.write("<note>")
        _write_xml_data(msg)
        print "</note>"
    print "</test>"

def test_fail(msg, start = None):
    print "<result>NOK</result>"
    if msg:
        sys.stdout.write("<note>")
        _write_xml_data(msg)
        print "</note>"
    print "</test>"

def test_except():
    print "<result>NOK</result>"
    print "<note>"
    exc = traceback.format_exc()
    _write_xml_data(exc)
    print "</note>"
    print "</test>"


def test_savetime(name, start):
    if start is not None:
        end = time.time()
        if name is not None:
            print "<%s>%g</%s>" % (name, (end - start), name)
        else:
            print "<steptime>%g</steptime>" % (end - start)
    start = time.time()
    return start
