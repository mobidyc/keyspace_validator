# -*- coding: utf-8 -*-
""" Parse/generate binary config-sections

"""

__all__ = ["GenerateSectionStart", "GenerateSectionEnd", "GenerateBranchStart", "GenerateBranchEnd", "GenerateInt", "GenerateString", "GenerateTimestamp", "GenerateRaw", "GenerateRawList", "ParseInt", "ParseString", "ParseTimestamp", "ParseRaw"]

import re


def GenerateSectionStart(name):
    data = "S%.4d%s\n" % (len(name), name)
    return data


def GenerateSectionEnd():
    return "s\n"

def GenerateBranchStart(name):
    data = "B%.4d%s\n" % (len(name), name)
    return data

def GenerateBranchEnd():
    return "b\n"

def GenerateInt(name, val):
    data = "V%.4d%sI%i\n" % (len(name), name, val)
    return data

def GenerateString(name, val):
    data = "V%.4d%sT%.12d%s\n" % (len(name), name, len(val), val)
    return data

def GenerateTimestamp(name, val):
    data = "V%.4d%sS%i\n" % (len(name), name, val)
    return data

def GenerateRaw(name, val):
    data = "V%.4d%sR%.12d" % (len(name), name, len(val))
    if isinstance(val, str):
        data += val
    else:
        """ Iterate other val """
        for d in val:
            data += d
    data += "\n"
    return data

def GenerateRawList(name, val):
    data = ["V%.4d%sR%.12d" % (len(name), name, len(val))]
    data.append(val)
    data.append("\n")
    return data

def GenerateAttrInt(name, val):
    data = "A%.4d%sI%i\n" % (len(name), name, val)
    return data

def GenerateAttrString(name, val):
    data = "A%.4d%sT%.12d%s\n" % (len(name), name, len(val), val)
    return data


def ParseInt(response, name):
    p = re.compile("^V%.4d%sI([0-9]+)$" % (len(name), name),
                   re.MULTILINE)
    res = p.search(response)
    if res:
        return int(res.group(1))
    return None

def ParseString(response, name):
    p = re.compile("^V%.4d%sT[0-9]{12}(.*)$" % (len(name), name),
                   re.MULTILINE)
    res = p.search(response)
    if res:
        return res.group(1)
    return None

def ParseTimestamp(response, name):
    p = re.compile("^V%.4d%sS([0-9]+)$" % (len(name), name),
                   re.MULTILINE)
    res = p.search(response)
    if res:
        return int(res.group(1))
    return None

def ParseRaw(response, name):
    p = re.compile("^V%.4d%sR([0-9]{12})" % (len(name), name),
                   re.MULTILINE)
    res = p.search(response)
    if not res:
        return None
    size = res.group(1)
    return response[res.end():res.end()+int(size)]
