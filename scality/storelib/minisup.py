# -*- coding: utf-8 -*-
""" Node command port interaction module for Message Store
"""

from httplib import HTTPSConnection
from scality.storelib.storeutils import (flatten_sequence,
                                         encode_multipart_formdata,
                                         wait_for)
import scality.storelib.bcs as bcs


__all__ = ["MiniSup"]
MINISUP_POLL_INTERVAL = 0.5
MINISUP_TIMEOUT = 1000


class NodeStatus(object):
    def __init__(self, cs):
        self.state = bcs.ParseString(cs, "state")

    def get_state(self):
        return self.state


class MiniSup(object):
    def __init__(self, hostport, vnodeid, keyfile, certfile):
        """
        Initialize a MiniSup object.

        hostport: host and port of the target node.
        """
        self.hostport = hostport
        self.vnodeid = vnodeid

        tmp = hostport.split(":")
        self.conn = HTTPSConnection(tmp[0], int(tmp[1]), keyfile, certfile)

    def __state_checkfunc(self, statelist):
        status = self.status()
        cur_state = status.get_state()
        for state in statelist:
            if cur_state == state:
                return (True, None)
        return (False, None)

    def SendRequest(self, body):
        """
        Send an HTTP command via the command port

        body is the body of the request

        If the request succeeds, True and the response body is returned
        as a tuple. If it fails, False and the reason of the failure are
        returned.

        """

        content_type, form_body = encode_multipart_formdata(
            (("data", body),), ())

        # Only one concatenation
        body = ""
        for string in flatten_sequence(form_body):
            body += string

        self.conn.request("POST",
                          "/chordsup",
                          body,
                          {"Content-Type": content_type})
        response = self.conn.getresponse()
        if response.status != 200:
            print "Connection to %s: Error %d: %s" % (self.hostport,
                                                      response.status,
                                                      response.reason)
            return False, response.reason
        respbody = response.read()
        # print "Debug response:\n" + respbody + "\n"
        return True, respbody

    def SendOpcode(self, opcode, data):
        """
        Send a supervisor opcode formatted in binary format.

        opcode is the string opcode
        data contains the already-formatted arguments of the opcode
        in the "data" branch.

        the result of the SendRequest() call is returned.

        """
        request = [bcs.GenerateSectionStart("command"),
                   bcs.GenerateBranchStart("cmd_desc"),
                   bcs.GenerateInt("id", 5),
                   bcs.GenerateString("type", "REQUEST"),
                   bcs.GenerateString("name", opcode),
                   bcs.GenerateInt("vnodeid", self.vnodeid),
                   bcs.GenerateBranchEnd(),
                   bcs.GenerateBranchStart("data"),
                   data,
                   bcs.GenerateBranchEnd(),
                   bcs.GenerateSectionEnd()]
        return self.SendRequest(request)

    def GetStatus(self, response):
        """
        Get the response status of a command.

        The status depends on the opcode

        TODO

        """
        return "ok"

    def status(self):
        """
        send "status" command
        """
        (res, response) = self.SendOpcode("STATUS", "")
        if res is False:
            return None
        return NodeStatus(response)

    def assignparam(self, name, value):
        """
        send "assignparam" command
        """
        data = bcs.GenerateString(name, value)
        return self.SendOpcode("assignparam", data)[0]

    def assignid(self, key):
        """
        send "assignid" command
        """
        data = bcs.GenerateString("key", key)
        return self.SendOpcode("assignid", data)[0]

    def wait_for_state(self, states,
                       poll_interval=MINISUP_POLL_INTERVAL,
                       timeout=MINISUP_TIMEOUT):
        if isinstance(states, str):
            statelist = [states]
        else:
            statelist = states
        wait_for("state %s" % " or ".join(statelist), poll_interval, timeout,
                 self.__state_checkfunc, statelist)

    def join(self, bootstrap, bootstrap_key):
        """
        send "join" command
        """
        data = bcs.GenerateString("bootstrap", bootstrap)
        data += bcs.GenerateString("bootstrap_key", bootstrap_key)
        return self.SendOpcode("join", data)[0]
