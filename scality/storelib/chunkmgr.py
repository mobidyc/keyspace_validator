# -*- coding: utf-8 -*-
"""ChunkMgr XCommand access functions

"""

__all__ = ["ChunkMgrStore"]

from xcommand import XCommand, get_child_elem, get_child_text
import sys

class ChunkMd(dict):
    def __init__(self):
        dict.__init__(self)

    def __getattribute__(self, name):
        try:
            return dict.__getitem__(self, name)
        except KeyError,e:
            return None


class ChunkMgrStoreResumeId(object):
    def __init__(self):
        self.id = None
        self.op = None


class ChunkMgrStore(XCommand):
    """This class is a high-level handler for ChunkMgr XCommands.

    These methods return wrapper objects that encapsulate the XML result
    sent back by the node, that provide convenience methods to parse
    the results.
    """

    CHECK_CRC_TIER1 = 4

    MD_DESC = {"mdver": "int",
               "mflags": "mflags",
               "atime": "int",
               "mtime": "int",
               "ctime": "int",
               "version": "int",
               "dataversion": "int",
               "crc32": "int",
               "size": "int"}

    MFLAGS_DESC = ["deleted", "placeholder"]

    def __init__(self, url, login=None, passwd=None, verbose=False,
                 dso="teststore", vnode="4242",
                 strategy="bizobj", strategy_params="",
                 bizionames=["default"], selector="00000000",
                 sflags=0, io_blk_size=16384):
        """Initialize the ChunkMgr store handler.

        url is the URL of the XCommand interface on the store
        """
        XCommand.__init__(self, url, login, passwd, verbose)

        self.dso = dso
        self.vnode = vnode
        self.strategy = strategy
        self.io_blk_size = io_blk_size
        self.suspend_at_event = None
        self.cur_op = None
        self.resume_id = None

        #will also set the session cookie for POST requests
        open_res = self.request({"cmd": "chunkapi store open",
                                 "dso": dso,
                                 "vnode": vnode,
                                 "strategy": strategy,
                                 "strategy_params": strategy_params,
                                 "bizionames": ",".join(bizionames),
                                 "selector": selector,
                                 "sflags": "%d" % sflags,
                                 "io_blk_size": "%d" % io_blk_size})

        root = get_child_elem(open_res, "chunkapistoreopen")
        result = get_child_elem(root, "result")
        status = get_child_text(result, "status")

        if not status == "ok":
            raise Exception("chunkmgr store open returned an error: %s" % status)
        self.store_opened = True


    def __del__(self):

        self.close()

        XCommand.__del__(self)

    def __parse_result(self, res):
        root = get_child_elem(res, "chunkapistoreop")
        result = get_child_elem(root, "result")

        status = get_child_text(result, "status")
        data = get_child_text(result, "data")
        usermd = get_child_text(result, "usermd")
        resume_id_str = get_child_text(result, "resume_id")
        self.resume_id = ChunkMgrStoreResumeId()
        if resume_id_str is not None:
            self.resume_id.id = int(resume_id_str)
            self.resume_id.op = self.cur_op
            self.cur_op = None

        md_present = False
        mds = ChunkMd()
        for md in self.MD_DESC.items():
            (mdkey, mdtype) = md
            mdvalue = get_child_text(result, mdkey)
            if mdvalue is not None:
                md_present = True
                if mdtype == "int":
                    mds[mdkey] = int(mdvalue)
                elif mdtype == "str":
                    mds[mdkey] = str(mdvalue)  # convert unicode to str
                elif mdtype == "float":
                    mds[mdkey] = float(mdvalue)
                elif mdtype == "mflags":
                    mflags = int(mdvalue)
                    mds['deleted'] = False
                    mds['placeholder'] = False
                    if mflags & 1:
                        mds['deleted'] = True
                    if mflags & 8:
                        mds['placeholder'] = True

        return (status, data, mds, usermd)

    def __op(self, op, chunkid, data=None, md=None, usermd=None,
             input_data_file=None, output_data_file=None,
             op_called=False):

        args = {"cmd": "chunkapi store op",
                "dso": self.dso,
                "vnode": self.vnode,
                "op": op,
                "chunkid": chunkid}

        if md is not None:
            for mdelem in md.items():
                (mdkey, mdval) = mdelem
                if mdval is not None and mdkey in self.MD_DESC:
                    if self.MD_DESC[mdkey] == "int":
                        args[mdkey] = "%d" % mdval
                    elif self.MD_DESC[mdkey] == "str":
                        args[mdkey] = str(mdval)
                    elif self.MD_DESC[mdkey] == "float":
                        args[mdkey] = "%f" % mdval
                    elif self.MD_DESC[mdkey] == "mflags":
                        mflags = 0
                        for mflag in mdval:
                            index = self.MFLAGS_DESC.index(mflag)
                            mflags += 2**index
                        args[mdkey] = mflags

        if usermd is not None:
            args["usermd"] = usermd
        if input_data_file is not None:
            args["input_data_file"] = input_data_file
        if output_data_file is not None:
            args["output_data_file"] = output_data_file

        if self.suspend_at_event is not None:
            args["suspend_at_event"] = self.suspend_at_event
            if op_called:
                self.cur_op = "op"
            elif op in ["get", "get_raw"]:
                if output_data_file is None:
                    self.cur_op = op
                else:
                    self.cur_op = op + "_to_file"
            else:
                self.cur_op = op
            self.suspend_at_event = None

        if op in ["put", "put_rebuild", "put_raw"]:
            if input_data_file is None:
                res = self.request_post(args, data)
            else:
                res = self.request(args)
        else:
            res = self.request(args)

        return self.__parse_result(res)




    def nop(self):
        res = self.request({"cmd": "chunkapi nop"})


    def __close(self):
        """Close the ChunkMgr store
        """

        res = self.request({"cmd": "chunkapi store close",
                            "dso": self.dso,
                            "vnode": self.vnode})

    def close(self):
        if self.store_opened:
            self.__close()
            self.store_opened = False


    def orphan(self):
        """ Disable all cleanup post-processing of this object
        """
        self.store_opened = False



    def op(self, op, chunkid, data=None, md=None, usermd=None,
           input_data_file=None, output_data_file=None):
        """Do an operation on a ChunkMgr store

        valid ops: "put", "put_raw", "get", "get_raw", "updatemd", "delete", "undelete", "stat", "physdelete", "physdelete_raw", "loadmd"
        """

        return self.__op(op, chunkid, data, md, usermd,
                         input_data_file, output_data_file,
                         op_called=True)

    def put(self, chunkid, majorversion, data, usermd=None,
            atime=None, mtime=None, ctime=None,
            input_data_file=None):

        (status, data, mds, usermd) = self.__op("put", chunkid, data,
                                                {"atime": atime,
                                                 "mtime": mtime,
                                                 "ctime": ctime,
                                                 "version": majorversion * 64},
                                                usermd,
                                                input_data_file, None)
        return status

    def put_rebuild(self, chunkid, data, md=None, usermd=None,
                    input_data_file=None):
        (status, data, mds, usermd) = self.__op("put_rebuild", chunkid,
                                                data, md, usermd,
                                                input_data_file, None)
        return status

    def put_raw(self, chunkid, data, md=None, usermd=None,
                input_data_file=None):
        (status, data, mds, usermd) = self.__op("put_raw", chunkid,
                                                data, md, usermd,
                                                input_data_file, None)
        return status

    def updatemd(self, chunkid, version=None,
                 atime=None, mtime=None, ctime=None,
                 usermd=None):
        (status, data, mds, usermd) = self.__op("updatemd", chunkid, None,
                                                {"atime": atime,
                                                 "mtime": mtime,
                                                 "ctime": ctime,
                                                 "version": version},
                                                usermd,
                                                None, None)
        return status

    def updatemd_rebuild(self, chunkid, md=None, usermd=None):
        (status, data, mds, usermd) = self.__op("updatemd_rebuild",
                                                chunkid, None, md, usermd,
                                                None, None)
        return status

    def updatemd_raw(self, chunkid, md=None, usermd=None):
        (status, data, mds, usermd) = self.__op("updatemd_raw", chunkid,
                                                None, md, usermd,
                                                None, None)
        return status

    def delete(self, chunkid, majorversion, mtime=None):
        (status, data, mds, usermd) = self.__op("delete",
                                                chunkid=chunkid,
                                                md=
                                                {"mtime": mtime,
                                                 "version": majorversion * 64})
        return status

    def undelete(self, chunkid, majorversion, mtime=None):
        (status, data, mds, usermd) = self.__op("undelete",
                                                chunkid=chunkid,
                                                md=
                                                {"mtime": mtime,
                                                 "version": majorversion * 64})
        return status


    def get(self, chunkid):
        (status, data, mds, usermd) = self.__op("get", chunkid)
        return (status, data, mds, usermd)

    def get_to_file(self, chunkid, output_data_file):
        (status, data, mds, usermd) = self.__op("get", chunkid,
                                                output_data_file=output_data_file)
        return (status, mds, usermd)

    def get_raw(self, chunkid):
        (status, data, mds, usermd) = self.__op("get_raw", chunkid)
        return (status, data, mds, usermd)

    def get_raw_to_file(self, chunkid, output_data_file):
        (status, data, mds, usermd) = self.__op("get_raw", chunkid,
                                                output_data_file=output_data_file)
        return (status, mds, usermd)

    def stat(self, chunkid):
        (status, data, mds, usermd) = self.__op("stat", chunkid)
        return (status, mds, usermd)


    def physdelete(self, chunkid):
        (status, data, mds, usermd) = self.__op("physdelete", chunkid)
        return status

    def physdelete_raw(self, chunkid):
        (status, data, mds, usermd) = self.__op("physdelete_raw", chunkid)
        return status

    def loadmd(self, chunkid):
        (status, data, mds, usermd) = self.__op("loadmd", chunkid)
        return (status, mds)


    def suspend_next_op(self, chunkmgrevent):
        if self.suspend_at_event is not None:
            raise Exception("suspend event already set: %s"
                            % self.suspend_at_event)

        self.suspend_at_event = chunkmgrevent

    def get_resume_id(self):
        return self.resume_id

    def resume_op(self, resume_id):
        args = {"cmd": "chunkapi store op resume",
                "resume_id": "%d" % resume_id.id}

        if self.suspend_at_event is not None:
            args["suspend_at_event"] = self.suspend_at_event
            self.suspend_at_event = None

        res  = self.request(args)
        self.cur_op = resume_id.op

        (status, data, mds, usermd) = self.__parse_result(res)

        if resume_id.op in ["op", "get", "get_raw"]:
            return (status, data, mds, usermd)
        elif resume_id.op in ["put", "put_raw", "updatemd", "delete",
                              "undelete", "physdelete", "physdelete_raw",
                              "loadmd"]:
            return status
        elif resume_id.op in ["get_to_file", "get_raw_to_file", "stat"]:
            return (status, mds, usermd)
        else:
            return None


    def bizio_disconnect(self, bizioname="default"):
        args = {"cmd": "asyncpersistentmemory test bizio disconnect",
                "bizioname": bizioname}

        res = self.request(args)

        root = get_child_elem(res, "asyncpersistentmemorytestbiziodisconnect")
        result = get_child_elem(root, "result")

        status = get_child_text(result, "status")

        if status != "ok":
            return False
        return True


def get_nodes_by_tags_in_list(nodelist, tags):
    res_list = []
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            res_list.append(node)
    return res_list


def get_first_node_by_tags_in_list(nodelist, tags):
    for node in nodelist:
        for cond in tags.keys():
            if get_child_text(node, cond) != str(tags[cond]):
                break
        else:
            return node
    return None
