# -*- coding: utf-8 -*-
""" Chord interaction module for Message Store

"""


__all__ = ["OwsCmd"]

import re
import string
import bcs
from storeutils import flatten_sequence
from httplib import *
import random
import os

class OwsCmd(object):
    """
    This class is used to send OWS requests to a specific accessor using HTTP
    and binary config-sections.
    """

    debuglevel = 2

    def __init__(self, hostport):
        """
        Initialize a OwsCmd object.

        hostport: host and port of the target node.
        """
        self.hostport = hostport
        self.conn = HTTPConnection(self.hostport)

    def SendRequest(self, uid, bodylist):
        """
        Send a custom HTTP OWS request to the node

        body is the body of the request

        If the request succeeds, True and the response body is returned
        as a tuple. If it fails, False and the reason of the failure are
        returned.

        """
        body = ""
        for s in flatten_sequence(bodylist):
            body += s

        if OwsCmd.debuglevel >= 2:
            print "OWS REQUEST:\n" + body + "\n"

        self.conn.request("POST",
                          "/ows/" + uid,
                          body,
                          {"Content-Type": "text/plain;"})
        response = self.conn.getresponse()
        if response.status != 200:
            print "Connection to %s: Error %d: %s" % (self.hostport, response.status, response.reason)
            return False, response.reason
        respbody = response.read()
        if OwsCmd.debuglevel >= 2:
            print "OWS RESPONSE:\n" + respbody + "\n"
        return True, respbody

    def Send(self, uid, chunk, md, usermd):
        """
        Send an OWS insert request formatted in binary format.

        uid is the target  chunk id
        chunk is the chunk contents
        md is the chunk metadata (internal part, raw = serialized)
        usermd is the chunk metadata (user part)

        the result of the SendRequest() call is returned.

        """
        id = 42
        s = []

        s.append(bcs.GenerateSectionStart("query"))
        s.append(bcs.GenerateAttrString("protocol", "ows"))
        s.append(bcs.GenerateAttrInt("version", 2))
        s.append(bcs.GenerateBranchStart("cmd"))
        s.append(bcs.GenerateInt("id", id))
        s.append(bcs.GenerateString("type", "insert"))
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateBranchStart("data"))
        s.append(bcs.GenerateString("chunkid", uid))
        if chunk is not None:
            s.append(bcs.GenerateRaw("chunk", chunk))
        if md is not None:
            s.append(bcs.GenerateRaw("md", md))
        if usermd is not None:
            s.append(bcs.GenerateRaw("usermd", usermd))
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateSectionEnd())

        return self.SendRequest(uid, s)

    def Delete(self, uid):
        """
        Send an OWS delete request formatted in binary format.

        uid is the target  chunk id

        the result of the SendRequest() call is returned.

        """
        id = 666
        s = []

        s.append(bcs.GenerateSectionStart("query"))
        s.append(bcs.GenerateAttrString("protocol", "ows"))
        s.append(bcs.GenerateAttrInt("version", 2))
        s.append(bcs.GenerateBranchStart("cmd"))
        s.append(bcs.GenerateInt("id", id))
        s.append(bcs.GenerateString("type", "delete"))
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateBranchStart("data"))
        s.append(bcs.GenerateString("chunkid", uid))
        s.append(bcs.GenerateBranchEnd())
        s.append(bcs.GenerateSectionEnd())

        return self.SendRequest(uid, s)

    def GetStatus(self, response):
        """
        Get the response status of an OWS request.

        The status depends on the opcode (see ov/modules/protocol/ows/README
        for details of status returned).

        """
        p = re.compile("^V0006statusT[0-9]{12}([a-z_]+)",
                       re.MULTILINE | re.IGNORECASE)
        res = p.search(response)
        if not res:
            print "%s: Status error: no status line" % self.hostport
            return None
        chord_status = res.group(1)
        return chord_status.lower()
