# -*- coding: utf-8 -*-
"""Message Store store interaction module

The main class is XCommandStore. It will send XCommands to a store (messages
operations)
"""

__all__ = ["XCommandStore", "InjectorStatsResult", "TestAPIManyResult"]

from xcommand import XCommand, get_child_elem, get_child_text




class InjectorRetrievePageResult(object):
    """InjectorRetrievePageResult is a XML wrapper for the response to the
    "msgstore test retrieve" xcommand. It is used to get the list of
    possible test files containing ids that we can get."""

    def __init__(self, resnode):
        self.__res = resnode

    def get_input_list(self):
        res = []
        for input in self.__res.getElementsByTagName("inputfile"):
            res.append(get_child_text(input, "file"))
        res.sort(reverse=True)
        return res

    def get_last_input(self):
        last_input = None
        for input in self.__res.getElementsByTagName("inputfile"):
            name = get_child_text(input, "file")
            if last_input is None or last_input < name:
                last_input = name
        return last_input




class InjectorStatsResult(object):
    """InjectorStatsResult  is a XML wrapper for the response to the
    "msgstore test statistics" xcommand. It has convenience methods that
    parse the XML and give the relevant information."""

    def __init__(self, resnode):
        self.__res = resnode

    def get_action(self):
        return get_child_text(self.__res, "action")

    def get_date(self):
        return get_child_text(self.__res, "testdate")

    def get_start(self):
        return get_child_text(self.__res, "teststart")

    def get_stop(self):
        return get_child_text(self.__res, "teststop")

    def is_running(self):
        stop = get_child_text(self.__res, "teststop")
        return stop == "-"

    def get_input(self):
        return get_child_text(self.__res, "inputfrom")

    def get_duration(self):
        return int(get_child_text(self.__res, "testduration"))


    def get_total_sessions(self):
        return int(get_child_text(self.__res, "totalsessions"))

    def get_bad_sessions(self):
        return int(get_child_text(self.__res, "badsessions"))

    def get_skip_sessions(self):
        return int(get_child_text(self.__res, "skipsessions"))

    def get_ok_sessions(self):
        return int(get_child_text(self.__res, "oksessions"))

    def get_max_sessions(self):
        return int(get_child_text(self.__res, "maxsessions"))

    def get_min_session_time(self):
        return int(get_child_text(self.__res, "minsessiontime"))

    def get_max_session_time(self):
        return int(get_child_text(self.__res, "maxsessiontime"))

    def get_avg_session_time(self):
        return int(get_child_text(self.__res, "avgsessiontime"))

    def get_min_session_data(self):
        return int(get_child_text(self.__res, "minsessionsize"))

    def get_max_session_data(self):
        return int(get_child_text(self.__res, "maxsessionsize"))

    def get_avg_session_data(self):
        return int(get_child_text(self.__res, "avgsessionsize"))

    def get_nb_replicas(self):
        return int(get_child_text(self.__res, "nrreplicas"))

    def get_total_data(self):
        return int(get_child_text(self.__res, "totalsize"))

    def get_sessions_per_sec(self):
        return int(get_child_text(self.__res, "sessionpersec"))

    def get_data_per_sec(self):
        return int(get_child_text(self.__res, "sizepersec"))

    def get_max_data_per_sec(self):
        return int(get_child_text(self.__res, "maxsizepersec"))


    def print_report(self):
        print "Start: %s" % self.get_start()
        print "Stop: %s" % self.get_stop()
        print "Input: %s" % self.get_input()
        print "Duration: %d" % self.get_duration()
        print "Good sessions: %d" % self.get_ok_sessions()
        print "Bad sessions: %d" % self.get_bad_sessions()
        print "Skipped sessions: %d" % self.get_skip_sessions()
        print "Total sessions: %d" % self.get_total_sessions()
        print "Replicas: %d" % self.get_nb_replicas()
        print "Max simultaneous sessions: %d" % self.get_max_sessions()
        print "Min session time: %d" % self.get_min_session_time()
        print "Max session time: %d" % self.get_max_session_time()
        print "Average session time: %d" % self.get_avg_session_time()
        print "Min data transferred/session: %d" % self.get_min_session_data()
        print "Max data transferred/session: %d" % self.get_max_session_data()
        print "Average data transferred/session: %d" % self.get_avg_session_data()
        print "Total data transfered: %d" % self.get_total_data()
        print "Throughput: sessions: %d" % self.get_sessions_per_sec()
        print "Throughput: bytes: %d" % self.get_data_per_sec()
        print "Throughput: bytes (max): %d" % self.get_max_data_per_sec()



class InjectorStatsAggregate(object):
    """Aggregate injector stats from multiple stores
    """

    def __init__(self, statsall):
        if not statsall:
            raise Exception("statsall must have at least one element")

        self.__statslist = statsall


    def __first_of(self, get_func):
        return get_func(self.__statslist[0])

    def __min_of(self, get_func):
        min = None
        for stats in self.__statslist:
            val = get_func(stats)
            if not min or val < min:
                min = val
        return min

    def __max_of(self, get_func):
        max = None
        for stats in self.__statslist:
            val = get_func(stats)
            if not max or val > max:
                max = val
        return max


    def __sum_of(self, get_func):
        sum = 0
        for stats in self.__statslist:
            sum += get_func(stats)
        return sum


    def __one_of(self, get_func, value):
        for stats in self.__statslist:
            if get_func(stats) == value:
                return False
        return True



    def get_action(self):
        return self.__first_of(InjectorStatsResult.get_action)

    def get_date(self):
        return self.__first_of(InjectorStatsResult.get_date)

    def get_start(self):
        return self.__first_of(InjectorStatsResult.get_start)

    def get_stop(self):
        return self.__first_of(InjectorStatsResult.get_stop)

    def is_running(self):
        return self.__one_of(InjectorStatsResult.is_running, True)

    def get_input(self):
        return self.__first_of(InjectorStatsResult.get_input)


    def get_total_sessions(self):
        return self.__sum_of(InjectorStatsResult.get_total_sessions)

    def get_bad_sessions(self):
        return self.__sum_of(InjectorStatsResult.get_bad_sessions)

    def get_skip_sessions(self):
        return self.__sum_of(InjectorStatsResult.get_skip_sessions)

    def get_ok_sessions(self):
        return self.__sum_of(InjectorStatsResult.get_ok_sessions)

    def get_min_session_time(self):
        return self.__min_of(InjectorStatsResult.get_min_session_time)

    def get_max_session_time(self):
        return self.__max_of(InjectorStatsResult.get_max_session_time)

    #TODO
    def get_avg_session_time(self):
         pass

    def get_min_session_data(self):
        return self.__min_of(InjectorStatsResult.get_min_session_data)

    def get_max_session_data(self):
        return self.__max_of(InjectorStatsResult.get_max_session_data)

    def get_avg_session_data(self):
        total = self.get_total_sessions()
        if total == 0:
            return 0
        return self.get_total_data() / total

    def get_nb_replicas(self):
        return self.__first_of(InjectorStatsResult.get_nb_replicas)

    def get_total_data(self):
        return self.__sum_of(InjectorStatsResult.get_total_data)


    def get_sessions_per_sec(self):
        return self.__sum_of(InjectorStatsResult.get_sessions_per_sec)

    def get_data_per_sec(self):
        return self.__sum_of(InjectorStatsResult.get_data_per_sec)


    def print_report(self):
        print "Start: %s" % self.get_start()
        print "Stop: %s" % self.get_stop()
        print "Input: %s" % self.get_input()
        print "Good sessions: %d" % self.get_ok_sessions()
        print "Bad sessions: %d" % self.get_bad_sessions()
        print "Skipped sessions: %d" % self.get_skip_sessions()
        print "Total sessions: %d" % self.get_total_sessions()
        print "Replicas: %d" % self.get_nb_replicas()
        print "Min session time: %d" % self.get_min_session_time()
        print "Max session time: %d" % self.get_max_session_time()
        print "Average session time: %d" % 0#self.get_avg_session_time()
        print "Min data transferred/session: %d" % self.get_min_session_data()
        print "Max data transferred/session: %d" % self.get_max_session_data()
        print "Average data transferred/session: %d" % self.get_avg_session_data()
        print "Total data transfered: %d" % self.get_total_data()
        print "Throughput: sessions: %d" % self.get_sessions_per_sec()
        print "Throughput: bytes: %d" % self.get_data_per_sec()




class TestAPIManyResult(object):
    """TestAPIResult is a XML wrapper for the responses to all
    "test chordapi" test xcommands. It has convenience methods that parse
    the XML and give the relevant information.
    """
    def __init__(self, resnode):
        self.__res = resnode


    def get_nb_failures(self):
        nbfailures = get_child_text(self.__res, "nbfailures")
        return int(nbfailures)


    def get_id_set(self):
        id_set = set()
        for result in self.__res.getElementsByTagName("result"):
            key = get_child_text(result, "id")
            id_set.add(key)
        return id_set


    def get_id_map(self):
        id_map = {}
        for result in self.__res.getElementsByTagName("result"):
            key = get_child_text(result, "id")
            md5 = get_child_text(result, "md5")
            id_map[key] = md5
        return id_map




class XCommandStore(XCommand):
    """This class is a high-level handler for store XCommands.

    It contains methods to inject, retrieve and delete chunks.
    These methods return wrapper objects that encapsulate the XML result
    sent back by the store, that provide convenience methods to parse
    the results.
    """

    def __init__(self, url, login=None, passwd=None, verbose=False):
        """Initialize the store handler.

        url is the URL of the XCommand interface on the store
        """
        XCommand.__init__(self, url, login, passwd, verbose)



    def injector_start_store(self, num_sessions, num_replicas, input_data):
        self.request({"cmd": "msgstore test store",
                      "action": "store",
                      "maxsessions": num_sessions,
                      "nrreplicas": num_replicas,
                      "datapath": input_data})


    def injector_get_retrieve_page(self):
        res = self.request({"cmd": "msgstore test retrieve"})
        resobj = get_child_elem(res, "msgstoretestretrieve")
        return InjectorRetrievePageResult(resobj)


    def injector_start_retrieve(self, num_sessions, input_ids):
        self.request({"cmd": "msgstore test retrieve",
                      "action": "retrieve",
                      "maxsessions": num_sessions,
                      "inputfile": input_ids})


    def injector_get_current_stats(self):
        res = self.request({"cmd": "msgstore test statistics"})
        resobj = get_child_elem(res, "msgstoreteststatistics")
        return InjectorStatsResult(resobj)




    def test_chordapi_put(self, num_replicas=2):
        res = self.request({"cmd": "test chordapi",
                            "action": "put",
                            "replicas": num_replicas})
        resobj = get_child_elem(res, "testchordapi")
        status = get_child_text(resobj, "status")
        if status != "OK":
            return (None, None)
        chordid = get_child_text(resobj, "id")
        md5 = get_child_text(resobj, "md5")
        return (chordid, md5)


    def test_chordapi_get(self, chordid):
        res = self.request({"cmd": "test chordapi",
                            "action": "get",
                            "id": chordid})
        resobj = get_child_elem(res, "testchordapi")
        status = get_child_text(resobj, "status")
        if status != "OK":
            return None
        md5 = get_child_text(resobj, "md5")
        return md5


    def test_chordapi_delete(self, chordid):
        res = self.request({"cmd": "test chordapi",
                            "action": "delete",
                            "id": chordid})
        resobj = get_child_elem(res, "testchordapi")
        status = get_child_text(resobj, "status")
        return status == "OK"


    def test_chordapi_stat(self, chordid):
        res = self.request({"cmd": "test chordapi",
                            "action": "stat",
                            "id": chordid})
        resobj = get_child_elem(res, "testchordapi")
        status = get_child_text(resobj, "status")
        if status != "OK":
            return (None, None, None, None)
        size = int(get_child_text(resobj, "size"))
        atime = int(get_child_text(resobj, "atime"))
        mtime = int(get_child_text(resobj, "mtime"))
        ctime = int(get_child_text(resobj, "ctime"))
        return (size, atime, mtime, ctime)



    def test_chordapi_put_many(self, count=1, num_replicas=2, simul=10):
        res = self.request({"cmd": "test chordapi",
                            "action": "putmany",
                            "replicas": num_replicas,
                            "count": count,
                            "simul": simul})
        resobj = get_child_elem(res, "testchordapi")
        return TestAPIManyResult(resobj)


    def test_chordapi_get_many(self, id_list, simul=10):
        res = self.request({"cmd": "test chordapi",
                            "action": "getmany",
                            "ids": ",".join(id_list),
                            "simul": simul})
        resobj = get_child_elem(res, "testchordapi")
        return TestAPIManyResult(resobj)


    def test_chordapi_delete_many(self, id_list, simul=10):
        res = self.request({"cmd": "test chordapi",
                            "action": "deletemany",
                            "ids": ",".join(id_list),
                            "simul": simul})
        resobj = get_child_elem(res, "testchordapi")
        return TestAPIManyResult(resobj)
