# -*- coding: utf-8 -*-
""" Probability utilities

"""


import random
import math
import bisect

def parse_quantity(qt):
    if qt is None:
        return None
    if isinstance(qt, float):
        return qt
    if isinstance(qt, int):
        return float(qt)
    if isinstance(qt, str):
        mul = 1
        l = len(qt)
        if l == 0:
            return None

        unit = qt[-1]
        if not unit.isdigit():
            if unit == "b" or unit == "B":
                l = l - 1
                qt = qt[0:l]
                unit = qt[-1]

        if not unit.isdigit():
            if unit == "k" or unit == "K":
                mul = 1000
            elif unit == "m" or unit == "M":
                mul = 1000000
            elif unit == "g" or unit == "G":
                mul = 1000000000
            l = l - 1
            qt = qt[0:l]

        if not qt.isdigit():
            return None
        return int(qt) * mul

    return float(qt)



class UniqueDistribution(object):

    def __init__(self, val):
        self.val = parse_quantity(val)

    def getrand(self):
        return self.val



class UniformDistribution(object):

    def __init__(self, min, max):
        self.min = parse_quantity(min)
        self.max = parse_quantity(max)

    def getrand(self):
        return random.uniform(self.min, self.max)


class GaussianDistribution(object):

    def __init__(self, mu, sigma):
        self.mu = parse_quantity(mu)
        self.sigma = parse_quantity(sigma)

    def getrand(self):
        return random.gauss(self.mu, self.sigma)


class HistogramDistribution(object):

    def __init__(self, csv_input, title_min_size, title_max_size, title_count):
        self.parse_csv_input(csv_input, title_min_size, title_max_size, title_count)

    def parse_csv_input(self, csv_input, title_min_size, title_max_size, title_count):
        i = open(csv_input, "r")
        if not i:
            raise Exception("Can't load histogram CSV file %s" % csv_input)

        titles = i.readline().split(",")
        min_size_index = None
        max_size_index = None
        count_index = None

        for (ti, t) in enumerate(titles):
            t = t.strip()
            if t == title_min_size:
                min_size_index = ti
            elif t == title_max_size:
                max_size_index = ti
            elif t == title_count:
                count_index = ti

        if min_size_index is None:
            raise Exception("Can't find column name %s" % title_min_size)
        if max_size_index is None:
            raise Exception("Can't find column name %s" % title_max_size)
        if count_index is None:
            raise Exception("Can't find column name %s" % title_count)

        self.histo = []
        for (li, l) in enumerate(i.readlines()):
            min_size = None
            max_size = None
            count = None
            for (fi, f) in enumerate(l.split(",")):
                if fi == min_size_index:
                    min_size = int(f)
                elif fi == max_size_index:
                    max_size = int(f)
                elif fi == count_index:
                    count = int(f)
            if (min_size is None or
                max_size is None or
                count is None):
                raise Exception("Missing field elements at line %s:%d" % (csv_input, li))
            self.histo.append([min_size, max_size, count])

        self.histo_sums = []
        sumcount = 0
        for b in self.histo:
            sumcount += b[2]
            self.histo_sums.append(sumcount)
        self.sumcount_total = sumcount


    def getrand(self):
        r = random.uniform(0.0, self.sumcount_total)
        idx = bisect.bisect(self.histo_sums, r)
        b = self.histo[idx]

        r2 = random.uniform(b[0], b[1])
        return r2


class CombinedDistribution(object):

    def __init__(self, distribs=None):
        self.dists = []
        self.probsum = 0.0
        if distribs is not None:
            self.add_distributions(distribs)


    def add_distribution(self, dist, weight):
        self.probsum += weight
        self.dists.append((dist, weight, self.probsum))

    def add_distributions(self, distribs):
        for d in distribs:
            self.add_distribution(d[0], d[1])


    def getrand(self):
        if len(self.dists) == 0:
            return -1
        cdf = random.uniform(0.0, self.probsum)
        elected = None
        remain = None
        for d in self.dists:
            if d[2] >= cdf:
                elected = d[0]
                remain = (cdf - (d[2] - d[1])) / d[1]
                break

        return elected.getrand()


class BoundedDistribution(object):

    def __init__(self, distrib,
                 leftbound=None, rightbound=None):
        self.distrib = distrib
        self.leftbound = parse_quantity(leftbound)
        self.rightbound = parse_quantity(rightbound)

    def getrand(self):
        value = self.distrib.getrand()
        if self.leftbound is not None:
            if value < self.leftbound:
                value = self.leftbound
        if self.rightbound is not None:
            if value > self.rightbound:
                value = self.rightbound
        return value


class FuzzyBoundedDistribution(object):

    def __init__(self, distrib,
                 leftfuzzybound=None, leftbound=None,
                 rightfuzzybound=None, rightbound=None):
        self.distrib = distrib
        self.lfb = parse_quantity(leftfuzzybound)
        self.lb = parse_quantity(leftbound)
        self.rfb = parse_quantity(rightfuzzybound)
        self.rb = parse_quantity(rightbound)

    def __get_bounded_value(self, value, fuzzbound, bound):
        if fuzzbound == bound:
            return bound
        sigma = math.fabs(fuzzbound - bound)
        return sigma * math.tanh((value - fuzzbound) / sigma) + fuzzbound

    def getrand(self):
        value = self.distrib.getrand()
        if self.lfb is not None and self.lb is not None:
            if value < self.lfb:
                value = self.__get_bounded_value(value, self.lfb, self.lb)
        if self.rfb is not None and self.rb is not None:
            if value > self.rfb:
                value = self.__get_bounded_value(value, self.rfb, self.rb)
        return value
