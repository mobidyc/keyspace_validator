# -*- coding: utf-8 -*-
"""Utility functions for DSO handling

"""

from storesup import XCommandSup
from storeutils import wait_for
import time
import sys


# poll interval to check dso stabilization, in seconds
DEFAULT_POLL_INTERVAL = 5.0
DEFAULT_TIMEOUT = 120.0

CLUSTER_POLL_INTERVAL = 15.0
CLUSTER_TIMEOUT = 120.0

STABILIZE_POLL_INTERVAL = 3.0
STABILIZE_TIMEOUT = 180.0



def append_port(host, port):
    if host.find(":") == -1:
        host = host + ":%d" % port
    return host


def get_cluster_node_name(host, port):
    if host.find("+") != -1:
        host = host[:host.find("+")]
    return append_port(host, port)

def get_chord_node_name(xcommandsup, host, confport):
    (hostport, vnodeid) = get_vnode(host, confport)
    node_view = xcommandsup.node_view(hostport, vnodeid)
    node = node_view.get_node()
    chordip = node.get_chord_ip()
    chordport = node.get_chord_port()

    return append_port(chordip, chordport)


def get_all_vnodes(nodes, confport):
    """nodes have the format: ip:port+nvnodes. Ex: 127.0.0.1:8084+3.
    port and nvnodes are optional.

    nodes: a list of physical nodes with the indicated format
    confport: default cluster configuration port of the physical nodes
    """
    res = []
    for host in nodes:
        vnode_idx = host.find("+")
        if -1 != vnode_idx:
            if len(host) == vnode_idx - 1:
                raise Exception("Invalid node: %s" % host)
            vnode_end = host.find(":", vnode_idx+1)
            if -1 != vnode_end:
                nvnodes = int(host[vnode_idx+1:vnode_end], 10)
                host = host[:vnode_idx] + host[vnode_end:]
            else:
                nvnodes = int(host[vnode_idx+1:], 10)
                host = host[:vnode_idx]
        else:
            nvnodes = 1

        if -1 == host.find(":"):
            host = host + ":%d" % confport

        for v in range(0, nvnodes):
            res.append((host, v))

    return res


def get_vnode(host, confport):
    """Get a specific vnode tuple. host has the format: ip:port=vnodeid.
    Ex: 127.0.0.1:8084=3, for 3rd vnode of 127.0.0.1 with conf port 8084.
    port and vnodeid are optional.
    """

    vnode_idx = host.find("=")
    if -1 != vnode_idx:
        if len(host) == vnode_idx - 1:
            raise Exception("Invalid node: %s" % host)
        vnode_end = host.find(":", vnode_idx+1)
        if -1 != vnode_end:
            vnodeid = int(host[vnode_idx+1:vnode_end], 10)
            host = host[:vnode_idx] + host[vnode_end:]
        else:
            vnodeid = int(host[vnode_idx+1:], 10)
            host = host[:vnode_idx]
    else:
        vnodeid = 0

    if -1 == host.find(":"):
        host = host + ":%d" % confport

    return (host, vnodeid)


def delete_cluster_group(xcommandsup, groupname, nodelist):
    try:
        unassign_all_nodes(xcommandsup, nodelist)
    except Exception:
        pass
    clusterview = xcommandsup.cluster_view()
    if clusterview.group_exists("/" + groupname):
        xcommandsup.cluster_delete_group("/", groupname)



def __node_aggregated_status(nodes):
    status = "Online"
    for node in nodes:
        nodestatus = node.get_status()
        if nodestatus == "Offline":
            return "Offline"
        if nodestatus == "Unknown" or \
                nodestatus == "Incomplete":
            status = "Unknown"
    return status


def __nodes_online_checkfunc(*args):
    xcommandsup = args[0]
    groupname = args[1]
    cluster_view = xcommandsup.cluster_view()
    nodes = cluster_view.get_nodes_in_group("/" + groupname)
    status = __node_aggregated_status(nodes)
    return (status != "Unknown", status)


def wait_for_nodes_online(xcommandsup, groupname,
                          poll_interval = CLUSTER_POLL_INTERVAL,
                          timeout = CLUSTER_TIMEOUT):
    last_status = wait_for("cluster status",
                           poll_interval, timeout,
                           __nodes_online_checkfunc, xcommandsup, groupname)
    if last_status == "Offline":
        raise Exception("Some nodes with Offline status, aborting")



def cluster_group_ok(xcommandsup, groupname, nodelist):
    view = xcommandsup.cluster_view()
    if not view.group_exists("/" + groupname):
        return False
    nodes = view.get_nodes_in_group("/" + groupname)
    names = [n.get_ip() + ":" + "%d" % n.get_port() for n in nodes]
    names.sort()
    sortedlist = sorted(nodelist)
    return names == sortedlist


def make_cluster_group(xcommandsup, groupname, nodelist,
                       poll_interval = CLUSTER_POLL_INTERVAL,
                       timeout = CLUSTER_TIMEOUT):
    delete_cluster_group(xcommandsup, groupname, nodelist)

    xcommandsup.cluster_add_group("/", groupname)
    for nodeid in nodelist:
        splitipport = nodeid.split(":")
        xcommandsup.cluster_add_node("/" + groupname,
                                     splitipport[0], int(splitipport[1]))

    wait_for_nodes_online(xcommandsup, groupname,
                          poll_interval, timeout)


def __stores_detection_checkfunc(*args):
    xcommandsup = args[0]
    groupname = args[1]
    storelist = args[2]

    cluster = xcommandsup.cluster_view()
    for storename in storelist:
        store = cluster.get_node_by_ipport("/" + groupname, storename)
        type = store.get_type()
        if not type:
            return (False, None)
        if type != "bizstore":
            raise Exception("Expected cluster node type: bizstore, found type: %s" % type)
    return (True, None)


def wait_for_stores_detection(xcommandsup, groupname, storelist,
                              poll_interval = CLUSTER_POLL_INTERVAL,
                              timeout = CLUSTER_TIMEOUT):
    wait_for("stores detection", poll_interval, timeout,
             __stores_detection_checkfunc, xcommandsup, groupname, storelist);


def dso_exists(xcommandsup, dsoname):
    """Returns if the dso "dsoname" exists on the supervisor.

    xcommandsup is the supervisor xcommand interface.
    returns True if the dso "dsoname" exists, False otherwise.
    """
    dso_list = xcommandsup.dso_list().get_dso_list()
    try:
        dso = dso_list.index(dsoname)
    except ValueError:
        return False
    return True


def unassign_all_nodes(xcommandsup, nodelist):
    """Stop all running nodes and put them in unassigned state

    xcommandsup is the supervisor xcommand interface
    nodelist is a list of node tuples (ip:port, vnodeid), unreachable nodes
    will be removed from the list
    """
    ok = True
    outlist = list(nodelist)
    for node in nodelist:
        node_view = xcommandsup.node_view(node[0], node[1])
        nodehandle = node_view.get_node()
        if not nodehandle.is_reachable():
            sys.stderr.write("Warning: node %s is unreachable\n" % node)
            outlist.remove(node)
            ok = False
            continue
        if nodehandle.get_key() != "N/A":
            xcommandsup.node_leave(node[0], node[1])

    wait_for_nodes_state(xcommandsup, nodelist, ["NEW", "OUT_OF_SERVICE"])

    for node in outlist:
        xcommandsup.node_deassociate(node[0], node[1])

    if not ok:
        del nodelist[:]
        nodelist += outlist
    return ok



def create_vnodes(xcommandsup, nodelist):
    """Create missing vnodes present in nodelist.

    xcommandsup is the supervisor xcommand interface
    nodelist is a list of node tuples (ip:port, vnodeid)
    """

    dso_view = xcommandsup.dso_view()

    vnode_max = {}
    for node in nodelist:
        if not vnode_max.has_key(node[0]):
            vnode_max[node[0]] = node[1]
        elif vnode_max[node[0]] < node[1]:
            vnode_max[node[0]] = node[1]

    for physnode in vnode_max.keys():
        vnodes_assigned = dso_view.get_node_vnodes(physnode, False)
        vnodes_unassigned = dso_view.get_node_vnodes(physnode, True)
        if not vnodes_assigned and not vnodes_unassigned:
            raise Exception("Node %s doesn't exist" % physnode)
        if not vnodes_assigned:
            vnodes_assigned = []
        elif not vnodes_unassigned:
            vnodes_unassigned = []
        vnodes = vnodes_assigned + vnodes_unassigned

        cur_vnode_max = 0
        vport_max = 0
        chordip = vnodes[0].get_chord_ip()
        for vnode in vnodes:
            if int(vnode.get_vnodeid()) > cur_vnode_max:
                cur_vnode_max = int(vnode.get_vnodeid())
            if vnode.get_chord_port() > vport_max:
                vport_max = vnode.get_chord_port()

        chordport = vport_max+1
        for vnode_id in range(cur_vnode_max+1, vnode_max[physnode]+1):
            xcommandsup.node_add_vnode(physnode, chordip, chordport)
            chordport += 1


def __stabilization_checkfunc(*args):
    xcommandsup = args[0]
    dso_view = xcommandsup.dso_view()
    dso_states = dso_view.get_dso_states()
    return (not dso_states.has_key("NOT STABILIZED") \
                or dso_states["NOT STABILIZED"] != "yes"), None


def wait_for_stabilization(xcommandsup,
                           poll_interval = STABILIZE_POLL_INTERVAL,
                           timeout = STABILIZE_TIMEOUT):
    wait_for("stabilization",
             poll_interval, timeout,
             __stabilization_checkfunc, xcommandsup)



def dso_ok(xcommandsup, dsoname, nodelist):
    if not dso_exists(xcommandsup, dsoname):
        return False
    xcommandsup.use_dso(dsoname)

    dso_view = xcommandsup.dso_view()
    for node in nodelist:
        nodehandle = dso_view.get_node_by_addr(node[0], node[1], False)
        if not nodehandle:
            nodehandle = dso_view.get_node_by_addr(node[0], node[1], True)
            if not nodehandle:
                return False
            if not nodehandle.is_reachable():
                sys.stderr.write("Warning: node %s is unreachable\n" % node)
                return False
            return False
        else:
            if nodehandle.get_state() != "RUN":
                return False

    dso_states = dso_view.get_dso_states()
    if dso_states.has_key("NOT STABILIZED") \
            and dso_states["NOT STABILIZED"] == "yes":
        wait_for_stabilization(xcommandsup)
    return True


def make_dso(xcommandsup, dsoname, nodelist):
    """Make a new DSO with the node hosts in nodelist. The bizstorenodes must
    be correctly configured and reachable. The function takes care of creating
    the DSO, stopping the nodes, creating missing vnodes and reassociate them
    if necessary. It will not run the nodes, call run_dso() for that purpose.

    xcommandsup is the supervisor xcommand interface
    dsoname is the name of the new DSO
    nodelist is a list of node hosts that are part of the DSO (ip:port)

    raises an exception if the DSO can't be created (no reachable node or
    timeout waiting for stabilization), if some nodes are unreachable,
    a warning will be printed but the DSO is still created.
    """
    if not dso_exists(xcommandsup, dsoname):
        xcommandsup.dso_add(dsoname)
    xcommandsup.use_dso(dsoname)

    unassign_all_nodes(xcommandsup, nodelist)
    if not nodelist:
        return

    create_vnodes(xcommandsup, nodelist)

    for node in nodelist:
        xcommandsup.node_associate(node[0], node[1])



def run_dso(xcommandsup, dsoname, nodelist,
            poll_interval = STABILIZE_POLL_INTERVAL,
            timeout = STABILIZE_TIMEOUT):
    """This function runs the nodes in nodelist and waits for stabilization
    to complete (as seen by the supervisor) before returning to the caller.

    xcommandsup is the supervisor xcommand interface
    dsoname is the name of the new DSO
    nodelist is a list of node tuples that are part of the DSO
    (ip:port, vnodeid)
    poll_interval is the interval between two checks for the end of
    stabilization
    timeout is the maximum time in seconds waiting for stabilization
    to complete before raising an exception
    """
    xcommandsup.use_dso(dsoname)
    wait_for_dso_nodes_state(xcommandsup, ["NEW", "OUT_OF_SERVICE"])
    for node in nodelist:
        xcommandsup.node_join(node[0], node[1])
    wait_for_stabilization(xcommandsup, poll_interval, timeout)



def __nodes_state_checkfunc(*args):
    xcommandsup = args[0]
    nodelist = args[1]
    statelist = args[2]

    for node in nodelist:
        node_view = xcommandsup.node_view(node[0], node[1])
        nodehandle = node_view.get_node()
        for state in statelist:
            if nodehandle.get_state() == state:
                break
        else:
            return (False, None)
    return (True, None)


def wait_for_nodes_state(xcommandsup,
                         nodelist,
                         statelist,
                         poll_interval = DEFAULT_POLL_INTERVAL,
                         timeout = DEFAULT_TIMEOUT):
    """Wait for the state of all nodes to be one of the states in statelist.
    """
    wait_for("nodes state %s" % " or ".join(statelist),
             poll_interval, timeout,
             __nodes_state_checkfunc, xcommandsup, nodelist, statelist)



def __dso_nodes_state_checkfunc(*args):
    xcommandsup = args[0]
    statelist = args[1]
    nodes = xcommandsup.dso_view().get_dso_nodes()
    for node in nodes:
        for state in statelist:
            if node.get_state() == state:
                break
        else:
            return (False, None)
    return (True, None)


def wait_for_dso_nodes_state(xcommandsup,
                             statelist,
                             poll_interval = DEFAULT_POLL_INTERVAL,
                             timeout = DEFAULT_TIMEOUT):
    """Wait for the state of all nodes to be one of the states in statelist.
    """
    wait_for("nodes state %s" % " or ".join(statelist),
             poll_interval, timeout,
             __dso_nodes_state_checkfunc, xcommandsup, statelist)



def __node_state_checkfunc(*args):
    xcommandsup = args[0]
    node = args[1]
    statelist = args[2]
    node = xcommandsup.node_view(node[0], node[1])
    if not node:
        raise Exception("Can't access node %s=%d" % (node[0], node[1]))
    for state in statelist:
        if node.get_node().get_state() == state:
            break
    else:
        return (False, None)
    return (True, None)


def wait_for_node_state(xcommandsup,
                        node,
                        statelist,
                        poll_interval = DEFAULT_POLL_INTERVAL,
                        timeout = DEFAULT_TIMEOUT):
    wait_for("node %s=%d state %s" % (node[0], node[1], " or ".join(statelist)),
             poll_interval, timeout,
             __node_state_checkfunc, xcommandsup, node, statelist)
