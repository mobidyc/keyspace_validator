# -*- coding: utf-8 -*-
""" RAIN interaction module

"""


__all__ = ["RainCmd"]

import re
import string
from storeutils import flatten_sequence, encode_multipart_formdata, GetTransacId
import bcs
from httplib import *
import random
import os
import sys

class RainCmd(object):
    """
    This class is used to send RAIN opcodes to a sraind daemon using HTTP
    and binary config-sections.
    """

    DEFAULT_PUT_BLOCK_SIZE = 1048576

    debuglevel = 0

    def __init__(self, hostport):
        """
        Initialize a RainCmd object.

        hostport: host and port of the target node.
        """
        self.hostport = hostport
        if ':' not in hostport:
            raise ValueError("Need host:port combination, got %r" % hostport)
        self.conn = HTTPConnection(self.hostport)


    def SendGetRequest(self, body_parts, outfile, key, expected_size, data_index, ndata, ncoding):
        """
        Send a custom HTTP RAIN request to the node

        body_parts is the body of the request

        If the request succeeds, True and the response body is returned
        as a tuple. If it fails, False and the reason of the failure are
        returned.

        """

        """ Only one concatenation """

        req = "/sraind.fcgi"

        if key is not None:
            req += "?id=" + key
            req += "&data_index=" + `data_index`
            req += "&ndata=" + `ndata`
            req += "&ncoding=" + `ncoding`


        body = ""
        for s in flatten_sequence(body_parts):
            body += s

        if RainCmd.debuglevel >= 1:
            print "RAIN REQUEST:\n" + body + "\n"

        self.conn.request("GET",
                          req,
                          body,
                          {"Content-Type": "application/octet-stream"})
        response = self.conn.getresponse()
        if response.status != 200:
            print "Connection to %s: Error %d: %s" % (self.hostport, response.status, response.reason)
            return False, response.reason

        bytesread = 0
        expected_size = 0
        contents_size = 0
        contents = ""
        status = ""

        respbody = response.read()
        while respbody is not None or (bytesread < expected_size):

            status = self.GetStatus(respbody)
            if status is None:
                return (0, "Missing return status in rain answer")

            contents, contents_size = self.GetContents(respbody)
            if contents is None:
                return (0, "Missing return contents in rain answer")

            bytesread += contents_size
            outfile.write(contents)

            if status == "success":
                response.close()
                return (1, "ok")
            elif status != "continue":
                response.close()
                return (0, "unknown status")

            respbody = response.read()

        if RainCmd.debuglevel >= 1:
            print "RAIN RESPONSE:\n" + outfile + "\n"

        response.close()
        return 1, "EOF!"


    def SendPutRequest(self, body_parts):
        """
        Send a custom HTTP RAIN request to the node

        body_parts is the body of the request

        If the request succeeds, True and the response body is returned
        as a tuple. If it fails, False and the reason of the failure are
        returned.

        """

        """ Only one concatenation """


        body = ""
        for s in flatten_sequence(body_parts):
            body += s

        if RainCmd.debuglevel >= 1:
            print "RAIN REQUEST:\n" + body + "\n"

        self.conn.request("PUT",
                          "/sraind.fcgi",
                          body,
                          {"Content-Type": "application/octet-stream"})
        response = self.conn.getresponse()
        if response.status != 200:
            print "Connection to %s: Error %d: %s" % (self.hostport, response.status, response.reason)
            return False, response.reason

        respbody = response.read()

        if RainCmd.debuglevel >= 1:
            print "RAIN RESPONSE:\n" + respbody + "\n"

        return True, respbody



    def GetStatus(self, response):
        """
        Get the response status of a RAIN command.

        """
        p = re.compile("^V0006statusT[0-9]{12}([a-z_]+)",
                       re.MULTILINE | re.IGNORECASE)
        res = p.search(response)
        if not res:
            print "%s: Status error: no status line" % self.hostport
            return None
        rain_status = res.group(1)
        return rain_status.lower()

    def GetKey(self, response):
        """
        Get the response key of a PUT command.

        """
        p = re.compile("^V0003keyT000000000040([a-f0-9]{40})",
                       re.MULTILINE | re.IGNORECASE)
        res = p.search(response)
        if not res:
            print "%s: Status error: no key line in buffer" % self.hostport
            return None
        rain_key = res.group(1)
        return rain_key.upper()

    def GetContents(self, response):
        """
        Get the contents of a GET response.

        return the tuple (contents, contents_size)
        """

        # 12, because it's the next type's length
        # eg. R0008contentsR000000000003FOO
        contents_tag_length = 12
        pattern = "R0008contentsR"
        pattern_len = len(pattern);

        offset = response.find(pattern);

        if -1 == offset:
            print "%s: Status error: no contents field in buffer" % response
            return None, 0

        start = offset + pattern_len

        size = int(response[start:start+contents_tag_length])
        start += contents_tag_length
        raw = response[start:start+size]

        if RainCmd.debuglevel >= 1:
            print "raw (len=%d): %s" % (len(raw), raw)

        return raw, len(raw)


    def put_blocks(self, key, data_blocks, n_coding_objects, n_schema, seqnum, objects_size):
        """
        Start/continue a RAIN put of data blocks

        data_blocks contains a list of next data blocks to send
        n_coding_objects is the number of coding objects desired
        seqnum is the sequence number
        objects_size is a list of integers telling the total size of each object
        n_schema the number of partitions in the rings

        if success and put is not finished, (1, None) is returned, else if success and put is finished (2, None) is returned, else (0, msg) is returned with an optional error message.

        """
        s = []

        s.append(bcs.GenerateSectionStart("query"))
        s.append(bcs.GenerateAttrString("protocol", "rain"))
        s.append(bcs.GenerateAttrInt("version", 1))

        s.append(bcs.GenerateBranchStart("control"))
        s.append(bcs.GenerateInt("ncoding", n_coding_objects))
        s.append(bcs.GenerateInt("ndata", len(objects_size)))
        s.append(bcs.GenerateInt("schema", n_schema))
        s.append(bcs.GenerateInt("seqnum", seqnum))

        if key is not None:
            s.append(bcs.GenerateString("key", key))

        s.append(bcs.GenerateBranchEnd())

        index = 0
        for block in data_blocks:
            if block is not None:
                s.append(bcs.GenerateBranchStart("datablock"))
                s.append(bcs.GenerateInt("index", index))
                if objects_size is not None:
                    s.append(bcs.GenerateInt("objectsize",
                                             objects_size[index]))
                s.append(bcs.GenerateRawList("contents", block))
                s.append(bcs.GenerateBranchEnd())
            index += 1

        s.append(bcs.GenerateSectionEnd())

        (res, response) = self.SendPutRequest(s)

        if not res:
            return (False, None, "SendPutRequest failed")

        status = self.GetStatus(response)

        if status is None:
            return (0, None, "Missing return status in rain answer")
        if status == "continue":
            key = self.GetKey(response)
            return (1, key, None)
        elif status == "success":
            key = self.GetKey(response)
            return (2, key, None)
        return (0, None, status)


    def get(self, key, outfile, expected_size, data_index=0, ndata=1, ncoding=0):
        """
        Do a RAIN get on a specific file

        key the key of the needed object

        """

        if key == None:
            return (0, "You need to provide a key")

        empty = []

        (ret, response) = self.SendGetRequest(empty, outfile, key, expected_size, data_index, ndata, ncoding)

        if RainCmd.debuglevel >= 1:
            print "GET result: %s" % response

        return (ret, response)



    def put(self, key, data_sources, data_sources_size, n_coding_objects, n_schema = None,
            blocksize = DEFAULT_PUT_BLOCK_SIZE):
        """
        Do a RAIN put of data objects

        data_sources contains a list of binary data stream sources from which to retrieve data to put with RAIN
        data_sources_size contains a list of sizes to read from each data source, respectively
        n_coding_objects is the number of coding objects desired
        blocksize is the chunk size in which each data object will be split

        if success, (True, None) is returned, else (False, msg) is returned with an optional error message

        """

        if n_schema == None:
            n_schema = blocksize + n_coding_objects

        remain_to_read = list(data_sources_size)

        maxlen = 0
        for i in xrange(len(data_sources_size)):
            if data_sources_size[i] > maxlen:
                maxlen = data_sources_size[i]

        bytepos = 0
        seqnum = 0

        while bytepos < maxlen:
            data_blocks = []
            for i in xrange(len(data_sources)):
                if remain_to_read[i] > 0:
                    readsize = min(blocksize, remain_to_read[i])
                    datablock = data_sources[i].read(readsize)
                    remain_to_read[i] -= readsize
                else:
                    datablock = None
                data_blocks.append(datablock)
            bytepos += blocksize
            seqnum += 1

            (res, returned_key, msg) = self.put_blocks(key, data_blocks, n_coding_objects, n_schema,
                                                       seqnum, data_sources_size)

            if res == 2:
                return (True, returned_key, None)
            if res == 0:
                return (False, None, msg)
            if key == None:
                key = returned_key

        return (True, key, None)
