# -*- coding: utf-8 -*-
from storenode import XCommandNode
from storeutils import genid
import getopt
import random
import time
from datetime import date

import dso
import testlink


SAMPLES = [ "JWRJUASFGOOOAGF2",
            "489G#Q%GQSLFGFJB",
            "348HVHFG412447$G",
            "2VVVIFGTHH4349DG",
            "1GFGH4HWQFSGAGP7",
            "MV,RML22TYGSDF56",
            "1FG6HQ][S]AG!DB1",
            "12R!~DFGLLA;SDGH",
            "MV,BCVBEARGAA4YA",
            "20399UIGFGSGDANB",
            "LJSXPOOAZEMLSD89",
            "3674KLKSLKSDUIQS",
            "HKJSHDUEY89IERKS"]

class Data(object):

    def __init__(self, len=64):
        self.data = self._gendata(len)

    def _gendata(self, len):
        data = ""
        for i in xrange(len / 16):
            data += random.choice(SAMPLES)
        if len % 16 != 0:
            data += random.choice(SAMPLES)[0:len%16]
        return data

    def get(self):
        return self.data

    def __len__(self):
        return len(self.data)

    def __str__(self):
        return self.data



options="h"
start = None
stepname = "step"
curstep_start = None


class OptionSet(dict):
    def __init__(self):
        dict.__init__(self)

    def __getattribute__(self, name):
        try:
            return dict.__getitem__(self, name)
        except KeyError,e:
            return None


def usage(opts):
    sys.stderr.write("Usage: ")
    first = True
    for opt in opts.items():
        optname = opt[0]
        optdefval = opt[1]
        showopt = "--" + optname
        if (isinstance(optdefval, bool) or
            optdefval == bool):
            showopt = "[" + showopt + "]"
        else:
            showopt += " " + optname
            if not isinstance(optdefval, type):
                if isinstance(optdefval, str):
                    defval = optdefval
                elif isinstance(optdefval, int):
                    defval = "%d" % optdefval
                elif isinstance(optdefval, float):
                    defval = "%g" % optdefval
                elif isinstance(optdefval, list):
                    defval = ",".join(optdefval)
                else:
                    defval = str(optdefval)
                showopt = "[" + showopt + " (" + defval + ")]"

        if not first:
            sys.stderr.write("       ")
        sys.stderr.write(showopt + "\n")
        first = False
    sys.stderr.write("       [--help|-h]\n")


def runtest(testname, startfunc, test_opts, args):

    global start
    global curstep

    test_desc = None
    try:
        options_long = ["help"]
        for opt in test_opts.items():
            optname = opt[0]
            optdefval = opt[1]
            if (not isinstance(optdefval, bool) and
                not optdefval == bool):
                optname += "="
            options_long.append(optname)

        opts,args = getopt.getopt(args,options,options_long)
    except getopt.GetoptError, err:
        print err.msg
        usage(test_opts)
        return False

    opt_table = OptionSet()

    for opt in test_opts.items():
        optname = opt[0]
        optdefval = opt[1]
        if not isinstance(optdefval, type):
            opt_table[optname] = optdefval
        elif optdefval == bool:
            opt_table[optname] = False

    for o,a in opts:
        if o in ("-h", "--help"):
            usage(test_opts)
            return True
        else:
            optname = o[2:]
            optdefval = test_opts[optname]
            if (optdefval == bool or
                isinstance(optdefval, bool)):
                opt_table[optname] = True
            elif (optdefval == list or
                  isinstance(optdefval, list)):
                opt_table[optname] = a.split(",")
            elif isinstance(optdefval, type):
                opt_table[optname] = optdefval(a)
            else:
                opt_table[optname] = optdefval.__class__(a)

    for opt in test_opts.items():
        optname = opt[0]
        optdefval = opt[1]
        if isinstance(optdefval, type):
            if not optname in opt_table:
                print "Missing option: %s" % optname
                usage(test_opts)
                return False

    start = testlink.test_start(testname)
    curstep = start

    try:
        ret = startfunc(opt_table)
        res = None
        msg = None
        if isinstance(ret, tuple):
            if len(ret) >= 2:
                (res, msg) = ret
            else:
                (res) = ret
        elif isinstance(ret, bool):
            res = ret
        if res:
            testlink.test_success(msg, start)
            return True
        else:
            testlink.test_fail(msg)
            return False
    except Exception:
        testlink.test_except()
        return False

    return True


def test_startstep(name):
    global curstep_start
    global stepname

    if name:
        stepname = name
    curstep_start = time.time()


def test_endstep(name=None):
    global curstep_start
    global stepname

    if name:
        curstep_start = testlink.test_savetime(name + "_duration",
                                               curstep_start)
    else:
        curstep_start = testlink.test_savetime(stepname + "_duration",
                                               curstep_start)
    stepname = "step"
