# -*- coding: utf-8 -*-
""" @file ring.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Ring class for sprov integration
"""

import copy
import math

import scality.common as common
from scality.key import Key
from scality.capacity import Capacity
from scality.sprov.component import Container, Site, Rack, Server, ServerWithDiskGroups, Node, Unit, DiskGroup
from scality.sprov.event import FailureSet, FailureEventSet
from scality.sprov.cos import ClassOfService, Schema
from scality.sprov.storage import DiskDef

import logging
logger = logging.getLogger("scality.sprov")


class Ring(Container):

    TYPENAME = "Ring"
    MEAN_FILE_SIZE = "1MB"
    NEXT_CHILD_TYPENAME = "Site"

    STANDARD_MANDATORY_FAILURE_SETS = [
        FailureSet(), # if class 0 is used
        FailureSet(Server=1),
        FailureSet(Rack=1), # if any rack
        FailureSet(Site=1) # if any site
    ]

    STANDARD_OPTIONAL_FAILURE_SETS = [
        FailureSet(Server=2),
        FailureSet(Rack=1, Server=1),
        FailureSet(Site=1, Server=1),
        FailureSet(Site=1, Rack=1),
        FailureSet(Site=1, DiskGroup=1)
    ]

    def __init__(self):
        super(Ring, self).__init__(component_type=None)

        self.schema = Schema()
        self.disk_defs = None

        self.cos_list = []
        self.events = FailureEventSet()
        self.dcprefix = None


    def setSchema(self, schema):
        if isinstance(schema, Schema):
            self.schema = schema
        else:
            self.schema = Schema(schema)


    def updateTopology(self,
                       nb_servers,
                       nb_sites=None,
                       nb_servers_per_rack=None,
                       nb_disks_per_server=None,
                       disk_def=None,
                       capacity_per_server=None,
                       nb_disk_groups_per_server=None,
                       nb_processes_per_server=None,
                       server_hostnames=None):
        """Add servers to the current topology, or create the topology if
        empty. The philosophy of the function is to use the best guess
        when an option is not provided, fetching properties from
        existing servers or using default values.

        nb_servers gives the number of servers to create

        nb_sites, nb_servers_per_rack, nb_disk_groups_per_server options give
        hierarchical information on placement of servers

        capacity_per_server provides the raw storage capacity of new
        servers, or (nb_disks_per_server, disk_def) options provide
        the disk type (DiskDef) and the count per server

        nb_processes_per_server gives the number of processes per
        server

        """

        if self.children:
            if nb_servers_per_rack is not None and not self.hasChildrenOfType(Rack):
                parent_cls = type(self.allChildrenOfType(Server).next().parent)

                for parent in self.allChildrenOfType(parent_cls):
                    nb_racks = ((parent.countComponentType(Server) + nb_servers_per_rack - 1)
                                / nb_servers_per_rack)
                    parent.attachChildrenEvenlyToNewParentComponents(Rack(), nb_racks)

            if (nb_disk_groups_per_server is not None and
                not self.hasChildrenOfType(DiskGroup)):
                raise Exception("Error: can't create disk groups because existing servers have no disk groups")

            if nb_sites is not None and not self.hasChildrenOfType(Site):
                self.attachChildrenEvenlyToNewParentComponents(Site(), 1)

        if nb_sites is not None:
            for _ in xrange(nb_sites - self.countComponentType(Site)):
                self.addChildComponent(Site())

        if nb_servers_per_rack is None and self.hasChildrenOfType(Rack):
            nb_servers_per_rack = max(len(rack.children)
                                      for rack in self.allChildrenOfType(Rack))

        if nb_servers_per_rack is not None:
            room_in_cur_racks = 0
            for rack in self.allChildrenOfType(Rack):
                if len(rack.children) < nb_servers_per_rack:
                    room_in_cur_racks += nb_servers_per_rack - len(rack.children)

            nb_new_racks = (((nb_servers - room_in_cur_racks) + nb_servers_per_rack - 1)
                            / nb_servers_per_rack)
            if nb_new_racks < 0:
                nb_new_racks = 0

            total_nb_racks = self.countComponentType(Rack) + nb_new_racks
            if nb_sites > total_nb_racks:
                raise Exception("Can't have more sites than racks (nb_sites=%d, nb_racks=%d)"
                                % (nb_sites, total_nb_racks))

            for rack in xrange(nb_new_racks):
                self.spreadChildComponentEvenly(Rack())


        total_nb_servers = self.countComponentType(Server) + nb_servers
        if nb_sites > total_nb_servers:
            raise Exception("Can't have more sites than servers (nb_sites=%d, nb_servers=%d)"
                            % (nb_sites, total_nb_servers))

        try:
            existing_server_tpl = self.allChildrenOfType(Server).next()
            server_tpl = copy.deepcopy(existing_server_tpl)
            server_tpl.clearNonTopologicalInfo()

        except StopIteration:
            server_tpl = None

        if (server_tpl is not None and
            nb_disk_groups_per_server is not None and
            (not isinstance(server_tpl, ServerWithDiskGroups) or
             len(server_tpl.children) != nb_disk_groups_per_server)):
            server_tpl = None # re-initialize template server because its topology is different

        if server_tpl is None:
            if nb_disks_per_server is None and capacity_per_server is None:
                raise Exception("Need to specify either (nb_disks_per_server, disk_capacity) or capacity_per_server")

            server_tpl = Server.create(nb_disk_groups_per_server,
                                       nb_processes_per_server)

        if nb_disks_per_server is not None and disk_def is not None:
            if self.disk_defs is None:
                self.disk_defs = [disk_def]
            else:
                matching_disk_defs = list(ddef
                                          for ddef in self.disk_defs
                                          if ddef.capacity == disk_def.capacity)
                if matching_disk_defs:
                    disk_def = matching_disk_defs[0]
                else:
                    self.disk_defs.append(disk_def)
            server_tpl.setStorage(disk_def, nb_disks_per_server)

        elif capacity_per_server is not None:
            server_tpl.setCapacity(capacity_per_server)

        if nb_processes_per_server is not None:
            server_tpl.setNbProcesses(nb_processes_per_server)


        for _ in xrange(nb_servers):
            self.spreadChildComponentEvenly(copy.deepcopy(server_tpl))


        self.spreadChildrenComponentNames(server_hostnames)


    def setClassesOfService(self, cos_list, schema=None, dcprefix=None):
        """Give a set of classes of service (ClassOfService) used to store
        data in this Ring. The schema is automatically derived if not
        provided explicitely.

        Existing events will be updated to reflect the new set of
        classes of service.

        """

        if schema is not None:
            self.setSchema(schema)

        if type(cos_list) is ClassOfService:
            cos_list = [cos_list]

        try:
            for cos in cos_list:
                self.schema.checkClassOfServiceCompatibility(cos)

        except:
            new_schema = Schema()
            for cos in cos_list:
                new_schema.addClassOfService(cos)

            if schema is not None:
                logger.warning("Warning: changing schema from %d to %d for class of service compatibility"
                               % (int(self.schema), int(new_schema)))
            self.schema = new_schema

        self.cos_list = cos_list
        if any(cos.need_dcprefix() for cos in cos_list) and \
           (dcprefix is None or dcprefix < 0 or dcprefix > 3):
            msg = "Aborting because data center prefix is invalid. Must be in [0,3]"
            logger.error(msg)
            raise ValueError(msg)
        else:
            self.dcprefix = dcprefix

        if self.events:
            # recompute events with new classes of service

            failure_sets = [event.failure_set
                            for event in self.events
                            if self.isFailureSetCompatible(event.failure_set)]

            self.events = FailureEventSet()

            for failure_set in failure_sets:
                self.addFailureEvent(failure_set)


    def isFailureSetCompatible(self, failure_set):

        # Check failure does not expect more components than necessary for COS
        for cos in self.cos_list:
            max_recover = cos.parts_count - cos.parts_needed
            if (max_recover < failure_set[Server] or
                max_recover < failure_set[DiskGroup]):
                return False

        for component_cls in [Site, Rack, DiskGroup]:
            if (failure_set[component_cls] > 0 and
                not self.hasChildrenOfType(component_cls)):
                return False
        return True


    def updateEvents(self,
                     mandatory_failure_sets=None,
                     optional_failure_sets=None,
                     force_update=False):
        """Automatically update events section with new failure sets:

         - if any of the failure sets in mandatory_failure_sets is not
           compatible with the Ring, False is returned. In this case,
           the call fails and no failure set is added (unless
           force_update=True, in which case, the subset of mandatory
           failure sets compatible with the Ring are still added)

         - if some failure sets in optional_failure_sets are not
           compatible with the Ring, they are not added and they do
           not affect the return value of updateEvents()

         - if (mandatory|optional)_failure_sets is None: create a new
           set of default events associated to the classes of service
           configured for the Ring (see Ring.setClassesOfService())

        """

        if not self.cos_list:
            raise Exception("Cannot update events without any class of service configured (use sprov.ring.setClassesOfService())")

        force_event_update = force_update

        if mandatory_failure_sets is None:
            mandatory_failure_sets = \
                [failure_set
                 for failure_set in Ring.STANDARD_MANDATORY_FAILURE_SETS
                 if self.isFailureSetCompatible(failure_set)]

            force_event_update = False

            if optional_failure_sets is None:
                optional_failure_sets = \
                    [failure_set
                     for failure_set in Ring.STANDARD_OPTIONAL_FAILURE_SETS
                     if self.isFailureSetCompatible(failure_set)]

        if type(mandatory_failure_sets) is FailureSet:
            mandatory_failure_sets = [mandatory_failure_sets]

        if type(optional_failure_sets) is FailureSet:
            optional_failure_sets = [optional_failure_sets]

        old_events = FailureEventSet(self.events)

        res = True
        for failure_set in mandatory_failure_sets:
            if not self.addFailureEvent(failure_set, self.cos_list,
                                        force_update=force_event_update):
                res = False
                if not force_update:
                    self.events = old_events
                    return False

        if optional_failure_sets is not None:
            for failure_set in optional_failure_sets:
                self.addFailureEvent(failure_set)

        return res



    def getClassesOfServiceCompatibleWithFailureSet(self, failure_set, cos_list=None):

        class CosData(object):
            def __init__(self, cos):
                self.cos = cos
                self.parts_remain = cos.parts_count

        def getAvailComponentsOfTypeSortedByCapacity(component_cls,
                                                     already_failed_comps):
            return sorted(filter(lambda comp:
                                 all(not comp.isChildOf(failed_comp)
                                     for failed_comp in already_failed_comps),
                                 self.allChildrenOfType(component_cls)),
                          key=lambda comp: comp.getTotalCapacity())

        total_capacity = self.getTotalCapacity()

        if cos_list is None:
            cos_list = self.cos_list

        cos_data_list = [CosData(cos) for cos in cos_list]

        failed_ratio = 0.0
        already_failed_comps = []

        try:
            for (component_cls, component_count) in failure_set:

                avail_comps_of_cls = \
                    getAvailComponentsOfTypeSortedByCapacity(component_cls,
                                                             already_failed_comps)

                for failure_type_idx in xrange(component_count):
                    failed_comp = avail_comps_of_cls.pop()
                    already_failed_comps.append(failed_comp)

                    cur_failed_cap = failed_comp.getTotalCapacity()
                    failed_ratio = (cur_failed_cap / total_capacity) * 0.999

                    for cos_data in cos_data_list:
                        cos = cos_data.cos
                        max_parts_lost = cos.max_parts_lost(failed_ratio)

                        cos_data.parts_remain -= max_parts_lost

            cos_list_for_failure_set = \
                [cos_data.cos for cos_data in cos_data_list
                 if cos_data.parts_remain >= cos_data.cos.parts_needed]

        except IndexError:
            # no remaining available component, thus
            # no class of service can satisfy this failure set
            cos_list_for_failure_set = []

        return cos_list_for_failure_set


    def addFailureEvent(self, failure_set, cos_list=None, force_update=False):
        """Add a new failure event to the list of events. The new event as
        well as existing events will be filtered to remove redundant
        information.

        If cos_list is None, classes of service bound to the Ring are
        used (see Ring.setClassesOfService())

        If force_update is True, the event will be added even if
        detected to not be compatible with the COS list provided (or
        bound to the Ring), False is still returned in this case.

        """

        if cos_list is not None:
            used_cos_list = cos_list
        else:
            used_cos_list = self.cos_list

        filtered_cos_list = self.getClassesOfServiceCompatibleWithFailureSet(failure_set, used_cos_list)

        res = True
        if set(filtered_cos_list) != set(used_cos_list):
            if cos_list is not None:
                logger.warning("Warning: {0} not supported because "
                               "not compatible with COS {1}"
                               .format(failure_set.desc(),
                                       [str(cos) for cos in set(cos_list)
                                            .difference(set(filtered_cos_list))]))
            if not force_update:
                return False
            else:
                res = False

        self.events.addFailureEvent(failure_set, used_cos_list)
        return res


    def toJson(self):
        json_obj = super(Ring, self).toJson()

        json_obj.update( {
            "schema": self.schema.toJson(),
            "meanfilesize": Ring.MEAN_FILE_SIZE
        } )

        if self.disk_defs is not None:
            json_obj["devices"] = [disk_def.toJson() for disk_def in self.disk_defs]

        json_obj["events"] = self.events.toJson()

        if self.dcprefix is not None:
            json_obj['dcprefix'] = self.dcprefix

        return json_obj

    def loadJson(self, json_obj, ring=None):

        if json_obj.has_key("devices"):
            devices = json_obj["devices"]
            if type(devices) is dict:
                devices = [devices]
            self.disk_defs = [DiskDef.createFromJson(device) for device in devices]

        super(Ring, self).loadJson(json_obj, self)

        self.schema = Schema.createFromJson(json_obj["schema"])

        if not json_obj.has_key("events"):
            raise Exception("Missing \"events\" section in JSON input")

        self.events = FailureEventSet.createFromJson(json_obj["events"])

        cos_set = set()
        for event in self.events:
            cos_set.update((cos for cos in event.cos_list))

        self.cos_list = sorted(list(cos_set))


    def mergeKeysFromCSV(self, csv_hdl):

        caption = [s.strip() for s in csv_hdl.readline().split(",")]
        for field_name in ["nid", "key", "unit"]:
            if field_name not in caption:
                raise Exception("Missing '%s' field in CSV input" % (field_name))

        nodes = []
        line = csv_hdl.readline()
        lnum = 1
        while line:
            fields = [s.strip() for s in line.split(",")]
            if len(fields) < len(caption):
                raise Exception("Missing fields in CSV input, line %d" % (lnum))

            (nid, key, unit_path) = (fields[caption.index(field_name)]
                                     for field_name in ["nid", "key", "unit"])
            nodes.append((nid, key, unit_path))

            line = csv_hdl.readline()
            lnum += 1

        unassigned_nodes = []
        for (nid, key, unit_path) in nodes:
            try:
                unit = self.findChildByPath(unit_path.split(":"))
                if not unit.addNodeIfNotExists(Node(nid, key)):
                    unassigned_nodes.append((nid, key, unit_path))

            except ValueError:
                unassigned_nodes.append((nid, key, unit_path))

        if not unassigned_nodes:
            return

        nodes = unassigned_nodes
        unassigned_nodes = []
        for (nid, key, unit_path) in nodes:
            try:
                unit = [unit for unit in self.allChildrenOfType(Unit)
                        if unit.name == unit_path][0]
                if not unit.addNodeIfNotExists(Node(nid, key)):
                    unassigned_nodes.append((nid, key, unit_path))

            except IndexError:
                try:
                    unit = [unit for unit in self.allChildrenOfType(Unit)
                            if unit.nodes is not None and key in [n.key for n in unit.nodes]][0]
                    print "unit", unit.name, "has key", key, "setting name to", unit_path
                    unit.setName(unit_path)

                except IndexError:
                    unassigned_nodes.append((nid, key, unit_path))

        if not unassigned_nodes:
            return

        nodes = unassigned_nodes
        assigned_nodes = []
        all_units = (unit for unit in self.allChildrenOfType(Unit)
                     if not unit.nodes)
        try:
            unit = all_units.next()
            for (nid, key, unit_path) in nodes:
                try:
                    named_unit = [u for u in self.allChildrenOfType(Unit)
                                  if u.name == unit_path][0]
                    if named_unit.addNodeIfNotExists(Node(nid, key)):
                        assigned_nodes.append((nid, key, unit_path))
                except IndexError:
                    while not unit.addNodeIfNotExists(Node(nid, key)):
                        unit = all_units.next()
                    unit.setName(unit_path)
                    assigned_nodes.append((nid, key, unit_path))

        except StopIteration:
            msg = ("%d nodes from CSV input could not be assigned to units:\n"
                   % (len(nodes) - len(assigned_nodes)))
            for (nid, key, unit_path) in filter(lambda node: node not in assigned_nodes,
                                                nodes):
                msg += ",".join([nid, key, unit_path]) + "\n"
            raise Exception(msg)


    def startKeySearch(self, **sprov_options):
        """Search optimized keys for this Ring

        The function does not modify the Ring in-place, but returns a
        search snapshot (Snapshot).

        """
        from scality.sprov.compute import searchKeys

        if not self.events:
            self.updateEvents()
        return searchKeys(self, **sprov_options)

    def updateFromSnapshot(self, snap):
        """Update the Ring keys from a search snapshot. The contents of this
        Ring are fully replaced by the updated Ring contained in the
        snapshot.

        snap is a Snapshot object.

        """
        new_ring = snap.getUpdatedRing()
        self.__dict__.update(new_ring.__dict__)

    @staticmethod
    def createFromJson(json_obj):
        ring = Ring()
        ring.loadJson(json_obj, ring)

        return ring
