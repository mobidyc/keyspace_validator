# -*- coding: utf-8 -*-
""" @file compute.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Computation-handling functions
"""

import subprocess
import os
import sys
import select
import time
import tempfile
import shutil
from multiprocessing import Process, Queue
from copy import deepcopy

import logging
logger = logging.getLogger("scality.sprov")

try:
    import simplejson as json
except ImportError:
    import json

from scality.sprov.snapshot import Snapshot

sprov_binary = "sprov"
sprov_stats_binary = "sprov_stats"

search_options = {}

_MILESTONES_ORDER = {
    "unset": 0,
    "constraints-solved": 1,
    "node-count-match": 2,
    "bare-optimization": 3,
    "sweet-optimization": 4,
    "ideal-keyspace": 5,
}

def options_dict_to_args(options_dict):
    option_args = []
    for (opt, arg) in options_dict.iteritems():
        if type(arg) is not bool or arg:
            option_args.append("--" + opt.replace("_", "-"))
        if type(arg) is not bool:
            option_args.append(str(arg))
    return option_args

def searchKeysMulti(contexts, paramname, paramvalues, **sprov_options):
    args = [sprov_binary, "--stdout"]
    args += options_dict_to_args(sprov_options)

    inputs = [] # pipe IDs, for the select
    flowids = {} # paramvalues indexed by pipe IDs, used to retrieve wanted context

    if paramname is not None:
        shparamname = "--" + paramname.replace("_", "-")
    for paramvalue in paramvalues:
        context = contexts[paramvalue]
        if paramname is not None:
            l_args = args + [shparamname, str(paramvalue)]
        else:
            l_args = args
        try:
            sp = subprocess.Popen(l_args,
                                  stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception, e:
            raise Exception("Failed to call '%s': %s" % (" ".join(l_args), str(e)))

        sp.stdin.write(json.dumps(context["obj"].toJson()))
        sp.stdin.close()

        flowids[sp.stdout] = ("out", paramvalue)
        flowids[sp.stderr] = ("err", paramvalue)
        inputs.append(sp.stdout)
        inputs.append(sp.stderr)
        context["json_snapshot"] = ""

    while inputs:
        try:
            (readable, writeable, errors) = select.select(inputs, [], inputs)
            if errors:
                raise IOError("Error on sprov output")

            for input in readable:
                dest, paramvalue = flowids[input]
                if dest == "out":
                    context = contexts[paramvalue]
                    json_snapshot_chunk = input.read()
                    if json_snapshot_chunk:
                        contexts[paramvalue]["json_snapshot"] += json_snapshot_chunk
                    else:
                        input.close()
                        inputs.remove(input)

                elif dest == "err":
                    line = input.readline()
                    if line:
                        logger.error("sprov: %s", line)
                    else:
                        input.close()
                        inputs.remove(input)

        except KeyboardInterrupt:
            pass

    for paramvalue in paramvalues:
        context = contexts[paramvalue]
        try:
            context["obj"] = Snapshot.createFromJson(json.loads(context["json_snapshot"]))
        except ValueError, e:
            logger.error("sprov output decode: %s", str(e))
        del context["json_snapshot"]

def searchKeys(input_obj, **sprov_options):
    result = searchKeysMulti(input_obj, None, ["dummy"], **sprov_options)
    return result["dummy"]["obj"]

def getMilestoneFromObj(obj):
    ms = obj.toJson().get("milestonereached", "unset")
    return ms

def checkConstraintsSolved(input_obj, max_run_sec="10", **sprov_options):
    out_snap = searchKeys(
        input_obj,
        max_run_sec=max_run_sec,
        stop_search_after_constraints_solved=True,
        **sprov_options
    )
    ms = getMilestoneFromObj(out_snap)
    return out_snap, ms == "constraints-solved"

def checkNodeCountMatch(input_obj, max_run_sec="60", **sprov_options):
    out_snap = searchKeys(
        input_obj,
        max_run_sec=max_run_sec,
        stop_search_after_node_count_match=True,
        **sprov_options
    )
    ms = getMilestoneFromObj(out_snap)
    return out_snap, ms == "node-count-match"

def checkBareOptimization(input_obj, max_run_sec="120", **sprov_options):
    out_snap = searchKeys(
        input_obj,
        max_run_sec=max_run_sec,
        stop_search_after_bare_optimization=True,
        **sprov_options
    )
    ms = getMilestoneFromObj(out_snap)
    return out_snap, ms == "bare-optimization"

def checkSweetOptimization(input_obj, max_run_sec="600", **sprov_options):
    out_snap = searchKeys(
        input_obj,
        max_run_sec=max_run_sec,
        stop_search_after_sweet_optimization=True,
        **sprov_options
    )
    ms = getMilestoneFromObj(out_snap)
    return out_snap, ms == "sweet-optimization"

def computeStats(ring, **sprov_options):
    args = [sprov_stats_binary, "--json"]
    args += options_dict_to_args(sprov_options)

    try:
        sp = subprocess.Popen(args,
                              stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    except Exception, e:
        raise Exception("Failed to call '%s': %s" % (" ".join(args), str(e)))

    (stdout, stderr) = sp.communicate(input=json.dumps(ring.toJson()))

    return json.loads(stdout)

def _filter_results(contexts, milestone):
    result = []
    assert(milestone in _MILESTONES_ORDER.keys())
    ms_to_reach = _MILESTONES_ORDER[milestone]
    for density, context in contexts.iteritems():
        ms = getMilestoneFromObj(context["obj"])
        ms_reached = _MILESTONES_ORDER.get(ms, "unset")
        if ms_reached >= ms_to_reach:
            result.append(density)
    return result

def checkConstraintsSolvedMulti(ring_model, densities_to_try, max_run_sec="10", **sprov_options):
    contexts = {}
    for density in densities_to_try:
        contexts[density] = {"obj": deepcopy(ring_model)}

    searchKeysMulti(
        contexts,
        "arranger_slice_density",
        densities_to_try,
        max_run_sec=max_run_sec,
        stop_search_after_constraints_solved=True,
        **sprov_options
    )
    return _filter_results(contexts, "constraints-solved"), contexts

def checkNodeCountMatchMulti(contexts, max_run_sec="60", **sprov_options):
    to_try = contexts.keys()
    searchKeysMulti(
        contexts,
        "arranger_slice_density",
        to_try,
        max_run_sec=max_run_sec,
        stop_search_after_node_count_match=True,
        **sprov_options
    )
    return _filter_results(contexts, "node-count-match")

def checkBareOptimMulti(contexts, max_run_sec="1800", **sprov_options):
    to_try = _filter_results(contexts, "node-count-match")
    searchKeysMulti(
        contexts,
        "arranger_slice_density",
        to_try,
        max_run_sec=max_run_sec,
        stop_search_after_bare_optimization=True,
        **sprov_options
    )
    return _filter_results(contexts, "bare-optimization")

def checkSweetOptimMulti(contexts, max_run_sec="600", **sprov_options):
    to_try = _filter_results(contexts, "bare-optimization")
    searchKeysMulti(
        contexts,
        "arranger_slice_density",
        to_try,
        max_run_sec=max_run_sec,
        stop_search_after_sweet_optimization=True,
        **sprov_options
    )
    return _filter_results(contexts, "sweet-optimization")

def getBestFromMulti(contexts, **sprov_options):
    """Find best keyspace in the contexts, one dict of snapshots
    indexed by densities, then call sprov to render it as a usable layout.
    The best keyspace is the one with the lower bal attribute"""

    best_bal = None
    best_snap = None
    for density, snap in contexts.iteritems():
        # First serialize snapshot
        js = snap["obj"].toJson()
        # extract bal
        eval_dict = js.get("bestring",{}).get("eval",{})
        if "bal" in eval_dict.keys():
            bal = float(eval_dict["bal"])
            # Compare
            if best_bal is None or bal > best_bal:
                best_bal = bal
                best_snap = js
    if best_bal is None:
        return False, "No good keyspace"

    # sprov is only able to output final ring in a file, so do it in a temp dir.
    tmpdir = tempfile.mkdtemp(prefix="scality_keyspace_")

    args = [sprov_binary, "-o", tmpdir, "--final-ring"]
    args += options_dict_to_args(sprov_options)
    try:
        sp = subprocess.Popen(
            args,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
    except Exception, e:
        raise Exception(
            "Failed to call '%s': %s" % (
                " ".join(args),
                str(e)
            )
        )
    (stdout, stderr) = sp.communicate(input=json.dumps(best_snap))

    filename = os.path.join(tmpdir, "layout_end.json")
    f = open(filename, "r")
    ring_js = json.load(f)
    f.close()
    shutil.rmtree(tmpdir)
    return True, ring_js
