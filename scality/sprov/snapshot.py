# -*- coding: utf-8 -*-
""" @file snapshot.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Sprov search snapshot object
"""

import logging
logger = logging.getLogger("scality.sprov")

try:
    import simplejson as json
except ImportError:
    import json

from scality.sprov.ring import Ring

class Snapshot(object):

    def __init__(self, json_obj):
        self.json_obj = json_obj

    def getOriginalRing(self):
        return Ring.createFromJson(self.json_obj["inputfile"])

    def getUpdatedRing(self):
        return Ring.createFromJson(self.json_obj["updatedring"])

    def resumeKeySearch(self, **sprov_options):
        from scality.sprov.compute import searchKeys
        new_snap = searchKeys(self, **sprov_options)
        self.__dict__.update(new_snap.__dict__)

    def toJson(self):
        return self.json_obj


    @staticmethod
    def createFromJson(json_obj):
        return Snapshot(json_obj)
