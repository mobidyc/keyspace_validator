# -*- coding: utf-8 -*-
""" @file storage.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Storage specs classes for sprov integration
"""

import copy
import math

import scality.common as common
from scality.key import Key
from scality.capacity import Capacity

import logging
logger = logging.getLogger("scality.sprov")


class DiskDef(object):

    MEAN_TIME_BEFORE_FAILURE = "220000h"
    REPAIR_TIME = "48h"
    CLASS = "consumer"
    LIFETIME = "5y"

    def __init__(self, capacity, label=None):

        if isinstance(capacity, Capacity):
            self.capacity = capacity
        else:
            self.capacity = Capacity(capacity, "TB")

        if label is not None:
            self.label = label
        else:
            self.label = DiskDef.getLabelAuto(capacity)


    def toJson(self):
        return {
            "device": self.label,
            "capacity": self.capacity.toJson(),
            "MTBF": DiskDef.MEAN_TIME_BEFORE_FAILURE,
            "repairtime": DiskDef.REPAIR_TIME,
            "deviceclass": DiskDef.CLASS,
            "lifetime": DiskDef.LIFETIME
        }

    @staticmethod
    def createFromJson(json_obj):
        capacity = json_obj["capacity"]
        label = json_obj["device"]
        return DiskDef(capacity, label)


    @staticmethod
    def getLabelAuto(capacity):
        return "Disk%s" % str(capacity)


class Storage(object):

    def __init__(self):
        self.disk_layout = []

    def addDisks(self, diskdef, diskcount):
        if not isinstance(diskdef, DiskDef):
            raise TypeError("diskdef must be of type DiskDef, not %s" % type(diskdef))
        if type(diskcount) is not int:
            raise TypeError("diskcount must be of type int, not %s" % type(diskcount))

        self.disk_layout.append((diskdef, diskcount))

    def getCapacity(self):
        cap = Capacity()
        for (diskdef, diskcount) in self.disk_layout:
            cap += diskcount * diskdef.capacity
        return cap


    def toJson(self):
        return [ {"device": diskdef.label,
                  "count": count } for (diskdef, count) in self.disk_layout ]


    @staticmethod
    def createFromJson(json_obj, disk_defs):
        storage = Storage()

        def addJsonEntry(json_entry):
            (disklabel, diskcount) = (json_entry["device"],
                                      json_entry["count"])
            storage.addDisks((disk_def for disk_def in disk_defs
                              if disk_def.label == disklabel).next(), diskcount)

        if type(json_obj) is list:
            for json_entry in json_obj:
                addJsonEntry(json_entry)
        else:
            addJsonEntry(json_obj)

        return storage
