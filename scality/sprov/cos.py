# -*- coding: utf-8 -*-
""" @file cos.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Class of service handling for sprov integration
"""

import copy
import math
import re

import scality.common as common
from scality.key import Key
from scality.capacity import Capacity

import logging
logger = logging.getLogger("scality.sprov")


class ClassOfService(object):

    # ndiv: angles on which replicas are located. Per COS schema
    # count: number of replicas to place on ndiv
    # needed: ?
    # skipped: describe which positions are not occupied by a replica.
    #          When ndiv==count, skipped should be empty. Otherwise,
    #          a sequence of positions in [0, ndiv[

    COS_PROPERTIES = {
        '0': { 'ndiv': 1, 'count': 1, 'needed': 1, "skipped": set()},
        '1': { 'ndiv': 2, 'count': 2, 'needed': 1, "skipped": set()},
        '2': { 'ndiv': 3, 'count': 3, 'needed': 1, "skipped": set()},
        '2+': { 'ndiv': 3, 'count': 3, 'needed': 2, "skipped": set()},
        '3': { 'ndiv': 6, 'count': 4, 'needed': 1, "skipped": set([1, 4])},
        '4': { 'ndiv': 6, 'count': 5, 'needed': 1, "skipped": set([4])},
        '4+': { 'ndiv': 6, 'count': 5, 'needed': 3, "skipped": set()},
        '5': { 'ndiv': 6, 'count': 6, 'needed': 1, "skipped": set()},
        '6': { 'ndiv': 4, 'count': 3, 'needed': 1, "skipped": set([1])},
        '8': { 'ndiv': 6, 'count': 3, 'needed': 1, "skipped": set([1, 4, 5])},
        "9": { "ndiv": 1, "count": 1, "needed": 1, "skipped": set() },
        "A": { "ndiv": 2, "count": 2, "needed": 1,"skipped": set() },
    }

    def __init__(self, cos_name):
        self.name = str(cos_name)

        if self.is_cos():
            p = ClassOfService.COS_PROPERTIES[self.name]

            cos_ndiv = p["ndiv"]
            self.parts_count = p["count"]
            self.parts_needed = p["needed"]
            self.parts_coding = None
            self.skipped = p['skipped']

            self.descr = "COS%s (Replication)" % self.name
        elif self.is_arc():
            m = re.match("ARC(\d+)\+(\d+)(,(\d+)|)$", self.name)
            if not m:
                raise ValueError("Bad COS: %s" % self.name)

            if m.group(4):
                if m.group(4) not in ClassOfService.COS_PROPERTIES:
                    raise ValueError("Bad COS: %s (invalid repl.)" % self.name)
                self.replicated = int(m.group(4))
            else:
                self.replicated = None

            k = int(m.group(1))
            m = int(m.group(2))

            cos_ndiv = ((k + m + 5) // 6) * 6

            self.parts_count = k + m
            self.parts_needed = k
            self.parts_coding = m
            self.skipped = set()

            self.descr = self.name
        else:
            raise ValueError("Bad COS: %s" % self.name)

        self.schema = Schema(cos_ndiv)

    def is_cos(self):
        return self.name in ClassOfService.COS_PROPERTIES

    def is_arc(self):
        return self.name.startswith("ARC")

    def need_dcprefix(self):
        return self.name in ["9", "A"]

    def cos(self):
        if self.is_cos():
            cos = int(self.name.replace("+", ""))
        elif self.replicated is not None:
            cos = self.replicated
        else:
            arc_to_cos = {
                "ARC4+2": 3,
                "ARC9+3": 3,
                "ARC14+4": 4
            }

            cos = arc_to_cos.get(self.name)
            if cos is None:
                cos = self.parts_coding

            # do not use an invalid CoS (like 7)
            if not str(cos) in ClassOfService.COS_PROPERTIES:
                # Let's use a safe value
                cos = 5

        return cos

    def __eq__(self, cos):
        return self.name == cos.name

    def __lt__(self, cos):
        return self.name < cos.name

    def __str__(self):
        return self.name

    def toJson(self):
        return self.name

    def max_parts_lost(self, failed_ratio):
        """ Compute maximum parts lost when 'failed_ratio'
        of the platform is unavailable

        Parameters:
        - failed_ratio: floating number in [0., 1.0]
        Returns:
        - Integer: maximum number of losts parts, in [0, self.count]
        """
        # Pessimistic: how much do we loose if all positions are occupied
        pessimistic = int(math.ceil(failed_ratio * self.schema.ndiv))
        if len(self.skipped) == 0:
            return pessimistic
        # Handle COS with custom data placement
        max_lost = 0
        for i in range(self.schema.ndiv):
            successors = set(j % self.schema.ndiv for j in range(i, i + pessimistic))
            max_lost = max(max_lost, len(successors.difference(self.skipped)))
        return max_lost

    @staticmethod
    def createFromJson(json_obj):
        return ClassOfService(cos_name=json_obj)


class Schema(object):

    def __init__(self, ndiv=1):
        if ndiv <= 0:
            raise ValueError("Bad schema number: %d" % ndiv)
        self.ndiv = ndiv

    def __int__(self):
        return self.ndiv

    def toJson(self):
        return self.ndiv


    @staticmethod
    def createFromJson(json_obj):
        return Schema(ndiv=json_obj)


    @staticmethod
    def gcd(a,b):
        while b: a, b = b, a%b
        return a

    @staticmethod
    def lcm(a,b):
        return (a*b) // Schema.gcd(a,b)

    def addClassOfService(self, cos):
        self.mergeSchema(cos.schema)

    def mergeSchema(self, schema):
        self.ndiv = Schema.lcm(self.ndiv, schema.ndiv)


    def checkClassOfServiceCompatibility(self, cos):
        schema_lcm = Schema.lcm(self.ndiv, cos.schema.ndiv)
        if schema_lcm != self.ndiv:
            raise Exception("Error: schema %d incompatible with COS %s"
                            % (self.ndiv, cos))
