# -*- coding: utf-8 -*-
""" @file component.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Component classes for sprov integration
"""

import copy

import scality.common as common
from scality.key import Key
from scality.capacity import Capacity
from scality.sprov.storage import Storage

import logging
logger = logging.getLogger("scality.sprov")


class Component(object):

    def __init__(self, **kwargs):
        super(Component, self).__init__()
        self.component_type = kwargs["component_type"]
        self.name = None
        self.parent = None


    def __str__(self):
        if self.name is not None:
            return "(%s:%s)" % (self.component_type, self.name)
        else:
            return "(%s:(unnamed))" % self.component_type

    def setType(self, component_type):
        self.component_type = component_type

    def setName(self, name):
        self.name = name

    def fullComponentName(self):
        component = self
        name = component.name

        while component.parent is not None:
            component = component.parent
            if component.name is not None:
                name = component.name + ":" + name

        return name

    def toJson(self):
        try:
            json_obj = super(Component, self).toJson()
        except AttributeError:
            json_obj = {}

        if self.component_type is not None:
            json_obj["componenttype"] = self.component_type
        if self.name is not None:
            json_obj["name"] = self.name

        return json_obj


    def isContainerTypeOf(self, component):
        cur_type = type(self)
        if cur_type.NEXT_CHILD_TYPENAME is None:
            return False
        while True:
            cur_type = globals()[cur_type.NEXT_CHILD_TYPENAME]
            if isinstance(component, cur_type):
                return True
            if cur_type.NEXT_CHILD_TYPENAME is None:
                return False

    def isChildOf(self, component):
        parent = self.parent
        while parent is not None:
            if parent == component:
                return True
            parent = parent.parent


    @classmethod
    def instantiateFromJson(cls, json_obj): # overriden by Server
        return cls()


    @staticmethod
    def createFromJson(json_obj, ring):
        component = None
        try:
            component_type = json_obj["componenttype"]

            for cls in [Site, Rack, Server, DiskGroup]:
                if cls.TYPENAME.lower() == component_type.lower():
                    component = cls.instantiateFromJson(json_obj)

        except KeyError:
            pass

        if component is None: # best effort
            if json_obj.has_key("capacity") or json_obj.has_key("storage"):
                component = Server.instantiateFromJson(json_obj)
            elif json_obj.has_key("elements"):
                component = Container()
            else:
                raise ValueError("No known component type for JSON object of type \"%s\""
                                 % component_type)

        component.loadJson(json_obj, ring)
        return component


    def loadJson(self, json_obj, ring):
        if json_obj.has_key("componenttype"):
            self.setType(json_obj["componenttype"])
        if json_obj.has_key("name"):
            self.setName(json_obj["name"])


class Container(Component):

    def __init__(self, **kwargs):
        super(Container, self).__init__(**kwargs)
        self.children = []

    def addChildComponent(self, child):
        if not isinstance(child, Component):
            raise TypeError("A container can only have children of type Component, not %s" % type(child))

        self.children.append(child)
        child.parent = self

    def getTotalCapacity(self):
        return sum(child.getTotalCapacity() for child in self.children)

    def clearNonTopologicalInfo(self):
        self.name = None

        for child in self.children:
            child.clearNonTopologicalInfo()


    def toJson(self):
        json_obj = super(Container, self).toJson()
        if self.children:
            json_obj["elements"] = [child.toJson() for child in self.children]

        return json_obj


    def allChildrenOfType(self, component_cls):
        if not isinstance(component_cls, type(Component)):
            raise TypeError("component_cls must be a subclass type of Component, not %s" % repr(component_cls))

        if isinstance(self, component_cls):
            yield self
        else:
            for child in self.children:
                if isinstance(child, component_cls):
                    yield child
                elif isinstance(child, Container):
                    for child in child.allChildrenOfType(component_cls):
                        yield child

    def hasChildrenOfType(self, component_cls):
        try:
            self.allChildrenOfType(component_cls).next()
            return True
        except StopIteration:
            return False


    def spreadChildComponentEvenly(self, component):
        if not self.isContainerTypeOf(component):
            raise Exception("Can't spread child component of type %s under parent of type %s"
                            % (type(component).TYPENAME, type(self).TYPENAME))

        if self.children:
            if self.children[0].isContainerTypeOf(component):
                chosen_child = min((child for child in self.children),
                                   key=lambda child: child.countComponentType(globals()[type(component).TYPENAME]))
                chosen_child.spreadChildComponentEvenly(component)
            else:
                self.addChildComponent(component)

        else:
            self.addChildComponent(component)


    def attachChildrenEvenlyToNewParentComponents(self, parent_comp,
                                                  parent_count):
        new_children = [ copy.deepcopy(parent_comp)
                         for idx in xrange(0, parent_count) ]

        partitioned_children = common.partition(self.children, len(new_children))

        for i, partition in enumerate(partitioned_children):
            parent = new_children[i]
            for child in partition:
                parent.addChildComponent(child)

        self.children = []
        for new_child in new_children:
            self.addChildComponent(new_child)

    def spreadChildrenComponentNames(self, hostnames):
        idx = 1
        for child in self.children:
            if child.name is None:
                name = "%s%d" % (child.component_type, idx)
                while any(child2.name == name
                          for child2 in self.children):
                    idx += 1
                    name = "%s%d" % (child.component_type, idx)
                child.setName(name)

            if hostnames is not None and isinstance(child, Server):
                match_name = child.fullComponentName()

                if match_name in hostnames:
                    child.setHostname(hostnames[match_name])

            idx += 1
            if isinstance(child, Container):
                child.spreadChildrenComponentNames(hostnames)


    def countComponentType(self, component_cls):
        count = 0
        for child in self.allChildrenOfType(component_cls):
            count += 1
        return count


    def loadJson(self, json_obj, ring):
        super(Container, self).loadJson(json_obj, ring)

        for json_child in json_obj["elements"]:
            child = Component.createFromJson(json_child, ring)
            self.addChildComponent(child)

    def findChildByPath(self, path):
        if not path:
            raise ValueError()

        for child in self.children:
            if child.name == path[0]:
                if len(path) == 1:
                    return child
                if not isinstance(child, Container):
                    raise ValueError()
                return child.findChildByPath(path[1:])
        raise ValueError()



class Site(Container):

    TYPENAME = "Site"
    NEXT_CHILD_TYPENAME = "Rack"

    def __init__(self):
        super(Site, self).__init__(component_type=Site.TYPENAME)

class Rack(Container):

    TYPENAME = "Rack"
    NEXT_CHILD_TYPENAME = "Server"

    def __init__(self):
        super(Rack, self).__init__(component_type=Rack.TYPENAME)



class Node(object):

    def __init__(self, nid, key):
        self.nid = int(nid)
        self.key = Key(key)

    def toJson(self):
        return { "nid": self.nid, "key": self.key.getHexPadded() }

    @staticmethod
    def createFromJson(json_obj):
        return Node(json_obj["nid"], json_obj["key"])


class Unit(Component):

    def __init__(self, **kwargs):

        super(Unit, self).__init__(**kwargs)

        self.capacity = None
        self.storage = None
        self.nb_processes = kwargs["nb_processes"]
        self.nodes = None
        self.minnodes = None
        self.maxnodes = None

    def setCapacity(self, capacity):
        self.storage = None

        if isinstance(capacity, Capacity):
            capacity = capacity.label

        self.capacity = Capacity(capacity)

    def setStorage(self, disk_def, disk_count):
        self.capacity = None

        storage = Storage()
        storage.addDisks(disk_def, disk_count)
        self.storage = storage

    def setNbProcesses(self, nb_processes):
        self.nb_processes = nb_processes

        if self.minnodes is not None:
            self.minnodes = nb_processes
        if self.maxnodes is not None:
            self.maxnodes = nb_processes


    def clearNonTopologicalInfo(self):
        self.name = None
        self.nodes = None


    def getTotalCapacity(self):
        if self.capacity is not None:
            return self.capacity
        elif self.storage is not None:
            return self.storage.getCapacity()
        else:
            raise Exception("error in getTotalCapacity(): unit %s has no bound capacity nor disk storage"
                            % str(self))


    def addNode(self, node):
        if self.nodes is None:
            self.nodes = [node]
        else:
            self.nodes.append(node)

    def addNodeIfNotExists(self, node):
        if self.nodes is None:
            self.nodes = [node]
        elif node.nid not in [n.nid for n in self.nodes]:
            self.nodes.append(node)
        elif node.key == [n.key for n in self.nodes if n.nid == node.nid][0]:
            return True
        else:
            #print "mismatch:", node.nid, node.key, self.name, [(n.nid, n.key) for n in self.nodes]
            return False
        return True


    def toJson(self):
        json_obj = super(Unit, self).toJson()

        json_obj["nnodes"] = self.nb_processes
        if self.minnodes is not None:
            json_obj["minnodes"] = self.minnodes
        if self.maxnodes is not None:
            json_obj["maxnodes"] = self.maxnodes
        if self.nodes is not None:
            json_obj["nodes"] = [node.toJson()
                                 for node in sorted(self.nodes,
                                                    key=lambda node: node.nid)]

        if self.capacity is not None:
            json_obj["capacity"] = self.capacity.toJson()
        elif self.storage is not None:
            json_obj["storage"] = self.storage.toJson()
        else:
            raise Exception("error in JSON dump: unit %s has no bound capacity nor disk storage"
                            % str(self))

        return json_obj


    def loadJson(self, json_obj, ring):
        super(Unit, self).loadJson(json_obj, ring)

        if json_obj.has_key("nnodes"):
            self.nb_processes = json_obj["nnodes"]

        if json_obj.has_key("nodes"):
            self.nodes = [Node.createFromJson(json_node)
                          for json_node in json_obj["nodes"]]

        if json_obj.has_key("minnodes"):
            self.minnodes = json_obj["minnodes"]
        if json_obj.has_key("maxnodes"):
            self.maxnodes = json_obj["maxnodes"]

        if json_obj.has_key("storage"):
            self.storage = Storage.createFromJson(json_obj["storage"], ring.disk_defs)
        elif json_obj.has_key("capacity"):
            self.capacity = Capacity.createFromJson(json_obj["capacity"])


class Server(object):

    TYPENAME = "Server"
    DEFAULT_NB_PROCESSES = 6
    NEXT_CHILD_TYPENAME = "DiskGroup"

    hostname = None

    def __init__(self, **kwargs):
        if ("nb_processes" not in kwargs or
            kwargs["nb_processes"] is None):
            kwargs["nb_processes"] = Server.DEFAULT_NB_PROCESSES

        super(Server, self).__init__(**kwargs)

    def setHostname(self, hostname):
        self.hostname = hostname

    @staticmethod
    def create(nb_disk_groups=None, nb_processes=None):
        if nb_disk_groups is not None:
            return ServerWithDiskGroups(nb_disk_groups, nb_processes)
        else:
            return ServerWithoutDiskGroups(nb_processes)

    @classmethod
    def instantiateFromJson(cls, json_obj):
        if json_obj.has_key("elements"):
            return ServerWithDiskGroups()
        else:
            return ServerWithoutDiskGroups()

    def insertJson(self, json_obj):
        if self.hostname is not None:
            json_obj["host"] = self.hostname


class ServerWithoutDiskGroups(Server, Unit):

    def __init__(self, nb_processes=None):
        super(ServerWithoutDiskGroups, self).__init__(component_type=Server.TYPENAME,
                                                      nb_processes=nb_processes)

    def toJson(self):
        json_obj = super(ServerWithoutDiskGroups, self).toJson()
        Server.insertJson(self, json_obj)
        return json_obj

    def loadJson(self, json_obj, ring):
        super(ServerWithoutDiskGroups, self).loadJson(json_obj, ring)


class ServerWithDiskGroups(Server, Container):

    def __init__(self, nb_disk_groups=None, nb_processes=None):
        super(ServerWithDiskGroups, self).__init__(component_type=Server.TYPENAME)

        if nb_disk_groups is None:
            return

        if nb_processes is None:
            nb_processes = Server.DEFAULT_NB_PROCESSES

        if nb_processes % nb_disk_groups != 0:
            raise Exception("number of processes per server not a multiple "
                            "of number of disk groups per server")

        nb_processes_per_group = nb_processes / nb_disk_groups
        for dg in xrange(nb_disk_groups):
            diskgroup = DiskGroup(nb_processes=nb_processes_per_group)
            diskgroup.setName("DiskGroup%s" % dg)
            self.addChildComponent(diskgroup)


    def setCapacity(self, input_capacity):
        capacity = input_capacity
        if isinstance(input_capacity, Capacity):
            capacity = input_capacity.label

        capacity = Capacity(capacity)
        capacity_per_disk_group = capacity / len(self.children)

        for child in self.children:
            child.setCapacity(capacity_per_disk_group)


    def setStorage(self, disk_def, disk_count):
        min_ndisks_per_group = disk_count / len(self.children)

        if not min_ndisks_per_group:
            raise Exception("Error: more disk groups than number of disks (n_groups=%d, n_disks=%d)"
                            % (len(self.children), disk_count))

        for dg in xrange(len(self.children)):
            if dg < disk_count % len(self.children):
                self.children[dg].setStorage(disk_def, min_ndisks_per_group + 1)
            else:
                self.children[dg].setStorage(disk_def, min_ndisks_per_group)


    def setNbProcesses(self, nb_processes):
        if nb_processes % len(self.children) != 0:
            raise Exception("number of processes per server not a multiple "
                            "of number of disk groups per server")

        for dg in self.children:
            dg.setNbProcesses(nb_processes / len(self.children))


    def toJson(self):
        json_obj = super(ServerWithDiskGroups, self).toJson()
        Server.insertJson(self, json_obj)
        return json_obj

    def loadJson(self, json_obj, ring):
        super(ServerWithDiskGroups, self).loadJson(json_obj, ring)


class DiskGroup(Unit):

    TYPENAME = "DiskGroup"
    NEXT_CHILD_TYPENAME = None

    def __init__(self, nb_processes=None):
        super(DiskGroup, self).__init__(component_type=DiskGroup.TYPENAME,
                                        nb_processes=nb_processes)
