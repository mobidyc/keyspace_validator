# -*- coding: utf-8 -*-
""" @file event.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief Handling of failure events for sprov integration
"""

import copy
import math

import scality.common as common
from scality.key import Key
from scality.capacity import Capacity
from scality.sprov.component import Site, Rack, Server, DiskGroup
from scality.sprov.cos import ClassOfService

import logging
logger = logging.getLogger("scality.sprov")


class FailureSet(object):

    ORDERED_TYPES = [Site, Rack, Server, DiskGroup]

    def __init__(self, **kwargs):
        self.failcounts = [0] * len(FailureSet.ORDERED_TYPES)
        self.addFailures(**kwargs)


    def __getitem__(self, component_cls):
        return self.failcounts[FailureSet.ORDERED_TYPES.index(component_cls)]

    def __setitem__(self, component_cls, fail_count):
        self.failcounts[FailureSet.ORDERED_TYPES.index(component_cls)] = fail_count

    def __delitem__(self, component_cls):
        self.failcounts[FailureSet.ORDERED_TYPES.index(component_cls)] = 0

    def __iter__(self):
        for f in xrange(len(self.failcounts)):
            if self.failcounts[f] > 0:
                yield (FailureSet.ORDERED_TYPES[f], self.failcounts[f])

    def __repr__(self):
        return ("FailureSet:(" +
                ",".join("%s=%d" % (failure_tuple[0].TYPENAME, failure_tuple[1])
                         for failure_tuple in self) +
                ")")

    def desc(self):
        return ("failure of " +
                " + ".join("%d %s" % (failure_tuple[1], failure_tuple[0].TYPENAME + (failure_tuple[1] > 1 and "s" or ""))
                           for failure_tuple in self))

    def issubset(self, failure_set):
        return all(self.failcounts[f] <= failure_set.failcounts[f]
                   for f in xrange(len(self.failcounts)))

    def issuperset(self, failure_set):
        return failure_set.issubset(self)

    def __eq__(self, failure_set):
        return all(self.failcounts[f] == failure_set.failcounts[f]
                   for f in xrange(len(self.failcounts)))


    def addFailures(self, **kwargs):
        for comp_type in kwargs.keys():
            component_cls = globals()[comp_type]
            f = FailureSet.ORDERED_TYPES.index(component_cls)
            self.failcounts[f] += kwargs[comp_type]

    def getAllFailedTypes(self):
        return list(FailureSet.ORDERED_TYPES[f]
                    for f in xrange(len(FailureSet.ORDERED_TYPES))
                    if self.failcounts[f] > 0)


class FailureEvent(object):

    def __init__(self, failure_set, cos_list):
        if type(cos_list) is not list:
            cos_list = [cos_list]
        (self.failure_set, self.cos_list) = (failure_set, cos_list)

    def __repr__(self):
        return ("(" + repr(self.failure_set) +
                " COS=" +
                ",".join([str(cos) for cos in self.cos_list]) + ")")

    def __eq__(self, fe):
        return (self.failure_set == fe.failure_set and
                sorted(self.cos_list) == sorted(fe.cos_list))

    def toJson(self):
        failed_types = self.failure_set.getAllFailedTypes()
        return {
            "eventtype": "shutdown",
            "componenttypes": [component_cls.TYPENAME
                               for component_cls in failed_types],
            "componentcounts": [self.failure_set[component_cls]
                                for component_cls in failed_types],
            "classofservice": [cos.toJson() for cos in self.cos_list]
        }

    @staticmethod
    def createFromJson(json_obj):
        failures_dict = {}
        for i in xrange(len(json_obj["componenttypes"])):
            failures_dict[json_obj["componenttypes"][i]] = json_obj["componentcounts"][i]

        if type(json_obj["classofservice"]) is list:
            cos_list = [ClassOfService.createFromJson(cos) for cos in json_obj["classofservice"]]
        else:
            cos_list = [ClassOfService.createFromJson(json_obj["classofservice"])]

        return FailureEvent(failure_set=FailureSet(**failures_dict),
                            cos_list=cos_list)


class FailureEventSet(object):

    def __init__(self, copy_from=None):
        if copy_from is None:
            self.events = []
        else:
            self.events = list(copy_from.events)

    def __len__(self):
        return len(self.events)

    def __iter__(self):
        return iter(self.events)


    def addFailureEvent(self, failure_set, cos_list):

        events_superset_of_current_failure_set = \
            [ev for ev in self.events
             if ev.failure_set.issuperset(failure_set)]

        events_subset_of_current_failure_set = \
            [ev for ev in self.events
             if ev.failure_set.issubset(failure_set)]

        filtered_cos_list = filter(lambda cos:
                                   all(cos not in ev.cos_list
                                       for ev in events_superset_of_current_failure_set),
                                   cos_list)

        for cos in filtered_cos_list:
            for ev in events_subset_of_current_failure_set:
                if cos in ev.cos_list:
                    ev.cos_list.remove(cos)

        self.events.append(FailureEvent(failure_set, filtered_cos_list))

        # cleanup events with empty COS list
        self.events = filter(lambda ev: ev.cos_list, self.events)

    def hasFailureEvent(self, failure_event):
        return any([ev == failure_event for ev in self.events])


    def toJson(self):
        return [event.toJson() for event in self.events]


    @staticmethod
    def createFromJson(json_obj):
        event_set = FailureEventSet()
        if type(json_obj) is list:
            for ev in json_obj:
                event_set.events.append(FailureEvent.createFromJson(ev))
        else:
            event_set.events.append(FailureEvent.createFromJson(json_obj))

        return event_set
