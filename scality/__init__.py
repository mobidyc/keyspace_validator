# -*- coding: utf-8 -*-

from pkgutil import extend_path


__path__ = extend_path(__path__, __name__)

__version__ = "7.4.1.5"
__revision__ = "0ac42e3b25"
