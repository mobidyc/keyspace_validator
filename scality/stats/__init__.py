# -*- coding: utf-8 -*-
"""
Pyscality stats API.

It allows to fetches stats from different source (file in procfs, HTTP url),
in different format (table and JSON), parse and map them in a python object
which can be queried.
"""

import os
import re
import time
import urllib2
from collections import namedtuple
from contextlib import closing

from scality.common import json_loads


STATS_LABELS = ["count", "par", "maxpar", "errors", "avg", "slowest",
                "time_ms", "bytes", "time_ms_sqr"]
STATS_REPORTED_LABELS = set(["count", "par", "errors", "time_ms",
                             "bytes", "time_ms_sqr"])

STATS_MAP_JSON_LABELS = {
    # map column labels in json stats to those we produce from text stats
    "count": "count",
    "concurrent": "par",
    "errors": "errors",
    "total_ms": "time_ms",
    "total_ms_square": "time_ms_sqr",
    "total_size": "bytes",
}

assert set(STATS_MAP_JSON_LABELS.values()).issuperset(STATS_REPORTED_LABELS), \
           "missing mappings in STATS_MAP_JSON_LABELS"

# extra lines in stats_sfused
IGNORED_LINES = re.compile("^$|^total number|^for the last|^\s+operation")

# map dicts with stats values into objects
ScalStatOp = namedtuple("ScalStatOp", [
    "count", "par", "errors", "time_ms", "time_ms_sqr"
])
ScalStatSizeOp = namedtuple("ScalStatSizeOp", [
    "count", "par", "errors", "time_ms", "time_ms_sqr", "bytes"
])
ScalStatEvent = namedtuple("ScalStatEvent", [
    "count"
])
ScalStatGauge = namedtuple("ScalStatGauge", [
    "count"
])

class ScalStats(object):
    """
    Scality stats class.
    """
    def __init__(self, ts, ops, size_ops, events, gauges, total_read, total_write):
        self.timestamp = ts
        self.ops = dict(
            (op, ScalStatOp(**d)) for (op, d) in ops.iteritems()
        )
        self.size_ops = dict(
            (op, ScalStatSizeOp(**d)) for (op, d) in size_ops.iteritems()
        )
        self.events = dict(
            (op, ScalStatEvent(**d)) for (op, d) in events.iteritems())
        self.gauges = dict(
            (op, ScalStatGauge(**d)) for (op, d) in gauges.iteritems())
        self.total_read = total_read
        self.total_write = total_write

    @staticmethod
    def _str_to_ts(string):
        """
        Convert a string to timestamp using local settings.
        """
        hms, microsec = string.split(".", 1)
        struct_time = time.strptime(hms, "%X")
        now = time.localtime()
        return time.mktime(
            (now.tm_year, now.tm_mon, now.tm_mday,
             struct_time.tm_hour, struct_time.tm_min,
             struct_time.tm_sec, now.tm_wday, now.tm_yday,
             now.tm_isdst)
        ) + float(microsec) / (1000 * 1000)

    @classmethod
    def _from_table(cls, stats_str):
        """
        Return a ScalStats from a buffer containing lines of text stats in table
        format.
        """
        timestamp = 0.0
        total_read = 0
        total_write = 0

        ops = {}
        size_ops = {}
        events = {}
        gauges = {}

        for line in stats_str.splitlines():
            if IGNORED_LINES.match(line):
                continue
            words = line.split()

            # timestamp line
            if words[0] == "timestamp:":
                timestamp = cls._str_to_ts(words[1])
                continue

            # total line
            if words[0] == "Total":
                total_read = int(words[2])
                total_write = int(words[4])
                continue

            # determine type of line
            is_size_op_bucket = words[1] in ["<", ">="]
            if is_size_op_bucket:
                opname = " ".join(words[0:4])
                cols = words[4:]
                is_evt = False
                is_op = False
                is_size_op = True
                is_gauge = False
            elif words[2] == '-':
                opname = words[0]
                cols = words[1:]
                is_gauge = True
                is_evt = False
                is_op = False
                is_size_op = False
            else:
                opname = words[0]
                cols = words[1:]
                is_gauge = False
                is_evt = len(cols) < len(STATS_LABELS)
                is_op = not is_evt and cols[-2] == "-"
                is_size_op = not is_evt and not is_op

            stat_dict = {}
            for value, label in zip(cols, STATS_LABELS):
                if label not in STATS_REPORTED_LABELS or value == "-":
                    continue

                if is_gauge:
                    stat_dict[label] = float(value)
                else:
                    stat_dict[label] = int(value)

            if is_op:
                ops[opname] = stat_dict
            elif is_size_op:
                size_ops[opname] = stat_dict
            elif is_gauge:
                gauges[opname] = stat_dict
            else:
                events[opname] = {"count": stat_dict["count"]}

        return cls(timestamp, ops, size_ops, events, gauges, total_read, total_write)

    @classmethod
    def _from_json(cls, stats_str):
        """
        Return a ScalStats from a buffer containing lines of text stats in JSON
        format.
        """
        stats_json = json_loads(stats_str)

        timestamp = cls._str_to_ts(stats_json["timestamp"])
        total_read = stats_json["total_read"]["value"]
        total_write = stats_json["total_written"]["value"]

        ops = {}
        size_ops = {}
        events = {}
        gauges = {}

        for opname, stat_col in stats_json["stats"].iteritems():
            # determine type of item
            is_evt = not "total_ms" in stat_col
            is_size_op = "total_size" in stat_col
            is_op = not is_evt and not is_size_op
            is_gauge = False

            stat_dict = {}
            for column_label, json_label in STATS_MAP_JSON_LABELS.iteritems():
                if column_label in stat_col:
                    stat_dict[json_label] = stat_col[column_label]["value"]
                    is_gauge = isinstance(stat_col[column_label]["value"], float)

            if is_op:
                ops[opname] = stat_dict
            elif is_size_op:
                size_ops[opname] = stat_dict
            elif is_gauge:
                gauges[opname] = stat_dict
            else:
                events[opname] = stat_dict

        return cls(timestamp, ops, size_ops, events, gauges, total_read, total_write)

    @classmethod
    def from_string(cls, stats_str):
        """
        Return a ScalStats from a buffer containing lines of text stats in JSON
        or table format.
        """
        try:
            stats = cls._from_json(stats_str)
        except ValueError:
            stats = cls._from_table(stats_str)

        return stats

    @classmethod
    def from_daemon(cls, daemon_name, stat_path,
                    sysfs_path="run/scality/connectors"):
        """
        Return a ScalStats from a deamon procfs stat file.
        """
        stat_file = os.path.join("/", sysfs_path, daemon_name, stat_path)
        with open(stat_file, "r") as handler:
            content = handler.read()

        return cls.from_string(content)

    @classmethod
    def from_url(cls, url):
        """
        Return a ScalStats from a deamon url stats.
        """
        with closing(urllib2.urlopen(url)) as handler:
            content = handler.read()

        return cls.from_string(content)
