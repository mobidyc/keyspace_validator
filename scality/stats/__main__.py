# -*- coding: utf-8 -*-
"""
Tool to fetches stats from different source (file in procfs, HTTP url),
in different format (table and JSON) and parse them.
"""

import argparse
import pprint
import sys

from scality.stats import ScalStats


def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        "--print", dest="print_stats", required=False,
        action="store_true", default=False,
        help="Print parsed stats"
    )

    subparsers = parser.add_subparsers(dest="source", help="Stats source")

    #
    # From file
    #

    file_parser = subparsers.add_parser("file")
    file_parser.add_argument("--path", "-p", required=True,
                             help="Stats file path")

    def from_file(args):
        with open(args.path, "r") as f:
            content = f.read()
        return ScalStats.from_string(content)
    file_parser.set_defaults(func=from_file)

    #
    # From daemon
    #

    daemon_parser = subparsers.add_parser("daemon")
    daemon_parser.add_argument("--name", "-n", required=True,
                               help="Daemon name")
    daemon_parser.add_argument("--path", "-p", required=True,
                               help="Stats file path")
    daemon_parser.add_argument("--sysfs_path", required=False,
                               help="Sysfs directory path")

    def from_daemon(args):
        kwargs = {}
        if args.sysfs_path:
            kwargs["sysfs_path"] = args.sysfs_path
        return ScalStats.from_daemon(args.name, args.path, **kwargs)
    daemon_parser.set_defaults(func=from_daemon)

    #
    # From url
    #

    url_parser = subparsers.add_parser("url")
    url_parser.add_argument("--url", "-u", required=True,
                            help="URL stats")
    def from_url(args):
        return ScalStats.from_url(args.url)
    url_parser.set_defaults(func=from_url)

    #
    # Parse arguments
    #

    args = parser.parse_args()

    stats = args.func(args)
    if args.print_stats:
        print("Timestamp: {0:f}".format(stats.timestamp))
        print("Ops: {0:s}".format(pprint.pformat(stats.ops)))
        print("Size ops: {0:s}".format(pprint.pformat(stats.size_ops)))
        print("Events: {0:s}".format(pprint.pformat(stats.events)))
        print("Gauges: {0:s}".format(pprint.pformat(stats.gauges)))
        print("Total read: {0:d}".format(stats.total_read))
        print("Total write: {0:d}".format(stats.total_write))


if sys.argv[0].endswith("__main__.py"):
    sys.argv[0] = "python -m scality.stats"

main()
