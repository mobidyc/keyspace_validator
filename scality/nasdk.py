# -*- coding: utf-8 -*-
""" @file nasdk.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:27:02 CEST 2012

    @brief module implementing generic nasdk daemon (do not use directly)
"""

import httplib
import copy

from scality.common import ScalDaemonExceptionInvalidParameter


class NasdkDaemon(object):

    """ a generic nasdk daemon,
    implementing methods used by several real daemons """

    def __init__(self):
        pass

    @staticmethod
    def _http_req(method, uri, data=None, headers=None):
        """ do a http request """
        if not method:
            raise ScalDaemonExceptionInvalidParameter("method")

        if not uri:
            raise ScalDaemonExceptionInvalidParameter("uri")
        if not uri.startswith("http://"):
            raise ScalDaemonExceptionInvalidParameter("uri", uri, "schema not supported")

        try:
            address = uri[len("http://"):].split("/")[0]
        except:
            raise ScalDaemonExceptionInvalidParameter("uri", uri,
                                                      "cannot extract address")

        if len(address) <= 0:
            raise ScalDaemonExceptionInvalidParameter("uri", uri, "invalid address")

        myheaders = {}
        if headers is not None:
            myheaders.update(headers)

        conn = httplib.HTTPConnection(address)
        if data:
            conn.request(method, uri, data, myheaders)
        else:
            conn.request(method, uri, None, myheaders)

        return conn.getresponse()


def normalize_config(config, desc, normalize_func=None):
    """ helper function to normalize a configuration for a given description """
    desc_conf = desc
    static_conf = copy.deepcopy(config)
    new_conf = {}

    if not isinstance(config, dict):
        return "NONE"
    if not isinstance(desc, dict):
        return "NONE"

    for name, section in desc_conf.items():
        union = False
        if name == "header":
            continue
        if "_key" in section:
            union = True

        section_in_sc = static_conf.get(name, None)

        if union:
            if section_in_sc is None:
                section_in_sc = {}
                section_in_sc[section["_key"]["key_id"]] = section["_key"]["default_value"]
            cur_section = section["_union"][section_in_sc[section["_key"]["key_id"]]] # get the correct section

        else:
            if section_in_sc is None:
                section_in_sc = {}
            cur_section = section

        for iname, ivalue in cur_section.items():
            if iname not in section_in_sc and "default_value" in ivalue:
                section_in_sc[iname] = ivalue["default_value"]
            if iname in section_in_sc:
                # normalize
                def normalize(confsec, sec, value):
                    if normalize_func is not None:
                        value = normalize_func(confsec, sec, value)
                    if sec["type"] == "boolean":
                        try:
                            if int(value):
                                return True
                        except ValueError:
                            if value.lower() == "true" or value.lower() == "yes" or value.lower() == "on":
                                return True
                        return False
                    if sec["type"] == "integer":
                        try:
                            return int(value, 0)
                        except:
                            return int(value)
                    if sec["type"] == "string":
                        return str(value)
                    if sec["type"] == "double":
                        return "%.2lf" % float(value)
                    return value
                section_in_sc[iname] = normalize(section_in_sc, ivalue, section_in_sc[iname])

        new_conf[name] = section_in_sc
    return new_conf
