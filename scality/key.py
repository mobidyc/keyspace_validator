# -*- coding: utf-8 -*-
""" @file key.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:23:59 CEST 2012

    @brief key management class
"""

import re
from storelib import storeutils


class ScalKeyException(Exception):
    pass


class Key(object):

    def __init__(self, value):
        if isinstance(value, Key):
            self.key = value.key
        elif isinstance(value, int) or isinstance(value, long):
            self.key = int(value)
        elif isinstance(value, str) or isinstance(value, unicode):
            self.key = int(value, 16)
        else:
            raise ScalKeyException(
                "invalid key (wrong type %s)" % (type(value)))

        if self.key < 0:
            raise ScalKeyException("invalid key (too low)")
        if self.key >= 2 ** 160:
            raise ScalKeyException("invalid key (too high)")

    def getHex(self):
        return "%X" % self.key

    def getHexPadded(self):
        return storeutils.get_padded_key(self.getHex())

    def getClass(self):
        return storeutils.getClass(self.key)

    def getReplica(self):
        return storeutils.getReplica(self.key)

    def getDataCenterPrefix(self):
        return storeutils.getDC(self.key)

    def setReplica(self, replica):
        """ set replica number """
        if replica < 0 or replica > 15:
            raise ScalKeyException("invalid replica")
        self.key = (self.key & ~ 0xF) | replica

    def setClass(self, cls):
        """ Set the CoS on the key """
        if cls < 0 or cls > 15:
            raise ScalKeyException("invalid class")
        self.key = (self.key & ~0xF0) | (cls << 4)

    def prev(self, dclist=None):
        try:
            return list(self.getReplicas(dclist=dclist))[-1]
        except:
            return None

    def next(self, dclist=None):
        try:
            return self.getReplicas(dclist=dclist).next()
        except:
            return None

    def isBetween(self, start, end):
        """ tests if the current key is between start and end """
        start = Key(start)
        end = Key(end)
        if start == end:
            return True
        if start > end:
            if self > start:
                return True
            if self <= end:
                return True
        else:
            if start <= self and self <= end:
                return True
        return False

    def __cmp__(self, a):
        a = Key(a)
        if self.key < a.key:
            return -1
        if self.key == a.key:
            return 0
        return 1

    def getReplicas(self, includeself=False, class1translate=False, dclist=None):
        if self.getHex().endswith("12"):
            class1translate = True  # Force class translation
        translate = class1translate and 1 or 0
        for k in storeutils.get_all_replicas(self.getHex(), translate, includeSelf=includeself):
            res = Key(k)
            if dclist is None:
                yield res
            elif res.getDataCenterPrefix() in dclist:
                yield res

    def __repr__(self):
        return "Key(%s)" % self.getHexPadded()


def KeyRandom(Class="00"):
    return Key(storeutils.genid(Class))


def KeyDiff(k1, k2, maxkey=2**160):
    """ @return the distance between k1 & k2 (similar to k2 - k1) but on the circle """
    k1, k2 = Key(k1), Key(k2)
    if k2 >= k1:
        return k2.key - k1.key
    return k2.key - k1.key + maxkey

def KeyPercent(start, end, pos):
    """ @return where pos is between start & end. 0.0 means pos == start, 1.0 means pos == end
    @note this follows the rule of the balance tasks
    The balance copies all data in the range start,end incrementally (starting from 0 if it exists)
    """
    start, end, pos = Key(start), Key(end), Key(pos)
    if start > end:
        if pos > start:
            diff = KeyDiff(0, end) + KeyDiff(start, pos)
        else:
            diff = KeyDiff(0, pos)
    else:
        diff = KeyDiff(start, pos)
    pct = diff * 1.0 / KeyDiff(start, end)
    pct = max(0.0, min(1.0, pct))
    return pct

def createKeyArc(md5_hash, version=0, sid=0xC0, k=9, m=3, schema=12, replica=0):
    """ create a valid arc key from scratch """
    cos = 7
    if sid < 0 or sid > 255:
        raise ScalKeyException("invalid service id")
    if version < 0 or version >= (1 << 32):
        raise ScalKeyException("invalid version")
    if replica < 0 or replica > 255:
        raise ScalKeyException("invalid replica")
    if k <= 0 or k >= 64:
        raise ScalKeyException("invalid number of data part")
    if m <= 0 or m >= 64:
        raise ScalKeyException("invalid number of coding part")
    if k + m > schema:
        raise ScalKeyException("invalid schema (too low)")
    if schema <= 0 or schema > 255:
        raise ScalKeyException("invalid schema")
    if replica < 0 or replica >= k + m:
        raise ScalKeyException("invalid replica number")
    if not isinstance(md5_hash, int) and not isinstance(md5_hash, long):
        md5_hash = int(md5_hash, 16)
    if md5_hash < 0 or md5_hash >= 1 << 88:
        raise ScalKeyException("invalid hash")

    key = "%.22X" % md5_hash
    key += "%.8X" % version
    key += "%.2X" % sid
    key += "%.3X" % ((k << 6) + m)
    key += "%.2X" % schema
    key += "%.1X" % ((replica & 0xF0) >> 4)
    key += "%.1X" % cos
    key += "%.1X" % (replica & 0xF)

    return Key(key)


class KeyRange(object):

    """ define a key range """

    def __init__(self, start, end):
        self.start = Key(start)
        self.end = Key(end)

    def __eq__(self, range2):
        return self.start == range2.start and self.end == range2.end

    def __cmp__(self, range2):
        if self.start != range2.start:
            return cmp(self.start, range2.start)
        return cmp(self.end, range2.end)

    def getRangeOverlap(self, otherrange):
        """ compute overlap between current range and the other """
        r1_start, r1_end, r2_start, r2_end = self.start, self.end, otherrange.start, otherrange.end
        if r1_start.isBetween(r2_start, r2_end):
            if r1_end.isBetween(r2_start, r2_end):
                if r2_start.isBetween(r1_start, r1_end):
                    return (KeyRange(r2_start, r1_end), KeyRange(r1_start, r2_end))
                else:
                    return (KeyRange(r1_start, r1_end),)
            else:
                return (KeyRange(r1_start, r2_end), )
        else:
            if r1_end.isBetween(r2_start, r2_end):
                return (KeyRange(r2_start, r1_end), )
            elif r2_start.isBetween(r1_start, r1_end):
                return (KeyRange(r2_start, r2_end), )
        return None

    def __repr__(self):
        """ internal """
        return "KeyRange(%s, %s)" % (repr(self.start), repr(self.end))


class CoS(object):

    """ declares a CoS object use to create new key with a given CoS """

    PROPERTIES = {
        0: { "nbreplicas": 1},
        1: { "nbreplicas": 2},
        2: { "nbreplicas": 3},
        3: { "nbreplicas": 4},
        4: { "nbreplicas": 5},
        5: { "nbreplicas": 6},
        6: { "nbreplicas": 3},
        8: { "nbreplicas": 3},
        9: { "nbreplicas": 4, "nbperdc": 1},
        10: { "nbreplicas": 8, "nbperdc": 2},
    }

    def __init__(self, cos, schema=None):
        """ internal """
        if isinstance(cos, (int, long)) and cos in CoS.PROPERTIES:
            self.type_ = "classic"
            self.value = cos
            return
        if isinstance(cos, (unicode, str)) and not cos.startswith("ARC"):
            cos = int(cos, 16)
            if not cos in CoS.PROPERTIES:
                raise ValueError("cos not recognized")
            self.type_ = "classic"
            self.value = cos
            return
        if isinstance(cos, (unicode, str)) and cos.startswith("ARC"):
            m = re.match(r"^ARC([1-9][0-9]?)\+([1-9][0-9]?)$", cos)
            if m:
                self.type_ = "arc"
                self.k = int(m.group(1))
                self.m = int(m.group(2))
                if not schema or self.k + self.m > int(schema):
                    raise Exception("invalid schema")
                self.schema = int(schema)
                return
        raise ValueError("cos not recognized")

    def transformKey(self, key):
        """ returns a key with the current CoS """
        if self.type_ == "classic":
            newkey = Key(key)
            newkey.setClass(self.value)
            newkey.setReplica(0)
            return newkey
        if self.type_ == "arc":
            return createKeyArc(key.getHexPadded()[0:22], k=self.k, m=self.m, schema=self.schema)
        raise NotImplementedError()

    def getNbReplicas(self, nbdc=None):
        if self.type_ == "arc":
            return self.k+self.m
        if self.value <= 5:
            return CoS.PROPERTIES[self.value]["nbreplicas"]
        if self.value >= 9 and self.value <= 11:
            if nbdc is not None:
                assert nbdc >= 1 and nbdc <= 4
                return CoS.PROPERTIES[self.value]["nbperdc"] * nbdc
            return CoS.PROPERTIES[self.value]["nbreplicas"]
        raise NotImplementedError() 

    def __repr__(self):
        """ internal """
        if self.type_ == "classic":
            return "CoS(%d)" % self.value
        if self.type_ == "arc":
            return "CoS(ARC%d+%d, %d)" % (self.k, self.m, self.schema)
        raise NotImplementedError()
