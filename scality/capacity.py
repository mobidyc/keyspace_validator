# -*- coding: utf-8 -*-
""" @file capacity.py
    @author Jonathan Gramain <jonathan.gramain@scality.com>
    @date

    @brief object that represents a storage capacity
"""


class Capacity(object):

    def __init__(self, capacity=0.0, default_unit="TB"):
        if type(capacity) is int:
            self.label = "%d%s" % (capacity, default_unit)
            (self.capacity, self.unit) = Capacity.strToCapacityUnitTuple(self.label, default_unit)
        elif type(capacity) is float:
            self.label = "%.6g%s" % (capacity, default_unit)
            (self.capacity, self.unit) = Capacity.strToCapacityUnitTuple(self.label, default_unit)
        elif type(capacity) in (str,unicode):
            (self.capacity, self.unit) = Capacity.strToCapacityUnitTuple(capacity, default_unit)
            self.label = "%.6g%s" % (self.capacity, self.unit)
        else:
            raise TypeError("Capacity can be built from (int,float,str), not %s" % type(capacity))

    def __str__(self):
        return self.label

    def __int__(self):
        return int(round(self.__float__()))

    def __cmp__(self, cap):
        if not isinstance(cap, Capacity):
            raise TypeError("cap must be a Capacity object, not %s" % type(cap))

        ratio = self.getUnitRatio(self.unit, cap.unit)
        if ratio < 1.0:
            cap1val = self.capacity
            cap2val = cap.capacity / ratio
        else:
            cap1val = self.capacity * ratio
            cap2val = cap.capacity

        if cap1val == 0.0:
            if cap2val > 0.0:
                return -1
            else:
                return 0
        elif cap2val == 0.0:
            return 1

        if cap1val < 1000.0:
            cap2val *= (1000.0 / cap1val)
            cap1val = 1000.0
        if cap2val < 1000.0:
            cap1val *= (1000.0 / cap2val)
            cap2val = 1000.0

        cap1val = int(round(cap1val))
        cap2val = int(round(cap2val))
        if cap1val < cap2val:
            return -1
        elif cap1val > cap2val:
            return 1
        else:
            return 0

    def __eq__(self, cap):
        return self.__cmp__(cap) == 0

    def __ne__(self, cap):
        return self.__cmp__(cap) != 0

    def __gt__(self, cap):
        return self.__cmp__(cap) > 0

    def __lt__(self, cap):
        return self.__cmp__(cap) < 0

    def __ge__(self, cap):
        return self.__cmp__(cap) >= 0

    def __le__(self, cap):
        return self.__cmp__(cap) <= 0

    def __float__(self):
        return self.capacity * Capacity.getUnitRatio(self.unit, "GB")

    def __add__(self, cap):
        if type(cap) is int or type(cap) is float:
            return Capacity(self.capacity + float(cap), self.unit)

        return Capacity(self.capacity +
                        cap.capacity * Capacity.getUnitRatio(cap.unit, self.unit),
                        self.unit)

    def __radd__(self, cap):
        return self.__add__(cap)

    def __mul__(self, by):
        if type(by) not in (int, float):
            raise TypeError("Can't multiply capacity by value of type %s" % type(by))

        return Capacity(self.capacity * by, self.unit)

    def __rmul__(self, by):
        return self.__mul__(by)

    def __div__(self, by):
        if type(by) is int or type(by) is float: # divide capacity
            return Capacity(self.capacity / by, self.unit)
        elif isinstance(by, Capacity): # ratio of capacities
            return self.capacity / (by.capacity * Capacity.getUnitRatio(by.unit, self.unit))
        else:
            raise TypeError("Can't divide capacity by value of type %s" % type(by))


    def toJson(self):
        return self.label

    @staticmethod
    def createFromJson(json_obj):
        return Capacity(capacity=json_obj)


    @staticmethod
    def strToCapacityUnitTuple(capacity_str, default_unit):
        unit_index = None
        for i in xrange(-1, -len(capacity_str)-1, -1):
            if capacity_str[i].isdigit():
                unit_index = i + 1
                break
        if unit_index is None:
            raise ValueError("invalid capacity: %s" % capacity_str)
        if unit_index == 0:
            (num, unit) = (float(capacity_str), default_unit)
        else:
            (num, unit) = (float(capacity_str[0:unit_index]), capacity_str[unit_index:])

        return (num, unit)


    @staticmethod
    def getUnitRatio(unit1, unit2):
        UNIT_TO_GB_POW2 = {
            "B": -30.0,
            "KB": -20.0,
            "MB": -10.0,
            "GB": 0.0,
            "TB": 10.0,
            "PB": 20.0,
            "EB": 30.0
        }
        try:
            dpow = (UNIT_TO_GB_POW2[unit1.upper()] -
                    UNIT_TO_GB_POW2[unit2.upper()])
            return 2.0 ** dpow
        except KeyError:
            raise ValueError("one of capacity units is invalid: %s, %s" % (unit1, unit2))


    @staticmethod
    def strToCapacityGB(capacity_str, default_unit):
        (num, unit) = Capacity.strToCapacityUnitTuple(capacity_str, default_unit)

        return float(num) * Capacity.getUnitRatio(unit, "GB")
