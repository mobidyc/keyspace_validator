# -*- coding: utf-8 -*-
from hashlib import sha1
import hmac
import base64
import urllib
import datetime
import time
import httplib

contentmd5 = ""
contenttype = ""
httpdate = ""
hdrtosign = []
resource = ""

class RS2Admin(object):

    def __init__(self, url="http://localhost:8180", password=None):
        self.conn = httplib.HTTPConnection(url)
        self.password = password


    def __encode(self, str, urlencode=False):
        b64_hmac = base64.encodestring(hmac.new(self.password, str, sha1).digest()).strip()
        if urlencode:
            return urllib.quote_plus(b64_hmac)
        else:
            return b64_hmac

    def list_bucket_user(self, user):

        resource = "/users/%s" % user

        httpdate = datetime.datetime.utcfromtimestamp(time.time()).strftime("%a, %d %b %Y %H:%M:%S +0000")
        headers = {"Date": httpdate}
        method = "GET"

        stringtosign = "%s\n%s\n%s\n%s\n%s%s" % (method, contentmd5, contenttype, httpdate, "\n".join(hdrtosign), resource)

        sig = self.__encode(stringtosign)
        # print "ToSign='%s'" % stringtosign

        headers["Authorization"] = "AWS none:" + sig

        self.conn.request(method, resource, None, headers)
        resp = self.conn.getresponse()

        if resp.status != 200:
            raise Exception(str(resp.status))
        res = resp.read()
        self.conn.close()
        return res

    def create_user(self, user, dname):

        resource = "/users/%s?dname=%s" % (user, dname)

        httpdate = datetime.datetime.utcfromtimestamp(time.time()).strftime("%a, %d %b %Y %H:%M:%S +0000")
        headers = {"Date": httpdate}
        method = "PUT"

        stringtosign = "%s\n%s\n%s\n%s\n%s%s" % (method, contentmd5, contenttype, httpdate, "\n".join(hdrtosign), resource)

        sig = self.__encode(stringtosign)
        # print "ToSign='%s'" % stringtosign

        headers["Authorization"] = "AWS none:" + sig
        headers["Content-Length"] = "0"

        self.conn.request(method, resource, None, headers)
        resp = self.conn.getresponse()

        if resp.status != 200:
            raise Exception(str(resp.status))
        res = resp.read()
        self.conn.close()
        return res

    def delete_user(self, user):

        resource = "/users/%s" % (user)

        httpdate = datetime.datetime.utcfromtimestamp(time.time()).strftime("%a, %d %b %Y %H:%M:%S +0000")
        headers = {"Date": httpdate}
        method = "DELETE"

        stringtosign = "%s\n%s\n%s\n%s\n%s%s" % (method, contentmd5, contenttype, httpdate, "\n".join(hdrtosign), resource)

        sig = self.__encode(stringtosign)
        # print "ToSign='%s'" % stringtosign

        headers["Authorization"] = "AWS none:" + sig

        self.conn.request(method, resource, None, headers)
        resp = self.conn.getresponse()

        if resp.status != 200:
            raise Exception(str(resp.status))
        res = resp.read()
        self.conn.close()
        return res
