#pylint: disable=invalid-name
# -*- coding: utf-8 -*-
""" @file supv2.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Wed Feb  6 17:04:32 CET 2013

    @brief implement methods to command a supv2
"""

import urllib
import logging
import sys
import time

from scality.ov import OvDaemon
from scality.common import ScalDaemonException, json_loads, json_dumps

logger = logging.getLogger("scality")


class Supv2(OvDaemon):

    """ Class to access a supv2 """

    def __init__(self, url="http://localhost:12345",
                 login=None, passwd=None, **kwargs):
        super(Supv2, self).__init__(url, login, passwd, **kwargs)
        self.url = url

    def __get(self, url, method="GET", body=None, **kwargs):
        url = self.url + '/' + url

        if kwargs:
            args = {}
            for name, value in kwargs.iteritems():
                if getattr(value, "__iter__", False):
                    value = ",".join([str(x) for x in value])
                if isinstance(value, bool):
                    value = 1 if value is True else 0
                args[name] = value if value is not None else ""

            url += "?" + urllib.urlencode(args, True)

        res = self._xc.curl_request(url, method=method, body=body)
        if res is None:
            return {}

        try:
            content = json_loads(res)
        except Exception as e:
            raise ScalDaemonException("Unable to parse json data \"%s\"" % res)

        return content

    def getStaticConfig(self):
        """ Return the static configuration of supv2 """
        return self.__get("config/static")

    def getConnectorsList(self, limit=None, offset=None, order_by=None, filters=None):
        params = {}

        if filters is not None:
            params.update(filters)

        if limit is not None:
            params["limit"] = limit

        if offset is not None:
            params["offset"] = offset

        if order_by is not None:
            params["order_by"] = order_by

        return self.__get("api/v1/connector", **params)

    def getConnectorInfo(self, address):
        return self.__get("api/v1/connector/%s" % address)

    def sendConnectorConf(self, address, ctype, conf):
        url = "api/v1/connector/%s/conf/%s" % (address, ctype)
        return self.__get(url, method="POST", body=json_dumps(conf))

    def changeRing(self, address, newring=None):
        if newring is None:
            newring = ""
        url = "api/v1/connector/%s/conf/static/general/ring" % address
        return self.__get(url, method="POST", body=newring)

    def sendConnectorCommand(self, address, command, params=None):
        url = "api/v1/connector/%s/command/%s" % (address, command)

        if params is not None:
            if not isinstance(params, dict):
                raise ScalDaemonException("invalid params type")

            data = urllib.urlencode(params)
        else:
            data = ""

        return self.__get(url, method="POST", body=data)

    def reloadConnector(self, address):
        return self.sendConnectorCommand(address, "reload")

    def createConnectorCatalog(self, address):
        return self.sendConnectorCommand(address, "create_catalog")

    def getBootstrapList(self, dso):
        url = "api/v1/ring/%s/bootstraplist" % dso
        return self.__get(url)

    def reloadConfig(self, module):
        url = "config/%s/reload" % module
        return self.__get(url)

    def listVolumes(self):
        return self.__get("api/v1/volume")

    def getVolume(self, volume_id):
        return self.__get("api/v1/volume/%s" % volume_id)

    def addVolumeConnectors(self, volume_id, connectors=None, role=None):
        if connectors is None or len(connectors) == 0:
            return None

        data = {
            "connectors": connectors,
            "role": role
        }

        url = "api/v1/volume/%s/connectors/add" % volume_id
        return self.__get(url, method="POST", body=json_dumps(data))

    def removeVolumeConnectors(self, volume_id, connectors=None):
        if len(connectors) == 0:
            return None

        data = {
            "connectors": connectors
        }

        url = "api/v1/volume/%s/connectors/remove" % volume_id
        return self.__get(url, method="POST", body=json_dumps(data))

    def getVolumeConnectors(self, volume_id, **kwargs):
        kwargs.update({"method": "GET", "body": None})
        url = "api/v1/volume/%s/connectors" % volume_id
        return self.__get(url, **kwargs)

    def getVolumeAvailableConnectors(self, volume_id=0, **kwargs):
        kwargs.update({"method": "GET", "body": None})
        url = "api/v1/volume/%s/connectors/available" % volume_id
        return self.__get(url, **kwargs)

    def cloneVolumeConnectorsConfig(self, volume_id,
                                    conn_src_id, conn_dest_ids):
        data = {
            "connectors": {
                "src": conn_src_id,
                "dest": conn_dest_ids
            }
        }

        url = "api/v1/volume/%s/connectors/clone_config" % volume_id
        return self.__get(url, method="POST", body=json_dumps(data))

    def fixVolumeConnectorConfig(self, volume_id, conn_id):
        url = "api/v1/volume/%s/connector/%s/config/fix" % (volume_id, conn_id)
        return self.__get(url)

    def setConnectorsDefaultRole(self, roles):
        url = "api/v1/connectors/roles"
        return self.__get(url, method="POST", body=json_dumps(roles))

    def createVolumeCatalog(self, volume_id):
        url = "api/v1/volume/%s/create_catalog" % volume_id
        return self.__get(url)
