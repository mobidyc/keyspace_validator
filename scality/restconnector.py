# -*- coding: utf-8 -*-
""" @file restconnector.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:29:38 CEST 2012

    @brief implementing rest connector interface
"""

from storelib import srws
from scality.key import Key
from scality.ov import OvDaemon
from scality.common import (ScalDaemonException,
                            ScalDaemonExceptionInvalidParameter,
                            ScalDaemonExceptionCommandError)
from scality.common import json_loads


class RestConnector(OvDaemon):

    def __init__(self, url="http://localhost:5580", login=None, passwd=None, srws_address=None, **kwargs):
        super(RestConnector, self).__init__(url, login, passwd, **kwargs)
        if srws_address is not None:
            self._srws = srws.SRWS(srws_address)
        else:
            self._srws = None

    def fixBucket(self, name=None, key=None):
        if name is None and key is None:
            raise ScalDaemonExceptionInvalidParameter("name/key", msg="invalid parameter no key or name")
        if name is not None and key is not None:
            raise ScalDaemonExceptionInvalidParameter("name/key", msg="too many parameters")

        if name:
            et = self._xc.get_ET("restapi bws fix bucket", {"name": name})
        elif key:
            et = self._xc.get_ET(
                "restapi bws fix bucket", {"id": Key(key).getHex()})

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("restapi bws fix bucket", et.text)

        # TODO do not return status, raise an exception
        return {"status": et.find("status").text}

    def checkBucket(self, action=None, target=None, mintime=None):
        params = {}
        if action is not None:
            params["action"] = action
        if target is not None:
            params["target"] = target
        if mintime is not None:
            params["mintime"] = mintime

        et = self._xc.get_ET("restapi bws check buckets", params)

        return self._checkbucket_parse(et)

    @staticmethod
    def _checkbucket_parse(et):
        result = {
            "offload": [],
            "drop": [],
        }
        for n in et.findall("./offload_failures/notification"):
            bucketname = n.find("./mesa_name").text
            hits = n.find("./hits").text
            first_failure = n.find("./first_failure").text.strip()
            last_failure = n.find("./last_failure").text.strip()
            result["offload"].append(
                {"bucketname": bucketname, "hits": hits, "first_failure": first_failure, "last_failure": last_failure})
        for n in et.findall("./drop_failures/notification"):
            bucketname = n.find("./mesa_name").text
            hits = n.find("./hits").text
            first_failure = n.find("./first_failure").text.strip()
            last_failure = n.find("./last_failure").text.strip()
            result["drop"].append({"bucketname": bucketname, "hits": hits,
                                   "first_failure": first_failure, "last_failure": last_failure})
        n = et.find("./job")
        if n:
            start = n.find("./start").text.strip()
            ok = n.find("./ok").text
            nok = n.find("./nok").text
            skip = n.find("./skip").text
            remain = n.find("./remain").text
            result["job"] = {
                "start": start, "ok": ok, "nok": nok, "remain": remain}
        return result

    def convertBucket(self, name):
        if name is None:
            raise ScalDaemonExceptionInvalidParameter("name")
        et = self._xc.get_ET(
            "restapi bws update bucket", {"action": "convert", "name": name})
        status = et.find("./status").text
        # TODO do not return status
        return {"status": status}

    def diagnoseBucket(self, name=None, key=None, verbose=False, counters=False):
        if name is None and key is None:
            raise ScalDaemonExceptionInvalidParameter("name/key", msg="invalid parameter no key or name")
        if name is not None and key is not None:
            raise ScalDaemonExceptionInvalidParameter("name/key", msg="too many parameters")

        params = { "verbose": verbose,
                   "counters": counters }
        if name is not None:
            params["name"] = name
        else:
            params["id"] = Key(key).getHex()

        et = self._xc.get_ET("restapi bws diagnose bucket", params)

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("restapi bws diagnose bucket", et.text)
        if et.find("./status").text != "success":
            raise ScalDaemonExceptionCommandError("restapi bws diagnose bucket", et.find("./reason").text)

        return json_loads(et.find("./diagnose").text)

    def srwsGet(self, key, fd, policy=None, nousermd=False):
        if not self._srws:
            raise ScalDaemonException("no srws conf")
        key = Key(key)
        extra_params = {}
        if policy is not None:
            if policy == "lazy":
                extra_params["x-biz-replica-policy"] = "lazy"
            else:
                raise ScalDaemonExceptionInvalidParameter("policy", policy, "unknown policy")
        if nousermd is True:
            extra_params["x-biz-cmd"] = "nousermd"

        res, contents, resp = self._srws.get(key.getHex(), extra_params)
        if res != True:
            raise ScalDaemonException("cannot get (key do not exists ?)")
        fd.write(contents)
        hdrs = {}
        for (k, v) in resp.getheaders():
            if k.startswith("x-biz-"):
                hdrs[k[len("x-biz-"):]] = v

        return hdrs

    @staticmethod
    def _restconnector_getstatus(cs):
        """ parse a configsection coming from a sagentd or a rest connector """
        res = {
            "global": {},
            "config": {},
        }
        param = cs.getBranch("param")
        if param is None:
            return res
        for i in param.iterValString():
            res["global"][i.name] = i.getValue()

        config = param.getBranch("config")
        if config is None:
            return res
        for i in config.iterBranch():
            for j in i.iterAll():
                res["config"].setdefault(i.name, {})[j.name] = j.getValue()
        return res

    def srwsPut(self, key, fd, usermd=None, split=False, cons=0):
        if not self._srws:
            raise ScalDaemonException("no srws conf")
        key = Key(key)
        extra_params = {}
        if usermd != None:
            extra_params["x-biz-usermd"] = usermd
        if split is True:
            extra_params["x-biz-split"] = "yes"
        if cons != 0:
            if cons == -1:
                extra_params["x-biz-replica-policy"] = "lazy"
            else:
                extra_params["x-biz-replica-policy"] = "wmin=" + str(cons)
        if fd is None and usermd is None:
            raise ScalDaemonExceptionInvalidParameter("fd/usermd")
        if fd is None:
            extra_params["x-biz-cmd"] = "updateusermd"
            res = self._srws.put(key.getHex(), None, extra_params)
        else:
            res = self._srws.put(key.getHex(), fd.read(), extra_params)
        if res != True:
            raise ScalDaemonException("cannot put")

    def srwsDelete(self, key):
        if not self._srws:
            raise ScalDaemonException("no srws conf")
        key = Key(key)
        res = self._srws.delete(key.getHex())
        if res != True:
            raise ScalDaemonException("cannot delete")
