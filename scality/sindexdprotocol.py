# -*- coding: utf-8 -*-
""" @file restconnector.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:30:34 CEST 2012

    @brief implements a sindexd class to contact, retrieve infos from an sindexd daemon
"""

import time
import httplib

from scality.common import json_loads, json_dumps

class sindexdException(Exception):
    """ sindexd Exception class """
    pass


class sindexd(object):
    """ Sample usage :
        from sindexd import sindexd

        s = sindexd("localhost:81", "552A3700000000000000005858BF980300000220", "2", 1482211224, "2")
        s.create("newindexd")
        print s.execute()

        s.load()
        s.get("name")
        s.add("name", "value")
        print s.execute()

        """

    def __init__(self, address, index_id, cos, vol_id, specific):
        """ Init """
        self.address = address
        self.conn = None
        self.req = None
        self.index_id = index_id
        self.cos = cos
        self.vol_id = vol_id
        self.specific = specific
        self.reset()

    def _conn(self, method, uri=None, data=None):
        """ internal """
        if uri == None:
            uri = "http://" + self.address + "/sindexd.fcgi"

        self.conn = httplib.HTTPConnection(self.address)
        if data:
            self.conn.request(method, uri, data)
        else:
            self.conn.request(method, uri)

        return self.conn.getresponse()

    def _load(self, version=None):
        """ internal """
        res = {"index_id": self.index_id, "cos": int(self.cos), "vol_id": int(self.vol_id), "specific": int(self.specific)}
        if version != None:
            res["version"] = int(version)
        return {"load": res}

    def _get(self, key):
        """ internal """
        res = [key]
        return {"get": res}

    def _getprefix(self, prefix="", start_key=None, limit=1000, delimiter=None):
        """ internal """
        res = {"prefix": prefix, "limit": int(limit)}
        if delimiter != None:
            res["delimiter"] = delimiter
        if start_key != None:
            res["marker"] = start_key
        return {"get": res}

    def _rename(self, src, dst):
        """ internal """
        return {"rename": {"from": src, "to": dst}}

    def _copy(self, index_id, cos, vol_id, specific, force=0):
        """ internal """
        return {"copy": {"index_id": index_id, "cos": int(cos), "vol_id": int(vol_id), "specific": int(specific), "force": int(force)}}

    def _delete(self, key, prefetch=False):
        """ internal """
        return {"delete": [key], "prefetch": prefetch}

    def _add(self, key, value, prefetch=False):
        """ internal """
        return {"add": {key: value}, "prefetch": prefetch}

    def _create(self):
        """ internal """
        return {"create": {"index_id": self.index_id, "cos": int(self.cos), "vol_id": int(self.vol_id), "specific": int(self.specific)}}

    def _drop(self, force=0, version=None):
        """ internal """
        res = {"index_id": self.index_id, "cos": int(self.cos), "vol_id": int(self.vol_id), "specific": int(self.specific), "force": int(force)}
        if version != None:
            res["version"] = int(version)
        return {"drop": res}

    def _stats(self, lowlevel=1, highlevel=1):
        """ internal """
        return {"stats": {"lowlevel": lowlevel, "highlevel": highlevel}}

    def _config(self):
        """ internal """
        return {"config": {}}

    def _drain(self):
        """ internal """
        return {"drain": {}}

    def _diagnose(self, verbose=False, counters=False):
        """ internal """
        res = {"index_id": self.index_id, "cos": int(self.cos), "vol_id": int(self.vol_id), "specific": int(self.specific), "verbose": bool(verbose), "counters": bool(counters)}
        return {"diagnose": res}

    def reset(self):
        """ reset current request """
        self.req = [{"hello": {"protocol": "sindexd-1"}}]

    def load(self):
        """ inserts a load statement """
        self.req.append(self._load())

    def stats(self):
        """ inserts a stats statement """
        self.req.append(self._stats())

    def drain(self):
        """ add a drain statement """
        self.req.append(self._drain())

    def config(self):
        """ inserts a stats statement """
        self.req.append(self._config())

    def rename(self, src, dst):
        """ inserts a rename statement """
        self.req.append(self._rename(src, dst))

    def copy(self, index_id, cos, vol_id, specific, force=0):
        """ inserts a copy order """
        self.req.append(self._copy(index_id, cos, vol_id, specific, force))

    def drop(self, force=0):
        """ inserts a drop statement """
        self.req.append(self._drop(force))

    def create(self):
        """ inserts a create statement """
        self.req.append(self._create())

    def getprefix(self, prefix="", start_key=None, limit=1000, delimiter=None):
        """ inserts a getprefix statetement """
        self.req.append(self._getprefix(prefix, start_key, limit, delimiter))

    def execute(self):
        """ execute request
        @returns the json structure
        """
        try:
            resp = self._conn("POST", data=json_dumps(self.req))
        except Exception, e:
            self.reset()
            raise sindexdException("Cannot execute " + str(e))

        self.reset()
        if resp.status != 200:
            raise sindexdException("Cannot execute " + resp.read())

        res = json_loads(resp.read())
        if int(res["status"]) != 200:
            raise sindexdException("Sindexd returned an error " + json_dumps(res, indent=2))

        return res

    def get(self, key):
        """ add a get statement """
        self.req.append(self._get(key))

    def delete(self, key, prefetch=False):
        """ add a delete statement """
        self.req.append(self._delete(key, prefetch))

    def add(self, key, value, prefetch=False):
        """ add an add statement """
        self.req.append(self._add(key, json_dumps(value), prefetch))

    def diagnose(self, verbose=False, counters=False):
        """ inserts a diagnose statement """
        self.req.append(self._diagnose(verbose, counters))
