# -*- coding: utf-8 -*-
""" @file sproxyd.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Tue Jul 24 10:24:50 CEST 2012

    @brief implement methods to command a srebuildd/sproxyd
"""

import re
from base64 import b64encode, b64decode

from scality.common import json_loads
from scality.nasdk import NasdkDaemon
from scality.key import Key
from scality.common import ScalDaemonException


class Sproxyd(NasdkDaemon):

    """ a sproxyd daemon """

    def __init__(self, url="http://localhost:81/proxy/", driver="chord", **kwargs):
        super(Sproxyd, self).__init__(**kwargs)
        self._sproxyd_url = url
        self._driver = driver

    def http_req(self, meth, route, content=None, headers=None):
        return self._http_req(meth, self._sproxyd_url + self._driver + "/" + route, content, headers)

    def put(self, key, fd, usermd=None, encodeusermd=True, cond=None, consistent=False, async_wmin=None):
        key = Key(key)
        headers = {}
        if usermd is not None:
            if encodeusermd:
                headers["x-scal-usermd"] = b64encode(usermd)
            else:
                headers["x-scal-usermd"] = usermd

        if cond:
            headers["x-scal-cond"] = cond

        replica_policies = []
        if consistent:
            replica_policies.append("consistent")
        if async_wmin is not None:
            replica_policies.append("wmin=%d" % async_wmin)

        if replica_policies:
            headers["x-scal-replica-policy"] = ",".join(replica_policies)

        resp = self._http_req(
            "PUT", self._sproxyd_url + self._driver + "/" + key.getHexPadded(), fd.read(), headers)
        if resp.status != 200:
            raise ScalDaemonException("cannot put data, response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        res = {}
        for header, value in resp.getheaders():
            if header.startswith("x-scal-"):
                res[header[len("x-scal-"):]] = value
        return res

    def updatemd(self, key, usermd, encodeusermd=True):
        key = Key(key)
        headers = {}
        headers["x-scal-cmd"] = "update-usermd"
        if encodeusermd:
            headers["x-scal-usermd"] = b64encode(usermd)
        else:
            headers["x-scal-usermd"] = usermd

        resp = self._http_req(
            "PUT", self._sproxyd_url + self._driver + "/" + key.getHexPadded(), None, headers)
        if resp.status != 200:
            raise ScalDaemonException("cannot update metadata, response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        res = {}
        for header, value in resp.getheaders():
            if header.startswith("x-scal-"):
                res[header[len("x-scal-"):]] = value
        return res

    def get(self, key, fd, decodeusermd=True, cond=None):
        key = Key(key)
        headers = {}
        if cond:
            headers["x-scal-cond"] = cond
        resp = self._http_req(
            "GET", self._sproxyd_url + self._driver + "/" + key.getHexPadded(), None, headers)
        if resp.status != 200:
            raise ScalDaemonException("cannot get data, response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        res = {}
        for header, value in resp.getheaders():
            if header.startswith("x-scal-"):
                res[header[len("x-scal-"):]] = value
        if decodeusermd and "usermd" in res:
            res["usermd"] = b64decode(res["usermd"])
        fd.write(resp.read())
        return res

    def delete(self, key, cond=None, consistent=False, async_wmin=None):
        key = Key(key)
        headers = {}

        if cond:
            headers["x-scal-cond"] = cond

        replica_policies = []
        if consistent:
            replica_policies.append("consistent")
        if async_wmin is not None:
            replica_policies.append("wmin=%d" % async_wmin)

        if replica_policies:
            headers["x-scal-replica-policy"] = ",".join(replica_policies)

        resp = self._http_req(
            "DELETE", self._sproxyd_url + self._driver + "/" + key.getHexPadded(), None, headers)
        if resp.status != 200:
            raise ScalDaemonException("cannot delete data, response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        res = {}
        for header, value in resp.getheaders():
            if header.startswith("x-scal-"):
                res[header[len("x-scal-"):]] = value
        return res

    def stat(self, key, cond=None, getusermd=True):
        key = Key(key)
        headers = {}
        if cond:
            headers["x-scal-cond"] = cond
        if getusermd is False:
            headers["x-scal-get-usermd"] = "no"
        resp = self._http_req(
            "HEAD", self._sproxyd_url + self._driver + "/" + key.getHexPadded(), None, headers)
        if resp.status != 200:
            raise ScalDaemonException("cannot stat data, response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        res = {}
        for header, value in resp.getheaders():
            if header.startswith("x-scal-"):
                res[header[len("x-scal-"):]] = value
        return res

    def stats(self):
        """ get stats from sproxyd daemon
        returns a dict() """
        resp = self._http_req("GET", self._sproxyd_url + ".jsonstats")
        if resp.status != 200:
            raise ScalDaemonException(
                "cannot get stats, response code is %s / error is %s" % (
                    resp.status,
                    resp.read()
                ))
        return json_loads(resp.read())

    def config(self):
        """ get configuration from a sproxyd daemon """
        resp = self._http_req("GET", self._sproxyd_url + ".conf")
        if resp.status != 200:
            raise ScalDaemonException(
                "cannot get config, response code is %s / error is %s" % (
                    resp.status,
                    resp.read()
                ))
        return self._parse_config(resp.read())

    @staticmethod
    def _parse_config(data):
        d = data.replace("<br>", "")
        cur_conf_block = "global"
        config = {}
        for l in d.split("\n"):
            l = l.lstrip(" ")
            mo = re.match("^\[(.*)\]", l)
            if mo is not None:
                cur_conf_block = mo.group(1)
                continue
            mo = re.match("(\S+)=(.*)", l)
            if mo is not None:
                config.setdefault(cur_conf_block, {})[mo.group(1)] = mo.group(
                    2).lstrip("\"").rstrip("\"")
                continue
        return config

    def diagnose(self, diagtype, item, verbose=False, counters=False):
        headers = {}
        qs = ""
        if "key" == diagtype:
            key = Key(item)
            qs = diagtype + "=" + key.getHexPadded()
        else:
            qs = diagtype + "=" + item

        if verbose:
            qs = qs + "&verbose=1"

        if counters:
            qs = qs + "&counters=1"

        resp = self._http_req(
            "GET", self._sproxyd_url + self._driver + "/.diagnose?" + qs, None, headers)
        if resp.status != 200:
            raise ScalDaemonException("response code is %s / error is %s" % (
                resp.status,
                resp.read()
            ))
        return json_loads(resp.read())
