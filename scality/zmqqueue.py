# -*- coding: utf-8 -*-
""" A queue replacement using 0MQ """

from Queue import Empty, Full
from tempfile import NamedTemporaryFile
import zmq
import os

try:
    import cPickle as pickle
except ImportError:
    import pickle


class ZMQqueueMeta(type):

    """
    Metaclass for ZMQqueue
    Create a temporaryfile when one ZMQqueue is used and
        delete it when the last ZMQqueue is deleted
    """
    ZMBASE = 0
    ZMTMPFILE = None

    def __new__(mcs, name, bases, attrs):
        def empty(*args, **kwargs):  # pylint: disable=W0613,C0111
            pass
        __old_del__ = attrs.get("__del__", empty)
        __old_init__ = attrs.get("__init__", empty)

        def __del__(*args, **kwargs):
            __old_del__(*args, **kwargs)
            ZMQqueueMeta.cleanup()
        attrs["__del__"] = __del__

        def __init__(*args, **kwargs):
            ZMQqueueMeta.newinstance()
            __old_init__(*args, **kwargs)
        attrs["__init__"] = __init__

        return super(ZMQqueueMeta, mcs).__new__(mcs, name, bases, attrs)

    @staticmethod
    def newinstance():
        """ is called before ZMQqueue.__init__() """
        ZMQqueueMeta.ZMBASE += 1

        if ZMQqueueMeta.ZMTMPFILE is None:
            ZMQqueueMeta.ZMTMPFILE = NamedTemporaryFile(prefix="zmqqueue-")

    @staticmethod
    def cleanup():
        """ is called after ZMQqueue.__del__() """
        ZMQqueueMeta.ZMBASE -= 1

        if ZMQqueueMeta.ZMBASE == 0:
            try:
                ZMQqueueMeta.ZMTMPFILE.close()
            except OSError:
                pass

            ZMQqueueMeta.ZMTMPFILE = None


class ZMQqueue(object):

    """ Create a queue object based on 0MQ.
    Maxsize is not used. It is just there for compatibility
    """
    __metaclass__ = ZMQqueueMeta

    def __init__(self, maxsize=0):
        self.maxsize = maxsize
        self.waiting = []
        self.vsend = None
        self.vrecv = None
        self.context = None

        self.basevalue = ZMQqueueMeta.ZMBASE - 1
        self.name = ZMQqueueMeta.ZMTMPFILE.name

    def __del__(self):
        try:
            os.unlink("%s_%d" % (self.name, self.basevalue))
        except OSError:
            pass

    @staticmethod
    def _poll(sock, timeout, flags):
        """ Internal to choose the right poll interface """
        if hasattr(sock, "poll"):
            return sock.poll(timeout, flags)
        poller = zmq.Poller()
        poller.register(sock, flags)
        fds = dict(poller.poll(timeout))
        return fds.get(sock, 0)

    @staticmethod
    def _hwm(sock):
        """ internal: set high water mark """
        try:
            sock.set_hwm(64)  # Try Api 3.0
        except:
            pass
        else:
            return

        try:
            sock.setsockopt(zmq.HWM, 64)  # Fallback Api 2.0
        except:
            pass
        else:
            return

        try:
            sock.setsockopt(zmq.SNDHWM, 64)  # Fallback Api 2.0 (alt)
            sock.setsockopt(zmq.RCVHWM, 64)  # Fallback Api 2.0 (alt)
        except:
            pass

    def _init_push(self):
        """ internal: init the push context at the last moment """
        if self.vsend:
            return
        self.context = zmq.Context()
        self.vsend = self.context.socket(zmq.PUSH)
        self._hwm(self.vsend)
        self.vsend.connect("ipc://%s_%d" % (self.name, self.basevalue))

    def _init_pull(self):
        """ internal: init pull context at the last moment """
        if self.vrecv:
            return
        self.context = zmq.Context()
        self.vrecv = self.context.socket(zmq.PULL)
        self._hwm(self.vrecv)
        self.vrecv.bind("ipc://%s_%d" % (self.name, self.basevalue))

    def peek(self, block=True, timeout=None):
        """ get a entry but do not remove it from queue """
        if self.waiting:
            return self.waiting[0]
        try:
            obj = self.get(block, timeout)
            self.waiting.append(obj)
            return self.waiting[0]
        except zmq.ZMQError:
            raise Empty

    def qsize(self):
        """ returns qsize (from the consumer PoV) """
        try:
            self.peek(False)
        except Empty:
            return 0

        if self.waiting:
            return len(self.waiting)
        else:
            return 0

    def empty(self):
        """ return True if the queue is waiting for something """
        return self.qsize() == 0

    def full(self):
        """ return True if queue is full """
        if 0 == self._poll(self.vrecv, 0, zmq.POLLOUT):
            return True
        return False

    def put(self, obj, block=True, timeout=None):
        """ put a new obj into queue. obj should be pickleable """
        self._init_push()
        if timeout is not None:
            try:
                res = self._poll(self.vsend, timeout, zmq.POLLOUT)
            except zmq.ZMQError:  # raise full if interrupted by signal
                raise Full
            if res == 0:
                raise Full
        flags = 0
        if block is False:
            flags = zmq.NOBLOCK
        try:
            self.vsend.send(pickle.dumps(obj), flags)
        except zmq.ZMQError:
            raise Full

    def fileno_reader(self):
        try:
            return self.vrecv.getsockopt(zmq.FD)
        except:
            pass

    def fileno_writer(self):
        try:
            return self.vsend.getsockopt(zmq.FD)
        except:
            pass

    def put_nowait(self, obj):
        """ put a object into queue without waiting """
        return self.put(obj, False)

    def get(self, block=True, timeout=None):
        """ get an object from queue """
        self._init_pull()
        if self.waiting:
            return self.waiting.pop(0)

        if timeout is not None:
            try:
                res = self._poll(self.vrecv, timeout, zmq.POLLIN)
            except zmq.ZMQError:  # if interrupted by signal, raise Empty
                raise Empty
            if res == 0:
                raise Empty

        flags = 0
        if block is False:
            flags = zmq.NOBLOCK
        try:
            return pickle.loads(self.vrecv.recv(flags))
        except zmq.ZMQError:
            raise Empty

    def get_nowait(self):
        """ equivalent to get(False) """
        return self.get(False)

    def close(self):
        """ placeholder """
        pass

    def join_thread(self):
        """ placeholder """
        pass

    def cancel_join_thread(self):
        """ placeholder """
        pass
