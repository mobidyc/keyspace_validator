# -*- coding: utf-8 -*-
""" @file config_section.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:20:55 CEST 2012

    @brief implement config_section (aka serialization format of store products)
"""

import time as _time
import copy
from datetime import timedelta, datetime, tzinfo
try:
    from xml.etree import cElementTree as ET
except ImportError:
    try:
        import xml.etree.ElementTree as ET
    except ImportError:
        try:
            import elementtree.ElementTree as ET
        except ImportError:
            raise ImportError(
                "No suitable ElementTree implementation was found.")

# from xml.etree.cElementTree import parse, dump
import calendar
import time
try:
    import cStringIO as StringIO
except:
    import StringIO
import dateutil.parser
# import traceback

CSECTION_ROOT = 0
CSECTION_BRANCH = 1
CSECTION_ATTRTEXT = 2
CSECTION_ATTRINT = 3
CSECTION_ATTRINT64 = 4
CSECTION_ATTRFLOAT = 5
CSECTION_ATTRDOUBLE = 6
CSECTION_TEXTNODE = 7
CSECTION_ATTRIP = 8
CSECTION_RAWNODE = 9
CSECTION_RAWNODENC = 10
CSECTION_FLOATNODE = 11
CSECTION_DOUBLENODE = 12
CSECTION_INTNODE = 13
CSECTION_INT64NODE = 14
CSECTION_IPNODE = 15
CSECTION_BOOLEAN = 16
CSECTION_TIMESTAMPNODE = 17
CSECTION_UNK = 9999


ZERO = timedelta(0)
# HOUR = timedelta(hours=1)


STDOFFSET = timedelta(seconds=-_time.timezone)
if _time.daylight:
    DSTOFFSET = timedelta(seconds=-_time.altzone)
else:
    DSTOFFSET = STDOFFSET

DSTDIFF = DSTOFFSET - STDOFFSET


class LocalTimezone(tzinfo):

    def utcoffset(self, dt):
        if self._isdst(dt):
            return DSTOFFSET
        else:
            return STDOFFSET

    def dst(self, dt):
        if self._isdst(dt):
            return DSTDIFF
        else:
            return ZERO

    def tzname(self, dt):
        return _time.tzname[self._isdst(dt)]

    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day,
              dt.hour, dt.minute, dt.second,
              dt.weekday(), 0, 0)
        stamp = _time.mktime(tt)
        tt = _time.localtime(stamp)
        return tt.tm_isdst > 0

Local = LocalTimezone()


def btoi(b):
    if b == True:
        return 1
    return 0


def itob(i):
    if i == 0:
        return False
    return True


def tval(value):
    new_value = value or ""
    return (len(new_value), new_value)


cs_mapping = {
    CSECTION_INTNODE: {
        "dump_bcs": lambda name, value: "V%.4d%sI%i\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<val><name>%s</name><int>%d</int></val>\n" % (name, value)
    },
    CSECTION_DOUBLENODE: {
        "dump_bcs": lambda name, value: "V%.4d%sD%.6f\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<val><name>%s</name><double>%lf</double></val>\n" % (name, value)
    },
    CSECTION_FLOATNODE: {
        "dump_bcs": lambda name, value: "V%.4d%sF%.6f\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<val><name>%s</name><float>%lf</float></val>\n" % (name, value)
    },
    CSECTION_INT64NODE: {
        "dump_bcs": lambda name, value: "V%.4d%sL%i\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<val><name>%s</name><int64>%d</int64></val>\n" % (name, value)
    },
    CSECTION_ATTRINT: {
        "dump_bcs": lambda name, value: "A%.4d%sI%i\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<attr><name>%s</name><int>%d</int></attr>\n" % (name, value)
    },
    CSECTION_ATTRINT64: {
        "dump_bcs": lambda name, value: "A%.4d%sL%i\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<attr><name>%s</name><int64>%d</int64></attr>\n" % (name, value)
    },
    CSECTION_ATTRTEXT: {
        "dump_bcs": lambda name, value: "A%.4d%sT%.12d%s\n" % ((len(name), name) + tval(value)),
        "dump_xml": lambda name, value: "<attr><name>%s</name><text>%s</text></attr>\n" % (name, tval(value)[1])
    },
    CSECTION_ATTRFLOAT: {
        "dump_bcs": lambda name, value: "A%.4d%sF%lf\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<attr><name>%s</name><float>%lf</float></attr>\n" % (name, value)
    },
    CSECTION_TEXTNODE: {
        "dump_bcs": lambda name, value: "V%.4d%sT%.12d%s\n" % ((len(name), name) + tval(value)),
        "dump_xml": lambda name, value: "<val><name>%s</name><text>%s</text></val>\n" % (name, tval(value)[1])
    },
    CSECTION_RAWNODE: {
        "dump_bcs": lambda name, value: "V%.4d%sR%.12d%s\n" % ((len(name), name) + tval(value)),
        "dump_xml": lambda name, value: "<val><name>%s</name><raw><![CDATA[%s]]></raw></val>\n" % (name, tval(value)[1])
    },
    CSECTION_TIMESTAMPNODE: {
        "dump_bcs": lambda name, value:  "V%.4d%sS%u\n" % (len(name), name, value),
        "dump_xml": lambda name, value: "<val><name>%s</name><timestamp>%s</timestamp></val>" % (name, datetime.fromtimestamp(value, Local).strftime("%Y-%m-%d %H:%M:%S %z %Z"))
    },
    CSECTION_BOOLEAN: {
        "dump_bcs": lambda name, value: "V%.4d%sB%i\n" % (len(name), name, btoi(value)),
        "dump_xml": lambda name, value: "<val><name>%s</name><boolean>%d</boolean></val>\n" % (name, btoi(value))
    }
}


class ConfigSectionException(Exception):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class ConfigSectionObject(object):
    name = "none"
    nodetype = CSECTION_UNK

    def __init__(self, name):
        self.name = name
        self.object_list = []
        self.attr_list = []

    def isBranch(self):
        raise ConfigSectionException("not defined")

    def isRoot(self):
        return False

    def isAttr(self):
        return False

    def getType(self):
        return self.nodetype

    def getName(self):
        return self.name

    def __copy__(self):
        new = type(self)(self.name)
        new.object_list = self.object_list
        new.attr_list = self.attr_list
        return new

    def __deepcopy__(self, memo):
        new = type(self)(self.name)
        new.object_list = [copy.deepcopy(x) for x in self.object_list]
        new.attr_list = [copy.deepcopy(x) for x in self.attr_list]
        return new


class ConfigSectionNode(ConfigSectionObject):
    nodevalue = None

    def isBranch(self):
        return False

    def setType(self, newtype):
        self.nodetype = newtype

    def setValue(self, value):
        self.nodevalue = value

    def getValue(self):
        return self.nodevalue

    def isAttr(self):
        t = self.getType()
        if t == CSECTION_ATTRTEXT or t == CSECTION_ATTRINT or t == CSECTION_ATTRINT64 or t == CSECTION_ATTRFLOAT or t == CSECTION_ATTRDOUBLE:
            return True
        return False

    def getBinary(self):
        t = self.getType()
        if t == CSECTION_UNK:
            raise ConfigSectionException("CS Type invalid")

        if t not in cs_mapping:
            raise ConfigSectionException("CS Type not known")
        return cs_mapping[t]["dump_bcs"](self.name, self.nodevalue)

    def getXml(self):
        t = self.getType()
        if t == CSECTION_UNK:
            raise ConfigSectionException("CS Type invalid")

        if t not in cs_mapping:
            raise ConfigSectionException("CS Type not known")
        try:
            return cs_mapping[t]["dump_xml"](self.name, self.nodevalue)
        except:
            raise ConfigSectionException(
                "CS cannot convert to xml name %s value %s" % (self.name, str(self.nodevalue)))

    def getDict(self):
        if self.isAttr():
            return {"ATTR::" + self.name: [str(self.nodevalue)]}
        return {self.name: [str(self.nodevalue)]}

    def __copy__(self):
        new = super(ConfigSectionNode, self).__copy__()
        new.nodevalue = self.nodevalue
        new.nodetype = self.nodetype
        return new

    def __deepcopy__(self, memo):
        new = super(ConfigSectionNode, self).__deepcopy__(memo)
        new.nodevalue = self.nodevalue
        new.nodetype = self.nodetype
        return new


class ConfigSectionBranch(ConfigSectionObject):
    nodetype = CSECTION_BRANCH

    def isBranch(self):
        return True

    def addBranch(self, name):
        b = ConfigSectionBranch(name)
        self.object_list.append(b)
        return(b)

    def addInt(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_INTNODE)
        n.setValue(value)
        return(n)

    def addFloat(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_FLOATNODE)
        n.setValue(value)
        return(n)

    def addDouble(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_DOUBLENODE)
        n.setValue(value)
        return(n)

    def addAttrInt(self, name, value):
        n = self._addAttrNode(name)
        n.setType(CSECTION_ATTRINT)
        n.setValue(value)
        return(n)

    def addAttrFloat(self, name, value):
        n = self._addAttrNode(name)
        n.setType(CSECTION_ATTRFLOAT)
        n.setValue(value)
        return(n)

    def addInt64(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_INT64NODE)
        n.setValue(value)
        return(n)

    def addAttrInt64(self, name, value):
        n = self._addAttrNode(name)
        n.setType(CSECTION_ATTRINT64)
        n.setValue(value)
        return(n)

    def addAttrText(self, name, value):
        n = self._addAttrNode(name)
        n.setType(CSECTION_ATTRTEXT)
        n.setValue(value)
        return(n)

    def addBool(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_BOOLEAN)
        n.setValue(value)
        return(n)

    def addText(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_TEXTNODE)
        n.setValue(value)
        return(n)

    def addRaw(self, name, value):
        n = self._addNode(name)
        n.setType(CSECTION_RAWNODE)
        n.setValue(value)
        return n

    def addTimestamp(self, name, value):  # accepting unix timestamp
        n = self._addNode(name)
        n.setType(CSECTION_TIMESTAMPNODE)
        n.setValue(value)
        return(n)

    def _addNode(self, name):
        n = ConfigSectionNode(name)
        self.object_list.append(n)
        return(n)

    def _addAttrNode(self, name):
        n = ConfigSectionNode(name)
        self.attr_list.append(n)
        return(n)

    def getBinary(self):
        t = self.getType()
        if t == CSECTION_UNK:
            raise ConfigSectionException("CS Type invalid")

        tmp = ""
        if t == CSECTION_BRANCH:
            tmp = tmp + "B%.4d%s\n" % (len(self.name), self.name)
        elif t == CSECTION_ROOT:
            tmp = tmp + "S%.4d%s\n" % (len(self.name), self.name)

        tmp = tmp + "".join([i.getBinary() for i in self.attr_list]
                            ) + "".join([i.getBinary() for i in self.object_list])
        if t == CSECTION_ROOT:
            tmp = tmp + "s\n"
        elif t == CSECTION_BRANCH:
            tmp = tmp + "b\n"
        return(tmp)

    def getXml(self):
        t = self.getType()
        if t == CSECTION_UNK:
            raise ConfigSectionException("CS Type invalid")
        tmp = ""
        if t == CSECTION_BRANCH:
            tmp = tmp + "<branch>" + "<name>" + self.name + "</name>\n"
        elif t == CSECTION_ROOT:
            tmp = tmp + "<section>" + "<name>" + \
                self.name + "</name>\n<version>2</version>\n"
        else:
            raise ConfigSectionException("cs type invalid")

        for i in self.attr_list:
            tmp = tmp + i.getXml()
        for i in self.object_list:
            tmp = tmp + i.getXml()
        if t == CSECTION_ROOT:
            tmp = tmp + "</section>\n"
        elif t == CSECTION_BRANCH:
            tmp = tmp + "</branch>\n"
        return(tmp)

    def getDict(self):
        t = self.getType()
        if t == CSECTION_UNK:
            raise ConfigSectionException("CS Type invalid")
        result = {}
        for i in self.attr_list:
            res = i.getDict()
            k = res.keys()[0]
            if k in result:
                result[k].extend(res.values()[0])
            else:
                result[k] = []
                result[k].extend(res.values()[0])

        for i in self.object_list:
            res = i.getDict()
            k = res.keys()[0]
            if k in result:
                result[k].extend(res.values()[0])
            else:
                result[k] = []
                result[k].extend(res.values()[0])

        return ({self.name: [result]})

    def _getChildVal(self, name):
        for i in self.object_list:
            if i.getName() == name:
                return i
        return None

    def _getAttrVal(self, name):
        for i in self.attr_list:
            if i.getName() == name:
                return i
        return None

    def _iter(self, t):
        for i in self.object_list:
            if t == i.getType():
                yield i

    def iterBranch(self):
        return self._iter(CSECTION_BRANCH)

    def iterBranchNamed(self, name):
        for branch in self.iterBranch():
            if branch.name == name:
                yield branch

    def iterAll(self):
        for i in self.object_list:
            yield i

    def iterValInt(self):
        return self._iter(CSECTION_INTNODE)

    def iterValInt64(self):
        return self._iter(CSECTION_INT64NODE)

    def iterValString(self):
        return self._iter(CSECTION_TEXTNODE)

    def iterValStringNamed(self, name):
        for text in self.iterValString():
            if text.name == name:
                yield text

    def getValInt(self, name):
        n = self._getChildVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_INTNODE:
            return n.getValue()
        raise ConfigSectionException("invalid type")

    def getValInt64(self, name):
        n = self._getChildVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_INT64NODE:
            return n.getValue()
        raise ConfigSectionException("invalid type")

    def getValString(self, name):
        n = self._getChildVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_TEXTNODE:
            return n.getValue()
        raise ConfigSectionException("invalid type")

    def getValBool(self, name):
        n = self._getChildVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_BOOLEAN:
            return n.getValue()
        raise ConfigSectionException("invalid type")

    def getBranch(self, name):
        n = self._getChildVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_BRANCH:
            return n
        raise ConfigSectionException("invalid type")

    def getAttrString(self, name):
        n = self._getAttrVal(name)
        if n == None:
            return None
        if n.getType() == CSECTION_ATTRTEXT:
            return n.getValue()
        raise ConfigSectionException("invalid type")


class ConfigSection(ConfigSectionBranch):
    nodetype = CSECTION_ROOT

    def isRoot(self):
        return True


def BCSParseBranch(buf, line, cs, root=0):
    if root == 1:
        if cs != None:
            raise ConfigSectionException("Multiple root")

    size_name = int(line[0:4])
    name = line[4:4 + size_name]
    if line[4 + size_name] != "\n":
        raise ConfigSectionException("Invalid key in bcs")
    if root == 1:
        cs = ConfigSection(name)
        BCSParse(buf, cs)
        return cs
    b = cs.addBranch(name)
    BCSParse(buf, b)
    return cs


def BCSParseAttr(buf, line, cs):
    if cs == None:
        raise ConfigSectionException("value without root")
    size_name = int(line[0:4])
    name = line[4:4 + size_name]
    curindex = 4 + size_name
    type_value = line[curindex]
    curindex += 1
    if type_value == "I":
        value = int(line[curindex:])
        cs.addAttrInt(name, value)
    elif type_value == "L":
        value = int(line[curindex:])
        cs.addAttrInt64(name, value)
#    elif type_value == "B":
#        value = itob(int(line[curindex:]))
#        cs.addAttrBool(name, value)
    elif type_value == "T":
        size_v = int(line[curindex:curindex + 12])
        curindex = curindex + 12
        value = line[curindex:curindex + size_v]
        cs.addAttrText(name, value)
    elif type_value == "F":
        value = float(line[curindex:])
        cs.addAttrFloat(name, value)


def BCSParseValue(buf, line, cs):
    if cs == None:
        raise ConfigSectionException("value without root")
    size_name = int(line[0:4])
    name = line[4:4 + size_name]
    curindex = 4 + size_name
    type_value = line[curindex]
    curindex += 1
    if type_value == "I":
        cs.addInt(name, int(line[curindex:]))
    elif type_value == "D":
        cs.addDouble(name, float(line[curindex:]))
    elif type_value == "F":
        cs.addFloat(name, float(line[curindex:]))
    elif type_value == "L":
        cs.addInt64(name, int(line[curindex:]))
    elif type_value == "B":
        cs.addBool(name, itob(int(line[curindex:])))
    elif type_value == "T":
        size_v = int(line[curindex:curindex + 12])
        curindex = curindex + 12
        value = line[curindex:curindex + size_v]
        if len(value) < size_v:
            value += buf.read(size_v - len(value)).decode()
            a = buf.readline()
            if a != "\n":
                raise ConfigSectionException(
                    "BCS value text value not terminated by \\n (%s)" % a)
            #value += buf.readline().decode()
        cs.addText(name, value)
    elif type_value == "R":
        size_v = int(line[curindex:curindex + 12])
        curindex = curindex + 12
        value = line[curindex:curindex + size_v]
        if len(value) < size_v:
            value += buf.read(size_v - len(value))
            # value += buf.readline()
        cs.addRaw(name, value)
        a = buf.readline()
        if a != "\n":
            raise ConfigSectionException(
                "BCS value raw value not terminated by \\n")
    elif type_value == "S":
        v = int(line[curindex:])
        if v > time.time() * 2:
            v = -1
        cs.addTimestamp(name, v)
    else:
        raise ConfigSectionException(
            "BCS value type not recognized (%s / %s)" % (type_value, line))


def BCSParse(buf, cs=None):
    l = buf.readline().decode()
    while True:
        if l[0] == "S":
            cs = BCSParseBranch(buf, l[1:], cs, 1)
            return cs
        elif l[0] == "B":
            cs = BCSParseBranch(buf, l[1:], cs, 0)
        elif l[0] == "b" or l[0] == "s":
            return cs
        elif l[0] == "V":
            BCSParseValue(buf, l[1:], cs)
        elif l[0] == "A":
            BCSParseAttr(buf, l[1:], cs)
        else:
            raise ConfigSectionException("invalid line " + l)
        l = buf.readline().decode()


def XMLParseAttr(cs, n):
    if not n.tag == "attr":
        raise ConfigSectionException("invalid tag " + n.tag)
    if cs == None:
        raise ConfigSectionException("empty cur node")
    try:
        name = n.find("name").text
        if n.find("int64") != None:
            value = int(n.find("int64").text)
            cs.addAttrInt64(name, value)
        elif n.find("int") != None:
            value = int(n.find("int").text)
            cs.addAttrInt(name, value)
        elif n.find("float") != None:
            value = float(n.find("float").text)
            cs.addAttrFloat(name, value)
#        elif n.find("boolean") != None:
#            value = itob(int(n.find("boolean").text))
#            cs.addAttrBool(name, value)
        elif n.find("text") != None:
            value = n.find("text").text
            cs.addAttrText(name, value)
#        elif n.find("timestamp") != None:
# value = n.find("timestamp").text  # in human readable format, must be converted
# value_no = " ".join(value.split(" ")[0:-1])  # removing redundant tz info
#            try:
# value = int(calendar.timegm(dateutil.parser.parse(value_no).utctimetuple()))  # conversion to a timestamp
#            except:
#                value = -1
#            cs.addAttrTimestamp(name, value)
        else:
            raise ConfigSectionException("Not implemented (%s)" % (name))
    except Exception, e:
        raise ConfigSectionException("STUB: not implemented " + str(e))


def _parse_time(cs, name, itext):
    try:
        return int(calendar.timegm(dateutil.parser.parse(" ".join(itext.split(" ")[0:-1])).utctimetuple()))
    except:
        return -1


def XMLParseRec(et, cs):
    """ A slower approach to parsing """

    ops = {
        "int64": lambda cs, name, itext: cs.addInt64(name, int(itext)),
        "float": lambda cs, name, itext: cs.addFloat(name, float(itext)),
        "double": lambda cs, name, itext: cs.addDouble(name, float(itext)),
        "int": lambda cs, name, itext: cs.addInt(name, int(itext)),
        "text": lambda cs, name, itext: cs.addText(name, itext),
        "boolean": lambda cs, name, itext: cs.addBool(name, itob(int(itext))),
        "timestamp": lambda cs, name, itext: cs.addTimestamp(name, _parse_time(cs, name, itext)),
        "raw": lambda cs, name, itext: cs.addRaw(name, itext),
    }

    for n in et.getchildren():
        tag = n.tag
        if tag == "branch":
            name = n.find("name").text
            XMLParseRec(n, cs.addBranch(name))
        elif tag == "val":
            name = n.find("name").text
            for i in n.getchildren():
                if i.tag == "name":
                    continue
                if not i.tag in ops:
                    raise ConfigSectionException("unknown type %s" % (i.tag))
                curnode = ops[i.tag](cs, name, i.text)
                break
        elif tag == "name" or tag == "version":
            pass
        elif tag == "attr":
            XMLParseAttr(cs, n)
        else:
            ET.dump(n)
            raise ConfigSectionException("Node cannot be parsed")


def ETParse(et):
    try:
        root = et.getroot()
    except:
        root = et
    if "section" != root.tag:
        raise ConfigSectionException("Not a CS")
    name = root.find("./name").text
    version = int(root.find("./version").text)
    if version != 2:
        raise ConfigSectionException("Version not recognized")
    cs = ConfigSection(name)
    XMLParseRec(root, cs)
    return cs


def XMLParse(buf):
    et = ET.parse(buf)
    return ETParse(et)


def StringParse(s):
    try:
        buf = StringIO.StringIO(s.encode())
        return BCSParse(buf)
    except Exception, e:
        raise ConfigSectionException("Parse failure " + str(e))


def StringParseXml(s):
    try:
        buf = StringIO.StringIO(s.encode())
        return XMLParse(buf)
    except Exception, e:
        raise ConfigSectionException("Parse failure " + str(e))
