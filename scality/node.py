# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
""" @file node.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:26:27 CEST 2012

    @brief implement methods to command a node
"""

from storelib import chord
from scality.key import Key, KeyPercent
from scality.ov import OvDaemon
from scality.common import (ScalDaemonException,
                            ScalDaemonExceptionInvalidParameter,
                            ScalDaemonExceptionCommandError)


class Node(OvDaemon):

    def __init__(self, url="http://localhost:5580", login=None, passwd=None,
                 chord_addr="localhost", chord_port="4244", dso=None,
                 xcommand_cmp_mode=False, name=None, **kwargs):
        super(Node, self).__init__(
            url, login, passwd, xcommand_cmp_mode, **kwargs)
        if ":" in chord_addr:
            self._chord = chord.ChordCmd(chord_addr)
            self.chord_port = chord_addr.split(":")[1]
        else:
            self._chord = chord.ChordCmd(chord_addr + ":" + str(chord_port))
            self.chord_port = chord_port
        self.dso = dso
        self.vnodeid = 0
        self.url = url
        self.name = name

    def supervisorServerDo(self, action, extra_params=None):
        if action not in ["assignid", "assignparam", "purge", "purge_cancel", "task_cancel", "t1syncstart", "t1syncstop", "join", "sendorder", "notify", "leave", "move", "rebuild", "update", "proxy", "unproxy", "proxylist", "status", "range"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)
        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({"action": action, "vnodeid": str(self.vnodeid)})
        return self._xc.get_xmlcs("supervisor server do", params)

    def nodeGetStatus(self):
        """ return the status of node """
        return self._nodegetstatus_parse(self.supervisorServerDo("status"))

    @staticmethod
    def _nodegetstatus_parse(cs, biziostores=None):
        node = cs.getBranch("vnode")
        res = {}
        # TODO support multiple vnode
        if not node:
            raise ScalDaemonException("invalid node status information (empty)")

        for n in [x for x in node.iterAll() if not x.isBranch()]:
            res[n.name] = n.getValue()
        res["vnodeid"] = res["nodeidx"]
        res["config"] = {}
        for conf_branch in node.getBranch("config").iterBranch():
            conf_name = conf_branch.getName()
            res["config"][conf_name] = dict((x.name, x.getValue())
                                            for x in conf_branch.iterAll()
                                            if not x.isBranch())

        if node.getBranch("realtime_stats"):
            res["realtime_stats"] = dict((x.name, x.getValue()) for x in node.getBranch(
                "realtime_stats").iterAll() if not x.isBranch())
        elif node.getBranch("archiver_stats"):
            res["realtime_stats"] = dict((x.name, x.getValue()) for x in node.getBranch(
                "archiver_stats").iterAll() if not x.isBranch())

        tasklist = []
        for task in [x for x in node.iterBranch() if x.name == "task"]:
            type_ = task.getValString("type")
            curpos = task.getValString("curpos")
            nbtotalchunk = task.getValInt("nbtotalchunk")
            nbchunkdone = task.getValInt("nbchunkdone")
            est_size_total = task.getValInt64("est_size_total")
            size_done = task.getValInt64("size_done")

            if type_ == "move" and curpos:
                start = task.getValString("start")
                end = task.getValString("end")
                try:
                    pct = KeyPercent(Key(start), Key(end), Key(curpos))
                    nbtotalchunk = int(float(nbchunkdone) / pct)
                    est_size_total = int(float(size_done) / pct)
                except:
                    pass
            tasklist.append({
                "taskid": task.getValInt("taskid"),
                "type": type_,
                "nbtotalchunk": nbtotalchunk,
                "nbchunkdone": nbchunkdone,
                "size_done": size_done,
                "est_size_total": est_size_total,
                "starting_time": task.getValInt("starting_time"),
                "status": task.getValString("status")
            })
        res["tasks"] = tasklist
        disks = []

        def __add_disk(disk):
            d = {
                "name": disk.getValString("bizioname"),
                "diskavail": disk.getValInt64("diskavail"),
                "diskfull": disk.getValInt("diskfull"),
                "diskstored":  disk.getValInt64("diskused"),
                "disktotal": disk.getValInt64("disktotal"),
                "diskused": str(int(disk.getValInt64("disktotal")) - int(disk.getValInt64("diskavail"))),
                "fsid0": disk.getValInt("fsid0"),
                "fsid1": disk.getValInt("fsid1"),
                "inodeavail": disk.getValInt64("inodeavail"),
                "inodeused": disk.getValInt64("inodeused"),
                "status": [x.getValue() for x in disk.getBranch("statuslist").iterValString()],
                "nb_io": disk.getValInt("nb_io")
            }
            #
            # Add the biziod statistics if present
            #
            for i in ["records", "queued_task", "pending_task"]:
                d[i] = {}
                b = disk.getBranch(i)
                if None == b:
                    break
                for j in ["finished_ops", "started_ops", "max_concurrent", "total_times", "max_times"]:
                    d[i][j] = disk.getBranch(i).getValInt(j)
                #
                # Add the Json formatted statistics
                #
            b = disk.getBranch("strategy")
            if None != b:
                d["strategy"] = disk.getBranch("strategy").getValString("json")
            disks.append(d)

        for disk in node.iterBranchNamed("biziostore"):
            __add_disk(disk)

        biziostorerefs = node.getBranch("biziostorerefs")
        if biziostorerefs is not None and \
           biziostores is not None:
            for ref in biziostorerefs.iterValStringNamed("ref"):
                disk = biziostores.get(ref)
                if disk is not None:
                    __add_disk(disk)

        res["disks"] = disks

        return [res]

    def nodeAssignId(self, key):
        key = Key(key)
        return self.supervisorServerDo("assignid", {"key": key.getHex()})

    def nodeRebuild(self, range_start="0", number="0", percent=0., flags=0, key=None, mask=None, filter_type=None, filter_value=None):
        start = Key(range_start)

        # using percentage (of keys) instead of number ?
        if number is not None:
            howmany = int(number)

        extra_params = {
            "range_start": start.getHex(), "number": str(howmany), "percent": str(percent), "flags": str(flags)}
        if key is not None:
            extra_params["key"] = key
            if mask is None:
                raise ScalDaemonExceptionInvalidParameter("mask", mask)
            extra_params["mask"] = mask
        elif mask is not None:
            raise ScalDaemonExceptionInvalidParameter("key", key)

        if filter_type is not None:
            extra_params["filter_type"] = filter_type
            extra_params["filter_value"] = filter_value
        return self.supervisorServerDo("rebuild", extra_params=extra_params)

    def nodeUpdate(self):
        return self.supervisorServerDo("update")

    def nodeTaskCancel(self, tid):
        if tid is None or not isinstance(tid, int):
            raise ScalDaemonExceptionInvalidParameter("tid", tid)
        return self.supervisorServerDo("task_cancel", extra_params={"tid": str(tid)})

    def nodePurge(self, ttl, syncmode="nosync", ttlisdate=False, purgeproxied=False, purgequeue=None, flags=None):
        """ launch a purge task on node """
        real_flags = 0
        if flags is not None:
            if not isinstance(flags, int):
                raise ScalDaemonExceptionInvalidParameter("flags", flags)
            real_flags = flags
        else:
            if purgequeue == "queue":
                real_flags |= 256
            elif purgequeue == "fullqueue":
                real_flags |= 512
            elif purgequeue == "tombstone":
                real_flags |= 1024
            elif purgequeue is not None:
                raise ScalDaemonExceptionInvalidParameter("purgequeue", purgequeue)

            if syncmode == "datasync":
                real_flags |= 34
            elif syncmode == "nosync":
                pass
            else:
                raise ScalDaemonExceptionInvalidParameter("syncmode", syncmode)

            if ttlisdate is True:
                real_flags |= 64

            if purgeproxied:
                real_flags |= 4096

        return self.supervisorServerDo("purge", extra_params={"time": ttl, "flags": str(real_flags)})

    def nodeT1syncStart(self):
        return self.supervisorServerDo("t1syncstart")

    def nodeT1syncStop(self):
        return self.supervisorServerDo("t1syncstop")

    _chord_cmds = {
        "check_local": {"result": ["status", "version", "size", "deleted"]},
        "reserve": {"result": ["status", "transacid", "version", "deleted"]},
        "find_successor": {"result": ["status", "address", "id"]},
        "find_nearest_above": {"result": ["res", "status", "nresults", "idlist"]},
        "get_local": {"result": ["res", "version", "contents", "metadata"]},
        "put_local": {"result": ["res"]},
        "put": {"result": ["res"]},
        "check": {"result": ["status", "version", "size", "deleted", "metadata"]},
        "delete_local": {"result": ["res"]},
        "ows_del": {"result": ["res"]},
    }

    def chordSetProxyAuto(self, vnodeid=0):
        """ force proxy """
        et = self._xc.get_ET("chord set proxy auto", {"vnodeid": str(vnodeid)})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord set proxy auto", et.text)

    def checkLocal(self, key):
        """ check_local """
        key = Key(key)
        return self._chord_cmd_generic(cmd="check_local", key=key.getHex())

    def check(self, key):
        """ check """
        key = Key(key)
        return self._chord_cmd_generic(cmd="check", key=key.getHex())

    def findSuccessor(self, key):
        """  find successor """
        key = Key(key)
        return self._chord_cmd_generic(cmd="find_successor", key=key.getHex())

    def findNearestAbove(self, key, maxid="FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", nnearest=10):
        key = Key(key)
        maxid = Key(maxid)
        return self._chord_cmd_generic(cmd="find_nearest_above", key=key.getHex(), maxid=maxid.getHex(), nnearest=nnearest)

    def reserve(self, key, intent, transacid=None):
        """ reserve """
        key = Key(key)
        if intent not in ["insert_request", "read_request", "delete_request", "cancel_request"]:
            raise ScalDaemonExceptionInvalidParameter("intent", intent)
        res = self._chord_cmd_generic(cmd="reserve", key=key.getHex(),
                                      intent=intent, transacid=transacid)
        if res["status"] == "error":
            raise ScalDaemonException("cannot reserve key (%s)" % res["status"])
        return res

    def getLocal(self, key, fd, usermd=None, flags=0):
        """ get_local """
        key = Key(key)
        res = self._chord_cmd_generic(
            cmd="get_local", key=key.getHex(), flags=flags)
        if res["res"] is True:
            res["datalen"] = len(res["contents"])
            fd.write(res["contents"])
            if usermd:
                usermd.write(res["metadata"])
            return res
        raise ScalDaemonException("cannot get_local key")

    def deleteLocal(self, key, version):
        """ delete_local """
        if version is None:
            raise ScalDaemonExceptionInvalidParameter(
                "version", version, "invalid version")

        key = Key(key)
        res = self._chord_cmd_generic(
            cmd="delete_local", key=key.getHex(), version=version)
        if res["res"] is not True:
            raise ScalDaemonException("cannot delete_local key")

    def putLocal(self, key, version, fd, **kwargs):
        """ put_local """
        if version is None:
            raise ScalDaemonExceptionInvalidParameter(
                "version", version, "invalid version")
        key = Key(key)
        res = self._chord_cmd_generic(
            cmd="put_local", key=key.getHex(), version=version, contents=fd.read(), **kwargs)
        if res["res"] is not True:
            raise ScalDaemonException("cannot put_local key")

    def put(self, key, transacid, version, contents=None, metadata=None):
        key = Key(key)
        real_content = None
        if contents is not None:
            real_content = contents.read()
        real_metadata = None
        if metadata is not None:
            real_metadata = metadata.read()
        res = self._chord_cmd_generic(cmd="put", key=key.getHex(
        ), transacid=transacid, version=version, contents=real_content, metadata=real_metadata)
        if res["res"] is not True:
            raise ScalDaemonException("cannot put key")

    def owsDel(self, key):
        key = Key(key)
        res = self._chord_cmd_generic(cmd="ows_del", key=key.getHex())
        if res["res"] is not True:
            raise ScalDaemonException("error in ows_del")

    def _chord_cmd_generic(self, cmd, key, **kwargs):
        cmd = cmd.lower()
        if cmd not in self._chord_cmds:
            raise ScalDaemonExceptionInvalidParameter(
                "cmd", cmd, "cmd not listed")
        if not hasattr(self._chord, cmd):
            raise ScalDaemonExceptionInvalidParameter(
                "cmd", cmd, "cannot find callback")
        methodtocall = getattr(self._chord, cmd)
        r = methodtocall(id=key, **kwargs)
        try:
            [x for x in r]
        except:
            r = (r,)
        res = {}
        for idx, name in enumerate(self._chord_cmds[cmd]["result"]):
            res[name] = None
            try:
                res[name] = r[idx]
            except:
                pass
        return res

    def chordListTombstone(self, fp):
        res = self._xc.get(
            "chordlisttombstone", {"vnodeid": str(self.vnodeid)})
        buf = res.read(65536)
        while len(buf):
            fp.write(buf)
            buf = res.read(65536)
        res.close()

    def listCmd(self, cmd_name, fd, extra_params=None):
        """custom list command with line-oriented results, return the flow
        directly in fd, filtering only empty lines

        """

        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({"dso": self.dso, "vnode": self.chord_port})

        res = self._xc.get(cmd_name, params)

        c = res.readline()
        while c:
            if c != "\n":
                # skip empty line (keep alive line)
                fd.write(c)
            c = res.readline()
        res.close()

    def listCmdIter(self, cmd_name, extra_params=None):
        """custom list command with line-oriented results, return results via
        an iterator, returns line by line

        """
        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({"dso": self.dso, "vnode": self.chord_port})
        res = self._xc.get(cmd_name, params)
        line = res.readline()
        while len(line):
            yield line
            line = res.readline()
        res.close()

    def listKeys(self, fd, extra_params=None):
        """ classic listKeys, returns the flow directly without filtering """

        self.listCmd("listkeys", fd, extra_params)

    def listKeysIter(self, extra_params=None):
        """ listKeys via an iterator, returns line by line """

        for line in self.listCmdIter("listkeys", extra_params):
            yield line

    def listDiskOrphans(self, fd, extra_params=None):
        """ classic listDiskOrphans, returns the flow directly without filtering """

        self.listCmd("listdiskorphans", fd, extra_params)

    def listDiskOrphansIter(self, extra_params=None):
        """ listDiskOrphans via an iterator, returns line by line """

        for line in self.listCmdIter("listdiskorphans", extra_params):
            yield line

    def clearDiskOrphans(self, bizioname):
        """ command to clean the list of disk orphans in memory associated to a given biziod """

        params = {"dso": self.dso,
                  "vnode": self.chord_port,
                  "bizioname": str(bizioname)}

        et = self._xc.get_ET("chunkapi diskorphans clear", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi diskorphans clear", et.text)
        res = et.find("result")
        status = res.find("status").text
        if status != "ok":
            raise ScalDaemonExceptionCommandError("chunkapi diskorphans clear",
                                                  "status is %s" % status)
        return int(res.find("count_removed").text)

    def chunkapiStoreOp(self, op, key, dso=None, port=None, extra_params=None):
        key = Key(key)
        params = {}
        if port is None:
            port_ = self.chord_port
        else:
            port_ = port
        if dso is None:
            dso_ = self.dso
        else:
            dso_ = dso
        if port_ is None or dso_ is None:
            raise ScalDaemonExceptionInvalidParameter(
                "port/dso", "None", "should define one of them")
        if extra_params:
            params.update(extra_params)
        params.update(
            {"dso": dso_, "vnode": port_, "op": op, "chunkid": key.getHexPadded()})

        et = self._xc.get_ET("chunkapi store op", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store op", et.text)
        return et

    def chunkapiStoreOpDelete(self, key, version, dso=None, port=None):
        if version is None:
            raise ScalDaemonExceptionInvalidParameter("version", version)
        et = self.chunkapiStoreOp("delete", Key(
            key), dso=dso, port=port, extra_params={"version": str(int(version))})
        return {"status": et.find("result").find("status").text}

    def chunkapiStoreOpPhysDelete(self, key, raw=False, dso=None, port=None):
        op = "physdelete"
        if raw is True:
            op = "physdelete_raw"
        et = self.chunkapiStoreOp(op, Key(key), dso=dso, port=port)
        status = et.find("result").find("status").text
        if status != "CHUNKAPI_STATUS_OK":
            raise ScalDaemonExceptionCommandError("chunkapi store op physdelete",
                                                  "status is %s" % status)

    def chunkapiStoreBizioReload(self, name, force_reload=False):
        if name is None:
            raise ScalDaemonExceptionInvalidParameter("name", name)
        fr = "0"
        if force_reload is True:
            fr = "1"
        et = self._xc.get_ET("chunkapi store bizio reload", {
                             "dso": self.dso, "vnode": self.chord_port, "bizioname": name, "force_reload": fr})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store bizio reload", et.text)

    def chordAddBizio(self, bizioname):
        if bizioname is None:
            raise ScalDaemonExceptionInvalidParameter("bizioname", bizioname)
        et = self._xc.get_ET("chord add bizio", {"vnodeid": str(
            self.vnodeid), "bizioname": bizioname, "dso": self.dso, "port": str(self.chord_port)})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord add bizio", et.text)
        status = et.find("result/status").text
        if status != "ok":
            raise ScalDaemonExceptionCommandError("chord add bizio", "status is %s" % status)

    def chordRemoveBizio(self, bizioname):
        if bizioname is None:
            raise ScalDaemonExceptionInvalidParameter("bizioname", bizioname)
        et = self._xc.get_ET("chord remove bizio", {"vnodeid": str(
            self.vnodeid), "bizioname": bizioname, "dso": self.dso, "port": str(self.chord_port)})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord remove bizio", et.text)
        status = et.find("result/status").text
        if status != "ok":
            raise ScalDaemonExceptionCommandError("chord remove bizio", "status is %s" % status)

    def chunkapiStoreCrawl(self, key, mask, nkeysperbatch=200):
        key = Key(key)
        mask = Key(mask)

        et = self._xc.get_ET("chunkapi store crawl", {"dso": self.dso,
                                                      "vnode": self.chord_port,
                                                      "key": key.getHex(),
                                                      "mask": key.getHex(),
                                                      "nkeysperbatch": nkeysperbatch})

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store crawl", et.text)

        return self._chunkapistorecrawl_parse(et)

    @staticmethod
    def _chunkapistorecrawl_parse(et):
        result = []
        for s in et.findall("result"):
            key = s.find("key").text
            disk = s.find("disk").text
            result.append({"key": key, "disk": disk})
        return result

    def chunkapiStoreOpen(self, strategy, strategy_params, sflags=0, io_blk_size=1048576):
        et = self._xc.get_ET("chunkapi store open",
                             {"dso": self.dso,
                              "vnode": str(self.chord_port),
                              "strategy": strategy,
                              "strategy_params": strategy_params,
                              "sflags": str(sflags),
                              "io_blk_size": str(io_blk_size)})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store open", et.text)
        status = et.find("result").find("status").text
        if status != "ok":
            raise ScalDaemonExceptionCommandError("chunkapi store open", "status is %s" % status)

    def chordWalkkeys(self, key, mask, extra_params=None):
        key = Key(key)
        mask = Key(mask)
        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({
            "vnodeid": str(self.vnodeid),
            "key": key.getHex(),
            "mask": mask.getHex()
        })
        et = self._xc.get_ET("chord walkkeys", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord walkkeys", et.text)
        if et.find("./res").text == "ok":
            return {"filename": et.find("./filename").text}
        else:
            status = et.find("./res").text.strip()
            raise ScalDaemonExceptionCommandError("chord walkkeys", "status is %s" % status)

    def chordDumpFingerTable(self):
        et = self._xc.get_ET("chord dump finger table", {})
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord dump finger table", et.text)

        return self._chorddumpfingertable_parse(et)

    @staticmethod
    def _chorddumpfingertable_parse(et):

        n = et.find("./vnode/predecessor")
        predecessor = {
            "ntype": "",
            "initialized": n.findtext("./initialized", ""),
            "ip": n.findtext("./ip", ""),
            "port": n.findtext("./port", ""),
            "key": n.findtext("./key", "")
        }

        successors = []
        for n in et.findall("./vnode/successors/successor"):
            successors.append({
                "pos": n.findtext("./pos", ""),
                "initialized": n.findtext("./initialized", ""),
                "ip": n.findtext("./ip", ""),
                "port": n.findtext("./port", ""),
                "key": n.findtext("./key", "")
            })

        fingers = []
        for n in et.findall("./vnode/fingers/finger"):
            fingers.append({
                "pos": n.findtext("./pos", ""),
                "initialized": n.findtext("./initialized", ""),
                "ip": n.findtext("./ip", ""),
                "port": n.findtext("./port", ""),
                "start": n.findtext("./start", ""),
                "successor": n.findtext("./successor", "")
            })

        return {"predecessor": predecessor, "successors": successors, "fingers": fingers}

    def chunkapiStoreOpPut(self, op, key, version, md=None, fd=None):
        key = Key(key)
        if fd is None:
            raise ScalDaemonExceptionInvalidParameter("fd", fd)
        if op not in ["put", "put_rebuild", "put_raw"]:
            raise ScalDaemonExceptionInvalidParameter("op", op)

        et = self._xc.get_ET("chunkapi nop", {})

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi nop", et.text)

        contents = fd.read()

        params = {
            "op": op,
            "chunkid": key.getHexPadded(),
            "version": version,
            "dso": self.dso,
            "vnode": self.chord_port
        }
        if md is not None:
            params.update(md)
        et = self._xc.get_ET("chunkapi store op", params, postdata=contents)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store op (%s)" % op, et.text)
        status = et.find("./result/status").text
        if status != "CHUNKAPI_STATUS_OK":
            raise ScalDaemonExceptionCommandError("chunkapi store op (%s)" % op,
                                                  "status is %s" % status)

    def chunkapiStoreBizioCleanupoos(self, bizioname=None, alldisks=False):
        """ command to remove entries in the bad & oos lists on a given node """
        if not bizioname and alldisks is False:
            raise ScalDaemonExceptionInvalidParameter("bizioname", bizioname)

        if alldisks is True and bizioname:
            raise ScalDaemonExceptionInvalidParameter("alldisks", alldisks)

        params = {"dso": self.dso, "vnode": self.chord_port}
        if bizioname:
            params["bizioname"] = str(bizioname)
        else:
            params["alldisks"] = 1

        et = self._xc.get_ET("chunkapi store bizio cleanupoos", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord store bizio cleanupoos", et.text)
        return

    def chordDumpProxyTable(self):
        et = self._xc.get_ET("chord dump proxy table", {})

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chord dump proxy table", et.text)

        return self._chordumpproxytable_parse(et)

    @staticmethod
    def _chordumpproxytable_parse(et):
        res = {}
        for n in et.findall("./vnode"):
            vnodeid = n.findtext("./vindex", "0")
            dsoname = n.findtext("./dsoname", "")
            res[vnodeid] = {"dsoname": dsoname, "proxies": []}
            for p in n.findall("./proxy"):
                res[vnodeid]["proxies"].append(
                    {
                        "target": p.findtext("target", ""),
                        "start": p.findtext("start", ""),
                        "end": p.findtext("end", ""),
                        "type": p.findtext("type", ""),
                        "expirationtime": p.findtext("expirationtime", ""),
                        "expirationdelta": p.findtext("expirationdelta", "")
                    })

        return res

    def chordArcRebuildStatus(self, newvalue=None):
        """ Get/set chord arc rebuild status """
        params = {
            "vnodeid": "0",
        }
        if newvalue is not None:
            params["newvalue"] = str(newvalue)
        et = self._xc.get_ET("chord arc rebuild status", params)
        previous = et.findtext("./previous", "0")
        new = et.findtext("./new", None)
        return { "previous": previous, "new": new }

    def chordBlacklistIp(self, ipaddr, time_s=25):
        """ xcommand to remove and timeout an IP addr on node Chord
        @return dict with status and statistics """
        if ipaddr is None:
            raise ValueError("invalid ipaddr value")
        et = self._xc.get_ET("chord blacklist ip", {"ipaddr": ipaddr, "time_s": str(time_s)})
        return self._chordblacklistip_parse(et)

    @staticmethod
    def _chordblacklistip_parse(et):
        """ helper/parsing function for chord blacklist ip xcommand """
        res = {}
        res["status"] = et.findtext("./result/status", "nok")
        for s in ["nr_timeout", "nr_fingerfixed", "nr_proxies"]:
            res[s] = int(et.findtext("./" + s, 0))
        return res

    def chordUnblacklistIp(self, ipaddr=None):
        """ xcommand to re-authorize a previously blacklisted IP
        @return dict with status and statistics """
        if ipaddr is not None:
            extra_params = {"ipaddr": ipaddr}
        else:
            extra_params = {}
        et = self._xc.get_ET("chord unblacklist ip", extra_params)
        return self._chordblacklistip_parse(et)

    def chunkapiStoreRangeEstimate(self, start, end):
        start, end = Key(start), Key(end)
        et = self._xc.get_ET("chunkapi store range estimate",
                             {"dso": self.dso,
                              "vnode": self.chord_port,
                              "start": start.getHex(),
                              "end": end.getHex()}
        )
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("chunkapi store range estimate", et.text)

        return self._chunkapistorerangeestimate_parse(et)

    @staticmethod
    def _chunkapistorerangeestimate_parse(et):
        return et.find("distance").text
    
    def chordKeyGetinfo(self, key):
        """ xcommand to get the location of key (disk & instanceid)
        key must exist """
        key = Key(key)
        et = self._xc.get_ET("chord key getinfo", params={"vnodeid":0, "key": key.getHexPadded()})
        if et.findtext("status", "unk") != "found":
            raise ScalDaemonExceptionCommandError("chord key getinfo",
                                                  "cannot get info for %s: %s" % (key.getHexPadded(),
                                                                                  et.findtext("status", "unk")))
        return {
            "disk": et.findtext("./bizioname", ""),
            "instanceid": et.findtext("instanceId", "").zfill(8),
        }

if __name__ == "__main__":
    pass
