# -*- coding: utf-8 -*-
""" @file ov.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:28:20 CEST 2012

    @brief module implementing common methods to OV daemons (do not use directly)
"""

from scality import xcommand
from scality.common import (ScalDaemonException,
                            ScalDaemonExceptionInvalidParameter,
                            ScalDaemonExceptionCommandError)
from scality.config_section import ConfigSection


LEVELMAP = {"critical": "1",
            "error": "2",
            "warning": "3",
            "normal": "4",
            "verbose": "5",
            "debug": "6"
           }

class OvError(object):  # pylint: disable=too-few-public-methods
    """ represent a new-style error from OV """
    def __init__(self, type_, msg, param=None):
        self.type_ = type_
        self.msg = msg
        self.param = param

    def __str__(self):
        if self.param:
            return "type: %s param: %s (%s)" % (self.type_, self.param, self.msg)
        else:
            return "type: %s (%s)" % (self.type_, self.msg)

    def __repr__(self):
        return str(self)

    def get_exception(self, command):
        """ returns a suitable exception to raise """
        if self.type_ in ["wrong-param", "missing-param"]:
            return ScalDaemonExceptionInvalidParameter(self.param, msg=self.msg)
        return ScalDaemonExceptionCommandError(command, self.msg)


class OvDaemon(object):

    """ a generic daemon based on OV framework """

    def __init__(self, url="http://localhost:5580",
                 login=None, passwd=None, xcommand_cmp_mode=False):
        if type(self) == OvDaemon:
            raise ScalDaemonException("no direct instanciation")
        self._xc = xcommand.XCommand(url=url, login=login, passwd=passwd,
                                     cmp_mode=xcommand_cmp_mode)

    def configViewModule(self):
        """ call config view module """
        et = self._xc.get_ET(
            "config view module", {"module": "ov_core_config"})
        return self._config_parse(et)

    @staticmethod
    def _config_parse(et):
        """ internal """
        cfg = {}
        for env in et.findall(".//env"):
            try:
                name = env.find("./name").text
                module = env.find("./module").text
                value = env.find("./value").text
                ref = env.find("./ref").text
                cfg.setdefault(module, {})[name] = {"value": value, "ref": ref}
            except:
                pass
        return cfg

    @staticmethod
    def _config_update_parse(et):
        """ internal """
        cfg = {}
        module_name = et.findtext("./module")
        cfg[module_name] = {}
        for env in et.findall(".//env"):
            try:
                name = env.find("./name").text
                value = env.find("./value").text
                cfg[module_name][name] = {"value": value, "ref": "0"}
            except:
                pass
        return cfg

    def configUpdateModule(self, module, values):
        """ updates config options of a single module
        via config update module """
        if module is None:
            raise ScalDaemonExceptionInvalidParameter("module")
        if not values:
            raise ScalDaemonExceptionInvalidParameter("values")

        params = {"module": str(module)}
        for option in values:
            params["e[" + str(option) + "]"] = str(values[option])
        et = self._xc.get_ET("config update module", params)
        return self._config_update_parse(et)

    def statTreeView(self, delay=None, raw=False):
        """ get stat tree values """
        params = {}
        if delay is not None and int(delay) > 0:
            if int(delay) > 300:
                raise ScalDaemonExceptionInvalidParameter("delay", delay)
            params["delta_s"] = str(int(delay))

        if raw is True:
            params["raw"] = "1"

        et = self._xc.get_ET("stat tree view", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("stat tree view", et.text)

        return self._stattree_parse(et)

    @staticmethod
    def _stattree_parse(et):
        """ internal """
        result = {}
        for node in et:
            items = {}
            try:
                _ = [items.__setitem__(x.tag, str(x.text))
                     for x in node.getchildren()]
                p_type = items["type"]
                if p_type == "SEP":
                    continue
                name = items["name"]
                if name is None:
                    continue
                desc = items["descr"]
                path = items["path"]
                value_prev = items.get("value_prev", None)
                p_value = items.get("value", None)
                if p_type == "TIMEELAPSED":
                    value_time = items.get("value_time", None)
                    value_nb = items.get("value_nb", None)
                    if value_time is not None and value_nb is not None:
                        if value_prev:
                            result.setdefault(path, {})[name] = {
                                "type": p_type, "descr": desc, "value": p_value, "value_time_spent": value_time, "value_count": value_nb, "value_prev": value_prev}
                        else:
                            result.setdefault(path, {})[name] = {
                                "type": p_type, "descr": desc, "value": p_value, "value_time_spent": value_time, "value_count": value_nb}
                        continue
                if p_value:
                    result.setdefault(path, {})[name] = {
                        "type": p_type, "descr": desc, "value": p_value, "value_prev": value_prev}
                else:
                    result.setdefault(path, {})[name] = {
                        "type": p_type, "descr": desc, "value": p_value}
            except:
                continue
        return result

    def reload(self, quick=False):
        """ reload a daemon """
        params = {"action": "doit"}
        if quick is True:
            params["action"] = "doquickrestart"
        et = self._xc.get_ET("daemon reload", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("daemon reload", et.text)

    def configLogsMain(self):
        """ get logs configuration """
        et = self._xc.get_ET("config logs main", {})
        return self._configlogs_parse(et)

    def configLogsMainMod(self, value):
        """ modify logs configuration """
        if not isinstance(value, dict):
            raise ScalDaemonExceptionInvalidParameter("value")

        logs = self.configLogsMain()
        logs.update(value)

        reqparams = {"action": "update"}
        for name in logs.keys():
            if not "loglevel" in logs[name]:
                raise ScalDaemonExceptionInvalidParameter("logs")
            lvl = logs[name]["loglevel"]
            if not lvl in LEVELMAP:
                raise ScalDaemonExceptionInvalidParameter("logs")
            reqparams["level[" + name + "]"] = LEVELMAP[lvl]
            if logs[name]["logfile"] is True:
                reqparams["fileenabled[" + name + "]"] = "1"
            if logs[name]["syslog"] is True:
                reqparams["syslogenabled[" + name + "]"] = "1"

        et = self._xc.get_ET("config logs main", reqparams)
        return self._configlogs_parse(et)

    @staticmethod
    def _configlogs_parse(et):
        """ internal """
        logs = {}
        for env in et.findall(".//log"):
            try:
                name = env.find("./name").text
                level = env.find("./level").text
                fileenabled = env.find("./fileenabled").text
                syslogenabled = env.find("./syslogenabled").text
                found = False
                for levelname, levelvalue in LEVELMAP.items():
                    if levelvalue == level:
                        found = True
                        level = levelname

                if found is False:
                    level = "unknown"

                if fileenabled == "1":
                    fileenabled = True
                else:
                    fileenabled = False
                if syslogenabled == "1":
                    syslogenabled = True
                else:
                    syslogenabled = False

                logs[name] = {"loglevel": level,
                              "logfile": fileenabled,
                              "syslog": syslogenabled}
            except:
                continue
        return logs

    @staticmethod
    def _create_request(command):
        cs = ConfigSection("command")
        b = cs.addBranch("cmd_desc")
        b.addText("name", command)
        b.addText("type", "REQUEST")
        b.addInt("vnodeid", 0)
        b.addBranch("data")
        return cs.getBinary()

    def showNetworkServersAll(self):
        et = self._xc.get_ET("show network servers all", {}, path="/docommand")
        return self._shownetworkserversall_parse(et)

    @staticmethod
    def _shownetworkserversall_parse(et):
        res = []
        for ent in et.findall("./ent"):
            res.append({
                "name": ent.findtext("name", ""),
                "ipsrc": ent.findtext("ipsrc", ""),
                "port": ent.findtext("port", ""),
                "type": ent.findtext("type", ""),
                "nbpendingsessions": ent.findtext("nbpendingsessions", "0"),
                "nbactivesessions": ent.findtext("nbactivesessions", "0"),
                "nbinactivesessions": ent.findtext("nbinactivesessions", "0"),
                "nbreadsessions": ent.findtext("nbreadsessions", "0"),
                "nbwritesessions": ent.findtext("nbwritesessions", "0"),
                "maxsessions": ent.findtext("maxsessions", "0"),
            })
        return res

    def getVersion(self):
        """ retrieve daemon's version using an xcommand"""
        xml_response = self._xc._get_ET("xcommand help", {"xcommand": "help"})
        response_root = xml_response.tree.getroot()
        daemon_branch = response_root.find("info").find("daemon")
        if hasattr(daemon_branch, "iter"): # python 2.7
            iterator = daemon_branch.iter()
        else: # python 2.6
            iterator = daemon_branch.getiterator()
        return dict((elem.tag, elem.text)
                    for elem in iterator if elem.tag != "daemon")

    @staticmethod
    def getError(et):
        """ parse the error from xcommand.
        Try to implement all mechanisms (type + (optional)param + (optional)msg or only msg)
        """
        error_tag = None
        if et.tag == "reserror":
            error_tag = None
        elif et.find("reserror"):
            error_tag = et.find("reserror")

        if error_tag is not None:
            if error_tag.findtext("type"):
                return OvError(error_tag.findtext("type", "generic"), error_tag.findtext("msg", ""), error_tag.findtext("param", None))
            else:
                return OvError("generic", error_tag.text)
        return None

    def showMemoryUsage(self):
        """ returns the memory usage of an ov daemon.
        Please note this will only report registered memory """
        memusage = self._xc.get_ucmd_ET("show memory usage", {})

        res = {"modules": []}

        for ent in memusage.findall("./ent"):
            modulestats = dict((x.tag, x.text) for x in ent.getchildren())
            modulestats["size"] = int(modulestats["size"])
            res["modules"].append(modulestats)

        res["xmemorytotalused"] = int(memusage.findtext("./xmemorytotalused"))
        res["percentprocess"] = float(memusage.findtext("./percentprocess"))

        res["active"] = memusage.findtext("./active") == "yes"
        res["callsactive"] = memusage.findtext("./callsactive") == "yes"
        return res

