# -*- coding: utf-8 -*-
"""
@descr read, parse, validate and allow manipulation of a exports.conf file
@author Benoit A. <benoit@scality.com>
@date Thu Mar 26 10:04:06 CET 2015
"""

import re
from StringIO import StringIO

EXPORTSOPTIONS = {
    "ro": {"param": False},
    "rw": {"param": False},
    "access": {"param": True, "valid": lambda x: x == "rw" or "ro"},
    "root_squash": {"param": False},
    "no_root_squash": {"param": False},
    "all_squash": {"param": False},
    "no_squash_all": {"param": False},
    "resvport": {"param": False},
    "noresvport": {"param": False},
    "anonuid": {"param": True, "valid": lambda x: int(x) is not None},
    "anongid": {"param": True, "valid": lambda x: int(x) is not None},
    "sec": {"param": True, "valid": lambda x: (x in ("none", "unix", "sys", "krb5", "krb5i", "krb5p"))},
}


class ExportsConfLine(object):
    regexp = {
        "path": re.compile(r"""
\s* (?P<path>/\S*) \s+ (?P<hosts>.*) \s*
""", re.X),

        "hosts": re.compile(r"""
(?P<host>[^\s\(]+)
\s*
(?:
  \(
    \s* (?P<options>[^\)]*?) \s*
  \)
)?
""", re.X)
    }

    def __init__(self, value):
        self.value = value

    def iscomment(self):
        """ @return True if the line is a comment """
        if self.value.lstrip("\t ").startswith("#"):
            return True
        return False

    def isempty(self):
        """ @return True if the line is empty """
        if len(self.value.lstrip("\t ")) <= 0:
            return True
        return False

    @staticmethod
    def _validate_options(options):
        """ internal """
        if not options:
            return None

        all_options = []
        for option in re.split("\s*,\s*", options):
            if "=" in option:
                key, value = option.split("=", 1)
            else:
                key = option
                value = None

            if key in EXPORTSOPTIONS:
                if value is None:
                    is_valid = EXPORTSOPTIONS[key]["param"] is False
                else:
                    is_valid = EXPORTSOPTIONS[key]["param"] is True and  \
                               EXPORTSOPTIONS[key]["valid"](value)
            else:
                is_valid = False

            if not is_valid:
                raise Exception("invalid option: %s" % option)

            if value is not None:
                option = "%s=%s" % (key, value)
            else:
                option = key
            all_options.append(option)

        return ", ".join(all_options)

    def __str__(self):
        """ internal """
        return self.value

    def parse(self):
        if self.isempty():
            return {
                "empty": True,
                "original": self.value
            }

        if self.iscomment():
            return {
                "comment": True,
                "original": self.value
            }

        m = ExportsConfLine.regexp["path"].match(self.value)
        if not m:
            raise Exception("Invalid line \"%s\"" % self.value)

        path, hosts = m.group("path", "hosts")

        res = {
            "mountpoint": path,
            "hosts": []
        }

        for m in ExportsConfLine.regexp["hosts"].finditer(hosts):
            hostname, options = m.group("host"), m.group("options")
            res["hosts"].append(
                (hostname, self._validate_options(options))
            )

        if not res["hosts"]:
            raise Exception("Invalid line \"%s\"" % self.value)

        return {
            "token": res,
            "original": self.value
        }

    @classmethod
    def _fromToken(cls, token):
        """ internal """
        def _format_host_options(h, o):
            if o is not None:
                return "%s(%s)" % (h, o)
            return h
        return " ".join([token["mountpoint"]] + [_format_host_options(x, y) for x, y in token["hosts"]])

    def toPython(self):
        return self.parse()

    @classmethod
    def fromPython(cls, value):
        if value.get("empty", False) is True:
            return cls(value["original"])
        if value.get("comment", False) is True:
            return cls(value["original"])

        if not "token" in value:
            raise Exception("unrecognized format")

        return cls(cls._fromToken(value["token"]))


class ExportsConfParser(object):

    def __init__(self):
        self._lines = []

    def _add_line(self, line):
        self._lines.append(line)

    def read(self, f):
        for l in f:
            l = l.rstrip("\n")
            while l.endswith("\\"):
                l = l[:len(l) - 1] + f.next()
            line = ExportsConfLine(l.rstrip("\n"))
            self._add_line(line)

    def __str__(self):
        return "\n".join(str(x) for x in self._lines)

    def parse(self):
        for l in self._lines:
            if l.isempty() or l.iscomment():
                continue
            yield l.parse()

    def toPython(self):
        return [x.toPython() for x in self._lines]

    @classmethod
    def fromString(cls, value):
        sbuf = StringIO(value)
        obj = cls()
        obj.read(sbuf)
        return obj

    @classmethod
    def fromPython(cls, value):
        conffile = cls()
        for x in value:
            line = ExportsConfLine.fromPython(x)
            conffile._add_line(line)
        return conffile
