# -*- coding: utf-8 -*-
"""
Platform-dependent values.

Platform-dependent values such as default configuration values and program paths.

@author Rached Ben Mustapha <rached.ben-mustapha@scality.com>
"""

from os import path, listdir, makedirs, readlink
from re import compile
from shutil import rmtree

from scality.management import (flatconf,
                                LocalDiskOperationError,
                                DISK_ERR_GENERIC,
                                DISK_ERR_INCONSISTENT_DISK_SETUP,
                                DISK_ERR_BIZIOD_SETUP_FAILED,)

from scality.management.process import safe_exec, ExternalProcessExecutionError

SYS_BLOCK = "/sys/block"

NL = "\n"

SCALITY_IOD = "/usr/bin/scality-iod"

BLKID = "/sbin/blkid"
LSBLK = "/bin/lsblk"
PARTED = "/sbin/parted"
BLOCKDEV = "/sbin/blockdev"
BLOCKDEV_RETRIES = 2
MKFS = "/sbin/mkfs"
TUNE2FS = "/sbin/tune2fs"
MOUNT = "/bin/mount"
UMOUNT = "/bin/umount"
KPARTX = "/sbin/kpartx"
UDEVADM = "/sbin/udevadm"

DEFAULT_FSTYPE = "ext4"
DEFAULT_FSOPTIONS = "noauto,noatime,data=ordered,barrier=1"
DEFAULT_FSOPTIONS_NOBARRIER = "noauto,noatime,data=writeback,barrier=0"
DEFAULT_FSOPTIONS_NVP = "noauto,noatime,data=ordered,barrier=1,discard"
DEFAULT_FSTAB_COMMENT = "# Added by scaldiskadd"
FSTAB = "/etc/fstab"

NODE_CONFIGURATION = "/etc/scality-node/nodes.conf"

IOD_CONF_DIR = "/etc/biziod"
DEFAULT_IOD_CONFIGURATION = {
    "bp": "",
    "reloc_verb": 1,
    "n_reloc": 10,
    "reloc_ratio_min": 10,
    "reloc_ratio_max": 60,
    "reloc_pass_time": 10,
    "reloc_manage_old_blobs_enable": 1,
    "reloc_manage_old_blobs_time": 240,
    "statcheck_start_time": 5400,
    "sweeper_start_time": 10800,
    "eiodetect": 1,
}

BIZIOOPEN = "/usr/bin/bizioopen"
BIZIOCTL = "/usr/bin/bizioctl"

IOD_STATUS_OK = "0x0"
IOD_STATUS_OOS_PERM = "0x2"

class SystemPlatform(object):
    conf = flatconf
    is_device = lambda self, n: n.startswith("/dev/") and path.exists(n)


    def extract_diskname(self, p):
        if p.startswith("UUID="):
            _, uuid = p.split("=", 2)
            dev = self.deref_uuid(uuid)
            if not dev:
                return None
            return self.extract_diskname(dev)
        else:
            return p.rstrip("/").split("/").pop()


    def deref_uuid(self, uuid):
        p = path.join("/dev/disk/by-uuid", uuid)
        if not path.exists(p):
            return None
        return readlink(p)


    def check_fstab_mountpoint_for_partition(self, uuid, partition):
        mountpoints = []
        for part, m in self.read_fstab():
            if part in ("UUID=%s" % (uuid,), partition,):
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              "partition is already in /etc/fstab",
                                              context={"partition": partition,
                                                       "uuid": uuid})
            mountpoints.append(m)
        return mountpoints


    def read_fstab_prefix(self, mp_prefix):
        """
        returns a list of mountpoints from /etc/fstab matching mp_prefix
        """
        mountpoints = zip(*self.read_fstab())[1]
        exp = compile("^" + mp_prefix + "\d+$")
        return filter(lambda m: exp.match(m), mountpoints)


    def read_fstab(self):
        """
        Reads /etc/fstab into a list of (dev, mountpoint).
        Contains only mountpoints starting with /, without trailing /.
        """
        try:
            with open(FSTAB) as fstab:
                lines = filter(lambda l: l.strip() and not l.strip().startswith("#"),
                               fstab.readlines())

                mountpoints = [tuple(l.split()[0:2]) for l in lines]
            return [(d, m.rstrip("/")) for (d, m) in mountpoints
                    if m is not "/" and m.startswith("/")]
        except BaseException as err:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "could not read /etc/fstab",
                                          context={"error": err})


    def read_proc_mounts(self):
        with open("/proc/mounts") as mounts:
            mountpoints = [tuple(l.split()[0:2]) for l in mounts.readlines()]
        return [(d, m.rstrip("/")) for (d, m) in mountpoints
                if m is not "/" and m.startswith("/")]


    def partition_list(self, disk, verbose=False):
        """
        Returns a list of partition names in disk, (without the /dev prefix)
        """
        errcontext = {"disk": disk}
        try:
            out = self.execute([KPARTX, "-sagl", disk], verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "could not list partitions on disk " +
                                          disk, context=errcontext)

        extract_name = lambda l: l.split(" :")[0]
        return filter(len, map(extract_name, out.split(NL)))


    def get_partition_uuid(self, partition, verbose=False):
        return self.execute([BLKID, "-o", "value", "-s", "UUID", partition], verbose)


    def get_all_uuids(self, verbose=False):
        return self.execute([LSBLK, "-l", "-o", "UUID", "-n", "-f"], verbose).split()


    def find_partition_parent_device(self, partition):
        """
        @param partition: the partition name (without /dev)
        @return the name of the device containing partition (without /dev)
        """
        for dev in listdir(SYS_BLOCK):
            partition_name = path.join(SYS_BLOCK, dev, partition, "partition")
            holder_name = path.join(SYS_BLOCK, dev, "holders", partition)
            if path.isfile(partition_name) or path.exists(holder_name):
                return dev
        return None


    def mountpoint_for_diskname(self, diskname, prefix):
        mountpoints = self.read_fstab_prefix(prefix)
        mountpoints = filter(lambda m: self.extract_diskname(m) == diskname, mountpoints)
        return mountpoints and mountpoints[0] or None


    def device_for_mountpoint(self, mountpoint):
        for partname, fmountpoint in self.read_fstab():
            if fmountpoint == mountpoint:
                partition = self.extract_diskname(partname)
                if not partition:
                    continue
                parent_dev = self.find_partition_parent_device(partition)
                if parent_dev:
                    return "/dev/"+parent_dev
        return None


    def mounted_devices(self):
        mount_entries = self.read_proc_mounts()
        if not mount_entries:
            return set()
        current_devs, _ = zip(*mount_entries)
        return set(filter(self.is_device, current_devs))


    def mounted_mountpoints(self):
        mount_entries = self.read_proc_mounts()
        if not mount_entries:
            return set()
        _, m = zip(*mount_entries)
        return set(m)


    def start_iod(self, iod_name, verbose):
        self.message("starting IOD", iod_name)
        errcontext = {"iod": iod_name}
        try:
            self.execute([SCALITY_IOD, "start", iod_name], verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_BIZIOD_SETUP_FAILED,
                                          "could not start IOD " +  iod_name,
                                          context=errcontext)


    def stop_iod(self, iod_name, verbose):
        self.message("stopping IOD", iod_name)
        errcontext = {"iod": iod_name}
        try:
            self.execute([SCALITY_IOD, "stop", iod_name], verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_BIZIOD_SETUP_FAILED,
                                          "could not stop IOD " +  iod_name,
                                          context=errcontext)


    def execute(self, args, verbose):
        return safe_exec(args, verbose)


    def store_name(self, strategy, dso):
        return "%s://%s:0" % (strategy, dso,)


    def message(self, *args):
        print " ".join(map(str, args))


    def makedirs(self, name, **kwargs):
        return makedirs(name, **kwargs)


    def rmtree(self, path):
        return rmtree(path)


    def exists(self, path_):
        return path.exists(path_)


    def trigger_device_refresh(self, verbose):
        self.message("triggering device refresh")
        try:
            self.execute([UDEVADM, "trigger"], verbose)
        except:
            self.message("WARNING: error while refreshing devices")


SYSTEM_PLATFORM = SystemPlatform()
