# -*- coding: utf-8 -*-
"""
Created on 17 Mar 2014

@author: Rached Ben Mustapha <rached.ben-mustapha@scality.com>
"""

from collections import namedtuple
from itertools import count, islice, repeat
from os import path
from time import sleep
from re import search, sub
from uuid import uuid1

from scality.management import platform, ts, LocalDiskOperationError
from scality.management import (DISK_ERR_INCONSISTENT_DISK_SETUP,
                                DISK_ERR_BIZIOD_SETUP_FAILED,
                                DISK_ERR_GENERIC,
                                DISK_ERR_MKFS_FAILED,
                                DISK_ERR_PARTITIONING_FAILED,
                                DISK_ERR_FSTAB_UPDATE_FAILED,
                                DISK_ERR_HOOK_FAILED,
                                DISK_ERR_DEVICE_NOT_FOUND,
                                DISK_ERR_UNHANDLED,)
from scality.management.exceptions import XCommandRequestSpecificError
from scality.management.process import ExternalProcessExecutionError
from platform import SYSTEM_PLATFORM
from ts import H

Disk = namedtuple("Disk", ("device", "mountpoint", "iod_name", "uuid"))

ADD_ALLOWED_NODE_STATES = ["RUN", "AVAILABLE", "CONF_MISMATCH", "BAL(SRC)",
                           "BAL(DST)", "DISKERR", "OUT_OF_SERVICE"]

REMOVE_ALLOWED_NODE_STATES = ADD_ALLOWED_NODE_STATES
REMOVE_ALLOWED_DISK_STATES = ["OOS_PERM", "CONNERR", "STOREERR"]

TS_TOTAL_DURATION = 24*H
TS_STATIC_SLOT_DURATION = 2*H
TS_CYCLE_EVERY = 12*H

NL = "\n"
DEV_DIRS = ("/dev", "/dev/mapper",)

class _Counter:
    def __init__(self):
        self._counters = {}

    def count(self, p, inc=1):
        index = self._counters.get(p, 0)
        index = index + inc
        self._counters[p] = index
        return index

    def get(self, p):
        return self._counters.get(p, 0)


class LocalDiskOperation(object):
    """
    Provides operations that can be applied on the local machine, like disk
    addition, removal, replacement, notifying the local storage nodes
    discoverable through sagentd
    """

    def __init__(self, sagentd=None, obj_strategy="bizobj",
                 credentials=None, verbose=False, platform=SYSTEM_PLATFORM):
        """
        Initializes a LocalDiskOperation object
        @param sagentd: the sagentd instance used to locate the storage nodes
        @param obj_strategy: bizio object strategy, defaults to bizobj
        @param credentials: map with login/passwd used to connect to sagentd and
                            the nodes
        @param verbose: whether to print more detailed progress information
        """
        self.obj_strategy = obj_strategy
        self.verbose = verbose
        self.credentials = credentials or {"login": "root", "passwd": "admin"}
        self.sagentd = sagentd
        self.platform = platform


    def _sanity_check_nvp(self, nvp, nvp_prefix, wanted_devices, current_devs):
        conf = self.platform.conf.read(platform.NODE_CONFIGURATION)
        mountpoints = self.platform.read_fstab_prefix(nvp_prefix)

        if nvp is None:
            return

        if nvp_prefix not in conf["mnt_prefix"]:
            msg = "nvp prefix %s not found in mnt_prefix from /etc/scality-node/nodes.conf" % (nvp_prefix,)
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP, msg)

        if nvp == "auto":
            if not mountpoints:
                msg = ("no mountpoints available in /etc/fstab with NVP prefix %s, can not choose one " +
                    "automatically as NVP") % (nvp_prefix,)
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              msg)

        elif self.platform.is_device(nvp):
            if nvp in wanted_devices:
                msg = "can not set up an external NVP and add it as a disk at the same time, "
                msg = msg + "it should be added first as a disk"
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              msg)

            self._sanity_check_disks([nvp], current_devs)

        else:
            # by disk name
            disknames = map(self.platform.extract_diskname, mountpoints)

            if nvp not in disknames:
                msg = "no mountpoint with prefix %s matching disk name %s in /etc/fstab" % \
                    (nvp_prefix, nvp)
                context = {"mountpoints_for_nvp_prefix": ",".join(mountpoints)}
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              msg, context)


    def _sanity_check_disks(self, wanted_devices, current_devs):
        for d in wanted_devices:
            parts = set(map(lambda d: "/dev/" + self.platform.extract_diskname(d),
                            self.platform.partition_list(d, self.verbose)))
            inter = parts & current_devs
            if inter:
                message = "disk %s contains mounted partitions (%s) and cannot be used" % (
                    d, ",".join(inter))
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                    message)


    def _sanity_check(self, partition, mkfs, fstab, nvp,
                      nvp_prefix, mountpoint_prefix, disk_config):
        if partition and not mkfs:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not partition without mkfs")

        if partition and not fstab:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not partition without updating /etc/fstab")

        if fstab and not mkfs:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not update /etc/fstab without mkfs "+
                                          "(unpredictable filesystem type and mount options)")

        conf = self.platform.conf.read(platform.NODE_CONFIGURATION)

        if "disk_mapping" in conf:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "Can not automatically add disks with the disk_mapping " +
                                          "option present in /etc/scality-node/nodes.conf" )

        if mountpoint_prefix not in conf["mnt_prefix"]:
            msg = "mountpoint prefix %s not found in mnt_prefix from /etc/scality-node/nodes.conf" \
                  % (mountpoint_prefix,)
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP, msg)

        wanted_devices, _, wanted_mountpoints = zip(*disk_config)
        current_devs = set("/dev/"+self.platform.extract_diskname(d)
                           for d in self.platform.mounted_devices())

        self._sanity_check_nvp(nvp, nvp_prefix, wanted_devices, current_devs)
        self._sanity_check_disks(wanted_devices, current_devs)

        for mountpoint in wanted_mountpoints:
            self._sanity_check_mountpoint(mountpoint, fstab)


    def _sanity_check_nodes(self, nodes, nvp, wanted_disknames):
        if not nodes:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "Unable to locate any active nodes for the given mountpoint prefix.\n" +
                                          "Please check your scality-node/scality-sagentd credentials, and the process_count attribute in /etc/scality-node/nodes.conf")

        dsos = set()

        for node in nodes:
            dsos.add(node.dso)

            if nvp and nvp == "auto" and not node.dso:
                message = ("node at %s does not have a ring assigned, "+
                    "it is needed to automatically determine the least used nvp") % (node.url)
                raise LocalDiskOperationError(DISK_ERR_GENERIC, message)

            state = node.nodeGetStatus()[0]["state"]
            if state not in ADD_ALLOWED_NODE_STATES:
                message = "node at %s state must be one of %s (is %s)" % (
                                node.url, ",".join(ADD_ALLOWED_NODE_STATES), state,)
                raise LocalDiskOperationError(DISK_ERR_GENERIC, message)

            inter = set(node_disks(node)) & set(wanted_disknames)
            if inter:
                message = "node at %s already has disk %s, cannot add" % (
                                node.url, str(inter))
                raise LocalDiskOperationError(DISK_ERR_GENERIC, message)

        if len(dsos) > 1:
            message = "Nodes must be part of the same ring (rings found: %s)" % (
                ", ".join(dsos))
            raise LocalDiskOperationError(DISK_ERR_GENERIC, message)


    def _sanity_check_mountpoint(self, mountpoint, fstab):
        if fstab:
            _, current_mountpoints = zip(*self._read_proc_mounts())
            _, fstab_mountpoints = zip(*self.platform.read_fstab())

            if mountpoint in current_mountpoints:
                message = ("directory %s already serves as a mountpoint for " +
                           "a device, but should be used according to current " +
                           "disk count and the fstab argument") % (mountpoint,)
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              message)

            if mountpoint in fstab_mountpoints:
                message = ("mountpoint %s is already in /etc/fstab, but should be " +
                    "added according to current disk count and the fstab " +
                    "argument. Please adjust disk_count in /etc/scality-node/nodes.conf "+
                    "accordingly.") % (mountpoint,)
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              message)
        else:
            if not path.exists(mountpoint):
                message = ("mountpoint %s does not exist, but should be used " +
                    "according to current disk count") % (mountpoint,)
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                              message)


    def add(self, disks=(), mkfs=False, partition=False, fstab=False, nvp=None,
            hook=None, mountpoint_prefix="/scality/disk", nvp_prefix="/scality/ssd",
            timeslot_update=False, verbose=False):
        """
        Adds disks the ring pointed to by mountpoint_prefix

        @param mkfs: whether to format the devices
        @param partition: whether to partition the devices
        @param fstab: whether to mount the devices and update fstab
        @param nvp: NVP to use. Can be device name, disk name or "auto". Defaults to None
        @param hook: program to call at stages pre-init-nvp, post-init-nvp, pre-init-disk, post-init-disk
        @param mountpoint_prefix: mountpoint prefix used to allocate mount points for disks
                and match node names from /etc/scality-node/nodes.conf
        @param nvp_prefix: mountpoint prefix used to allocate mount points for nvp
                and match node names from /etc/scality-node/nodes.conf
        @param verbose: whether to print detailed information about progress. Defaults to False.

        @return: None

        @raise LocalDiskOperationError: if anything went wrong
        """
        # XXX refactor as per #12614
        try:
            self._add(set(disks), mkfs, partition, fstab, nvp,
                      hook, mountpoint_prefix, nvp_prefix,
                      timeslot_update, verbose)

        except LocalDiskOperationError as err:
            raise err

        except XCommandRequestSpecificError as err:
            msg = "Could not contact %s: %s" % (err.url, err.message,)
            msg2 = "Please check your sagentd, scality-node and scality-supervisor " \
                   "credentials, and that the server is reachable."
            raise LocalDiskOperationError(DISK_ERR_GENERIC, msg + "\n" + msg2,
                                          context={"error": err})

        except BaseException as err:
            raise LocalDiskOperationError(DISK_ERR_UNHANDLED,
                                          "Unhandled error: %s" % (str(err)),
                                          context={"error": err})

    def _add(self, disks=(), mkfs=False, partition=False, fstab=False, nvp=None,
            hook=None, mountpoint_prefix="/scality/disk", nvp_prefix="/scality/ssd",
            timeslot_update=False, verbose=False):

        if not hasattr(disks, "__iter__"):
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "disks element should be an iterable container")

        sanity_check_conf(self.platform.conf.read(platform.NODE_CONFIGURATION))

        firstdisknum = self._next_disknum(mountpoint_prefix)
        diskname_prefix = self.platform.extract_diskname(mountpoint_prefix)

        wanted_disknames = [numbered_disk(diskname_prefix, i) for i in
                            islice(count(firstdisknum), len(disks))]
        mountpoints = [numbered_disk(mountpoint_prefix, i) for i in
                            islice(count(firstdisknum), len(disks))]

        disk_config = zip(disks, wanted_disknames, mountpoints)
        self._sanity_check(partition, mkfs, fstab, nvp, nvp_prefix,
                           mountpoint_prefix, disk_config)

        nodes = self._storage_nodes_for_mount_prefix(mountpoint_prefix)
        self._sanity_check_nodes(nodes, nvp, wanted_disknames)

        nvp_mountpoint = None
        if nvp and self.platform.is_device(nvp):
            nvp_mountpoint = self._prepare_nvp_device(nvp, nvp_prefix,
                                               mkfs, partition, fstab, hook)

        for disknum, (disk, diskname, mountpoint) in enumerate(disk_config, firstdisknum):
            if nvp:
                if not self.platform.is_device(nvp):
                    nvp_mountpoint = self._pick_nvp_mountpoint(nvp, nvp_prefix)

            self._setup_one_disk(disk, mkfs, partition, nvp_mountpoint, fstab,
                                 hook, mountpoint, diskname, disknum, nodes,
                                 nvp_prefix=nvp_prefix,
                                 mountpoint_prefix=mountpoint_prefix)

        if timeslot_update:
            self.platform.message("updating relocation timeslots")
            self._update_relocation_timeslots(mountpoint_prefix, nvp_prefix)


    def match(self, disk, mountpoint_prefix="/scality/disk", verbose=False):
        """
        Finds a disk, relative to mounpoint_prefix

        @param disk: a mount point, partition or diskname
        @param mountpoint_prefix: the prefix against which to match
        @param verbose: if more information should be printed

        @return: a Disk(device, mountpoint, diskname) namedtuple or None if disk is unknown

        @raise LocalDiskOperationError: if anything went wrong
        """

        # XXX refactor as per #12614
        fstab = self.platform.read_fstab()
        mountpoints = self.platform.read_fstab_prefix(mountpoint_prefix)

        device = None
        mountpoint = None
        diskname = None
        uuid = None

        if self.platform.is_device(disk):
            if verbose: self.platform.message(disk+" is a device")
            device = disk
            partition_names = self.platform.partition_list(disk, verbose)
            partition_candidates = []

            for p in partition_names:
                for dp in DEV_DIRS:
                    dev = path.join(dp, p)
                    if self.platform.is_device(dev):
                        partition_candidates.append(dev)
                        u = self._get_partition_uuid(dev)
                        partition_candidates.append("UUID=%s"%(u))

            for partname, fmountpoint in fstab:
                if partname in partition_candidates and fmountpoint in mountpoints:
                    mountpoint = fmountpoint
                    break

            if mountpoint:
                diskname = self.platform.extract_diskname(mountpoint)

        elif disk in mountpoints:
            if verbose: self.platform.message(disk+" is a mountpoint")
            mountpoint = disk
            diskname = self.platform.extract_diskname(mountpoint)
            device = self.platform.device_for_mountpoint(mountpoint)

        else:
            if verbose: self.platform.message(disk+" is a diskname")
            diskname = disk
            mountpoint = self.platform.mountpoint_for_diskname(diskname, mountpoint_prefix)
            device = self.platform.device_for_mountpoint(mountpoint)

        if not mountpoint or not diskname:
            if verbose: self.platform.message("could not map "+disk+" to a node-attached disk")
            return None

        for partname, fmountpoint in fstab:
            if mountpoint == fmountpoint:
                if not partname.startswith("UUID="):
                    self.platform.message("Warning: mountpoint", mountpoint,
                                          "in /etc/fstab is not referred to by UUID")
                    return None
                _, uuid = partname.split("=", 2)
                if verbose: self.platform.message("found configured uuid "+uuid+" for disk "+disk)
                break

        if not uuid:
            if verbose: self.platform.message("no UUID match for disk "+disk)
            return None
        else:
            if uuid not in self.platform.get_all_uuids(verbose):
                self.platform.message('WARNING: uuid '+uuid+' not found in the system device mappings. '
                                      'Running blockdev --rereadpt with the new partition unmounted can help.')
        
        return Disk(device, mountpoint, diskname, uuid)


    def _nvps_for_disk(self, disk, nodes):
        """
        Returns all the nvp directories on all the stores on disk.
        The stores are found by enumerating the DSOs on this host's storenodes.
        This is needed to support multiple stores per biziod, each with its own nvp.
        """
        dso_by_diskname = dict()
        for n in nodes:
            for d in node_disks(n):
                dso_by_diskname.setdefault(d, set()).add(n.dso)

        dsos_disks = zip(dso_by_diskname[disk], repeat([disk]))
        confs = iter_iod_confs(self.platform, self.obj_strategy, dsos_disks)
        all_nvps =  [c["nvp"][0] for _, d, c in confs if d == disk and "nvp" in c]
        return all_nvps


    def _replace_sanity_check_indexes_on_disk(self, disk):
        all_nodes = self._all_storage_nodes()
        all_nvps = dict()
        all_disks = set()

        for n in all_nodes:
            all_disks.update(node_disks(n))

        for d in all_disks:
            all_nvps.setdefault(d, set()).update(self._nvps_for_disk(d, all_nodes))

        for diskname, dirs in all_nvps.iteritems():
            dirs = filter(lambda d: d.startswith(disk.mountpoint+"/"), dirs)
            if dirs:
                statuses = disk_status(diskname, all_nodes)
                for s in statuses:
                    if s not in REMOVE_ALLOWED_DISK_STATES:
                        msg = ("The disk %s with status %s uses %s as NVP,\n"+
                               "but it should be marked OOS_PERM "+
                               "for %s to be safely replaced.") % (diskname,
                                                                   s,
                                                                   ", ".join(dirs),
                                                                   disk.iod_name,)
                        raise LocalDiskOperationError(DISK_ERR_GENERIC, msg)


    def remove(self, disk, setup_node=True):
        """
        Removes the disk identified by disk from the running storage nodes,
        stops its IOD process

        @param disk: a Disk nametuple

        @raise LocalDiskOperationError: if an error occurs
        """

        # XXX refactor as per #12614
        nodes_with_disk = self._nodes_with_disk(disk.iod_name,
                                                REMOVE_ALLOWED_NODE_STATES,
                                                REMOVE_ALLOWED_DISK_STATES)

        if not nodes_with_disk:
            msg = disk.iod_name+": device not found on any node"
            raise LocalDiskOperationError(DISK_ERR_DEVICE_NOT_FOUND, msg,
                                          context={"disk": disk})

        self.platform.stop_iod(disk.iod_name, self.verbose)
        nvps = self._nvps_for_disk(disk.iod_name, self._all_storage_nodes())
        for nvp in nvps:
            if self.platform.exists(nvp):
                self.platform.message("wiping nvp ", nvp)
                self.platform.rmtree(nvp)
            else:
                self.platform.message("nvp", nvp, "does not exist, not wiping")

        if setup_node:
            for node in nodes_with_disk:
                node.chordRemoveBizio(disk.iod_name)


    def replace(self, olddisk, newdisk, mkfs=False, partition=False, fstab=False,
                hook=None, verbose=False, mountpoint_prefix="/scality/disk"):
        """
        Replaces one failing disk by a new disk, initializing it if needed

        @param olddisk: the disk to replace, identified as a partition (/dev/sdd1),
                        a mountpoint (/scality/disk14), or a diskname (disk14)
        @param newdisk: the new disk to use, as a device node (/dev/sdd)
        @param mkfs: if the new disk should be formatted
        @param partition: if a new partition table should be written. makes mkfs
                          mandatory
        @param fstab: whether to upate the /etc/fstab file
        @param hook: a program to launch before and after initializing the new disk
        @param verbose: if more verbose information should be printed
        @param mountpoint_prefix: the prefix against which to match disknames and
                                  mountpoints

        @raise LocalDiskOperationError: if an error occurs
        """
        try:
            self._replace(olddisk, newdisk, mkfs, partition, fstab, hook,
                          verbose, mountpoint_prefix)

        except LocalDiskOperationError as err:
            raise err

        except XCommandRequestSpecificError as err:
            msg = "Could not contact %s: %s" % (err.url, err.message,)
            msg2 = "Please check your sagentd, scality-node and scality-supervisor " \
                   "credentials, and that the server is reachable."
            raise LocalDiskOperationError(DISK_ERR_GENERIC, msg + "\n" + msg2,
                                          context={"error": err})

        except BaseException as err:
            raise LocalDiskOperationError(DISK_ERR_UNHANDLED,
                                          "Unhandled error: %s" % (str(err)),
                                          context={"error": err})

    def _replace(self, olddisk, newdisk, mkfs=False, partition=False, fstab=False,
                hook=None, verbose=False, mountpoint_prefix="/scality/disk"):
        self.platform.trigger_device_refresh(verbose)

        # XXX refactor as per #12614
        if not self.platform.is_device(newdisk):
            raise LocalDiskOperationError(DISK_ERR_DEVICE_NOT_FOUND,
                                          "%s is not a valid block device" % (newdisk,))

        if partition and not mkfs:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not use partition and not mkfs")

        if fstab and not mkfs:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not update /etc/fstab without mkfs "+
                                          "(unpredictable filesystem type and mount options)")

        if mkfs and not fstab:
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "can not perform mkfs without updating /etc/fstab")

        matched_old_disk = self.match(olddisk, mountpoint_prefix, verbose)
        if not matched_old_disk:
            msg = "%s could not be matched to a disk. " \
                  "Is the mountpoint-prefix=%s option correct?" % (olddisk,
                                                                   mountpoint_prefix,)
            raise LocalDiskOperationError(DISK_ERR_DEVICE_NOT_FOUND, msg)

        if fstab and self.match(newdisk, mountpoint_prefix, verbose):
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          "Disk %s is already in use" % (newdisk,))

        if not fstab:
            if newdisk != matched_old_disk.device:
                msg = ("Disk %s should be initialized, with first partition mounted"+
                    " on %s, and /etc/fstab updated with the new device UUID"
                    ) % (newdisk, matched_old_disk.mountpoint)
                ctx = {"matched_old_disk": matched_old_disk, "newdisk": newdisk}
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP, msg, context=ctx)

        if mkfs:
            self._sanity_check_disks(set([newdisk]),
                                     self.platform.mounted_devices())
            self._replace_sanity_check_indexes_on_disk(matched_old_disk)
        else:
            # using an already initialized device
            if matched_old_disk.mountpoint not in self.platform.mounted_mountpoints():
                msg = "new device is not mounted on %s, and is not being initialized"
                raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          msg % (matched_old_disk.mountpoint,))

        nodes = self._nodes_with_disk(matched_old_disk.iod_name,
                                      REMOVE_ALLOWED_NODE_STATES,
                                      REMOVE_ALLOWED_DISK_STATES)

        part, uuid = self._setup_disk_lowlevel(newdisk, partition, mkfs, hook, False)

        self.remove(matched_old_disk, setup_node=False)

        if fstab:
            if matched_old_disk.mountpoint in self.platform.mounted_mountpoints():
                self.umount(matched_old_disk.mountpoint)

            self._update_fstab_remove_uuid(matched_old_disk.uuid)
            self._update_fstab(uuid, matched_old_disk.mountpoint,
                               platform.DEFAULT_FSTYPE,
                               platform.DEFAULT_FSOPTIONS)
            self._mount(part, uuid, matched_old_disk.mountpoint,
                        platform.DEFAULT_FSTYPE,
                        platform.DEFAULT_FSOPTIONS)

        # #12833 if some disks had their NVP on this one and are OOS,
        # clear their OOS flag
        self._register_iod(matched_old_disk.iod_name, nodes, mountpoint_prefix, setup_node=False)


    def _nodes_with_disk(self, diskname, nodes_statuses=(), disk_statuses=()):
        nodes = self.sagentd.getNodes()
        nodes_with_disk = set()

        for node in nodes:
            s = node.nodeGetStatus()[0]
            for d in s["disks"]:
                if d["name"] == diskname:
                    if s["state"] not in nodes_statuses:
                        msg = "node at %s state must be one of %s (is %s)" % (
                                    node.url, ",".join(nodes_statuses),
                                    s["state"],)
                        raise LocalDiskOperationError(DISK_ERR_GENERIC, msg)

                    status_intersect = set(disk_statuses) & set(d["status"])
                    if not status_intersect:
                        msg = "node at %s disk %s state must be one of %s (is %s)" % (
                            node.url, diskname, ",".join(disk_statuses),
                            d["status"],)
                        raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP, msg)

                    nodes_with_disk.add(node)

        return nodes_with_disk


    def _write_iod_configuration(self, disknum, diskname, mountpoint, nvp_path):
        conf = platform.DEFAULT_IOD_CONFIGURATION.copy()
        conf["bp"] = mountpoint
        if nvp_path:
            conf["nvp"] = nvp_path
        write_iod_conf(self.platform, self.obj_strategy, diskname, conf)


    def _set_diskgroup_count(self, mountpoint, disknum):
        mountpoint_prefix = search(r'^(.+[^\d])[\d]+$', mountpoint).group(1)
        try:
            conf = self.platform.conf.read(platform.NODE_CONFIGURATION)
            diskgroup_index = conf["mnt_prefix"].index(mountpoint_prefix)
            conf["disk_count"][diskgroup_index] = disknum
            self.platform.conf.write(platform.NODE_CONFIGURATION, conf)
        except BaseException as err:
            message = "could not set disk group count for mnt_prefix %s to %d in %s " % (
                 mountpoint_prefix, disknum, platform.NODE_CONFIGURATION)
            raise LocalDiskOperationError(DISK_ERR_BIZIOD_SETUP_FAILED, message,
                                          {"error": err})


    def _notify_storage_nodes(self, nodes, diskname, add=True):
        for node in nodes:
            if add:
                self.platform.message("adding", diskname, "to", node.url)
                node.chordAddBizio(diskname)
            node.chunkapiStoreBizioReload(diskname)


    def _all_storage_nodes(self, name_filter=None):
        return [n for n in self.sagentd.getNodes()
                if (not name_filter or name_filter(n.name))]


    def _storage_nodes_for_mount_prefix(self, mountpoint_prefix):
        try:
            conf = self.platform.conf.read(platform.NODE_CONFIGURATION)
            diskgroup_index = conf['mnt_prefix'].index(mountpoint_prefix)
            name_prefix = conf['name_prefix'][diskgroup_index]
            process_count = [int(c) for c in conf['process_count']]

            c = _Counter()
            # compute non zero-padded node names, zero-padding is removed
            # from the node names we compare to by the nnorm function below
            nodes = [
                '{0}{1}'.format(prefix, c.count(prefix))
                for prefix, count_ in zip(conf['name_prefix'], process_count)
                for num in range(1, count_ + 1)]
            prefixes = [
                prefix
                for prefix, count_ in zip(conf['mnt_prefix'], process_count)
                for num in range(1, count_ + 1)]
            node_names = [ n for n, p in zip(nodes, prefixes) if p == mountpoint_prefix]

            nnorm = lambda n: sub(name_prefix+"0+", name_prefix, n)
            nfilter = lambda n: nnorm(n) in node_names

            return self._all_storage_nodes(nfilter)

        except LocalDiskOperationError:
            raise

        except BaseException as err:
            message = "could not get storage nodes list through sagentd"
            raise LocalDiskOperationError(DISK_ERR_GENERIC, message,
                                          {"error": err})


    def _open_iod_store(self, diskname, dso):
        store = self.platform.store_name(self.obj_strategy, dso)
        self.platform.message("opening store", store, "on", diskname)
        errcontext = {"diskname":diskname, "ringname":dso}

        try:
            self.platform.execute([platform.BIZIOCTL,
                                   "-N", diskname,
                                   "-c", "del_mflags",
                                   "-a", platform.IOD_STATUS_OOS_PERM,
                                   store], self.verbose)
        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())

        try:
            self.platform.execute([platform.BIZIOCTL,
                                   "-N", diskname,
                                   "-c", "clear_repair_failed",
                                   store], self.verbose)
        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())

        try:
            self.platform.execute([platform.BIZIOOPEN,
                                   "-N", diskname,
                                   "-c", store], self.verbose)
        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            msg = "unable to open store %s" % (store,)
            # Here are given the full info about the failures
            # (from del_mflags to open store)
            raise LocalDiskOperationError(DISK_ERR_BIZIOD_SETUP_FAILED,
                                          msg, context=errcontext)


    def _push_iod_ts(self, diskname, ts, dso):
        store = self.platform.store_name(self.obj_strategy, dso,)
        self.platform.message("notifying store", store, "on", diskname, "of new reloc timeslot:", ts)
        errcontext = {"diskname":diskname, "ts":ts, "ringname":dso}

        try:
            self.platform.execute([platform.BIZIOCTL, "-N", diskname,
                        "-c", "bizobj_set_timeslots",
                        "-a", ts, store], self.verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            msg = "unable to notify new ts to IOD"
            raise LocalDiskOperationError(DISK_ERR_BIZIOD_SETUP_FAILED,
                                          msg, context=errcontext)


    def _write_iod_ts(self, obj_strategy, mount_prefix, diskname, ts, dso):
        conf = load_iod_conf(self.platform, obj_strategy, diskname, dso)
        if "ts" not in conf or ",".join(conf["ts"]) != ts:
            conf["ts"] = ts
            write_iod_conf(self.platform, obj_strategy, diskname, conf, dso)
            try:
                self._push_iod_ts(diskname, ts, dso)
            except:
                # the disk could be waiting for a replacement
                pass

        conf = load_iod_conf(self.platform, obj_strategy, diskname)
        if "ts" not in conf or ",".join(conf["ts"]) != ts:
            conf["ts"] = ts
            write_iod_conf(self.platform, obj_strategy, diskname, conf)


    def _setup_disk_lowlevel(self, disk, partition, mkfs, hook, is_nvp):
        if hook:
            stage = is_nvp and "pre-init-nvp" or "pre-init-disk"
            self._run_hook(hook, stage, disk)

        if partition:
            self._do_partition(disk)

        part = self._ensure_unique_partition(disk)

        if mkfs:
            uuid = str(uuid1())
            self._mkfs(part, platform.DEFAULT_FSTYPE, uuid)
        else:
            uuid = self._get_partition_uuid(part)

        if hook:
            stage = is_nvp and "post-init-nvp" or "post-init-disk"
            self._run_hook(hook, stage, part)

        return part, uuid


    def _setup_one_disk(self, disk, mkfs, partition, nvp_mountpoint, fstab,
                        hook, mountpoint, diskname, disknum, nodes=[],
                        is_nvp=False, nvp_prefix=None, mountpoint_prefix=None):
        self.platform.message("setting up disk", disk)

        part, uuid = self._setup_disk_lowlevel(disk, partition, mkfs, hook, is_nvp)

        if fstab:
            fsoptions = is_nvp and platform.DEFAULT_FSOPTIONS_NVP \
                or platform.DEFAULT_FSOPTIONS
            self._make_mountpoint(mountpoint, part, uuid)
            self._mount(part, uuid, mountpoint, platform.DEFAULT_FSTYPE,
                        fsoptions)
            self._update_fstab(uuid, mountpoint, platform.DEFAULT_FSTYPE,
                               fsoptions)

        if nvp_mountpoint:
            self.platform.message("using", nvp_mountpoint, "as external nvp base")
        nvp_path = self._make_nvp_path(nvp_mountpoint, diskname)

        self._write_iod_configuration(disknum, diskname, mountpoint, nvp_path)
        self._set_diskgroup_count(mountpoint, disknum)

        if nodes:
            self._register_iod(diskname, nodes, mountpoint_prefix)

        self.platform.message("")


    def _register_iod(self, diskname, nodes, mountpoint_prefix, setup_node=True):
        self.platform.start_iod(diskname, self.verbose)
        self._open_iod_store(diskname, self._find_dso(mountpoint_prefix))
        self._notify_storage_nodes(nodes, diskname, add=setup_node)


    def _find_dso(self, mountpoint_prefix):
        return self._storage_nodes_for_mount_prefix(mountpoint_prefix)[0].dso


    def _update_relocation_timeslots(self, mountpoint_prefix, nvp_prefix):
        nodes = self._all_storage_nodes()
        dso_by_diskname = dict()
        for n in nodes:
            for d in node_disks(n):
                dso_by_diskname[d] = n.dso

        dsos_disks = list(set([(node.dso, node_disks(node),)
                               for node in nodes]))

        confs = iter_iod_confs(self.platform, self.obj_strategy, dsos_disks)
        all_nvps =  [(diskname, c.get("nvp", [None])[0]) for _, diskname, c in confs]

        nvp_occupation = dict()
        for diskname, nvp in all_nvps:
            if nvp:
                nvp_mountpoint = extract_mountpoint(self.platform.read_fstab_prefix(nvp_prefix), nvp)[0]
            else:
                nvp_mountpoint = None

            s = nvp_occupation.get(nvp_mountpoint, set())
            s.add(diskname)
            nvp_occupation[nvp_mountpoint] = s

        if nvp_occupation:
            real_nvps = [len(v) for k, v in nvp_occupation.iteritems() if k]
            if real_nvps:
                slot_duration = TS_CYCLE_EVERY / max(real_nvps)
            else:
                slot_duration = TS_STATIC_SLOT_DURATION

        else:
            # first disk being added, avoid using all the time
            slot_duration = TS_CYCLE_EVERY / 2

        nvp_scheduler = ts.ScheduleGroup(TS_TOTAL_DURATION, slot_duration,
                                         TS_CYCLE_EVERY, nvp_occupation)
        scheduler = ts.ScheduleGroup(TS_TOTAL_DURATION, TS_STATIC_SLOT_DURATION,
                                     TS_CYCLE_EVERY)

        for nvp, disknames in nvp_occupation.iteritems():
            for diskname in disknames:
                s = nvp_scheduler if nvp else scheduler
                slots = s.allocate_timeslot(diskname)
                slots_s = ts.serialize_ts(slots)
                self._write_iod_ts(self.obj_strategy, mountpoint_prefix,
                                   diskname, slots_s, dso_by_diskname[diskname])


    def _prepare_nvp_device(self, nvp, nvp_prefix, mkfs, partition, fstab, hook):
        if not nvp:
            return None

        if self.platform.is_device(nvp):
            nvp_disknum = self._next_disknum(nvp_prefix)
            nvp_diskname_prefix = self.platform.extract_diskname(nvp_prefix)
            nvp_diskname = numbered_disk(nvp_diskname_prefix, nvp_disknum)
            nvp_mountpoint = numbered_disk(nvp_prefix, nvp_disknum)
            self._sanity_check_mountpoint(nvp_mountpoint, fstab)
            self._setup_one_disk(nvp, mkfs, partition, None, fstab,
                                 hook, nvp_mountpoint, nvp_diskname, nvp_disknum,
                                 is_nvp=True, nvp_prefix=nvp_prefix)
            return nvp_mountpoint
        else:
            return self._pick_nvp_mountpoint(nvp, nvp_prefix)


    def _pick_nvp_mountpoint(self, nvp, nvp_prefix):
        if nvp == "auto":
            mountpoints = self.platform.read_fstab_prefix(nvp_prefix)

            nodes = self._all_storage_nodes()
            dsos_disks = [(node.dso, node_disks(node),) for node in nodes]
            confs = iter_iod_confs(self.platform, self.obj_strategy, set(dsos_disks))

            return least_used_nvp_mountpoint(mountpoints, confs)

        else:
            # by disk name
            return self.platform.mountpoint_for_diskname(nvp, nvp_prefix)


    def _make_nvp_path(self, nvp_mountpoint, diskname):
        if not nvp_mountpoint:
            return None

        dir_name = "%s-%s" % (self.obj_strategy, diskname,)
        local_path = path.join(nvp_mountpoint, dir_name)
        if not path.exists(local_path):
            self.platform.makedirs(local_path)

        return local_path


    def _get_partition_uuid(self, partition):
        errcontext = {"partition": partition}

        try:
            return self.platform.get_partition_uuid(partition, self.verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_PARTITIONING_FAILED,
                                          "could not read UUID on " + partition,
                                          context=errcontext)


    def _refresh_partition_table(self, disk):
        errcontext = {'disk': disk}
        remaining_allowed_fail = platform.BLOCKDEV_RETRIES
        failure = True

        while remaining_allowed_fail > 0:
            try:
                self.platform.execute([platform.BLOCKDEV, '--rereadpt', disk], self.verbose)
            except ExternalProcessExecutionError:
                remaining_allowed_fail -= 1
                sleep(5)
            else:
                failure = False
                break

        if failure:
            raise LocalDiskOperationError(DISK_ERR_PARTITIONING_FAILED,
                                          'blockdev failed on ' + disk,
                                          context=errcontext)


    def _do_partition(self, disk):
        self.wipe_disk(disk)

        self.platform.message("creating partition table on", disk)
        errcontext = {"disk": disk}

        try:
            self.platform.execute([platform.PARTED, "-s", disk, "-a", "optimal", "--",
                        "mkpart", "primary", "1", "-1"], self.verbose)
            self.platform.execute([platform.PARTED, "-s", disk, "-a", "optimal", "--",
                        "name", "1", str(uuid1())], self.verbose)
            self._refresh_partition_table(disk)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_PARTITIONING_FAILED,
                                          "partition creation failed on " + disk,
                                          context=errcontext)


    def wipe_disk(self, disk):
        self.platform.message("wiping disk", disk)
        errcontext = {"disk": disk}

        try:
            self.platform.execute([platform.PARTED, "-s", disk, "-a", "optimal", "--",
                        "mklabel", "gpt"], self.verbose)
            self._refresh_partition_table(disk)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_PARTITIONING_FAILED,
                                          "partition table wipe failed on " + disk,
                                          context=errcontext)


    def _mkfs(self, partition_node, fstype, uuid):
        self.platform.message("running mkfs on", partition_node)
        errcontext = {"partition": partition_node}

        try:
            self.platform.execute([platform.MKFS, "-t", fstype, partition_node],
                                  self.verbose)
            if fstype.startswith("ext"):
                self.platform.execute([platform.TUNE2FS, "-m0", "-c0", "-C0", "-i0",
                            "-U", uuid, partition_node], self.verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_MKFS_FAILED,
                                          "filesystem creation failed on " +
                                          partition_node,
                                          context=errcontext)


    def _mount(self, partition, uuid, mountpoint, fstype, options):
        self.platform.message("mounting", partition)
        errcontext = {"partition": partition}
        part = "UUID=%s" % (uuid,)

        try:
            self.platform.execute([platform.MOUNT, "-t", fstype, "-o", options,
                                   part, mountpoint],
                                  self.verbose)

        except ExternalProcessExecutionError as err:
            message = "could not mount %s on %s" % (partition, mountpoint,)
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_MKFS_FAILED,
                                          message, context=errcontext)


    def mount_by_mountpoint(self, mountpoint):
        self.platform.message("mounting", mountpoint)
        errcontext = {"mountpoint": mountpoint}

        if not path.exists(mountpoint):
            self.platform.makedirs(mountpoint, mode=0700)

        try:
            self.platform.execute([platform.MOUNT, mountpoint], self.verbose)

        except ExternalProcessExecutionError as err:
            message = "could not mount on %s" % (mountpoint,)
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_MKFS_FAILED,
                                          message, context=errcontext)


    def umount(self, mountpoint):
        self.platform.message("unmounting", mountpoint)
        errcontext = {"mountpoint": mountpoint}

        try:
            self.platform.execute([platform.UMOUNT, mountpoint], self.verbose)

        except ExternalProcessExecutionError as err:
            message = "could not unmount %s" % (mountpoint,)
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_MKFS_FAILED,
                                          message, context=errcontext)


    def _update_fstab(self, uuid, mountpoint, fstype, options):
        """
        Add the partition with the specified uuid in /etc/fstab (atomic update)
        A backup copy is kept in /etc/fstab.scaladddisk-DATE
        Permissions, owner, and group are copied from the original file
        """
        self.platform.message("updating fstab for", mountpoint)
        errcontext = {"uuid": uuid}

        try:
            stanza = self._fstab_stanza(uuid, mountpoint, fstype, options)
            with open(platform.FSTAB) as fstab:
                fstab_content = fstab.read() + stanza

            self.platform.conf.write_file_atomic(platform.FSTAB, fstab_content)

        except BaseException as err:
            errcontext.update({"error": err})
            raise LocalDiskOperationError(DISK_ERR_FSTAB_UPDATE_FAILED,
                                          "could not update /etc/fstab",
                                          context=errcontext)


    def _update_fstab_remove_uuid(self, uuid):
        """
        Add the partition with the specified uuid in /etc/fstab (atomic update)
        A backup copy is kept in /etc/fstab.scaladddisk-DATE
        Permissions, owner, and group are copied from the original file
        """
        self.platform.message("updating fstab: removing uuid", uuid)
        errcontext = {"uuid": uuid}

        try:
            new_fstab = []
            ff = lambda l: not l.lstrip(" \t").startswith("UUID=%s" % (uuid,))
            with open(platform.FSTAB) as fstab:
                new_fstab = filter(ff, fstab.readlines())

            self.platform.conf.write_file_atomic(platform.FSTAB,
                                                 "".join(new_fstab))

        except BaseException as err:
            errcontext.update({"error": err})
            raise LocalDiskOperationError(DISK_ERR_FSTAB_UPDATE_FAILED,
                                          "could not update /etc/fstab",
                                          context=errcontext)


    def _fstab_stanza(self, uuid, mountpoint, fstype, options):
        line = "UUID=%s\t%s\t%s\t%s\t0\t0" % (uuid, mountpoint, fstype, options)
        return NL + platform.DEFAULT_FSTAB_COMMENT + NL + line + NL


    def _ensure_unique_partition(self, disk):
        errcontext = {"disk": disk}
        partition_list = self.platform.partition_list(disk, self.verbose)

        if len(partition_list) != 1:
            message = "partition count on %s must be exactly 1 - missing partition option?" % (disk,)
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          message, context=errcontext)

        partname = partition_list.pop()
        rs = filter(self.platform.is_device, [path.join(d, partname)
                                              for d in reversed(DEV_DIRS)])

        if not rs:
            message = "partition device node %s not found on %s" % (partname, disk,)
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP,
                                          message, context=errcontext)

        return rs.pop()


    def _make_mountpoint(self, mountpoint, partition, uuid):
        mountpoints = self.platform.check_fstab_mountpoint_for_partition(uuid, partition)

        if mountpoint in mountpoints:
            msg = "mountpoint %s already in fstab" % (mountpoint,)
            raise LocalDiskOperationError(DISK_ERR_INCONSISTENT_DISK_SETUP, msg)

        if not path.exists(mountpoint):
            self.platform.makedirs(mountpoint, mode=0700)

        return mountpoint


    def _read_proc_mounts(self):
        """
        Reads /proc/mounts into a list of (dev, mountpoint).
        Contains only mountpoints starting with /, without trailing /.
        """
        try:
            return self.platform.read_proc_mounts()
        except BaseException as err:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "could not read /proc/mounts",
                                          context={"error": err})


    def _run_hook(self, hook, stage, disk):
        self.platform.message("running hook", stage, "on", disk)
        errcontext = {"disk": disk, "stage": stage}

        try:
            self.platform.execute([hook, stage, disk], self.verbose)

        except ExternalProcessExecutionError as err:
            errcontext.update(err.contextdict())
            raise LocalDiskOperationError(DISK_ERR_HOOK_FAILED,
                                          "hook exited with non-zero status",
                                          context=errcontext)


    def _next_disknum(self, mountpoint_prefix):
        try:
            conf = self.platform.conf.read(platform.NODE_CONFIGURATION)
        except BaseException as err:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "could not read configuration file " +
                                          platform.NODE_CONFIGURATION,
                                          context={"error": err})
        try:
            return next_disknum_in_prefix(conf, mountpoint_prefix)

        except (ValueError, IndexError):
            message = "no disk group for mnt_prefix %s found in %s " % (
                 mountpoint_prefix, platform.NODE_CONFIGURATION)
            raise LocalDiskOperationError(DISK_ERR_GENERIC, message)


def node_disks(node):
    return tuple([d["name"] for d in node.nodeGetStatus()[0]["disks"]])


def disk_status(diskname, nodes):
    ret = set()
    for n in nodes:
        s = n.nodeGetStatus()[0]
        for d in s["disks"]:
            if d["name"] == diskname:
                ret.update(d["status"])
    return ret or ["OK"]


def least_used_nvp_mountpoint(eligible_mountpoints, confs):
    """
    chooses the mountpoint which hosts the least bizobj.bin according to confs
    @param eligible_mountpoints: an iterable of mountpoints which could be used
    @param confs: an iterable of (biziod instance configurations)
    """
    all_nvps =  [c["nvp"][0] for _, _, c in confs if "nvp" in c]
    nvps_by_mount = group_nvps_by_mountpoint(eligible_mountpoints, all_nvps)
    # skip unknown nvp mountpoints which are grouped to None
    nusers = lambda mountpoint: len(nvps_by_mount.get(mountpoint, set()))
    # sort by number of users then alphabetically
    compare_users = lambda m1, m2: cmp(nusers(m1), nusers(m2)) or cmp(m1, m2)

    keys = sorted(eligible_mountpoints, compare_users)
    return keys and keys[0] or None


def iter_iod_confs(platform, strategy, rings_disks):
    """
    load disk configurations then overwrite with disk-dso values
    @param rings_disks: list of (dso, [diskname1, diskname2])
    """
    for ringname, disknames in rings_disks:
        for diskname in disknames:
            c1 = load_iod_conf(platform, strategy, diskname)
            c2 = load_iod_conf(platform, strategy, diskname, ringname)
            c1.update(c2)
            yield (ringname, diskname, c1,)


def group_nvps_by_mountpoint(mountpoints, nvps):
    ret = {}
    for nvp in nvps:
        mountpoint, _ = extract_mountpoint(mountpoints, nvp)
        if mountpoint:
            s = ret.get(mountpoint, set())
            s.add(nvp)
            ret[mountpoint] = s
    return ret


def extract_mountpoint(mountpoints, nvp):
    for mountpoint in mountpoints:
        if nvp.startswith(mountpoint+"/"):
            return (mountpoint, nvp)
    return (None, nvp)


def load_iod_conf(platform, strategy, diskname, ringname=None):
    filepath = iod_conf_filename(strategy, diskname, ringname)
    return platform.conf.read(filepath)


def write_iod_conf(platform_, strategy, diskname, conf, ringname=None):
    filepath = iod_conf_filename(strategy, diskname, ringname)
    if not path.exists(platform.IOD_CONF_DIR):
        platform_.makedirs(platform.IOD_CONF_DIR)
    platform_.conf.write(filepath, conf)


def iod_conf_filename(strategy, diskname, ringname=None):
    if ringname:
        filename = "%s.%s.%s" % (strategy, ringname, diskname,)
    else:
        filename = "%s.%s" % (strategy, diskname,)
    return path.join(platform.IOD_CONF_DIR, filename)


def next_disknum_in_prefix(conf, prefix):
    indexes = (i for i, p in enumerate(conf["mnt_prefix"]) if p == prefix)
    diskcount = sum(int(conf["disk_count"][i]) for i in indexes)
    return diskcount + 1


def sanity_check_conf(conf):
    lengths = set()
    for var in ("name_prefix", "mnt_prefix", "disk_count", "process_count"):
        if var not in conf:
            raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                          "Missing %s in configuration"%(var,))
        lengths.add(len(conf[var]))

    if len(lengths) != 1:
        raise LocalDiskOperationError(DISK_ERR_GENERIC,
                                      "Invalid multi-ring setup: some columns do not have values for all the rings")

    return conf


numbered_disk = lambda d, n: "%s%d" % (d, n,)
