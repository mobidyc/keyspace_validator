# -*- coding: utf-8 -*-
from re import match

S = 1
M = S*60
H = M*60
HMS_PATTERN = r"(\d+h)(\d+m)?(\d+s)?"


class InvalidTimespecException(BaseException): pass
class InvalidIntervalException(BaseException): pass
class NoTimeSlotsAvailableException(BaseException): pass


def apply_unit(s):
    if not s:
        return 0

    if s.endswith("h"):
        return int(s.rstrip("h")) * H

    if s.endswith("m"):
        return int(s.rstrip("m")) * M

    if s.endswith("s"):
        return int(s.rstrip("s")) * S


def parse_timespec(t):
    res = match(HMS_PATTERN, t)
    if not res:
        raise InvalidTimespecException("invalid timespec \"%s\"" % (t,))

    h, m, s = res.groups()
    return apply_unit(h) + apply_unit(m) + apply_unit(s)


def parse_timespec_range(start_s, end_s):
    start = parse_timespec(start_s)
    end = parse_timespec(end_s)
    return (start, end,)


def split_ts(ts):
    ranges = ts.split(",")
    return [parse_timespec_range(*r.split("-")) for r in ranges]


def serialize_ts(slot_list):
    return ",".join([to_range_str(*t) for t in slot_list])


def to_range_str(start, end):
    s_str = to_time_str(start)
    e_str = to_time_str(end)
    return "%s-%s" % (s_str, e_str,)


def to_time_str(t):
    h = t / H
    ret = "%02dh" % (h,)

    m = t % H
    if m:
        m = m / M
        ret = ret + ("%02dm" % (m,))

    s = t % M
    if s:
        ret = ret + ("%02ds" % (s,))

    return ret


def _build_ts_occupation(total_interval, slot_duration, repeat_every, nvp_map=None):
    if not total_interval:
        raise InvalidIntervalException("zero interval")

    if not slot_duration:
        raise InvalidIntervalException("zero slot duration")

    if not nvp_map or len(nvp_map) == 1 or slot_duration > repeat_every / 2:
        slot_duration = min(slot_duration, 2*H)

    ret = dict()
    start = 0
    n_intervals = repeat_every / slot_duration

    while n_intervals:
        end = start + slot_duration
        if end == total_interval:
            end = end - 1
        ret[(start, end,)] = set()
        n_intervals = n_intervals - 1
        start = end

    return ret


def adjust_end(e, total):
    if e == total:
        return e-1
    return e


def time_range_tuple(start, end, cycle_len, occurence, total):
    return (start + cycle_len*occurence, adjust_end(end + cycle_len*occurence, total),)


def ranges_overlap(range1, range2):
    s1, e1 = range1
    s2, e2 = range2

    r1_starts_within = s2 < s1 < e2
    r1_ends_within = s2 < e1 < e2
    r2_starts_within = s1 < s2 < e1
    r2_ends_within = s1 < e2 < e1

    return (range1 == range2 or
            r1_starts_within or r1_ends_within or
            r2_starts_within or r2_ends_within)


class ScheduleGroup(object):
    def __init__(self, total_interval, slot_duration, repeat_every, nvp_occupation=None):
        self.actual_ts_occupation = _build_ts_occupation(total_interval,
                                                         slot_duration,
                                                         repeat_every,
                                                         nvp_occupation)
        self.nvp_occupation = nvp_occupation or dict()

        self.total_interval = total_interval
        self.repeat_every = repeat_every
        self.n_cycles = total_interval / repeat_every


    def register_str(self, diskname, ts_string, nvp=None):
        self.register(diskname, split_ts(ts_string), nvp)


    def register(self, diskname, slots, nvp=None):
        for slot in slots:
            for kslot in self.actual_ts_occupation.keys():
                if ranges_overlap(slot, kslot):
                    self.actual_ts_occupation[kslot].add((nvp, diskname,))


    def get(self, slot):
        return self.actual_ts_occupation[slot]


    def allocate_timeslot(self, diskname):
        nvp, _ = self._find_nvp(diskname)
        slots = self.pick_timeslot(diskname)
        self.register(diskname, slots, nvp)
        return slots


    def pick_timeslot(self, diskname):
        nvp, siblings = self._find_nvp(diskname)
        candidates = self.actual_ts_occupation.keys()

        if nvp:
            def filter_no_siblings(slot):
                for s in siblings:
                    if (nvp, s) in self.actual_ts_occupation[slot]:
                        return False
                return True
            candidates = filter(filter_no_siblings, candidates)

        lens = lambda a: len(self.actual_ts_occupation[a])
        sort = lambda a, b: cmp(lens(a), lens(b)) or cmp(a, b)

        candidates = sorted(candidates, cmp=sort)

        if not candidates:
            raise NoTimeSlotsAvailableException()

        return [time_range_tuple(start, end, self.repeat_every, i, self.total_interval)
            for start, end in candidates[0:1] for i in xrange(self.n_cycles)]


    def _find_nvp(self, diskname):
        for nvp, disks in self.nvp_occupation.iteritems():
            if diskname in disks:
                return nvp, disks

        return None, set()
