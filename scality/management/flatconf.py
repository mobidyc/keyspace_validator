# -*- coding: utf-8 -*-
"""
Flat configuration file operations.

read/write flat k=v configuration for biziod, scality-node and such.

@author Rached Ben Mustapha <rached.ben-mustapha@scality.com>
"""

from datetime import datetime
from os import chown, path, rename, stat, mkdir
from shutil import copyfile, copymode
from tempfile import NamedTemporaryFile

BACKUP = True

def backup(filepath):
    if not path.exists(filepath):
        return
    filestat = stat(filepath)
    now = datetime.isoformat(datetime.now())

    filedir = path.dirname(filepath) or "."
    backupdir = path.join(filedir, "backup")
    if not path.isdir(backupdir):
        mkdir(backupdir)

    filename = path.basename(filepath)
    filebackup = path.join(backupdir, "%s.scality-%s" % (filename, now))

    copyfile(filepath, filebackup)
    copymode(filepath, filebackup)
    chown(filebackup, filestat.st_uid, filestat.st_gid)


def flatten(v):
    if isinstance(v, list):
        return ",".join(map(str, v))
    return str(v)


def write(filepath, conf):
    """
    Backs up filepath if applicable then overwrites
    """
    conf_lines = ["%s=%s\n" % (k, flatten(v),)
                    for (k, v) in conf.iteritems()]
    write_file_atomic(filepath, "".join(sorted(conf_lines)))


def read(filename):
    conf = {}
    if path.exists(filename):
        with open(filename) as conffile:
            for line in conffile.readlines():
                line = line.strip()
                if line and not line.startswith('#'):
                    k, v = line.split('=', 1)
                    conf[k] = v.split(',')
    return conf


def write_file_atomic(filepath, content):
    """
    Write contents to filename.
    Backs up filepath if applicable then overwrites, keeping mode/owner/group
    """
    with NamedTemporaryFile(prefix=filepath, delete=False) as new_file:
        new_name = new_file.name
        new_file.write(content)

    if path.exists(filepath):
        copymode(filepath, new_name)
        fstat = stat(filepath)
        chown(new_name, fstat.st_uid, fstat.st_gid)
        if BACKUP:
            backup(filepath)

    rename(new_name, filepath)
