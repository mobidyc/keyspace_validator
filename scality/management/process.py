# -*- coding: utf-8 -*-
"""
Created on 17 Mar 2014

@author: Rached Ben Mustapha <rached.ben-mustapha@scality.com>
"""

class ExternalProcessExecutionError(Exception):
    """
    classdocs
    """

    def __init__(self, exitcode=-1, executable="", args=[], stdout="",
                 stderr=""):
        super(ExternalProcessExecutionError, self).__init__()
        self.exitcode = exitcode
        self.args = args
        self.stderr = stderr
        self.stdout = stdout
        self.executable = executable

    def contextdict(self):
        return {
            "executable": self.executable,
            "args": self.args,
            "exitcode": self.exitcode,
            "stderr": self.stderr,
            "stdout": self.stdout,
        }

def safe_exec(args, verbose=False):
    from subprocess import (Popen, PIPE,)
    if verbose:
        print "DEBUG:", " ".join(args),

    process = Popen(args, stdout=PIPE, stderr=PIPE, close_fds=True,
                    shell=False)
    (stdout, stderr) = process.communicate()

    if verbose:
        print " >returned> ", process.returncode

    if process.returncode != 0:
        raise ExternalProcessExecutionError(process.returncode, args[0],
                                            args[1:], stdout, stderr)

    return stdout.strip()
