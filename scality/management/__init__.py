# -*- coding: utf-8 -*-
DISK_OK = 0
DISK_ERR_MKFS_FAILED = 1
DISK_ERR_PARTITIONING_FAILED = 2
DISK_ERR_MOUNT_FAILED = 3
DISK_ERR_HOOK_FAILED = 4
DISK_ERR_BIZIOD_SETUP_FAILED = 6
DISK_ERR_INCONSISTENT_DISK_SETUP = 7
DISK_ERR_FSTAB_UPDATE_FAILED = 8
DISK_ERR_DEVICE_NOT_FOUND = 9
DISK_ERR_GENERIC = 20
DISK_ERR_UNHANDLED = 21


class LocalDiskOperationError(Exception):
    """
    Error thrown when performing a local disk operation
    """

    def __init__(self, errcode, message=None, context={}):
        super(LocalDiskOperationError, self).__init__(message)
        self.errcode = errcode
        self.message = message
        self.context = context

    def __str__(self):
        return self.message + " context=" + str(self.context)


__all__ = ("LocalDiskOperationError",
           "DISK_OK",
           "DISK_ERR_MKFS_FAILED",
           "DISK_ERR_PARTITIONING_FAILED",
           "DISK_ERR_MOUNT_FAILED",
           "DISK_ERR_HOOK_FAILED",
           "DISK_ERR_BIZIOD_SETUP_FAILED",
           "DISK_ERR_INCONSISTENT_DISK_SETUP",
           "DISK_ERR_FSTAB_UPDATE_FAILED",
           "DISK_ERR_DEVICE_NOT_FOUND",
           "DISK_ERR_GENERIC",
           "DISK_ERR_UNHANDLED")
