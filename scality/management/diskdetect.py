# -*- coding: utf-8 -*-
"""
Manage the disks detection on machines during installation process.
"""


import os
import subprocess
import shlex
import re
import time
from collections import namedtuple

from platform import SYSTEM_PLATFORM as platform
from platform import DEFAULT_FSTAB_COMMENT

# lsblk called as :
#  $ lsblk -b -P -n -o NAME,TYPE,SIZE,ROTA,RM,RO,FSTYPE,MOUNTPOINT,KNAME
LSBLK_COLUMNS = [
    "NAME",
    "TYPE",
    "SIZE",
    "ROTA",
    "RM",
    "RO",
    "FSTYPE",
    "MOUNTPOINT",
    "KNAME",
]

#path to special file that can override lsblk real path
OVERRIDE_LSBLK_BIN = "/tmp/scality_lsblk.sh"

DEFAULT_SSD_QUERY_TOOL = os.environ.get("SCALITY_SSD_QUERY_EXECUTABLE",
                                        "/usr/bin/scality-iops-meter")

NOT_FORMATED_FSTYPE = ("", "linux_raid_member", "LVM2_member")

# Arbitrary defined value to avoid fetching USB sticks, it should be hardened.
RM_DEV_MIN_SIZE = 214748364800  # 200GB

#if you modify these values, don't forget to edit scality_installer_ui.py,
#because these constants are defined too for the unittest environment
PARTITION_LIMIT_SIZE = 1073741824
FAST_SSD_LIMIT = 8000
SLOW_SSD_LIMIT = 1000


class NamedSSDDetectionStrategy(object):
    @classmethod
    def match(cls, name):
        return name == getattr(cls, "name", None)

    @classmethod
    def make(cls, *unused):
        return cls()


class ExternalScriptSSDDetection(namedtuple("ExternalScriptSSDDetection", "name"),
                                 NamedSSDDetectionStrategy):
    @property
    def script_cmdline(self):
        """Return the cmdline of the external script."""
        return self.name

    @staticmethod
    def _terminate_process(proc):
        """Try to terminate properly the given process.

         - Send a SIGTERM
         - Wait 3s max
         - Send a SIGKILL
         - Wait ...

        Args:
            - proc (Popen object): Process to terminate

        """
        proc.terminate()

        start_time = time.time()
        while time.time() < start_time + 3:
            if proc.poll() is not None:
                return
            time.sleep(0.01)

        proc.kill()
        proc.wait()

    def _wait_processes(self, processes, timeout=120):
        """Wait the end of all given processes.

        Args:
            - processes (list): List of diskdetect processes
            - timeout: Timeout max of each process

        """
        while processes:
            now = time.time()
            end_processes = []
            for disk_name, popts in processes.iteritems():
                proc = popts["process"]
                if proc.poll() is None:
                    if now > popts["start_time"] + timeout:
                        self._terminate_process(proc)
                        returncode = 1
                        output = "Error: Timeout ({timeout}s)".format(
                            timeout=timeout
                        )
                    else:
                        continue
                else:
                    returncode = proc.returncode
                    output = proc.stdout.read(4096)

                end_processes.append(disk_name)

                yield (disk_name, popts["cmdline"], returncode, output)

            for disk_name in end_processes:
                del processes[disk_name]

            if processes:
                time.sleep(0.01)

    def _launch_process(self, subdisks_name):
        """Execute the batch of diskdetect processes

        Args:
            - subdisks_name (list of string): List of disk name.

        """
        processes = {}
        for disk_name in subdisks_name:
            cmd = self.script_cmdline.split()
            cmd.append("/dev/{disk_name}".format(disk_name=disk_name))

            processes[disk_name] = {
                "cmdline": " ".join(cmd),
                "process": subprocess.Popen(
                    cmd,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=False
                ),
                "start_time": time.time()
            }

        for ret in self._wait_processes(processes):
            yield ret

    def _iter_process(self, disks, pool_size=10):
        """Execute all external diskdetect scripts per batch of processes.

        Args:
            - disks (dict): List of disks
            - pool_size (int): Number of processes to launch simultaneously.

        """
        disks_name = disks.keys()
        for idisk in xrange(0, len(disks_name), pool_size):
            subdisks_name = disks_name[idisk:idisk + pool_size]
            for ret in self._launch_process(subdisks_name):
                yield ret

    def do_detect(self, disks, *args):
        """Execute the external script for all given disks.

        Args:
            - disks (dict): List of disks with the name of the disk as key.
            - *args: Not used

        Returns:
            Add `ssd` and `ssd_seeks` properties in the disks
                data input for each disk.

            - ssd (bool): `True` if the disk is a SSD.
            - ssd_seeks (int): Value returned by the external script.

        """
        for (disk, cmdline, returncode, output) in self._iter_process(disks):
            if returncode == 0:
                try:
                    seeks = int(output)
                except (TypeError, ValueError):
                    platform.message(
                        "Warning: Unable to parse output of the command %r: %s" % (
                            cmdline, output
                        )
                    )
                    seeks = 0
            else:
                platform.message(
                    "Warning: Command %r failed (exitcode: %d): %s" % (
                        cmdline, returncode, output
                    )
                )
                seeks = 0

            disks[disk].update({
                "ssd": seeks >= SLOW_SSD_LIMIT,
                "ssd_seeks": seeks
            })

    @classmethod
    def match(cls, name):
        return name and name.startswith("/")

    @classmethod
    def make(cls, *args):
        if not args:
            return None
        return ExternalScriptSSDDetection(*args)

class NoSSDDetection(NamedSSDDetectionStrategy):
    name = "off"

    def do_detect(self, disks, lsblk_facts):
        platform.message("SSD detection is off")

class SysfsSSDDetection(NamedSSDDetectionStrategy):
    name = "sysfs"

    def do_detect(self, disks, lsblk_facts):
        for values in lsblk_facts:
            if values['NAME'] in disks.keys():
                disks[values['NAME']]['ssd'] = values['ROTA'] == '0'
            elif values['KNAME'] in disks.keys():
                disks[values['KNAME']]['ssd'] = values['ROTA'] == '0'

class DefaultSSDDetectionProxy(NamedSSDDetectionStrategy):
    name = "default"

    @classmethod
    def make(cls, *args):
        return IOPSHeuristicsSSDDetectionProxy.make()

class IOPSHeuristicsSSDDetectionProxy(NamedSSDDetectionStrategy):
    name = "iops-heuristics"

    @classmethod
    def make(cls, *args):
        return ExternalScriptSSDDetection.make(DEFAULT_SSD_QUERY_TOOL)

ALL_SSD_DETECTION_STRATEGIES = [
    NoSSDDetection,
    SysfsSSDDetection,
    ExternalScriptSSDDetection,
    IOPSHeuristicsSSDDetectionProxy,
    DefaultSSDDetectionProxy,
]


def get_all_disks(ssd_strategy="default", lsblk_options=None, **testdata):
    """ Execute `lsblk` command to collect all devices informations on the
        machine.

        ssd_strategy  -- the strategy for ssd detection
        lsblk_options -- options to pass to lsblk command
        **testdata    -- keyword arguments to override lsblk_dict, lsblk_info,
                         disks_dict
    """
    if not testdata.has_key("lsblk_dict") or \
            not testdata.has_key("lsblk_info"):
        treedata, reverse_lsblk = _get_treedata()
        stdoutdata = _get_infodata(lsblk_options, reverse_lsblk)
        lsblk_dict = _lsblk_to_dict(treedata, reverse_lsblk)
        lsblk_info = _lsblk_to_info(stdoutdata)
    else:
        lsblk_dict = testdata.get("lsblk_dict")
        lsblk_info = testdata.get("lsblk_info")

    disks_dict = _disks_to_dict(lsblk_dict, lsblk_info)

    #set ssd strategy for detection
    ssd_detect_strategy = _parse_ssd_detection_method(ssd_strategy)
    if ssd_detect_strategy:
        ssd_detect_strategy.do_detect(disks_dict, lsblk_info)

    return disks_dict


def filter_disks(disks_dict):
    """ Filter the disks dictionnary and return only partitions installer
        can partition, format or already managed by itself.

        disks_dict -- disks dictionnary to filter
    """
    uuids = get_our_disks_uuids()

    toremove = []
    for name, desc in sorted(disks_dict.iteritems()):
        disk_usable = False
        try:
            uuid = platform.get_partition_uuid("/dev/" + name)
        except:
            uuid = "dummy_value"
        if uuid in uuids:
            disk_usable = True
            disks_dict[name]["scality"] = True
            continue
        if not disk_usable:
            disk_usable = _disk_is_usable(name, desc)
        if not disk_usable:
            toremove.append(name)

    root_device = None
    for name in toremove:
        if disks_dict[name].get("mountpoint", "") == "/":
            root_device = disks_dict[name].get("lower_device")
            toremove.remove(name)
            del disks_dict[name]
            break

    problems = {}
    for name in toremove:
        if root_device != disks_dict[name].get("lower_device"):
            problems[name] = disks_dict[name]
        del disks_dict[name]

    return problems


def get_our_disks_uuids(fstab="/etc/fstab"):
    """ Lists device names of disks configured by scality in fstab

        fstab -- path to the fstab file
    """
    uuids = []
    parse_next = False
    cre = re.compile("^UUID=(\\S+)\\s+.*")

    f = open(fstab, "r")
    for line in f:
        if parse_next:
            res = cre.search(line)
            if res:
                uuids.append(res.group(1))
            parse_next = False
        if line == DEFAULT_FSTAB_COMMENT + "\n":
            parse_next = True
    f.close()
    return uuids


def _parse_ssd_detection_method(method):
    """ Get the ssd detection strategy method from name.

        method -- the strategy name for ssd detection
    """
    strat = None

    for strategy in ALL_SSD_DETECTION_STRATEGIES:
        if strategy.match(method):
            strat = strategy.make(method)
            break

    if strat is None:
        strat = DefaultSSDDetectionProxy.make()
        if strat is None:
            raise RuntimeError("Could build fallback SSD detection strategy")
        platform.message(
            "ERROR: could not parse SSD detection method %s, using default %s" \
                % (method, strat.name,))

    return strat


def _run_lsblk(options):
    """ Run `lsblk` command and return the process object to be used by
        communicate. If OVERRIDE_LSBLK_BIN exists and is executable, use it
        in place of legit lsblk binary.

        cmd -- the command line options to execute with lsblk
    """
    lsblk_bin = "lsblk"

    if os.path.isfile(OVERRIDE_LSBLK_BIN) and \
            os.access(OVERRIDE_LSBLK_BIN, os.X_OK):
        lsblk_bin = OVERRIDE_LSBLK_BIN

    cmd = options.split()
    cmd.insert(0, lsblk_bin)

    pobj = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=False
    )

    return pobj


def _get_treedata():
    """ Check if `lsblk -s` is available on the platform and collect disk
        topology information.
    """
    reverse_lsblk = True

    pobj = _run_lsblk("-s -i -o NAME")
    stdoutdata, stderrdata = pobj.communicate()

    if pobj.returncode == 0:
        treedata = stdoutdata
    elif pobj.returncode == 1:
        reverse_lsblk = False
        #run normal lsblk expected the "-s" option isnt available on the
        #platform
        pobj = _run_lsblk("-i -o NAME")
        stdoutdata, stderrdata = pobj.communicate()
        if pobj.returncode != 0:
            raise Exception("Error while running lsblk: %s" % stderrdata)
        else:
            treedata = stdoutdata
    else:
        raise Exception("Error while running lsblk: %s" % stderrdata)

    return treedata, reverse_lsblk


def _get_infodata(lsblk_options, reverse_lsblk):
    """ Collect disks informations with lsblk.

        lsblk_options -- the command line options to pass to lsblk
        reverse_lsblk -- flag to know if lsblk have the `-s` switch
    """
    #compute final lsblk command to execute to collect devices details
    if reverse_lsblk:
        if lsblk_options is None:
            lsblk_options = "-s -b -P -n -o %s" % ",".join(LSBLK_COLUMNS)
        else:
            lsblk_options = "-s %s" % lsblk_options
    else:
        if lsblk_options is None:
            lsblk_options = "-b -P -n -o %s" % ",".join(LSBLK_COLUMNS)
        else:
            lsblk_options = "%s" % lsblk_options

    pobj = _run_lsblk(lsblk_options)
    stdoutdata, stderrdata = pobj.communicate()

    if pobj.returncode != 0:
        raise Exception("Error while running lsblk: %s" % stderrdata)

    return stdoutdata


def _lsblk_to_info(output):
    """ Reads the output of lsblk command and fill structured dict of disks
        infos. One line looks like NAME="sda1" TYPE="part" ...

        output -- the text data from lsblk
    """
    ret = []
    for line in output.split("\n"):
        if len(line) == 0:
            break

        # split line in an array [["NAME", "sda1"], ["TYPE", "part"] ...]
        pairs = [kv.split("=") for kv in shlex.split(line)]

        # Check all the fields are present
        keys = [pair[0] for pair in pairs]
        if 0 != cmp(keys, LSBLK_COLUMNS):
            raise Exception("bad lsblk output, maybe version mismatch")

        values = dict((k, v) for (k, v) in pairs)
        ret.append(values)

    return ret


def _lsblk_to_dict(treedata, reverse_lsblk):
    """ Parse the output of lsblk and return it as dict list with final
        partitions leaf as key.

        treedata -- the tree data output returned by lsblk -s -i / lsblk -i
        reverse_lsblk -- flag to indicate if lsblk launched with -s or not
    """
    cur_depth = 0
    lsblk_dict = {}
    nodepath = []

    for line in treedata.split("\n"):
        if not line:
            continue

        depth, name = _get_depth_name(line)

        if depth == -1:
            if nodepath:
                if not reverse_lsblk:
                    nodepath.reverse()
                _nodepath_as_dict(nodepath, lsblk_dict)
            nodepath = [name]
            cur_depth = depth
        elif depth == cur_depth and name != nodepath[-1]:
            tmppath = nodepath[:]
            if not reverse_lsblk:
                tmppath.reverse()
            _nodepath_as_dict(tmppath, lsblk_dict)
            nodepath.pop(-1)
            nodepath.append(name)
        elif depth > cur_depth:
            nodepath.append(name)
            cur_depth = depth
        else:
            if not reverse_lsblk:
                nodepath.reverse()
            _nodepath_as_dict(nodepath, lsblk_dict)
            cur_depth = depth

    if nodepath:
        if not reverse_lsblk:
            nodepath.reverse()
        _nodepath_as_dict(nodepath, lsblk_dict)

    return lsblk_dict


def _disks_to_dict(lsblk_dict, lsblk_facts):
    """ Read the output of the lsblk command and fill a structured dict of
        disks.

        lsblkdata -- the data list returned by the lsblk command
    """
    disks_dict = {}

    for values in lsblk_facts:
        curdisk = values["NAME"]

        #skip partitions that arent leaf
        #problem on centos with NAME that change between:
        #lsblk -s and lsblk -s -P, so try to compute the final name
        key = None
        if lsblk_dict.has_key(curdisk):
            key = curdisk
        elif lsblk_dict.has_key("%s (%s)" % (curdisk, values["KNAME"])):
            key = "%s (%s)" % (curdisk, values["KNAME"])

        if key is None:
            continue
        else:
            lower_device = lsblk_dict[key]["lower_device"]

        if (values["RM"] == "0" or int(values["SIZE"]) > RM_DEV_MIN_SIZE) \
           and values["RO"] == "0":
            name = values["KNAME"]
            #skip already registred partition
            if disks_dict.has_key(name):
                continue
            #setup partition details
            disks_dict[name] = {
                "size": values["SIZE"],
                "ssd" : False,
                "ssd_seeks": None,
                "partitioned" : not values["TYPE"] in ("disk", "rom"),
                "mounted": values["MOUNTPOINT"] != "",
                "formated": not values["FSTYPE"] in NOT_FORMATED_FSTYPE,
                "mountpoint": values["MOUNTPOINT"],
                "type": values["TYPE"],
                "lower_device": lower_device,
            }

    return disks_dict


def _disk_is_usable(name, desc):
    """ Usable disks are only those un partitioned or having an unformatted
        partition.

        name -- the name of the partition
        desc -- the dictionnary details of the partition
    """
    if int(desc["size"]) < PARTITION_LIMIT_SIZE:
        return False
    if desc["formated"] or desc["mounted"]:
        return False

    return True


def _get_depth_name(line):
    """ Return the depth and device name for a line returned by
        lsblk -s -i / lsblk -i.

        line -- one line from lsblk output
    """
    depth = line.find("|")
    if depth == -1:
        depth = line.find("`")

    if depth == -1:
        return (depth, line)
    else:
        return (depth/2, line[depth+2:])


def _nodepath_as_dict(nodepath, data, lower_device=None):
    """ Set data dictionnary with devices from path.

        nodepath -- the device and/or partitions path to set
        data     -- the dictionnary where set device path
    """
    if len(nodepath):
        key = nodepath[0]
        if lower_device is None:
            lower_device = nodepath[-1]
        if not data.has_key(key):
            data[key] = {"lower_device": lower_device}
        _nodepath_as_dict(nodepath[1:], data[key], lower_device)
