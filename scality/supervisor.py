# -*- coding: utf-8 -*-
""" @file supervisor.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:32:19 CEST 2012

    @brief implement methods to command a supervisor
"""

import re
import signal

from multiprocessing import Pool

from scality.ov import OvDaemon
from scality.node import Node
from scality.common import (ScalDaemonException,
                            ScalDaemonExceptionInvalidParameter,
                            ScalDaemonExceptionCommandError)
from scality.key import Key, KeyRandom, KeyPercent

from scality.common import json_load

try:
    import hashlib

    def md5():
        """ a dummy function to make pylint & co happier """
        return hashlib.new("md5")
except ImportError:
    from md5 import md5

DEFAULTCMPTREE = "/dso/unassociated/"


def flatten(et):
    res = {}
    if et == None:
        return None
    for p in et:
        if (p.text != None and p.text != ""):
            res[p.tag] = p.text
        else:
            res[p.tag] = flatten(p)
    return res


class Supervisor(OvDaemon):

    @property
    def url(self):
        return self._xc.url

    @property
    def login(self):
        return self._xc.login

    @property
    def passwd(self):
        return self._xc.passwd

    def supervisorActionRingNode(self, action, dsoname, f_state_is=None, f_task_gt=None, f_name_co=None, f_ttype_is=None, limit_count=None):
        """ API to launch a command on all or a part of ring """
        if action not in ["join", "pushconf", "rebuild", "repair", "balance", "purge", "stoptasks", "t1syncstart", "t1syncstop", "leave", "arstart", "arstop", "arresume", "arsuspend", "deassociate", "cleartaskblock", "forget"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        params = {
            "action": action,
            "dsoname": dsoname,
        }
        if f_state_is is not None:
            params["f_state_is"] = str(f_state_is)
        if f_task_gt is not None:
            params["f_task_gt"] = str(f_task_gt)
        if f_name_co is not None:
            params["f_name_co"] = str(f_name_co)
        if f_ttype_is is not None:
            params["f_ttype_is"] = str(f_ttype_is)
        if limit_count is not None:
            params["limit_count"] = str(limit_count)

        et = self._xc.get_ET("supervisor action ring node", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor action ring node", et.text)
        return self._supervisoractionring_parse(et)

    def supervisorActionRingConnector(self, action, dsoname, f_state_is=None):
        """ API to launch a command on all or a part of ring """
        if action not in ["setauto", "sendconf", "forget"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        params = {
            "action": action,
            "dsoname": dsoname,
        }
        if f_state_is is not None:
            params["f_state_is"] = str(f_state_is)
        et = self._xc.get_ET("supervisor action ring connector", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor action ring connector", et.text)
        return self._supervisoractionring_parse(et)

    @staticmethod
    def _supervisoractionring_parse(et):
        """ internal """
        res = {"meta": {}, "objects": {}}

        for s in et.findall("./meta/*"):
            res["meta"][s.tag] = s.text

        for s in et.findall("./objects/node"):
            name = s.findtext("name")
            addr = s.findtext("addr")
            vnodeid = s.findtext("vnodeid", None)
            result = s.findtext("result")
            msg = s.findtext("msg", "")
            res["objects"][name + "/" + str(vnodeid)] = {
                "addr": addr, "vnodeid": vnodeid, "name": name, "result": result, "msg": msg}
        return res

    def supervisorShowHardware(self, f_address_is=None, limit=None, offset=None, f_name_co=None, f_state_is=None, f_zone_is=None):
        """ API for showing zones & agents """
        params = {}
        if f_address_is is not None:
            params["f_address_is"] = f_address_is
        if limit:
            params["limit_count"] = limit
        if offset:
            params["limit_offset"] = offset
        if f_name_co:
            params["f_name_co"] = f_name_co
        if f_state_is:
            params["f_state_is"] = f_state_is
        if f_zone_is:
            params["f_zone_is"] = f_zone_is
        et = self._xc.get_ET("supervisor show hardware", params)
        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor show hardware", et.text)

        return self._supervisorshowhardware_parse(et)

    @staticmethod
    def _supervisorshowhardware_parse(et):
        """ internal """
        res = []
        for z in et.findall("./zones/zone"):
            zname = z.findtext("name")
            zdescr = z.findtext("descr")
            agents = []
            zstats = {}
            [zstats.__setitem__(x.tag, x.text) for x in z.findall("stats/*")]
            for s in z.findall("./agent"):
                node = {}
                [node.__setitem__(x.tag, x.text) for x in s.findall("node/*")]
                stats = {}
                [stats.__setitem__(x.tag, x.text)
                 for x in s.findall("stats/*")]

                daemons = []
                all_disks = {}

                for d in s.findall("daemon"):
                    name = d.findtext("name")
                    address = d.findtext("address")
                    state = d.findtext("state")
                    type_ = d.findtext("type")
                    # parse

                    daemon = {
                        "name": name, "address": address, "type": type_, "state": state}

                    if type_ == "node":
                        node_ = None
                        if d.find("node"):
                            node_, disks = Supervisor._supervisorconfigdso_node_parse(
                                d.find("node"))
                            node_["disks"] = disks
                            all_disks.update(disks)
                        daemon["node"] = node_
                    elif type_ == "restconnector":
                        daemon["restconnector"] = Supervisor._scp_connector_parse(
                            d.find("bizstore"))
                    daemons.append(daemon)

                agents.append(
                    {"node": node, "stats": stats, "daemons": daemons, "disks": all_disks})

            res.append(
                {"name": zname, "agents": agents, "stats": zstats, "descr": zdescr})
        stats = {}
        [stats.__setitem__(x.tag, x.text)
         for x in et.findall("./globalstats/*")]
        return {"zones": res, "stats": stats}

    def cmpClusterList(self, action=None, extra_params=None, doparse=True):
        params = {}
        if action is not None:
            if action not in ["add", "delete", "move", "deletetab"]:
                raise ScalDaemonExceptionInvalidParameter("action", action)
            params["action"] = action
            if extra_params is not None:
                params.update(extra_params)
        elif extra_params is not None:
            raise ScalDaemonExceptionInvalidParameter(
                "extra_params", extra_params, "extra_params is not None but no action")

        et = self._xc.get_ET("cmp cluster list", params)
        error = self.getError(et)
        if error:
            raise error.get_exception("cmp cluster list")
        if doparse:
            return self._cmpclusterlist_parse(et)

    def cmpClusterCreateGroup(self, path, name):
        """ helper function to create a group """
        if path is None or len(path) == 0:
            raise ScalDaemonExceptionInvalidParameter("path", path)
        if name is None or len(name) == 0:
            raise ScalDaemonExceptionInvalidParameter("name", name)
        self.cmpClusterList(action="add", extra_params={
                            "type": "group", "path": path, "name": name}, doparse=False)

    def cmpClusterDeleteGroup(self, path, name):
        """ helper function to delete a group """
        if path is None or len(path) == 0:
            raise ScalDaemonExceptionInvalidParameter("path", path)
        if name is None or len(name) == 0:
            raise ScalDaemonExceptionInvalidParameter("name", name)
        self.cmpClusterList(action="delete", extra_params={
                            "type": "group", "path": path, "name": name}, doparse=False)

    def supervisorConfigMain(self, action="view", dsoname=None, doparse=True, **extra_params):
        params = {}
        if action not in ["view", "add", "del"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        if action == "add":
            if not dsoname:
                raise ScalDaemonExceptionInvalidParameter(
                    "dsoname", dsoname, "cannot add if no dso set")
            params["action"] = "add"
            params["dsoname"] = dsoname
        elif action == "del":
            if not dsoname:
                raise ScalDaemonExceptionInvalidParameter(
                    "dsoname", dsoname, "cannot del if no dso set")
            params["action"] = "del"
            params["dsoname"] = dsoname
        elif dsoname is not None:
            raise ScalDaemonExceptionInvalidParameter(
                "dsoname", dsoname, "dso set but action is not add or del")

        params.update(extra_params)

        et = self._xc.get_ET("supervisor config main", params)
        if et.tag == "reswarning" or et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor config main", et.text)

        if doparse is True:
            return self._supervisorconfigmain_parse(et)

    @staticmethod
    def _supervisorconfigmain_parse(et):
        dlist = {}
        for r in et.findall("dso"):
            states = []
            for s in r.findall("states/state"):
                name = s.find("name").text
                val = s.find("value").text
                states.append({"name": name, "value": val})

            name = r.find("name").text
            count = r.find("count").text
            keysize = r.find("keysize").text

            dlist[name] = {"name": name, "count": count,
                           "keysize": keysize, "states": states}
        return dlist

    def supervisorConfigNode(self, action="view", address=None, vnodeid=0, extra_params=None, doparse=True):
        """ call supervisor config node on bizstoresup allowing to get the status of a node or
        to send a command (action) """
        if action not in ["view", "join", "move", "leave", "task_cancel", "forget"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        if address is None:
            raise ScalDaemonExceptionInvalidParameter("address", address)

        if not isinstance(vnodeid, int) or vnodeid < 0:
            raise ScalDaemonExceptionInvalidParameter("vnodeid", vnodeid)

        params = {}
        if extra_params:
            params.update(extra_params)

        params["action"] = action
        params["addr"] = address
        params["vnodeid"] = str(vnodeid)

        et = self._xc.get_ET("supervisor config node", params)

        error = self.getError(et)
        if error:
            raise error.get_exception("supervisor config node")

        if doparse is True:
            return self._supervisorconfignode_parse(et)

    @staticmethod
    def _supervisorconfignode_parse(et):
        state = [et.find("./node/state").text] + \
            [s.text for s in et.findall("./node/states/state/name")]

        node, disks = Supervisor._supervisorconfigdso_node_parse(
            et.find("./node"))
        node["disks"] = disks
        node["tasks"] = [Supervisor._supervisorconfigdso_task_parse(
            x) for x in et.findall("./tasks/task")]
        return node

    @staticmethod
    def _supervisorconfigdso_task_parse(t):
        """ internal: parse a task """
        type_ = t.findtext("./type", None)
        done = t.findtext("./done", None)
        total_prev = total = t.findtext("./total", None)
        tid = t.findtext("./tid", None)
        dest = t.findtext("./dest", None)
        running = t.findtext("./running", None)
        start = t.findtext("./start", None)
        end = t.findtext("./end", None)
        size_done = int(t.findtext("./size_done", "0"))
        est_size_total_prev = est_size_total = int(t.findtext("./est_size_total", "0"))
        flag_diskrebuild = t.findtext("./flag_diskrebuild", None)
        curpos = t.findtext("curpos", None)
        if type_ == "move" and curpos:
            try:
                pct = KeyPercent(Key(start), Key(end), Key(curpos))
                total = int(float(done) / pct)
                est_size_total = int(float(size_done) / pct)
            except:
                pass

        return {
            "type": type_,
            "done": done,
            "total": total,
            "total_prev": total_prev,
            "tid": tid,
            "dest": dest,
            "running": running,
            "start": start,
            "end": end,
            "curpos": curpos,
            "size_done": size_done,
            "est_size_total": est_size_total,
            "est_size_total_prev": est_size_total_prev,
            "flag_diskrebuild": flag_diskrebuild,
            "starting_time": int(t.findtext("./starting_time", "0"))
        }

    def supervisorTasksDso(self, dsoname=None, f_type_is=None, f_name_co=None, limit_offset=0, limit_count=0, doparse=True):
        """ call supervisor tasks dso xcmd and return a parsed result """
        if dsoname is None:
            raise ScalDaemonExceptionInvalidParameter("dsoname", dsoname)

        params = {"dsoname": dsoname}
        if f_type_is:
            params["f_type_is"] = f_type_is

        if f_name_co:
            params["f_name_co"] = f_name_co

        if limit_offset:
            params["limit_offset"] = limit_offset

        if limit_count:
            params["limit_count"] = limit_count

        et = self._xc.get_ET("supervisor tasks dso", params)

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor tasks dso", et.text)

        if doparse:
            return self._supervisortasksdso_parse(et)

    @staticmethod
    def _supervisortasksdso_parse(et):
        """ parse a "sup tasks dso" xcommand result """
        meta = {}
        _ = [meta.__setitem__(x.tag, x.text) for x in et.findall("./meta/*")]

        tasks = []
        for t in et.findall("./tasks/task"):
            new = Supervisor._supervisorconfigdso_task_parse(t)
            new["node"] = {
                "name": t.findtext("./node/name", None),
                "vnodeid": int(t.findtext("./node/vnodeid", "0")),
                "addr": t.findtext("./node/addr", None),
            }
            tasks.append(new)
        return {"meta": meta, "tasks": tasks}

    def supervisorConfigDso(self, action="view", dsoname=None, extra_params=None, doparse=True):
        if action not in ["view", "move", "join", "leave", "purge", "params",
                          "addctmpl", "delctmpl", "modctmpl", "deassociate",
                          "associate", "assignid", "forceproxy", "diffconfig"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        if dsoname is None:
            raise ScalDaemonExceptionInvalidParameter("dsoname", dsoname)

        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update(
            {"action": action, "mergedisks": "1", "dsoname": dsoname})

        et = self._xc.get_ET("supervisor config dso", params)

        error = self.getError(et)
        if error:
            raise error.get_exception("supervisor config dso")

        if doparse and action == "diffconfig":
            return self._supervisorconfigdso_diffconfig_parse(et)
        elif doparse:
            return self._supervisorconfigdso_parse(et)

    def _supervisorconfigdso_diffconfig_parse(self, et):
        node_diff = []
        for node in et.findall("./config_check/node"):
            status = {
                "name": node.findtext("name"),
                "addr": node.findtext("addr"),
                "status": node.findtext("status"),
            }
            for diff in node.findall("./diff"):
                status.setdefault("diff", {})[diff.findtext("name")] = {
                    "node": diff.findtext("node_value"),
                    "ring": diff.findtext("ring_value"),
                }

            node_diff.append(status)
        return node_diff

    def supervisorConfigVolume(self, action="view", extra_params=None):
        """ main function to manage volumes (supervisor point of view) """
        if action not in ["view", "add", "del", "modify", "create-catalog"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)

        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({
            "action": action
        })

        et = self._xc.get_ET("supervisor config volume", params)
        if et is not None and (et.tag == "reserror" or et.tag == "error"):
            raise ScalDaemonExceptionCommandError("supervisor config volume", et.text)

        return self._supervisorconfigvol_parse(et)

    def volumeSofsAdd(self, name, sofs_dev_id,
                      sofs_ring_name, sofs_repl_policy,
                      sofs_md_ring_name, sofs_md_repl_policy):
        """ helper to add a new SoFS volume """
        params = {
            "type": "sofs",
            "name": name,
            "sofs_dev_id": sofs_dev_id,
            "sofs_ring_name": sofs_ring_name,
            "sofs_repl_policy": sofs_repl_policy,
            "sofs_md_ring_name": sofs_md_ring_name,
            "sofs_md_repl_policy": sofs_md_repl_policy,
        }
        return self.supervisorConfigVolume("add", params)

    def volumeDel(self, id_):
        """ helper to remove a volume """
        params = {
            "id": id_,
        }
        return self.supervisorConfigVolume("del", params)

    def volumeMod(self, id_, params=None):
        """ helper to modify a volume (only change of name right now) """
        try:
            id_ = int(id_)
        except ValueError:
            raise ScalDaemonExceptionInvalidParameter("id_", id_)

        extra_params = {}
        if params:
            extra_params.update(params)
        extra_params["id"] = str(id_)

        return self.supervisorConfigVolume("modify", extra_params)

    def nodeAssignId(self, key, dsoname, address, ip=None, vnodeid=0):
        key = Key(key)

        if dsoname is None or dsoname == "":
            raise ScalDaemonExceptionInvalidParameter("dsoname", dsoname)

        if address is None or ":" not in address:
            raise ScalDaemonExceptionInvalidParameter("address", address)

        if ip is None:
            ip = address.split(":")[0]

        return self.supervisorConfigDso(action="assignid", dsoname=dsoname, extra_params={
            "addr": address,
            "vnodeid": str(vnodeid),
            "ip": ip,
            "key": key.getHex()
        })

    @staticmethod
    def _supervisorconfigdso_node_parse(n, alldisks={}):
        if n is None:
            return None, None
        disks = {}
        status = [
            {"name": s.text.replace(" ", "_"), "value": "OK"} for s in n.findall("state")]
        for s in n.findall("states/state"):
            name = s.find("name").text.replace(" ", "_")
            if name == "BALANCING(SOURCE)":
                name = "BAL(SRC)"
            elif name == "BALANCING(DEST)":
                name = "BAL(DST)"
            val = s.find("value").text
            status.append({"name": name, "value": val})

        addr = n.find("addr").text
        adminaddress, cmpport = addr.split(":")
        vnodeid = n.find("vnodeid").text
        name = n.find("name").text
        reachable = n.find("reachable").text
        key = n.find("key").text
        nbtasks = int(n.findtext("./stats/tasks/nb", "0"))
        tasks_blocked = n.find("tasks_blocked").text
        sig = n.findtext("./sig_config", "")
        sig_dso = n.findtext("./sig_config_dso", "")

        chordport = n.findtext("config/port", "")
        ip = n.findtext("config/ip", "")  # AKA chordip
        assigned_key = n.findtext("./config/key", "")
        adminport = n.findtext("config/adminport", "")
        nbchunks = n.findtext("./stats/chunks/nb", "")

        stats = flatten(n.find("./stats"))
        dso = n.findtext("./ring/dsoname")
        predecessor_key = n.findtext("./ring/predecessor/key", "")
        successor_key = n.findtext("./ring/successor/key", "")
        dso = n.findtext("./ring/dsoname")

        conf_mismatch = "no"
        if sig is not None and sig_dso is not None and sig != sig_dso:
            conf_mismatch = "yes"

        node = {
            "name": name,
            "vnodeid": vnodeid,
            "reachable": reachable,
            "addr": addr,
            "adminaddress": adminaddress,
            "adminport": adminport,
            "chordaddress": ip,
            "chordport": chordport,
            "cmpport": cmpport,
            "nbchunks": nbchunks,
            "ip": ip,
            "key": key,
            "assigned_key": assigned_key,
            "predecessor_key": predecessor_key,
            "successor_key": successor_key,
            "min_key": n.findtext("./range/start", ""),
            "max_key": n.findtext("./range/last", ""),
            "tasks_blocked": tasks_blocked,
            "nbtasks": nbtasks,
            "conf_mismatch": conf_mismatch,
            "dso": dso,
            "stats": stats,
            "state": [st["name"] for st in status]
        }
        if n.find("biziostoreslist") is not None:
            for disk in n.find("biziostoreslist").findall("disk"):
                diskkey = disk.text
                if diskkey in alldisks:
                    fsid0 = alldisks[diskkey]["fsid0"]
                    fsid1 = alldisks[diskkey]["fsid1"]
                    disks[fsid0 + fsid1] = alldisks[diskkey]
        elif n.find("biziostores") is not None:
            for disk in n.find("biziostores").findall("biziostore"):
                fsid0 = disk.findtext("fsid0", "")
                fsid1 = disk.findtext("fsid1", "")
                dname = disk.findtext("name", "")
                if len(fsid0 + fsid1) < 8:
                    m = md5()
                    m.update(dname)
                    m.update(ip)
                    dig = m.hexdigest().upper()
                    fsid0 = dig[0:16]
                    fsid1 = dig[16:32]
                if fsid0 + fsid1 in disks:
                    continue
                disktotal = disk.findtext("disktotal", "")
                diskused = disk.findtext("diskused", "")
                diskavail = disk.findtext("diskavail", "")
                diskfull = disk.findtext("diskfull", "")
                diskstatus = []
                for s in disk.findall("./statuslist/status/name"):
                    name = re.sub(r"\s", "_", s.text)
                    diskstatus.append(name)
                if len(diskstatus) <= 0:
                    diskstatus.append("OK")

                disks[fsid0 + fsid1] = {
                    "name": dname,
                    "ip": ip,
                    "host": addr.split(":")[0],
                    "diskused": diskused,
                    "diskavail": diskavail,
                    "disktotal": disktotal,
                    "diskfull": diskfull,
                    "status": diskstatus,
                    "fsid0": fsid0,
                    "fsid1": fsid1,
                }
        node["disks"] = [x["name"] for x in disks.values()]
        return node, disks

    @staticmethod
    def _supervisorconfigdso_parse(et):
        state = []
        for s in et.findall("dso/states/state"):
            name = re.sub(r"\s", "_", s.find("name").text)
            val = s.find("value").text
            state.append(name)

        nodes = []
        disks = {}
        for d in et.findall("dso/disks/disk"):
            fullname = d.findtext("name", "")
            ip, dname = fullname.split("/")
            for disk in d.findall("biziostore"):
                dname = disk.findtext("name", "")
                disktotal = disk.findtext("disktotal", "")
                diskused = disk.findtext("diskused", "")
                diskavail = disk.findtext("diskavail", "")
                fsid0 = disk.findtext("fsid0", "")
                fsid1 = disk.findtext("fsid1", "")
                diskfull = disk.findtext("diskfull", "")
                diskstatus = []
                for s in disk.findall("./statuslist/status/name"):
                    name = re.sub(r"\s", "_", s.text)
                    diskstatus.append(name)
                if len(diskstatus) <= 0:
                    diskstatus.append("OK")
                if not re.match("[0-9A-F]{8,}", fsid0 + fsid1):
                    m = md5()
                    m.update(dname)
                    m.update(ip)
                    dig = m.hexdigest().upper()
                    fsid0 = dig[0:16]
                    fsid1 = dig[16:32]

                disks[fsid0 + fsid1] = {
                    "name": dname,
                    "ip": ip,
                    "host": fullname.split(":")[0],
                    "diskused": diskused,
                    "diskavail": diskavail,
                    "disktotal": disktotal,
                    "diskfull": diskfull,
                    "fullname": fullname,
                    "status": diskstatus,
                    "fsid0": fsid0,
                    "fsid1": fsid1,
                }

        disks_by_fullname = dict([(x["fullname"], x) for x in disks.values()])

        for n in et.findall("dso/nodes/node"):
            mynode, mydisks = Supervisor._supervisorconfigdso_node_parse(n, disks_by_fullname)
            if mynode:
                nodes.append(mynode)
            if mydisks:
                disks.update(mydisks)

        params = []
        for s in et.findall("dso/params/*"):
            params.append((s.tag, s.text))

        ctmpls = {}
        for c in et.findall("dso/ctmpls/*"):
            name = c.findtext("name", "")
            ctmpl = []
            for e in c.findall("conf"):
                confname = e.findtext("./name", "")
                conftype = e.findtext("./type", "")
                confvalue = e.findtext("./value", "")
                if confvalue == None:
                    confvalue = ""
                ctmpl.append((confname, conftype, confvalue))
            ctmpls[name] = ctmpl

        return {"state": list(set(state)), "nodes": nodes, "params": params, "ctmpls": ctmpls, "disks": disks.values()}

    @staticmethod
    def _supervisorconfigvol_parse(et):
        volumes = []
        if not et:
            return volumes

        if et.tag == "volume":
            et_volumes = [et]
        else:
            et_volumes = et.findall("volume")

        for data in et_volumes:
            volume = {
                "id": int(data.find("id").text),
                "name": data.find("name").text,
                "type": data.find("type").text
            }

            if volume["type"] == "sofs":
                volume.update({
                    "sofs_device_id": int(data.find("sofs_dev_id").text),
                    "sofs_ring_name": data.find("sofs_ring_name").text,
                    "sofs_repl_policy": data.find("sofs_repl_policy").text,
                    "sofs_md_ring_name": data.find("sofs_md_ring_name").text,
                    "sofs_md_repl_policy": data.find("sofs_md_repl_policy").text,
                    "sofs_catalog_created_at": int(data.find("sofs_catalog_created_at").text)
                })

            volumes.append(volume)

        return volumes

    @staticmethod
    def _cmpclusterlist_parse(et):
        p = []
        p.append((et, ""))

        grouplist = {}
        nodelist = []

        while len(p) > 0:
            (et, root) = p.pop(0)
            name = et.get("name")
            if name is not None:
                grpname = root + "/" + name
                grouplist[grpname] = {"name": grpname}
            else:
                grpname = root
            for g in et.findall("group"):
                if name is not None:
                    p.append((g, root + "/" + name))
                else:
                    p.append((g, root))

            for i in et.findall("./imp"):
                iname = i.findtext("name")
                if not iname:
                    continue
                fullname = grpname + "/" + iname
                adminport = i.findtext("./admin/port", "")
                ip = i.findtext("./ip", "")
                port = i.findtext("./port", "")
                hostname = ip + ":" + port
                itype = i.findtext("./type", "")
                version = i.findtext("./version", "")
                buildrevision = i.findtext("./buildrevision", "")
                build = i.findtext("./build", "")
                status = i.findtext("./status", "") != "0" and True or False
                dopoll = i.findtext("./dopoll", "") != "0" and True or False
                polling = i.findtext("./polling", "") != "0" and True or False
                desc = i.findtext("./description", "")
                ssl = not (
                    i.findtext("./pollnossl", "0") == "1" and True or False)
                stats = {}
                for s in i.findall("./stats/*"):
                    sname = s.tag
                    value = s.findtext("./value", "")
                    ratio = s.findtext("./ratio", "")
                    stats[name] = {
                        "name": sname, "value": value, "ratio": ratio}

                nodelist.append({"fullname": fullname,
                                 "name": iname,
                                 "grpname": grpname,
                                 "adminport": adminport,
                                 "ip": ip,
                                 "port": port,
                                 "hostname": hostname,
                                 "ssl": ssl,
                                 "itype": itype,
                                 "version": version,
                                 "buildrevision": buildrevision,
                                 "build": build,
                                 "status": status,
                                 "dopoll": dopoll,
                                 "polling": polling,
                                 "desc": desc,
                                 "stats": stats
                                 })

        return {"grouplist": grouplist, "nodelist": nodelist}

    def supervisorConfigBizstore(self, action="view", dso_filter=None, extra_params=None, doparse=True):
        """ implement supervisor config bizstore """
        if action not in ["view", "sendconf", "changeparam", "setauto", "forget"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)
        params = {}
        if extra_params is not None:
            params.update(extra_params)
        params.update({"action": action})
        if dso_filter is not None:
            params["f_dso_is"] = dso_filter

        et = self._xc.get_ET("supervisor config bizstore", params)

        if et.tag == "reserror":
            raise ScalDaemonExceptionCommandError("supervisor config bizstore", et.text)
        if doparse:
            return self._supervisorconfigbizstore_parse(et, dso_filter=dso_filter)

    @staticmethod
    def _scp_connector_parse(r):
        """ parse a single bizstore leaf """
        ident = r.findtext("name/ident")
        port = ident.split(":")[1]
        # sagentdport = r.findtext("name/port")
        reachable = r.findtext("./reachable")
        state = set()
        if reachable == "yes":
            state.add("RUN")
        bootstraps = []
        for abootstrap in r.findall("./bootstraps/node"):
            bootstraps.append(
                (abootstrap.findtext("ip"), abootstrap.findtext("port")))
        if len(bootstraps) <= 0 and reachable == "yes":
            state.add("NO_BOOTSTRAP")

        state.update(i.text for i in r.findall("states/state/name"))

        ctmpl_name = r.findtext("ctmpl_name", "")
        if not ctmpl_name:
            ctmpl_name = "default"
        return {
            "name": r.findtext("name/node"),
            "ident": ident,
            "addr": r.findtext("name/ip"),
            "port": port,
            "state": list(state),
            "reachable": reachable,
            "restaddress": r.findtext("rest/ip"),
            "restport": r.findtext("rest/port"),
            "adminaddress": ident.split(":")[0],
            "adminport": r.findtext("name/adminport"),
            "bootstraps": bootstraps,
            "dso": r.findtext("dso"),
            "dtype": r.findtext("type", "store"),
            "sig": r.findtext("./sig_config"),
            "sig_dso": r.findtext("./sig_config_dso"),
            "ctmpl_name": ctmpl_name
        }

    @staticmethod
    def _supervisorconfigbizstore_parse(et, dso_filter=None):
        """ internal: parse the supervisor config bizstore xcommand """
        def _parse(leaf, result):
            """ internal: glue between the scb xcommand and the parse of an individual bizstore leaf """
            conn = Supervisor._scp_connector_parse(leaf)
            if dso_filter is None or dso_filter == "" or dso_filter == conn["dso"]:
                result.append(conn)

        online = []
        for r in et.findall("bizstorelist/bizstore"):
            _parse(r, online)
        available = []
        for r in et.findall("bizstorelist/bizstore_available"):
            _parse(r, available)

        return {"restconnectors": online, "restconnectors_available": available}

    def supervisorStatsDso(self, dsoname):
        if dsoname is None or len(dsoname) == 0:
            raise ScalDaemonExceptionInvalidParameter("dsoname", dsoname)

        et = self._xc.get_ET("supervisor stats dso", {"dsoname": dsoname})
        return self._supervisorstatsdso_parse(et)

    @staticmethod
    def _supervisorstatsdso_parse(et):
        g = et.find("./global")
        res = {}
        for i in ["nbobjects", "payload_bytes", "deletedlasttime", "disksizedeleted", "diskavail", "diskused", "disktotal", "replicated_objects_count", "arc_objects_count", "arc_data_parts_count", "arc_checksum_bytes", "arc_coding_parts_count", "arc_payload_bytes", "oos_chunk_count", "bad_chunk_count", "repaired_chunk_count"]:
            res[i] = int(g.findtext("./" + i, "0"))
        return res

    def supervisorConfigZone(self, action="view", extra_params=None, doparse=True):
        if action not in ["view", "add", "del"]:
            raise ScalDaemonExceptionInvalidParameter("action", action)
        params = {}
        if extra_params:
            params.update(extra_params)
        params["action"] = action
        et = self._xc.get_ET("supervisor config zone", params)
        return self._supervisorconfigzone_parse(et)

    @staticmethod
    def _supervisorconfigzone_parse(et):
        zones = {}
        for z in et.findall("zone"):
            name = z.find("./name").text
            descr = z.findtext("./descr", "")
            count = int(z.findtext("./count", "0"))
            zones[name] = {"descr": descr, "count": count}
        return zones

    def dataclusterdaily(self, group, days, stats):
        if not isinstance(days, int) or days <= 0:
            raise ScalDaemonExceptionInvalidParameter("days", days)
        if stats is None or len(stats) == 0:
            raise ScalDaemonExceptionInvalidParameter("stats", stats)

        params = {"group": group, "days": str(days), "stats": ",".join(stats)}
        res = self._xc.get("dataclusterdaily", params)
        return self._dataclusterdaily_parse(res)

    @staticmethod
    def _dataclusterdaily_parse(data):
        v = json_load(data)
        res = {"columns": v["stats"], "date": v["date"], "data": []}
        for i in v["data"]:
            res["data"].append({"values": i["val"]})
        return res

    def nodeCmpMove(self, oldpath, newpath, ip, port):
        if newpath is None:
            raise ScalDaemonExceptionInvalidParameter("newpath", newpath)

        cmplist = self.cmpClusterList()
        if oldpath is None:
            if ip is None:
                raise ScalDaemonExceptionInvalidParameter("ip", ip)
            if port is None:
                raise ScalDaemonExceptionInvalidParameter("port", port)
            oldpath = [x["grpname"] for x in cmplist["nodelist"]
                       if x["ip"] == ip and x["port"] == port][0]
            oldpath += "/"

        self._create_cmp_tree(cmplist, newpath)
        self.cmpClusterList(action="move", extra_params={
                            "path": oldpath, "newpath": newpath, "ip": ip, "port": port}, doparse=False)

    def nodeSetRing(self, name, node, cmpport):
        """ associate a node to a ring """
        try:
            self.nodeCmpMove(None, "/dso/" + name + "/nodes/", node, cmpport)
        except:
            pass  # failing is ok
        self.supervisorConfigDso(action="associate", dsoname=name, extra_params={
                                 "addr": node + ":" + str(cmpport), "vnodeid": "0"})

    def nodeRemoveRing(self, name, node, cmpport):
        """ deassociate a node from a ring """
        try:
            self.nodeCmpMove(None, "/dso/unassociated/nodes/", node, cmpport)
        except:
            pass  # failing is ok
        self.supervisorConfigDso(action="deassociate", dsoname=name, extra_params={
                                 "addr": node + ":" + str(cmpport), "vnodeid": "0"})

    def connectorSetRing(self, name, node, cmpport):
        """ associate a rest connector to a ring """
        try:
            self.nodeCmpMove(
                None, "/dso/" + name + "/accessors/", node, cmpport)
        except:
            pass  # failing is ok
        ident = node + ":" + cmpport
        self.supervisorConfigBizstore(action="changeparam", extra_params={
                                      "ident": ident, "dso": name, "dsoname": name}, doparse=False)

    def connectorRemoveRing(self, name, node, cmpport):
        """ remove a rest connector from a ring """
        try:
            self.nodeCmpMove(
                None, "/dso/unassociated/accessors/", node, cmpport)
        except:
            pass  # failing is ok
        ident = node + ":" + cmpport
        self.supervisorConfigBizstore(action="changeparam", extra_params={
                                      "ident": ident, "dso": "", "dsoname": ""}, doparse=False)

    def ringCreate(self, name):
        """ create a new ring """
        cmplist = self.cmpClusterList()
        newpath = "/dso/%s/nodes/" % (name)
        self._create_cmp_tree(cmplist, newpath)
        cmplist = self.cmpClusterList()
        newpath = "/dso/%s/accessors/" % (name)
        self._create_cmp_tree(cmplist, newpath)
        self.supervisorConfigMain(action="add", dsoname=name, doparse=False)

    def ringDelete(self, name, force=False):
        """ delete an existing ring """
        if not force:
            cmplist = self.cmpClusterList()
            path = "/dso/" + name + "/"
            l = len(
                [x for x in cmplist["nodelist"] if x["fullname"].startswith(path)])
            if l != 0:
                raise ScalDaemonException("Cannot delete DSO, there are still %d element(s) in it" % l)

        for p in ["/dso/" + name + "/nodes/", "/dso/" + name + "/accessors/",  "/dso/" + name + "/"]:
            try:
                cmplist = self.cmpClusterList()
                self._delete_cmp_tree(cmplist, p)
            except:
                pass

        self.supervisorConfigMain(action="del", dsoname=name, doparse=False)

    def serverList(self, sfilter=".*"):
        cmplist = self.cmpClusterList()
        return [v for v in sorted(cmplist["nodelist"], key=lambda x: x["fullname"]) if re.search(sfilter, v["fullname"])]

    def serverAdd(self, name, address, cmpport, nossl):
        """ add a new server """
        cmplist = self.cmpClusterList()
        newpath = DEFAULTCMPTREE
        try:
            self._create_cmp_tree(cmplist, newpath)
        except ScalDaemonExceptionCommandError:
            pass  # do not fail if cmp tree cannot be created see RING-17108
        if nossl:
            nossl = "1"
        else:
            nossl = "0"
        self.cmpClusterList(action="add", extra_params={
                            "type": "imp", "path": newpath, "ip": address, "name": name, "port": cmpport, "pollnossl": nossl, "description": ""}, doparse=False)

    def serverRemove(self, address, cmpport):
        """ remove a server from hardware list """
        cmplist = self.cmpClusterList()
        oldpath = DEFAULTCMPTREE
        try:
            oldpath = [x["grpname"] for x in cmplist["nodelist"]
                       if x["ip"] == address and x["port"] == cmpport][0]
            oldpath += "/"
        except:
            pass
        pathkey = "fpath[%s]" % ("%s:%s" % (address, cmpport))
        self.cmpClusterList(
            action="deletetab", extra_params={pathkey: oldpath}, doparse=False)

    def _create_cmp_tree(self, cmplist, path):
        """ internal """
        el = path.split("/")

        base = "/"
        while len(el) > 0:
            d = el.pop(0)
            if d is None or d == "":
                continue
            curname = base + d
            if curname in cmplist["grouplist"]:
                base = curname + "/"
                continue
            self.cmpClusterCreateGroup(base, d)
            base = curname + "/"

    def _delete_cmp_tree(self, _, path):
        """ internal """
        try:
            p = path.split("/")
            name = p[-2]
            del p[-2]
            path = "/".join(p)
        except:
            return
        self.cmpClusterDeleteGroup(path, name)


class KeyProducer(object):

    """ do not instantiate """

    def __new__(cls, *args, **kwargs):
        raise TypeError("cannot be instantiated")

    @classmethod
    def execute(cls, sup, ringname, limit=1000, nb_processes=4):
        """ Produces at least <limit> keys on the ring <ringname>
            - At least one of the replicas exists
        """
        r = []
        try:
            p = Pool(nb_processes, _kp_init,
                     (sup, ringname, sup.supervisorConfigDso(dsoname=ringname)))
            r = p.map(_kp_do, [limit / nb_processes] * nb_processes)
        except KeyboardInterrupt:
            p.terminate()
        p.close()
        p.join()
        res = []
        for x in r:
            res += x
        return res


def _kp_init(sup, ringname, ring):
    """ internal: to be used by main function """
    cls = KeyProducer
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    cls.ring = ring
    cls.ringname = ringname
    cls.nodes = {}
    for n in cls.ring["nodes"]:
        if not "RUN" in n["state"]:
            continue
        addr = n["ip"] + ":" + str(n["chordport"])
        cls.nodes[addr] = Node(chord_addr=addr)


def _kp_do(limit=1000, max_failures=1000):
    """ internal: to be used by main function """
    cls = KeyProducer
    ring = cls.ring
    n = next((x for x in ring["nodes"] if "RUN" in x["state"]), None)
    if n is None:
        raise ValueError("No running node in ring %r" % cls.ringname)
    remote_addr = "%s:%s" % (n["addr"].split(":")[0], n["adminport"])
    node = Node(chord_addr=n["ip"], chord_port=n[
                "chordport"], url="https://%s" % remote_addr, dso=cls.ringname)
    keylist = []
    failures = 0
    while len(keylist) < limit and failures < max_failures:
        #sys.stdout.write("Keys produced: %.5d out of %.5d\r" % (len(keylist), limit))
        k = KeyRandom()
        fs = node.findSuccessor(k)
        if fs["status"] is not True:
            # print "Cannot get FS for %s" % k.getHexPadded()
            continue
        addr = fs["address"]
        if not addr in cls.nodes:
            cls.nodes[addr] = Node(chord_addr=addr)
        newnode = cls.nodes[addr]
        res = newnode.findNearestAbove(k)
        if len(res["idlist"]) > 0:
            k = Key(res["idlist"][0]["id"])
        else:
            failures += 1
            continue

        # keylist.append(k)
        try:
            keylist.append(k.getReplicas(class1translate=True).next())
        except StopIteration:
            pass
    return keylist


class KeyCheck(object):

    """ do not instantiate """
    def __new__(cls, *args, **kwargs):
        raise TypeError("cannot be instantiated")

    @classmethod
    def execute(cls, sup, ringname, keylist, nb_processes=4, method="findnearestabove"):
        """ do a check on all keys in keylist
        method can be one of :
            - reserve: accurate but a little slower
            - checklocal : quickest but does not use proxy
            - check: quick and use proxy but more prone to false positive
            - findnearestabove (default): quick, use proxy but only checks existence
            """
        r = []
        try:
            p = Pool(nb_processes, _kc_init,
                     (sup, ringname, sup.supervisorConfigDso(dsoname=ringname)))
            if method == "reserve":
                r = p.map(_kc_do_reserve, keylist)
            elif method == "check":
                r = p.map(_kc_do_reserve, keylist)
            elif method == "checklocal":
                r = p.map(_kc_do_reserve, keylist)
            elif method == "findnearestabove":
                r = p.map(_kc_do_find_nearest_above, keylist)
            else:
                raise Exception("unknown method")
        except KeyboardInterrupt:
            p.terminate()
        except Exception, e:
            print str(e)
            raise e
        p.close()
        p.join()
        return r


def _kc_init(sup, ringname, ring):
    """ internal """
    cls = KeyCheck
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    n = next((x for x in ring["nodes"] if "RUN" in x["state"]), None)
    if n is None:
        raise ValueError("No running node in ring %r" % ringname)
    remote_addr = "%s:%s" % (n["addr"].split(":")[0], n["adminport"])
    cls.node = Node(chord_addr=n["ip"], chord_port=n[
                    "chordport"], url="https://%s" % remote_addr, dso=ringname)
    cls.nodes = {}
    for n in ring["nodes"]:
        if not "RUN" in n["state"]:
            continue
        addr = n["ip"] + ":" + str(n["chordport"])
        cls.nodes[addr] = Node(chord_addr=addr)


def _kc_do_check(k):
    """ internal """
    cls = KeyCheck
    node = cls.node
    fs = node.findSuccessor(k)
    if fs["status"] is not True:
        print "Cannot get FS for %s" % k.getHexPadded()
        return None
    addr = fs["address"]
    if not addr in cls.nodes:
        cls.nodes[addr] = Node(chord_addr=addr)
    newnode = cls.nodes[addr]
    cl = newnode.check(k)
    status = {"key": k, "nodeaddr": addr, "status": cl["status"]}
    return status


def _kc_do_check_local(k):
    """ internal """
    cls = KeyCheck
    node = cls.node
    fs = node.findSuccessor(k)
    if fs["status"] is not True:
        print "Cannot get FS for %s" % k.getHexPadded()
        return None
    addr = fs["address"]
    if not addr in cls.nodes:
        cls.nodes[addr] = Node(chord_addr=addr)
    newnode = cls.nodes[addr]
    cl = newnode.checkLocal(k)
    status = {"key": k, "nodeaddr": addr, "status": cl["status"]}
    return status


def _kc_do_find_nearest_above(k):
    """ internal """
    cls = KeyCheck
    node = cls.node
    fs = node.findSuccessor(k)
    if fs["status"] is not True:
        print "Cannot get FS for %s" % k.getHexPadded()
        return None
    addr = fs["address"]
    if not addr in cls.nodes:
        cls.nodes[addr] = Node(chord_addr=addr)
    newnode = cls.nodes[addr]
    prevk = Key(int(k.getHexPadded(), 16) - 1)
    res = newnode.findNearestAbove(prevk)
    status = {"key": k, "nodeaddr": addr, "status": "free"}
    if len(res["idlist"]) > 0:
        for tmp in res["idlist"]:
            if Key(tmp["id"]).getHexPadded() == k.getHexPadded():
                status = {"key": k, "nodeaddr": addr, "status": "exist"}
                break
    return status


def _kc_do_reserve(k):
    """ internal """
    cls = KeyCheck
    node = cls.node
    try:
        fs = node.findSuccessor(k)
    except Exception, e:
        print str(e)
        return {"key": k, "nodeaddr": "unk", "status": "error"}

    if fs["status"] is not True:
        # print "Cannot get FS for %s" % k.getHexPadded()
        return {"key": k, "nodeaddr": "unk", "status": "error"}
    addr = fs["address"]
    if not addr in cls.nodes:
        cls.nodes[addr] = Node(chord_addr=addr)
    newnode = cls.nodes[addr]
    try:
        cl = newnode.reserve(k, "read_request")
        status = {"key": k, "nodeaddr": addr, "status": cl["status"]}
        newnode.reserve(k, "cancel_request", cl["transacid"])
    except Exception, e:
        print "%s for %s" % (str(e), k.getHexPadded())
        status = {"key": k, "nodeaddr": addr, "status": "error"}
    return status
