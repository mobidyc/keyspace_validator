#pylint: disable=invalid-name, missing-docstring, unused-argument, no-self-use, line-too-long, unused-variable, too-many-return-statements, too-many-public-methods, too-many-branches, abstract-class-not-used, broad-except

""" @file sophia.py
    @author Harish Mara <harish.mara@scality.com>
    @date Mon Oct 4 14:25:00 EST 2015

    @brief Implementation of operations on sharedcache
"""
import socket
import json
import os
import subprocess
import logging
import copy

from scality.xcommand import XCommand, ScalXCommandException
from scality.common import ScalDaemonException
from scality.sophia.exc import (
    SharedCacheUsageException,
    SharedCacheInvalidConfigException
)


jstatMapStr = {
    "S0C": "capacityK",
    "S1C": "capacityK",
    "S0U": "utilizationK",
    "S1U": "utilizationK",
    "EC": "capacityK",
    "EU": "utilizationK",
    "OC": "capacityK",
    "OU": "utilizationK",
    "PC": "capacityK",
    "PU": "utilizationK",
    "NGC": "capacityK",
    "NGCMN": "mincapacityK",
    "NGCMX": "maxcapacityK",
    "OGC": "capacityK",
    "OGCMN": "mincapacityK",
    "OGCMX": "maxcapacityK",
    "PGC": "capacityK",
    "PGCMN": "mincapacityK",
    "PGCMX": "maxcapacityK",
    "YGC": "young-gc-count",
    "YGCT": "young-gc-time",
    "FGC": "full-gc-count",
    "FGCT": "full-gc-time",
    "GCT": "total-gc-time"
}

DEFAULT_THRESHOLD_INSTANCES = 3
DEFAULT_THRESHOLD_PERIOD = 10

class Sophia(object):
    zkCommandURL = "/commands"

    """ Class for shared cache Operations """
    def __init__(self, config_path):
        """ Initialize the config and other settings"""
        self.config_path = config_path
        self.static_config = None
        self.running_config = None

        self.readConf()

    def readConf(self):
        """
        Open the JSON format conf file and load the settings as a dictionary.
        """
        if not os.path.exists(self.config_path):
            raise SharedCacheInvalidConfigException(
                "Config file {filepath} doesn't exist".format(
                    filepath=self.config_path
                )
            )

        try:
            with open(self.config_path, "r") as f:
                config = json.load(f)
        except Exception as exc:
            raise SharedCacheInvalidConfigException(
                'Unable to load config file "{filepath}": {error}'.format(
                    filepath=self.config_path,
                    error=str(exc)
                )
            )

        self.validateConf(config)

        self.static_config = config
        self.running_config = copy.deepcopy(self.static_config)

    def pushConf(self, raw_config, subpath=None):
        """
        Push the whole configuration or part of it as a JSON object.
        """

        try:
            new_config = json.loads(raw_config)
        except ValueError as exc:
            raise SharedCacheInvalidConfigException(
                "Unable to parse the given configuration: {error}".format(
                    error=str(exc)
                )
            )

        def checkKeyHierarchyIsEq(d1, d2):
            if type(d1) != type(d2):
                raise SharedCacheInvalidConfigException(
                    "Type mismatch between {0} and {1}".format(
                        json.dumps(d1),
                        json.dumps(d2)
                    )
                )

            if not isinstance(d1, dict):
                return

            d1_keys, d2_keys = d1.keys(), d2.keys()
            if set(d1_keys) != set(d2_keys):
                raise SharedCacheInvalidConfigException(
                    "Keys mismatch between config sections: ({0}) and ({1})".format(
                        ",".join(d1_keys),
                        ",".join(d2_keys)
                    )
                )

            for key in d1_keys:
                checkKeyHierarchyIsEq(d1[key], d2[key])

        if subpath is not None and subpath != "/":
            if not subpath.startswith('/'):
                raise SharedCacheInvalidConfigException(
                    "config subpath '{0}' must start with '/'".format(subpath)
                )

            full_conf = copy.deepcopy(self.static_config)
            config_item = full_conf
            conf_subitem_names = subpath.split('/')[1:]
            for conf_subitem_name in conf_subitem_names[:-1]:
                config_item = config_item[conf_subitem_name]

            checkKeyHierarchyIsEq(
                new_config,
                config_item[conf_subitem_names[-1]]
            )

            config_item[conf_subitem_names[-1]] = new_config
        else:
            checkKeyHierarchyIsEq(new_config, self.static_config)
            full_conf = new_config

        self.validateConf(full_conf)

        tmp_filepath = "{filepath}.tmp".format(filepath=self.config_path)
        try:
            with open(tmp_filepath, "w") as f:
                json.dump(full_conf, f, indent=4)
            os.rename(tmp_filepath, self.config_path)
        finally:
            if os.path.exists(tmp_filepath):
                try:
                    os.unlink(tmp_filepath)
                except EnvironmentError as exc:
                    logging.warning(
                        "Unable to remove '{filepath}': {error}".format(
                            filepath=tmp_filepath, error=str(exc)
                        )
                    )

        self.static_config = full_conf

    def validateConf(self, config):
        """
        TODO: implement config validation in this function,
        throw SharedCacheInvalidConfigException on error
        """
        pass

    def isSharedCacheEnabled(self):
        """ Return a bool whether shared cache is enabled or not """
        return self.running_config.get('config').get('cache_enabled')

    def getPollPeriodMs(self):
        """ Return the poll period read from the config """
        return self.running_config.get('watchdog').get('pollPeriodMs')

    def getRestartThresholdInstances(self):
        """ Return the threshold Instances for zookeeper restarts.
            If the zookeeper restarts more than the threshold instances during
            the threshold period, then the watchdog will exit and zookeeper will not
            be restarted.
            When this happens, the watchdog thread needs to be started through a command
            to sophiactl's RESTful API."""
        try:
            instances = int(self.running_config.get('watchdog').get('restartThresholdInstances'))
        except (KeyError, Exception):
            # Should we catch the base exception? Yes, we don't want failures here
            instances = DEFAULT_THRESHOLD_INSTANCES

        return instances

    def getRestartThresholdPeriod(self):
        """ Return the threshold period for zookeeper restarts """
        try:
            period = int(self.running_config.get('watchdog').get('restartThresholdPeriod'))
        except (KeyError, Exception):
            period = DEFAULT_THRESHOLD_PERIOD

        return period

    def getWatchdogPortForHost(self, node):
        """ Return the watchdog port for the given host """
        host_port = self.running_config.get('watchdog').get('bootstrap_list').split(',')
        for i in host_port:
            host, port = i.split(':')
            if host == node:
                return int(port)

    def getHosts(self):
        """ Load the zookeeper nodes and client ports to local variable for easy access """
        nodes = self.running_config.get('zookeeper').get('nodes').split(',')
        cports = self.running_config.get('zookeeper').get('admin.serverPort').split(',')
        if len(nodes) != len(cports):
            raise SharedCacheInvalidConfigException("Number of nodes and ports don't match")

        return dict(zip(nodes, cports))

    def getZKConfPath(self):
        """ Get the path for zookeeper config file """
        return self.running_config.get('zookeeper').get('config_dir')

    def zkNodeIsAlive(self, node):
        """ Return bool whether a given node is responding to HeartBeat('ruok') message """
        json_res = self.getZKNodeStatus(node)
        if json_res is None:
            return False
        return (json_res["error"] is None or
                json_res["error"] == "This ZooKeeper instance is not currently serving requests")

    def getZKNodeStatus(self, node):
        """ Given a zookeeper node name, obtain the status if the node is in ensemble
            Obtain the client port from the config and call the helper function """
        zkHosts = self.getHosts()
        if not zkHosts.has_key(node):
            return None
        port = int(zkHosts[node])
        return self.__getZooKeeperNodeStatus(node, port)

    def __getZooKeeperNodeStatus(self, node, port):
        """ Helper function to get the status of a single node """
        url = "http://" + node + ":" + str(port) + self.zkCommandURL + "/ruok"
        try:
            status = XCommand(url=url, login="dummy", passwd="dummy").curl_request(url)
        except (ScalXCommandException, ScalDaemonException, Exception) as e:
            return dict({"error": str(e)})
        return json.loads(status)

    def __getZooKeeperStatus(self):
        """ Return a string with the status of all nodes in the ensemble """
        res = dict()
        zkHosts = self.getHosts()
        for i in zkHosts.keys():
            status = self.getZKNodeStatus(i)
            node_status = {i: status}
            res.update(node_status)
        return res

    def __getFlushDaemonStatus(self):
        """ To be implemented """
        return dict()

    def __getApiDaemonStatus(self):
        """ To be implemented """
        return dict()

    def getStatus(self, filt='all'):
        """ Return the status as a dictionary according to the filter provided
            The default is 'all', i.e for flushing daemons, api-daemons and zookeeper """

        if filt not in ('zookeeper', 'flushing-daemons', 'api-daemons', 'all'):
            raise SharedCacheUsageException("Invalid filter")

        res = dict()
        if filt in ('all', 'zookeeper'):
            status = self.__getZooKeeperStatus()
            zkStatus = {"zookeeper": status}
            res.update(zkStatus)
        if filt in ('all', 'flushing-daemons'):
            status = self.__getFlushDaemonStatus()
            fdStatus = {"flushing-daemons": status}
            res.update(fdStatus)
        if filt in ('all', 'api-daemons'):
            status = self.__getApiDaemonStatus()
            apStatus = {"api-daemons": status}
            res.update(apStatus)
        return res

    def __getZooKeeperNodeStats(self, node, port, cmd):
        """ Helper function to get stats from single node """
        url = "http://" + node + ":" + str(port) + self.zkCommandURL + "/" + cmd
        try:
            status = XCommand(url=url, login="dummy", passwd="dummy").curl_request(url)
        except ScalXCommandException as e:
            return dict({"error": str(e)})
        except ScalDaemonException as e:
            return dict({"error": str(e)})
        return json.loads(status)

    def getZKNodeStats(self, node, cmd):
        """ Given a zookeeper node name, obtain the stat if the node is in ensemble
            Obtain the client port from the config and call the helper function """
        zkHosts = self.getHosts()

        if not zkHosts.has_key(node):
            return None
        port = int(zkHosts[node])
        return self.__getZooKeeperNodeStats(node, port, cmd)

    def getJstatOutputAsDict(self, out):
        """ return dictionary of key-value pairs of jstat output.
            out should be output string from jstat commands """
        keyval = out.split("\\n")
        keys = keyval[0].split()
        # first entry is a quotation mark; get rid of it
        del keys[0]
        vals = keyval[1].split()
        return dict(zip(keys, vals))

    def parseJstatGeneric(self, out, jvm_stats):
        """ jvm_stats is the dict corresponding to the jvm_stats json field.
            out should be output from jstat -gc <pid> command. """
        odict = self.getJstatOutputAsDict(out)

        # general gc stats
        jvm_stats.update({jstatMapStr["YGC"]:float(odict["YGC"])})
        jvm_stats.update({jstatMapStr["YGCT"]:float(odict["YGCT"])})
        jvm_stats.update({jstatMapStr["FGC"]:float(odict["FGC"])})
        jvm_stats.update({jstatMapStr["FGCT"]:float(odict["FGCT"])})
        jvm_stats.update({jstatMapStr["GCT"]:float(odict["GCT"])})

    def parseJstatGCCapacity(self, out, ygen, ogen, pgen):
        """ out should be output string from jstat -gccapacity <pid> command """
        odict = self.getJstatOutputAsDict(out)

        # setup newgen dict
        newgen = {"newgen": {}}
        newgen["newgen"].update({jstatMapStr["NGC"]:float(odict["NGC"])})
        newgen["newgen"].update({jstatMapStr["NGCMN"]:float(odict["NGCMN"])})
        newgen["newgen"].update({jstatMapStr["NGCMX"]:float(odict["NGCMX"])})
        ygen["younggen"].update(newgen)

        # add min/cur/max capacities for oldgen
        ogen["oldgen"].update({jstatMapStr["OGCMN"]:float(odict["OGCMN"])})
        ogen["oldgen"].update({jstatMapStr["OGCMX"]:float(odict["OGCMX"])})
        ogen["oldgen"].update({jstatMapStr["OGC"]:float(odict["OGC"])})

        # add min/cur/max capacities for permgen if they exist
        if "PGC" in odict.keys():
            pgen["permgen"].update({jstatMapStr["PGCMN"]:float(odict["PGCMN"])})
            pgen["permgen"].update({jstatMapStr["PGCMX"]:float(odict["PGCMX"])})
            pgen["permgen"].update({jstatMapStr["PGC"]:float(odict["PGC"])})

    def parseJstatGC(self, out, ygen, ogen, pgen):
        """ out should be output string from jstat -gc <pid> command. """
        odict = self.getJstatOutputAsDict(out)

        # setup eden dict
        eden = {"eden":{}}
        eden["eden"].update({jstatMapStr["EC"]:float(odict["EC"])})
        eden["eden"].update({jstatMapStr["EU"]:float(odict["EU"])})
        ygen["younggen"].update(eden)

        # setup survivors dicts
        survivor0 = {"survivor0":{}}
        survivor0["survivor0"].update({jstatMapStr["S0C"]:float(odict["S0C"])})
        survivor0["survivor0"].update({jstatMapStr["S0U"]:float(odict["S0U"])})
        ygen["younggen"].update(survivor0)

        survivor1 = {"survivor1":{}}
        survivor1["survivor1"].update({jstatMapStr["S1C"]:float(odict["S1C"])})
        survivor1["survivor1"].update({jstatMapStr["S1U"]:float(odict["S1U"])})
        ygen["younggen"].update(survivor1)

        # add oldgen utilization
        ogen["oldgen"].update({jstatMapStr["OU"]:float(odict["OU"])})

        # add permgen utilization if it exists
        if "PU" in odict.keys():
            pgen["permgen"].update({jstatMapStr["PU"]:float(odict["PU"])})

    def getJVMNodeStats(self, node, pid):
        """ returns output of jstat -gc and jstat -gccapacity as json.
            by passing json.dumps() adds escaped newline characters which we
            can  use to split into lists sets of strings, keys, and values """
        jvm_stats = dict()
        ygen = {"younggen": dict()}
        ogen = {"oldgen": dict()}
        pgen = {"permgen": dict()}
        if pid is None:
            return jvm_stats

        command = ['jstat', '-gc', str(pid)]
        try:
            gc = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0]
        except OSError: # Popen raises OSError
            return jvm_stats

        if gc is not None:
            # XXX cleanup json.dumps()
            self.parseJstatGeneric(json.dumps(gc, indent=4), jvm_stats)
            self.parseJstatGC(json.dumps(gc, indent=4), ygen, ogen, pgen)

        command = ['jstat', '-gccapacity', str(pid)]
        try:
            gccap = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0]
        except OSError:
            return jvm_stats

        if gccap is not None:
            self.parseJstatGCCapacity(json.dumps(gccap, indent=4), ygen, ogen, pgen)

        jvm_stats.update(ygen)
        jvm_stats.update(ogen)
        jvm_stats.update(pgen)
        return jvm_stats

    def __getZooKeeperStats(self, cmd):
        """ Return a string with the status of all nodes in the ensemble """
        res = dict()
        zkHosts = self.getHosts()
        for i in zkHosts.keys():
            status = self.getZKNodeStats(i, cmd)
            node_status = {i: status}
            res.update(node_status)
        return res

    def getAllStats(self, cmd='stat'):
        """ Return zookeeper stats for the entire ensemble.
            There are other commands, cons, mntr, that may be useful.
            crst, srst clear reset connection and server stats respectively.
            Those commands can be invoked manually at present. """
        res = dict()
        stats = self.__getZooKeeperStats(cmd)
        zkStats = {"zookeeper": stats}
        res.update(zkStats)
        return res

    def resetAllStats(self, cmd='srst'):
        """ Reset zookeeper stats that are returned through 'stat' command.
            Use 'crst' for cmd to reset connection stats.  We can cheat and
            reuse the get functions but just change the cmd argument """
        stats = self.__getZooKeeperStats(cmd)

    def isZKNode(self, node):
        """ Return whether the given node is in zookeeper ensemble """
        nodes = self.running_config.get('zookeeper').get('nodes').split(',')
        return node in nodes

    def __getConf(self, conf_obj, subpath):
        if subpath == '/':
            subpath = None
        if subpath is None:
            return conf_obj
        if subpath[0] != '/':
            raise ValueError("config subpath {0} must start with '/'".format(subpath))
        try:
            config_item = conf_obj
            for conf_subitem_name in subpath.split('/')[1:]:
                config_item = config_item[conf_subitem_name]
            return config_item
        except (TypeError, KeyError):
            return None

    def getStaticConf(self, subpath=None):
        """ Return the static (stored) conf as a dict """
        return self.__getConf(self.static_config, subpath)

    def getRunningConf(self, subpath=None):
        """ Return the running conf as a dict """
        return self.__getConf(self.running_config, subpath)

    def __stopNode(self, node):
        """ Stop a single node """
        port = self.getWatchdogPortForHost(node)
        url = "http://" + node + ":" + str(port) + "/post/stop"
        try:
            status = XCommand(url=url, login="dummy", passwd="dummy").curl_request(url, method="POST", body="stop")
        except ScalXCommandException as e:
            return e.message
        except ScalDaemonException as e:
            return e.message
        return status

    def stopNode(self, node):
        """ Redirect stop request to sophiactl on all or given node """
        node_list = []
        if node == 'all':
            node_list = self.running_config.get('zookeeper').get('nodes').split(',')
        elif node == 'localhost':
            node_list.insert(socket.gethostbyname())
        else:
            if self.isZKNode(node):
                node_list.insert(node)
            else:
                raise SharedCacheUsageException("Node not in ensemble")

        res = dict()
        for i in node_list:
            status = self.__stopNode(i)
            node_status = {i: status}
            res.update(node_status)
        return res

    def __startNode(self, node):
        """ Start a single node """
        port = self.getWatchdogPortForHost(node)
        url = "http://" + node + ":" + str(port) + "/post/start"
        try:
            status = XCommand(url=url, login="dummy", passwd="dummy").curl_request(url, method="POST", body="start")
        except ScalXCommandException as e:
            return e.message
        except ScalDaemonException as e:
            return e.message
        return status

    def startNode(self, node):
        """ Redirect start request to sophiactl on all or given node """
        node_list = []
        if node == 'all':
            node_list = self.running_config.get('zookeeper').get('nodes').split(',')
        elif node == 'localhost':
            node_list.insert(socket.gethostbyname())
        else:
            if self.isZKNode(node):
                node_list.insert(node)
            else:
                raise SharedCacheUsageException("Node not in ensemble")

        res = dict()
        for i in node_list:
            status = self.__startNode(i)
            node_status = {i: status}
            res.update(node_status)
        return res

    def disableCache(self):
        """ Disable shared cache """
        raise NotImplementedError()

    def enableCache(self):
        """ Enable shared cache """
        raise NotImplementedError()
