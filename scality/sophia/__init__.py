""" sophia init file """
# -*- coding: utf-8 -*-

from scality.sophia.exc import (
    SharedCacheException,
    SharedCacheUsageException,
    SharedCacheInvalidConfigException
)
from scality.sophia.sophia import Sophia
