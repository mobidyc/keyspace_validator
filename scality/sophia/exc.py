# -*- coding: utf-8 -*-


class SharedCacheException(Exception):
    pass


class SharedCacheUsageException(SharedCacheException):
    """ Exception for shared cache status filter """
    pass


class SharedCacheInvalidConfigException(SharedCacheException):
    """ Exception for shared cache config settings """
    pass
