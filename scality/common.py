# -*- coding: utf-8 -*-
""" @file common.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:20:55 CEST 2012

    @brief common module to everyone
"""

try:
    import cjson
except ImportError:
    cjson = None
else:
    if cjson.decode(cjson.encode('/')) != '/':
        import warnings

        warnings.warn(
            "bug detected in 'cjson' library; falling back to default "
            "library"
        )

        cjson = None  # pylint: disable=invalid-name

import functools

try:
    import simplejson as json
except ImportError:
    import json


def json_dumps(obj, **kwargs):
    if cjson and not kwargs:
        try:
            return cjson.encode(obj)
        except cjson.EncodeError as exc:
            raise TypeError(str(exc))
    else:
        return json.dumps(obj, **kwargs)


json_dump = json.dump  # pylint: disable=invalid-name
json_dump_indent = functools.partial(  # pylint: disable=invalid-name
    json_dump, sort_keys=True, indent=4
)


def json_loads(data):
    if cjson:
        try:
            return cjson.decode(data)
        except cjson.DecodeError as exc:
            raise ValueError(str(exc))
    else:
        return json.loads(data)


json_load = json.load  # pylint: disable=invalid-name


class ScalDaemonException(Exception):
    """ generic exception / should be used directly """
    def __init__(self, msg=None):
        super(ScalDaemonException, self).__init__(msg)
        self.msg = msg.rstrip("\r\n") if msg else None

    def __str__(self):
        return self.msg


class ScalDaemonExceptionInvalidParameter(ScalDaemonException):
    """ a paramater is invalid """
    def __init__(self, name, value=None, msg=None):
        super(ScalDaemonExceptionInvalidParameter, self).__init__(msg)
        self.name = name
        self.value = value

    def __str__(self):
        msg = "InvalidParameter: '%s'" % self.name
        if self.value:
            msg += ", received '%s'" % self.value
        if self.msg:
            msg += ": " + self.msg
        return msg


class ScalDaemonExceptionCommandError(ScalDaemonException):
    """ the command has failed """
    def __init__(self, name, msg=None):
        self.name = name
        super(ScalDaemonExceptionCommandError, self).__init__(msg)

    def __str__(self):
        msg = "Failed to execute command '%s'" % self.name
        if self.msg:
            msg += ": " + self.msg
        return msg


def partition(lst, n):
    q, r = divmod(len(lst), n)
    indices = [q*i + min(i, r) for i in xrange(n+1)]
    return [lst[indices[i]:indices[i+1]] for i in xrange(n)]
