#pylint: disable=invalid-name
# -*- coding: utf-8 -*-
""" @file sagentd.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Tue Jan 29 09:57:58 CET 2013

    @brief implement methods to command a sagentd
"""
import logging

from scality.common import (json_loads, json_dumps, ScalDaemonException)

# from scality.storelib import chord
# from scality.key import Key
from scality.ov import OvDaemon
from scality.config_section import StringParse
from scality.restconnector import RestConnector
from scality.node import Node


logger = logging.getLogger("scality")


class Sagentd(OvDaemon):
    """ Class to access a sagentd """

    connector_url = "{url}/api/v1/connector/{addr}{uri}"
    sophiactl_url = "{url}/api/v1/sophiactl/{addr}{uri}"

    def __init__(self,
                 url="http://localhost:7084",
                 login="root",
                 passwd=None,
                 spass=None,
                 **kwargs):
        super(Sagentd, self).__init__(url, login, passwd, **kwargs)
        self.url = url
        self.spass = spass
        self.suser = "super"

    def __connector_request(self, addr, uri, **kwargs):
        """ Send HTTP request to connector endpoint """
        url = self.connector_url.format(
            url=self.url, addr=addr, uri=uri
        )

        return self._xc.curl_request(
            url, **kwargs
        )

    def restartConnector(self, addr):
        """ restart a nasdk connector """
        return self.__connector_request(addr, "/command/reload", method="POST", body="")

    def blacklistIpConnector(self, addr, ip):
        """ Send a "blacklist_ip" command to the nasdk connector asking it to stop all connections to this ip """
        return self.__connector_request(addr, "/command/blacklist_ip", method="POST", body=ip)

    def unblacklistIpConnector(self, addr):
        """ helper command to unblacklist all IPs on the nasdk connector """
        return self.blacklistIpConnector(addr, "unblacklist")

    def createCatalogConnector(self, addr):
        """ create the catalog on sfused """
        return self.__connector_request(
            addr, "/command/create_catalog", method="POST", body=""
        )

    def createDirectoryConnector(self, addr, path):
        """ create a directory on sfused """
        return self.__connector_request(
            addr, "/command/create_directory", method="POST", body=path
        )

    def putExportsConfig(self, addr, conf):
        return self.__connector_request(
            addr, "/conf/exports/", method="POST", body=conf,
            multipart=True
        )

    def putStaticConfig(self, addr, conf):
        """ push a new static configuration to a connector (using sagentd rest api) """
        return self.__connector_request(
            addr, "/conf/static/", method="POST", body=conf,
            multipart=True
        )

    def getStatus(self):
        """ return status (aka list of node and connectors) from a sagentd """
        data = self._create_request("STATUS")
        res = self._xc.curl_request(
            "{s.url}/chordsup?spass={s.spass}&suser={s.suser}".format(s=self),
            body=data, method="POST"
        )
        return self._get_status_parse(res)

    def getNodes(self):
        """return an iterable of scality.node.Node objects"""
        res = []
        for name, node in self.getStatus()["nodes"].iteritems():
            if not len(node["status"]):
                continue

            ip = node["addr"].split(":")[0]
            adminport = node["status"][0]["config"]["core"]["adminport"]

            addr = "https://%s:%s" % (ip, adminport)
            dso = node["status"][0]["dsoname"]
            chord_addr = node["status"][0]["chord_addr"]

            node = Node(url=addr, dso=dso, chord_addr=chord_addr,
                        login=self._xc.login, passwd=self._xc.passwd,
                        xcommand_cmp_mode=False, name=name)
            res.append(node)
        return res

    @staticmethod
    def _stattree_parse(et):
        multiple = et.find("multiple")
        res = {}
        if multiple is not None and multiple.text == "yes":
            res["_multiple"] = True
            for et_daemon in et.findall("./res/stattreeview/daemons/*"):
                daemon_name = et_daemon.find("./name").text
                et_stats = et_daemon.findall("./stats/*/stattreeview/*")
                res[daemon_name] = OvDaemon._stattree_parse(et_stats)
        else:
            res["none"] = OvDaemon._stattree_parse(
                et.findall("./res/stattreeview/*"))
            res["_multiple"] = False
        return res

    @staticmethod
    def _get_status_parse(data):
        """ internal data to parse a sagentd """
        cs = StringParse(data)

        try:
            multiple = cs.getBranch("cmd_desc").getValString("multiple")
        except:
            multiple = None

        try:
            dt = cs.getBranch("cmd_desc").getValInt64("dt")
        except:
            dt = None

        if not multiple or multiple != "yes":
            return {
                "restconnectors": {},
                "nodes": {
                    "noname": {
                        "type": "node",
                        "name": "noname",
                        "addr": "noaddr",
                        "status": Node._nodegetstatus_parse(
                            cs.getBranch("data")
                        )
                    }
                },
                "nasdkconnectors": {},
            }

        data = cs.getBranch("data")

        disks = {}
        biziostores = data.getBranch("biziostores")
        if biziostores is not None:
            for biziostore in biziostores.iterBranchNamed("biziostore"):
                ref = biziostore.getAttrString("ref")
                if ref:
                    disks[ref] = biziostore

        def _fill_state(branch, get_daemon_status):
            ret = {}
            for daemon in branch:
                try:
                    status = get_daemon_status(daemon)
                except ScalDaemonException:
                    status = {}
                ret[daemon.getValString("name")] = {
                    "type": daemon.getValString("type"),
                    "name": daemon.getValString("name"),
                    "addr": daemon.getValString("addr"),
                    "status": status
                }
            return ret

        return {
            "restconnectors": _fill_state(data.iterBranchNamed("restconnector"),
                                          RestConnector._restconnector_getstatus),
            "restdaemons": _fill_state(data.iterBranchNamed("restdaemon"),
                                       lambda x: Sagentd._restdaemon_getstatus_parse(x.getBranch("status"))),
            "nasdkconnectors": _fill_state(data.iterBranchNamed("nasdkconnector"),
                                           lambda x: Sagentd._nasdkconnector_getstatus_parse(x.getBranch("status"))),
            "nodes": _fill_state(data.iterBranchNamed("node"),
                                 lambda x: Node._nodegetstatus_parse(x, disks)),
            "dt":  dt
        }

    @staticmethod
    def _restdaemon_getstatus_parse(branch):
        return {"state": branch.getValString("state"),
                "zookeeper": branch.getValString("zookeeper")}

    @staticmethod
    def _nasdkconnector_getstatus_parse(branch):
        """ temp location """
        res = {}

        if branch is None:
            return {}

        for node in [x for x in branch.iterAll() if x.isBranch() is False]:
            res[node.name] = node.getValue()

        def parse_conf_from_status(status_branch, conf_branch_name):
            """ This encapsulates all the BS needed to parse the confs from the status branch """
            branch =  status_branch.getBranch(conf_branch_name)
            conf_nodes = [n for n in branch.iterAll()]

            # There's only one text node that contains the json dump of the conf
            if len(conf_nodes) == 0:
                return {}

            raw_json = conf_nodes[0].getValue()
            try:
                parsed = json_loads(raw_json)
            except (ValueError, TypeError) as exc:
                logger.warning(
                    "Unable to parse JSON ({branch_name}): {error}".format(
                        branch_name=conf_branch_name,
                        error=str(exc)
                    )
                )
                parsed = {}

            return parsed

        static_conf = parse_conf_from_status(branch, "static_conf")
        meta = parse_conf_from_status(branch, "meta")
        running_conf = parse_conf_from_status(branch, "running_conf")
        desc_conf = parse_conf_from_status(branch, "desc_conf")

        if branch.getBranch("stats"):
            stats = parse_conf_from_status(branch, "stats")
        else:
            stats = None

        try:
            exports_conf = parse_conf_from_status(branch, "exports.conf")
        except Exception:
            exports_conf = None

        res.update({
            "static_conf": static_conf,
            "meta": meta,
            "running_conf": running_conf,
            "desc_conf": desc_conf,
            "stats": stats,
            "exports_conf": exports_conf
        })

        return res

    def __sophiactl_request(self, addr, uri, **kwargs):
        """ Send HTTP request to sophiactl endpoint """
        url = self.sophiactl_url.format(
            url=self.url, addr=addr, uri=uri
        )

        return self._xc.curl_request(
            url, **kwargs
        )

    def sophiactlGetDaemonInfo(self, addr):
        """ Return high-level info about the daemon """
        return self.__sophiactl_request(addr, "/")

    def sophiactlStartWatchdog(self, addr):
        """ Start watchdog locally if not already started """
        return self.__sophiactl_request(addr, "/command/start", method="POST", body="")

    def sophiactlStopWatchdog(self, addr):
        """ Stop watchdog locally if it is running """
        return self.__sophiactl_request(addr, "/command/stop", method="POST", body="")

    def sophiactlRestartWatchdog(self, addr):
        """ Restart the watchdog child process """
        return self.__sophiactl_request(addr, "/command/restart", method="POST", body="")

    def sophiactlGetConfig(self, addr):
        """ Return the sophia config or part of it """
        return self.__sophiactl_request(addr, "/config")

    def sophiactlPushConfig(self, addr, config):
        """ Push a new configuration as a whole """
        return self.__sophiactl_request(addr, "/config", method="PUT",
                                        body=json_dumps(config))

    def sophiactlGetStatus(self, addr):
        """ Return the status details """
        return self.__sophiactl_request(addr, "/status")

    def sophiactlGetStats(self, addr):
        """ consolidate zk and jvm stats and output them as json """
        return self.__sophiactl_request(addr, "/stats")

    def sophiactlResetStats(self, addr):
        return self.__sophiactl_request(addr, "/stats", method="DELETE")

    def sophiactlGetJVMStats(self, addr):
        """ return jvm stats output for single point in time """
        return self.__sophiactl_request(addr, "/jvmstats")

    def sophiactlGetAllStats(self, addr):
        """ Returns stats for all zk nodes in the ensemble. """
        return self.__sophiactl_request(addr, "/allstats")

    def sophiactlResetAllStats(self, addr):
        return self.__sophiactl_request(addr, "/allstats", method="DELETE")
