# -*- coding: utf-8 -*-
""" @file daemon.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:23:13 CEST 2012

    @brief pseudo factory class to create daemon objects
"""

from scality.node import Node
from scality.restconnector import RestConnector
from scality.supervisor import Supervisor
from scality.sproxyd import Sproxyd
from scality.sagentd import Sagentd
from scality.supv2 import Supv2


class ScalFactoryExceptionTypeNotFound(Exception):

    """ the daemon type is not known """

    def __init__(self, dtype, msg=None):
        self.dtype = dtype
        self.msg = None

    def __str__(self):
        if self.msg:
            return "DaemonTypeNotFound: '%s', %s" % (self.dtype, self.msg)
        else:
            return "DaemonTypeNotFound: '%s'" % (self.dtype)


class DaemonFactory(object):

    """ singleton to instanciate new daemon objects """
    _inst = None

    def __init__(self):
        self.last_inst = None

    @classmethod
    def get_instance(cls):
        """ method to return the singleton """
        if not cls._inst:
            cls._inst = DaemonFactory()
        return cls._inst

    def get_daemon(self, dtype, **kwargs):
        """ returns a daemon of type "dtype"
            - possible types are "node", "supervisor", "restconnector" and others
        """
        self.last_inst = dtype
        if dtype == "node":
            return Node(**kwargs)
        elif dtype == "supervisor":
            return Supervisor(**kwargs)
        elif dtype == "restconnector":
            return RestConnector(**kwargs)
        elif dtype == "sproxyd":
            return Sproxyd(**kwargs)
        elif dtype == "sagentd":
            return Sagentd(**kwargs)
        elif dtype == "supv2":
            return Supv2(**kwargs)
        raise ScalFactoryExceptionTypeNotFound(dtype)
