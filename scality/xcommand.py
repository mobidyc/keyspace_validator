# -*- coding: utf-8 -*-
""" @file xcommand.py
    @author Benoit Artuso <benoit.artuso@scality.com>
    @date Mon Jul  9 15:33:00 CEST 2012

    @brief implement xcommand api above storelib
"""

import threading
import pycurl
from io import BytesIO
from StringIO import StringIO

from scality.storelib import xcommand
from scality.storelib.xcommand import XCommandResult
from scality.common import ScalDaemonException
from scality.config_section import ETParse


class ScalXCommandException(Exception):
    pass


class XCommand(object):

    def __init__(self, url=None, login=None, passwd=None, verbose=False, cmp_mode=False):
        if url is None:
            raise ScalXCommandException("no url")
        if login is None:
            raise ScalXCommandException("no login")
        if passwd is None:
            raise ScalXCommandException("no passwd")

        self.url = url
        self.login = login
        self.passwd = passwd
        self.verbose = verbose
        self.cmp_mode = cmp_mode

        self._c = None

    @property
    def c(self):
        if self._c is None:
            self._c = xcommand.XCommand(self.url, login=self.login, passwd=self.passwd,
                                       verbose=self.verbose,
                                       cmp_mode=self.cmp_mode)
        return self._c

    def _get_ET(self, cmd, params, postdata=None, path=None):
        """ sends an xcommand to demaon and retreive Element_tree """
        if postdata is None:
            try:
                res = self.c.ET_request(cmd, params, path=path)
            except Exception as e:
                raise ScalDaemonException("GET \"%s\": %r" % (self.url, e))
        else:
            # BAD: dup'ing code from store/lib
            # TODO path should be used here as well
            args = params.copy()
            args["cmd"] = cmd
            try:
                data = self.c._request_post(args, postdata)
                res = XCommandResult(data)
            except Exception as e:
                raise ScalDaemonException("POST \"%s\": %r" % (self.url, e))
        return res

    def get_ET(self, cmd, params, postdata=None, path=None):
        """ sends an xcommand to demaon and retreive Element_tree.result """
        return self._get_ET(cmd, params, postdata, path).get_ET()

    def get_xmlcs(self, cmd, params):
        et = self.get_ET(cmd, params)
        return ETParse(et.getchildren()[0])

    def get(self, page, params={}):
        return self.c.generic_request(page, params)

    def get_ucmd_ET(self, cmd, params):
        return self.get_ET(cmd, params, path="/docommand")

    def curl_request(self, url, body=None, method="GET", multipart=False):
        try:
            curl = pycurl.Curl()
        except pycurl.error as e:
            raise ScalDaemonException("Failed to initialize pycurl: %r" % e)

        curl.setopt(pycurl.URL, str(url))

        curl.setopt(pycurl.FOLLOWLOCATION, False)
        curl.setopt(pycurl.CONNECTTIMEOUT_MS, 5000)
        curl.setopt(pycurl.TIMEOUT_MS, 30000)

        output = BytesIO()
        curl.setopt(pycurl.WRITEFUNCTION, output.write)

        curl_options = {
            "GET": pycurl.HTTPGET,
            "POST": pycurl.POST,
            "PUT": pycurl.UPLOAD,
            "HEAD": pycurl.NOBODY,
        }
        custom_methods = set(["DELETE", "OPTIONS", "PATCH"])

        for opt in curl_options.values():
            curl.setopt(opt, False)

        if method in curl_options:
            curl.unsetopt(pycurl.CUSTOMREQUEST)
            curl.setopt(curl_options[method], True)
        elif method in custom_methods:
            curl.setopt(pycurl.CUSTOMREQUEST, method)
        else:
            raise KeyError('unknown method "{method}"'.format(method=method))

        if method == "GET":
            if body is not None:
                raise ValueError('Body must be None for GET request')
        elif method in ("POST", "PUT") or body:
            if body is None:
                raise ValueError(
                    'Body must not be None for "{method}" request'.format(
                        method=method
                    )
                )
            # Make sure this is a string. libcurl on C6 requires it RING-18913
            try:
                body = body.encode("ascii")
            except (UnicodeEncodeError, AttributeError):
                raise ValueError('Body must be an ASCII string')

            if method == "POST" and multipart is True:
                # This will use multipart/formdata. "data" is the hardcoded name in sagentd & ov
                curl.setopt(pycurl.HTTPPOST, [('data', body)])
            else:
                # see RING-17227: POST is supposedly text or unicode
                request_buffer = StringIO(str(body))

                def ioctl(cmd):
                    if cmd == curl.IOCMD_RESTARTREAD:
                        request_buffer.seek(0)

                curl.setopt(pycurl.READFUNCTION, request_buffer.read)
                curl.setopt(pycurl.IOCTLFUNCTION, ioctl)

                if method == "POST":
                    curl.setopt(pycurl.POSTFIELDSIZE, len(body))
                else:
                    curl.setopt(pycurl.UPLOAD, True)
                    curl.setopt(pycurl.INFILESIZE, len(body))

        if threading.active_count() > 1:
            # libcurl/pycurl is not thread-safe by default.  When multiple threads
            # are used, signals should be disabled.  This has the side effect
            # of disabling DNS timeouts in some environments (when libcurl is
            # not linked against ares), so we don't do it when there is only one
            # thread.  Applications that use many short-lived threads may need
            # to set NOSIGNAL manually in a prepare_curl_callback since
            # there may not be any other threads running at the time we call
            # threading.activeCount.
            curl.setopt(pycurl.NOSIGNAL, 1)

        try:
            curl.perform()
        except pycurl.error as e:
            raise ScalDaemonException("HTTP Error: %r" % e)

        data = output.getvalue()

        code = curl.getinfo(curl.RESPONSE_CODE)
        if code != 200:
            raise ScalDaemonException("HTTP Error (%d): %s" % (code, data))

        return data
