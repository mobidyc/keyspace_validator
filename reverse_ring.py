#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=3:shiftwidth=3:smarttab:expandtab:softtabstop=3:autoindent
import sys
sys.dont_write_bytecode = True

# fix #RING-29951
import logging
logging.basicConfig(
   format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(sys.argv[0])

import os
import json
import yaml
import getopt
import subprocess
import traceback
from pprint import pprint as pp

from scality.common import ScalDaemonException
from scality.supervisor import Supervisor
from scality.sprov.ring import Ring as SprovRing
from scality.sprov.component import Server, Node, DiskGroup, Site
from scality.sprov.cos import ClassOfService


def supervisor_cnx(url, login, passwd, verbose=False):
   sup = Supervisor(url=url, login=login, passwd=passwd)
   try:
      sup.serverList()
   except ScalDaemonException as e:
      if verbose:
         logger.debug(e)
      return False

   logger.info("Supervisor connexion successful")
   return sup


def check_sup_cnx(supurl, login, passwd):
   """
      Try to connect and check the credentials everywhere
   """
   sup = supervisor_cnx(supurl, login, passwd)

   # If the cnx was not a success, try to guess credentials from local files

   # "/tmp/scality-installer-credentials"
   if not sup:
      logger.warning("Invalid credentials, I'll try to guess from local files")
      auth_file = "/tmp/scality-installer-credentials"
      if os.path.isfile(auth_file):
         with open(auth_file) as json_file:
            try:
               data = json.load(json_file)
            except:
               data = {}
         if "internal-management-requests" in data:
            d = data["internal-management-requests"]
            if "username" in d and "password" in d:
               login = d["username"]
               passwd = d["password"]
               sup = supervisor_cnx(supurl, login, passwd)

   # "/etc/supv2.yaml"
   if not sup:
      auth_file = "/etc/supv2.yaml"
      if os.path.isfile(auth_file):
         with open(auth_file) as yaml_file:
            try:
               data = yaml.load(yaml_file)
            except:
               data = {}
         if "sup_login" in data and "sup_passwd" in data:
            login = data["sup_login"]
            passwd = data["sup_passwd"]
            sup = supervisor_cnx(supurl, login, passwd)

   # ringsh
   if not sup:
      # Try to guess the passwd from ringsh
      def which(program):
         def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

         fpath, fname = os.path.split(program)
         del fname
         if fpath:
            if is_exe(program):
               return program
         else:
            for path in os.environ["PATH"].split(os.pathsep):
               exe_file = os.path.join(path, program)
               if is_exe(exe_file):
                  return exe_file
         return None

      ringsh = which("ringsh")
      if ringsh:
         ringsh_args = [ringsh, "show", "conf"]
         try:
            sp = subprocess.Popen(
               ringsh_args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
         except Exception, e:
            raise Exception(
               "Failed to call '%s': %s" % (" ".join(ringsh_args), str(e)))
         (stdout, stderr) = sp.communicate()
         del stderr

         for line in stdout.split("\n"):
            fields = line.split()
            if len(fields) > 4:
               if fields[3] == "user,":
                  login = fields[5]
               if fields[3] == "password,":
                  passwd = fields[5]
         sup = supervisor_cnx(supurl, login, passwd)

   if not sup:
      raise Exception("I'm unable to connect to {0} using all known methods".format(supurl))

   return sup

def sprov_compute(supurl, login, passwd, dso_id, test_params, output_file):
   # Default values
   all_cos = []
   all_fail_events = []
   dcprefix = None

   # Supervisor connection
   try:
      sup = check_sup_cnx(supurl, login, passwd)
   except Exception as e:
      logger.error(e)
      sys.exit(1)

   # Ring current configuration
   try:
      dso = sup.supervisorConfigDso(dsoname=dso_id)
   except Exception as e:
      logger.error(e)
      sys.exit(1)

   # Do we need provcoslist, provfaileventlist and dcprefix?
   if test_params:
      def fe2str(x):
         name, value = x.split("=", 1)
         return (name, int(value))

      dso_params = dict(dso["params"])
      all_cos = dso_params["provcoslist"].split(",")
      all_fail_events = map(fe2str, dso_params["provfaileventlist"].split(","))

      try:
         dcprefix = int(dso_params["chunkidclasssitelist"])
      except:
         pass

   # disks are to be sorted to compare later and check if we use diskgroups
   for node in dso["nodes"]:
      node["disks"].sort()

   # nodes sorting per ip:port
   gdso = sorted(dso["nodes"], key=lambda x: x["addr"])

   # dictionnary template, one ip address become a key
   dso_grouped = {}
   [
      dso_grouped.setdefault(x["addr"].split(":")[0], []).append(x)
      for x in gdso
   ]

   dso_store = {}
   for store in dso_grouped:
      # create the node
      dso_store.update({store: {}})
      groups = {}  # Handle the diskgroups

      for x in dso_grouped[store]:
         disklist = "".join(str(l) for l in x["disks"])

         # create an index per diskgroup
         if disklist not in groups:
            groups.update({disklist: (len(groups))})

         # and get the index, if we stay at 0, there is no diskgroup
         diskgroup = "DiskGroup{nb}".format(nb=groups.get(disklist))
         if diskgroup not in dso_store[store]:
            dso_store[store].update({diskgroup: []})
         dso_store[store][diskgroup].append(x)

   # get topology and registered components
   hw = sup.supervisorShowHardware()
   agents = {}
   for zone in hw.get('zones', []):
      if int(zone['stats']['nb_nodes_total']) > 0:
         for sagentd in zone.get('agents', []):
            try:
               node = sagentd['node']
               stats = sagentd['stats']
               if int(stats['nb_nodes_total']) > 0:
                  agents[node['ipstr']] = {
                     'name': node['name'],
                     'address': node['ipstr'],
                     'zone': zone['name'],
                     'state': node['state']
                  }
               # will be used later if a multisite env is detected
               dso_store[node['ipstr']]['DiskGroup0'][0].update({
                  'zone':
                  zone['name']
               })
            except KeyError:
               continue

   # zones = {v['zone'] for x, v in agents.items()}
   # Method to use in python 2.6
   zones = set( [v['zone'] for x, v in agents.items()] )

   dcs = {}
   if len(zones) > 1:
      for zone in zones:
         dc = Site()
         dc.setName(zone)
         dcs.update({zone: dc})

   ring = SprovRing()
   for x, y in dso_store.items():
      # nb of diskgroups
      dgroup = len(y)
      if dgroup > 1:
         s = Server.create(nb_disk_groups=dgroup)
         s.children = []

      # for each diskgroups
      for a, b in y.items():
         nb_processes = len(b)
         if dgroup > 1:
            s.setName(x)
            d = DiskGroup(nb_processes=nb_processes)
            d.setName(a)
            capa = b[0]['stats']['disk']['capacity']['total']
            d.setCapacity(capa)
            for i, c in enumerate(b):
               try:
                  d.addNode(Node(i, c['key']))
               except:
                  logger.error("Missing node {0}".format(x))
            s.addChildComponent(d)
         else:
            s = Server.create(nb_processes=nb_processes)
            s.setName(x)
            capa = b[0]['stats']['disk']['capacity']['total']
            s.setCapacity(capa)
            for i, c in enumerate(b):
               try:
                  s.addNode(Node(i, c['key']))
               except:
                  logger.error("Missing node {0}".format(x))

      if len(zones) > 1:
         zonename = dso_store[x]['DiskGroup0'][0]['zone']
         for i in dcs:
            if dcs[i].name == zonename:
               dcs[i].addChildComponent(s)
               break
      else:
         ring.addChildComponent(s)

   if len(zones) > 1:
      for i in dcs:
         if 'elements' in dcs[i].toJson():
            ring.addChildComponent(dcs[i])


   # We do not trust provcoslist
   if all_cos:
      try:
         ring.setClassesOfService([ClassOfService(x) for x in all_cos], dcprefix=dcprefix)
      except ValueError as e:
         sys.exit(1)
      except Exception:
         logger.error("Generic Exception: {}".format(traceback.format_exc()))
         sys.exit(1)

   # We do not trust provfaileventlist
   if all_fail_events and all_cos:
      from scality.sprov.event import FailureSet, FailureEventSet
      ring.events = FailureEventSet()
      for x, y in all_fail_events:
         afe = {x: y}
         ring.events.addFailureEvent(
            FailureSet(**afe), [ClassOfService(x) for x in all_cos])
      try:
         ring.updateEvents()
      except Exception:
         logger.error("Generic Exception: {}".format(traceback.format_exc()))
         return False

   with open(output_file, 'w') as f:
      json.dump(ring.toJson(), f, indent=4)


def usage(output):
   output.write("""Usage: %s [options]
        Options:
        -h|--help ________ Show this help message
        -r|--ring ________ Ring name
        -s|--supurl ______ Supervisor Url (default http://127.0.0.1:5580)
        -l|--login _______ Supervisor login (default to root)
        -p|-pass _________ Supervisor password (default to admin)
        -S|--ring_params _ Do not use the system params (ring provcos, provfail, etc...)
        -o|-output _______ Output file (default to /tmp/model.json)
""" % os.path.basename(sys.argv[0]))


if __name__ == "__main__":
   options = "hr:s:l:p:o:S"
   long_options = ["help", "ring=", "supurl=", "login=", "pass=", "output=", ""]

   try:
      opts, args = getopt.getopt(sys.argv[1:], options, long_options)
   except getopt.GetoptError as err:
      sys.stderr.write("getopt error %s" % err)
      usage(sys.stderr)
      sys.exit(2)

   ring = None
   sup = None
   passwd = "admin"
   login = "root"
   system = True
   output_file = "/tmp/model.json"
   for o, a in opts:
      if o in ("-h", "--help"):
         usage(sys.stdout)
         sys.exit(0)
      elif o in ("-r", "--ring"):
         ring = a
      elif o in ("-s", "--supurl"):
         sup = a
      elif o in ("-p", "--pass"):
         passwd = a
      elif o in ("-l", "--login"):
         login = a
      elif o in ("-S", "--ring_params"):
         system = False
      elif o in ("-o", "--output"):
         output_file = a
      else:
         usage(sys.stderr)
         sys.exit(2)
   if not ring:
      usage(sys.stderr)
      sys.exit(2)

   if not sup:
      sup = 'http://127.0.0.1:5580'

   sprov_compute(sup, login, passwd, ring, system, output_file)
